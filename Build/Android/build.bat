set project_name="Magic Tiles Vocal 2019"

xcopy .\Config .\%project_name% /Y /s
cd %project_name%
::call gradlew.bat clean
call gradlew.bat assembleRelease
cd ..
move /Y %project_name%\build\outputs\apk\release\%project_name%-armeabi-v7a-release.apk .\
move /Y %project_name%\build\outputs\apk\release\%project_name%-arm64-v8a-release.apk .\
pause