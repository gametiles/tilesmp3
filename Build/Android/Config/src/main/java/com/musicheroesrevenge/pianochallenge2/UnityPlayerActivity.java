package com.musicheroesrevenge.pianochallenge2;

import com.unity3d.player.*;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Notification.Builder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Window;
import android.util.Log;

public class UnityPlayerActivity extends Activity
{
	protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code

	public static final String PN_CLICKED_ID = "PN clicked id";
	public static int PN_FLAGS = PendingIntent.FLAG_UPDATE_CURRENT;

	// Setup activity layout
	@Override protected void onCreate (Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		getWindow().setFormat(PixelFormat.RGBX_8888); // <--- This makes xperia play happy

		mUnityPlayer = new UnityPlayer(this);
		setContentView(mUnityPlayer);
		mUnityPlayer.requestFocus();
		checkIntentForPN(getIntent());
	}

	void checkIntentForPN(Intent intent)
	{
		if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey("daily_pn_id"))
		{
			int pnId = intent.getExtras().getInt("daily_pn_id");
			setPreferenceInt(PN_CLICKED_ID, pnId);
			Log.e("HaiNgo", "Push Notification clicked " + pnId);
		}
		else
		{
			setPreferenceInt(PN_CLICKED_ID, 0);
		}
	}

	@Override protected void onNewIntent(Intent intent)
	{
		// To support deep linking, we need to make sure that the client can get access to
		// the last sent intent. The clients access this through a JNI api that allows them
		// to get the intent set on launch. To update that after launch we have to manually
		// replace the intent with the one caught here.
		setIntent(intent);
		checkIntentForPN(intent);
	}

	// Quit Unity
	@Override protected void onDestroy ()
	{
		mUnityPlayer.quit();
		super.onDestroy();
	}

	// Pause Unity
	@Override protected void onPause()
	{
		super.onPause();
		mUnityPlayer.pause();
	}

	// Resume Unity
	@Override protected void onResume()
	{
		super.onResume();
		mUnityPlayer.resume();
	}

	// Low Memory Unity
	@Override public void onLowMemory()
	{
		super.onLowMemory();
		mUnityPlayer.lowMemory();
	}

	// Trim Memory Unity
	@Override public void onTrimMemory(int level)
	{
		super.onTrimMemory(level);
		if (level == TRIM_MEMORY_RUNNING_CRITICAL)
		{
			mUnityPlayer.lowMemory();
		}
	}

	// This ensures the layout will be correct.
	@Override public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		mUnityPlayer.configurationChanged(newConfig);
	}

	// Notify Unity of the focus change.
	@Override public void onWindowFocusChanged(boolean hasFocus)
	{
		super.onWindowFocusChanged(hasFocus);
		mUnityPlayer.windowFocusChanged(hasFocus);
	}

	// For some reason the multiple keyevent type is not supported by the ndk.
	// Force event injection by overriding dispatchKeyEvent().
	@Override public boolean dispatchKeyEvent(KeyEvent event)
	{
		if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
			return mUnityPlayer.injectEvent(event);
		return super.dispatchKeyEvent(event);
	}

	// Pass any events not handled by (unfocused) views straight to UnityPlayer
	@Override public boolean onKeyUp(int keyCode, KeyEvent event)     { return mUnityPlayer.injectEvent(event); }
	@Override public boolean onKeyDown(int keyCode, KeyEvent event)   { return mUnityPlayer.injectEvent(event); }
	@Override public boolean onTouchEvent(MotionEvent event)          { return mUnityPlayer.injectEvent(event); }
	/*API12*/ public boolean onGenericMotionEvent(MotionEvent event)  { return mUnityPlayer.injectEvent(event); }

	public static final String PREFS_NAME = "Piano Challenges 2";

	public void setPreferenceInt (String key, int value) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public int getPreferenceInt (String key) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		int value = settings.getInt(key, 0);
		return value;
	}

	public int getClickedPnId()
	{
		return getPreferenceInt(PN_CLICKED_ID);
	}

	public void clearClickedPnId()
	{
		setPreferenceInt(PN_CLICKED_ID, 0);
	}
}
