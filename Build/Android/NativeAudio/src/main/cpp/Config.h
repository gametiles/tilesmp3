
#pragma once

// This flag is important, set it to 1 if you are making MonoArray plugin
#define BUILD_UNITY_PLUGIN					1

#define AUDIO_USE_STB_VORBIS				1
#define AUDIO_USE_LIB_VORBIS				0
#define AUDIO_USE_MPG123					1
#define AUDIO_USE_BUILTIN					0
#define AUDIO_LOAD_FROM_ASSETS				1

#define REMOVE_SILENCE						0
#define USE_LIB_SAMPLE_RATE					0
#define USE_LIB_SOXR						1

#define AUDIO_MULTITHREAD_LOADING           1

#define AUDIO_POOL_VERSION                  1
#define AUDIO_POOL_CACHING                  0
#define AUDIO_POOL_CACHE_FILE               "audio.pool"