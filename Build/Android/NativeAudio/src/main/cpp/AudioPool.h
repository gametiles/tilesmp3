//
// Created by Nama on 8/29/2017.
//

#pragma once

#include <map>

#include "Config.h"

namespace nama
{
	class Audio;

    typedef void (*OnAudioLoaded)(int);
    typedef void (*OnAudiosLoaded)();

	class AudioPool
	{
	private:
		AudioPool();
		~AudioPool();

	public:
		static AudioPool& instance();
		void initialize();

		void shutdown();
		bool isLoaded() const;
		void play(int iAudio, float volume);

		bool loadCache();
		bool beginLoading();
		bool endLoading();

		bool loadPcm32(int id, float *data, int nChannels, int nSamples, int sampleRate);

		bool loadOgg(int id, void *data, int size);
		bool loadOgg(int id, const char *szAsset);
		bool loadMp3(int id, void *data, int size);

		bool loadMp3(int id, const char *szAsset);

        void loadPackedMp3s(const char *szAsset, bool multithread);
        void loadPackedMp3s(void* data, int size, bool multithread);
        void loadPackedOggs(const char *szAsset, bool multithread);
        void loadPackedOggs(void* data, int size, bool multithread);

        int audioCount() { return m_audios.size(); }

        void setCallbacks(OnAudioLoaded onAudioLoaded, OnAudiosLoaded onAudiosLoaded)
        {
            m_onAudioLoaded = onAudioLoaded;
            m_onAudiosLoaded = onAudiosLoaded;
        }

	private:
        bool storeAudio(Audio* pAudio, int id);

	private:

		std::map<int, Audio*> m_audios;
		bool m_loaded = false;

        OnAudioLoaded m_onAudioLoaded = nullptr;
        OnAudiosLoaded m_onAudiosLoaded = nullptr;
	};
}
