

#include <list>
#include <condition_variable>

namespace nama
{
	class Audio;
	class AudioPlayer;

	enum
	{
		// The maximum number of players is 30 on Android theoretically
		MAX_NUMBER_OF_PLAYERS = 30,
	};

	class AudioEngine
	{
	protected:
		AudioEngine() {}
		virtual ~AudioEngine();

	public:
		static AudioEngine& instance();

		virtual void initialize(
				int nativeSampleRate,
				int nativeBufferSize) = 0;
		virtual void shutdown() = 0;
		virtual void onPause() = 0;
		virtual void onResume() = 0;

		void play(Audio* pAudio, float volume);
		int sampleRate() const { return m_sampleRate; }
		int bufferSize() const { return m_bufferSize; }

		virtual void onPlayingDone(AudioPlayer* pPlayer) = 0;

	protected:
		void destroyAllPlayers();
		AudioPlayer* idlePlayer();

		void lock();
		void unlock();

	protected:
		std::list<AudioPlayer*> m_players;

		int m_sampleRate;
		int m_bufferSize;

		std::mutex m_lock;
		std::condition_variable m_condition;
		int m_nWaiting = 0;

        bool m_appPaused = false;
	};
}