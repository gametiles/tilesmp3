//
// Created by Nama on 8/29/2017.
//

#include "AudioPlayer.h"
#include "AudioEngineInternal.h"
#include "Audio.h"
#include "Log.h"

// for native audio
#include <SLES/OpenSLES_Android.h>
#include <cmath>
#include <cassert>

#define ADJUST_VOLUME 1

namespace nama
{
	static SLmillibel maxVolumeLevel = SL_MILLIBEL_MIN;

	class AudioPlayerInternal : public AudioPlayer
	{
    private:
        AudioPlayerInternal(const AudioPlayerInternal& player) = delete;

	public:
		AudioPlayerInternal(AudioPlayerInternal&& player);
		AudioPlayerInternal();
		~AudioPlayerInternal();
		void destroy();

		// this callback handler is called every time a buffer finishes playing
		static void playingCallback(SLAndroidSimpleBufferQueueItf bq, void *context);

		void play(Audio* pAudio, float volume) override;
		void stop() override;
		bool setVolume(float volume) override;
		void onPlayed();

		// buffer queue player interfaces
		SLObjectItf bqPlayerObject = nullptr;
		SLPlayItf bqPlayerPlay = nullptr;
#if ADJUST_VOLUME
		SLVolumeItf bqPlayerVolume = nullptr;
#endif
		SLAndroidSimpleBufferQueueItf bqPlayerBufferQueue = nullptr;
	};

	AudioPlayerInternal::AudioPlayerInternal(AudioPlayerInternal &&player) {
		this->bqPlayerObject = player.bqPlayerObject;
		this->bqPlayerPlay = player.bqPlayerPlay;
#if ADJUST_VOLUME
		this->bqPlayerVolume = player.bqPlayerVolume;
#endif
		this->bqPlayerBufferQueue = player.bqPlayerBufferQueue;
        player.bqPlayerObject = nullptr;

        // register callback on the buffer queue
        (*bqPlayerBufferQueue)->RegisterCallback(
                bqPlayerBufferQueue,
                AudioPlayerInternal::playingCallback,
                this);
	}

	AudioPlayerInternal::AudioPlayerInternal()
	{
		auto slEngine = ((AudioEngineInternal&)AudioEngine::instance()).slEngine;
		if (!slEngine)
		{
			return;
		}

		auto outputMixObject = ((AudioEngineInternal&)AudioEngine::instance()).outputMixObject;
		if (!outputMixObject)
		{
			return;
		}

		int sampleRate = AudioEngine::instance().sampleRate() * 1000;

		SLresult result;

		// configure audio source
		SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {
				SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE,
				2
		};

		SLDataFormat_PCM format_pcm = {
				SL_DATAFORMAT_PCM,
				2,
				(SLuint32)sampleRate,
				SL_PCMSAMPLEFORMAT_FIXED_16,
				SL_PCMSAMPLEFORMAT_FIXED_16,
				SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT,
				SL_BYTEORDER_LITTLEENDIAN
		};
		SLDataSource audioSrc = {&loc_bufq, &format_pcm};

		// configure audio sink
		SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, outputMixObject};
		SLDataSink audioSnk = {&loc_outmix, NULL};

		/*
		 * create audio player:
		 *     fast audio does not support when SL_IID_EFFECTSEND is required, skip it
		 *     for fast audio case
		 */
		const SLInterfaceID ids[] = {SL_IID_BUFFERQUEUE, SL_IID_VOLUME };
		const SLboolean req[] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};

		result = (*slEngine)->CreateAudioPlayer(
				slEngine,
				&bqPlayerObject,
				&audioSrc,
				&audioSnk,
#if ADJUST_VOLUME
				2,
#else
				1,
#endif
				ids,
				req);

		if (SL_RESULT_SUCCESS != result)
		{
			return;
		}

		while(true) // error trap
		{
			// realize the player
			result = (*bqPlayerObject)->Realize(bqPlayerObject, SL_BOOLEAN_FALSE);
			if (SL_RESULT_SUCCESS != result)
			{
				break;
			}

			// get the play interface
			result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_PLAY, &bqPlayerPlay);
			if (SL_RESULT_SUCCESS != result)
			{
				break;
			}

			// get the buffer queue interface
			result = (*bqPlayerObject)->GetInterface(
					bqPlayerObject,
					SL_IID_BUFFERQUEUE,
					&bqPlayerBufferQueue);
			break;
		}

		// Check for error
		if (SL_RESULT_SUCCESS != result)
		{
			destroy();
			return;
		}

#if ADJUST_VOLUME
		// get the volume interface
		result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_VOLUME, &bqPlayerVolume);
		if (SL_RESULT_SUCCESS != result)
		{
			bqPlayerVolume = nullptr;
			return;
		}

		if (maxVolumeLevel > SL_MILLIBEL_MIN)
		{ // No need to get max volume if we have it already
			return;
		}

		result = (*bqPlayerVolume)->GetMaxVolumeLevel(bqPlayerVolume, &maxVolumeLevel);
		if (SL_RESULT_SUCCESS != result)
		{
			bqPlayerVolume = nullptr;
			return;
		}
#endif
	}

	AudioPlayerInternal::~AudioPlayerInternal()
	{
		destroy();
	}

	void AudioPlayerInternal::destroy()
	{
		// destroy buffer queue audio player object, and invalidate all associated interfaces
		if (bqPlayerObject == nullptr)
		{
			return;
		}
		(*bqPlayerObject)->Destroy(bqPlayerObject);
		bqPlayerObject = nullptr;
		bqPlayerPlay = nullptr;
		bqPlayerBufferQueue = nullptr;
		bqPlayerVolume = nullptr;
	}

	void AudioPlayerInternal::playingCallback(SLAndroidSimpleBufferQueueItf bq, void *context)
	{
		assert(context != nullptr);

		auto pPlayer = (AudioPlayerInternal*)context;
		assert(bq == pPlayer->bqPlayerBufferQueue);

		pPlayer->onPlayed();
	}

	void AudioPlayerInternal::play(nama::Audio *pAudio, float volume)
	{
		if (!bqPlayerPlay || !bqPlayerBufferQueue)
		{
			LOG("Cannot play audio %d! Player not initialized!", pAudio->id());
			return;
		}

		m_pAudio = pAudio;
		setVolume(volume);

		// set the player's state to playing
		(*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PLAYING);
		(*bqPlayerBufferQueue)->Enqueue(bqPlayerBufferQueue, pAudio->data(), pAudio->size());
	}


	bool AudioPlayerInternal::setVolume(float volume)
	{
#if ADJUST_VOLUME
		if (!bqPlayerVolume)
		{
			return false;
		}

		//calc SLES volume
		SLmillibel level;
		if (volume <= 0)
		{
			level = SL_MILLIBEL_MIN;
		}
		else if (volume >= 1)
		{
			level = maxVolumeLevel;
		}
		else
		{
			SLmillibel dBLevel = static_cast<SLmillibel>(20 * log10(volume));
			level = dBLevel * (SLmillibel)100; //1dB = 100mB
		}

		LOG("Play at %d mB (%f volume)", level, volume);

		//set
		SLresult result = (*bqPlayerVolume)->SetVolumeLevel(bqPlayerVolume, level);
		return result == SL_RESULT_SUCCESS;
#else
		return false;
#endif
	}

	void AudioPlayerInternal::onPlayed()
	{
        LOG("Playing audio %d done", m_pAudio->id());
        m_pAudio = nullptr;

        AudioEngine::instance().onPlayingDone(this);
        return;
	}

	void AudioPlayerInternal::stop()
	{
		if (!bqPlayerPlay || !playing())
		{
			return;
		}
		SLresult result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_STOPPED);
		if (result == SL_RESULT_SUCCESS)
		{
			(*bqPlayerBufferQueue)->Clear(bqPlayerBufferQueue);
		}
		m_pAudio = nullptr;
	}

	AudioPlayer *AudioPlayer::create()
	{
		AudioPlayerInternal player;
		if (player.bqPlayerBufferQueue && player.bqPlayerObject)
		{
			return new AudioPlayerInternal(std::move(player));
		}
		return nullptr;
	}

	void AudioPlayer::destroy(AudioPlayer *pPlayer)
	{
		delete pPlayer;
	}

	Audio *AudioPlayer::audio() const
	{
		return m_pAudio;
	}
}
