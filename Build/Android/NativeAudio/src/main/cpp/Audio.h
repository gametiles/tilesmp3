
#pragma once

#include <cstdlib>

namespace nama
{
	class Audio
	{
	public:
		Audio(void* data, int size);
		~Audio();

		const void* data() const { return m_data; }
		int size() const { return m_size; }
		int id() const { return m_id; }
		void setId(int id) { m_id = id; }

	private:
		void* m_data;
		int m_size;
		int m_id = -1;
	};
}

