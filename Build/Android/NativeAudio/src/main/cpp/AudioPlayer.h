//
// Created by Nama on 8/29/2017.
//

#pragma once

namespace nama
{
	class Audio;

	class AudioPlayer
	{
	protected:
		AudioPlayer() {}
		virtual ~AudioPlayer() {}

	public:
		static AudioPlayer* create();
		static void destroy(AudioPlayer* pPlayer);
		Audio* audio() const;

		virtual void play(Audio* pAudio, float volume) = 0;
		virtual void stop() = 0;
		virtual bool setVolume(float volume) = 0;
		bool playing() const { return m_pAudio != nullptr; }

	protected:
		// The audio being played
		Audio* m_pAudio = nullptr;
	};
}
