//
// Created by Nama on 9/14/2017.
//

#pragma once

#include "Config.h"

#if AUDIO_USE_BUILTIN && defined(__ANDROID__)

#include <cstdint>

namespace nama
{
	enum class AudioFormat
	{
		UNKOWN,
		MP3,
		OGG
	};

	class AudioDecoder
	{
	public:
		virtual ~AudioDecoder() {}
		static AudioDecoder& instance();
		virtual void initialize() = 0;
		virtual void shutdown() = 0;
		virtual int decode(
				uint8_t* bytes, int size, int* nChannels, int* sampleRate, short** output) = 0;
		virtual int decodeAsset(const char* szAsset, short** output, AudioFormat format) = 0;
	};
}

#endif