
#include "Config.h"
#include "Android.h"
#include "AudioEngine.h"
#include "AudioPool.h"
#include "Log.h"

// for native asset manager
#include <android/asset_manager_jni.h>

#include <string>
#include <cstdlib>
#include <cassert>

namespace nama
{
namespace android
{
	static AAssetManager *pAssetManager = nullptr;

	static int s_nativeAudioSampleRate = 0;
	static int s_nativeAudioBufferSize = 0;
	static char s_dataDir[128] = {0};

    static JavaVM *s_jvm = nullptr;
    static jobject activity = nullptr;
    static jmethodID mid_onAudioLoaded = nullptr;
    static jmethodID mid_onAudiosLoaded = nullptr;
    static jclass activityClass = nullptr;

    static jclass unityPlayerClass;
    static jmethodID mid_UnityPlayer_SendMessage = nullptr;
    static jstring unityAndroidManager = nullptr;
    static jstring unityOnAudioLoaded = nullptr;
    static jstring unityOnAudiosLoaded = nullptr;
    static jstring emptyString = nullptr;

    struct JniEnv
    {
        JniEnv()
        {
            jint result = s_jvm->GetEnv((void **) &env, JNI_VERSION_1_6);

            if (result == JNI_EDETACHED)
            {
                s_jvm->AttachCurrentThread(&env, nullptr);
                attached = true;
            }
            else
            {
                attached = false;
            }
        }

        ~JniEnv()
        {
            if (env && attached)
            {
                s_jvm->DetachCurrentThread();
            }
        }

        JNIEnv* operator->() { return env; }
        const JNIEnv* operator->() const { return env; }

        JNIEnv* env = nullptr;
        bool attached = false;
    };

    static jstring createGlobalString(JniEnv& env, const char* s)
    {
        jobject obj = env->NewStringUTF("AudioManager");
        jobject str = env->NewGlobalRef(obj);
        env->DeleteLocalRef(obj);
        return jstring(str);
    }

    static void deleteUnityCallbackNames(JniEnv& env)
    {
        if (unityAndroidManager)
        {
            env->DeleteGlobalRef(unityAndroidManager);
            unityAndroidManager = nullptr;

            if (unityOnAudioLoaded)
            {
                env->DeleteGlobalRef(unityOnAudioLoaded);
                unityOnAudioLoaded = nullptr;
            }

            if (unityOnAudiosLoaded)
            {
                env->DeleteGlobalRef(unityOnAudiosLoaded);
                unityOnAudiosLoaded = nullptr;
            }
        }
    }

	void initialize(JavaVM *jvm)
	{
        s_jvm = jvm;
        JniEnv env;

        // Get asset manager
		jmethodID mid = nullptr;

#if BUILD_UNITY_PLUGIN
		unityPlayerClass = env->FindClass("com/unity3d/player/UnityPlayer");
        unityPlayerClass = (jclass)env->NewGlobalRef(unityPlayerClass);

		jfieldID fid = env->GetStaticFieldID(
				unityPlayerClass,
				"currentActivity",
				"Landroid/app/Activity;");
		if (!fid)
		{
			LOG("currentActivity not found");
		}
		activityClass = env->FindClass("android/app/Activity");
		activity = env->GetStaticObjectField(unityPlayerClass, fid);
		if (!activity)
		{
			LOG("Activity is null");
		}

        mid_UnityPlayer_SendMessage = env->GetStaticMethodID(
                unityPlayerClass,
                "UnitySendMessage",
                "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
#else
		activityClass = env->FindClass("com/nama/audiotest/MainActivity");
		mid = env->GetStaticMethodID(
				activityClass,
				"currentActivity",
				"()Lcom/nama/audiotest/MainActivity;");
		activity = env->CallStaticObjectMethod(activityClass, mid);

        mid_onAudioLoaded = env->GetMethodID(
                activityClass,
                "onAudioLoaded",
                "()V");

        mid_onAudiosLoaded = env->GetMethodID(
                activityClass,
                "onAudiosLoaded",
                "()V");
#endif
        activity = env->NewGlobalRef(activity);

		mid = env->GetMethodID(
				activityClass,
				"getAssets",
				"()Landroid/content/res/AssetManager;");
		jobject assetManager = (jobject) env->CallObjectMethod(activity, mid);

		// use asset manager to open asset by filename
		pAssetManager = AAssetManager_fromJava(env.env, assetManager);
		assert(NULL != pAssetManager);


		s_nativeAudioSampleRate = 0;
		s_nativeAudioBufferSize = 0;
		jclass cls = env->FindClass("android/os/Build$VERSION");
		int version = env->GetStaticIntField(cls, env->GetStaticFieldID(cls, "SDK_INT", "I"));
		if (version >= 17)
		{
			// Utility
			jclass cls_Integer = env->FindClass("java/lang/Integer");
			jmethodID mid_Integer_parseInt = env->GetStaticMethodID(
					cls_Integer, "parseInt",
					"(Ljava/lang/String;)I");

			// Get AudioManager
			jstring param = env->NewStringUTF("audio");
			mid = env->GetMethodID(
					activityClass,
					"getSystemService",
					"(Ljava/lang/String;)Ljava/lang/Object;");
			jobject audioManager = env->CallObjectMethod(activity, mid, param);
			env->DeleteLocalRef(param);

			// getProperty method
			jclass cls_AudioManager = env->FindClass("android/media/AudioManager");
			jmethodID mid_AudioManager_getProperty = env->GetMethodID(
					cls_AudioManager,
					"getProperty",
					"(Ljava/lang/String;)Ljava/lang/String;");

			// native sample rate
			param = env->NewStringUTF("android.media.property.OUTPUT_SAMPLE_RATE");
			jstring strSampleRate = (jstring) env->CallObjectMethod(
					audioManager,
					mid_AudioManager_getProperty,
					param);
			env->DeleteLocalRef(param);

			s_nativeAudioSampleRate = env->CallStaticIntMethod(
					cls_Integer,
					mid_Integer_parseInt,
					strSampleRate);
			env->DeleteLocalRef(strSampleRate);

			// native buffer size
			param = env->NewStringUTF("android.media.property.OUTPUT_FRAMES_PER_BUFFER");
			jstring strFramesPerBuffer = (jstring) env->CallObjectMethod(
					audioManager,
					mid_AudioManager_getProperty,
					param);
			env->DeleteLocalRef(param);

			s_nativeAudioBufferSize = env->CallStaticIntMethod(
					cls_Integer,
					mid_Integer_parseInt,
					strFramesPerBuffer);
			env->DeleteLocalRef(strFramesPerBuffer);
		}

		LOG("Native buffer size %d", s_nativeAudioBufferSize);
		LOG("Native sample rate %d", s_nativeAudioSampleRate);

		// Get data dir
		jclass cls_File = env->FindClass("java/io/File");
		jmethodID mid_File_toString = env->GetMethodID(cls_File, "toString", "()Ljava/lang/String;");
		mid = env->GetMethodID(activityClass, "getExternalFilesDir", "(Ljava/lang/String;)Ljava/io/File;");
		jobject fileDir = env->CallObjectMethod(activity, mid, nullptr);
		jstring javaString = (jstring)env->CallObjectMethod(fileDir, mid_File_toString);
		const char* nativeString = env->GetStringUTFChars(javaString, 0);
		strcpy(s_dataDir, nativeString);
		env->ReleaseStringUTFChars(javaString, nativeString);

		LOG("Data dir: %s", s_dataDir);
	}

    static void shutdown()
    {
        nama::AudioPool::instance().shutdown();
        nama::AudioEngine::instance().shutdown();

        if (!activity)
        {
            return;
        }

        JniEnv env;

        env->DeleteGlobalRef(activity);
        activity = nullptr;

        if (!mid_UnityPlayer_SendMessage)
        {
            return;
        }

        env->DeleteGlobalRef(unityPlayerClass);
        unityPlayerClass = nullptr;

        deleteUnityCallbackNames(env);

        if (emptyString)
        {
            env->DeleteGlobalRef(emptyString);
            emptyString = nullptr;
        }
    }

	uint8_t *getAsset(const char *szFile, const char *szDir, int *size)
	{
		AAssetDir *assetDir = AAssetManager_openDir(pAssetManager, szDir);

		const char *filename;

		while ((filename = AAssetDir_getNextFileName(assetDir)) != NULL)
		{
			//search for desired file
			if (strcmp(filename, szFile))
			{
				continue;
			}

			std::string fileName;
			if (szDir[0])
			{
				fileName = std::string(szDir) + "/" + szFile;
				szFile = fileName.c_str();
			}
			AAsset *asset = AAssetManager_open(pAssetManager, szFile, AASSET_MODE_UNKNOWN);

			//holds size of searched file
			off64_t length = AAsset_getLength64(asset);
            uint8_t *data = reinterpret_cast<uint8_t *>(malloc(length));
            AAsset_read (asset,data,length);
			AAsset_close(asset);

			*size = length;
			return data;
		}
		return nullptr;
	}

	void freeAsset(uint8_t *assetData)
	{
		free(assetData);
	}

	int getFileDescriptor(const char* szAsset, off_t* start, off_t* length)
	{
		AAsset* asset = AAssetManager_open(pAssetManager, szAsset, AASSET_MODE_UNKNOWN);
		int fd = AAsset_openFileDescriptor(asset, start, length);
		AAsset_close(asset);
		return fd;
	}

	int nativeAudioSampleRate()
	{
		return s_nativeAudioSampleRate;
	}

	int nativeAudioBufferSize()
	{
		return s_nativeAudioBufferSize;
	}

	const char* externalDirectory()
	{
		return s_dataDir;
	}

    void onAudioLoaded()
    {
        if (mid_onAudioLoaded)
        {
            JniEnv env;
            env->CallVoidMethod(activity, mid_onAudioLoaded);
        }
        else if (mid_UnityPlayer_SendMessage && unityAndroidManager && unityOnAudioLoaded)
        {
            LOG("Call MonoArray AudioManager.OnAudioLoaded()");
            JniEnv env;
            env->CallStaticVoidMethod(
                    unityPlayerClass,
                    mid_UnityPlayer_SendMessage,
                    unityAndroidManager,
                    unityOnAudioLoaded,
                    emptyString);
        }
    }

    void onAudiosLoaded()
    {
        if (mid_onAudiosLoaded)
        {
            JniEnv env;
            env->CallVoidMethod(activity, mid_onAudiosLoaded);
        }
        else if (mid_UnityPlayer_SendMessage && unityAndroidManager && unityOnAudiosLoaded)
        {
            LOG("Call MonoArray AudioManager.OnAudiosLoaded()");
            JniEnv env;
            env->CallStaticVoidMethod(
                    unityPlayerClass,
                    mid_UnityPlayer_SendMessage,
                    unityAndroidManager,
                    unityOnAudiosLoaded,
                    emptyString);
        }
    }

    void setUnityAudioCallbacks(
            const char* szUnityObject,
            const char* szOnAudioLoaded,
            const char* szOnAudiosLoaded)
    {
        JniEnv env;
        deleteUnityCallbackNames(env);
        if (!szUnityObject)
        {
            return;
        }

        unityAndroidManager = createGlobalString(env, szUnityObject);

        if (szOnAudioLoaded)
        {
            unityOnAudioLoaded = createGlobalString(env, szOnAudioLoaded);
        }

        if (szOnAudiosLoaded)
        {
            unityOnAudiosLoaded = createGlobalString(env, szOnAudiosLoaded);
        }

        if (!emptyString)
        {
            emptyString = createGlobalString(env, "");
        }
    }
}
}

// JNI OnLoad
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* jvm, void* reserved)
{
	nama::android::initialize(jvm);
	return JNI_VERSION_1_6;
}

// JNI OnLoad
JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* jvm, void* reserved)
{
    nama::android::shutdown();
}
