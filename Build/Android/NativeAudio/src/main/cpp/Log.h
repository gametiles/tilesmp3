
#pragma once

#define LOG_ENABLED 1

#if LOG_ENABLED && defined(__ANDROID__)
#include <android/log.h>
#define LOG(...) __android_log_print(ANDROID_LOG_ERROR, "Nama", __VA_ARGS__)
#else
#define LOG(...)
#endif