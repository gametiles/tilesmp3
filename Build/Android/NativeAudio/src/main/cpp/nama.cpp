#include <jni.h>
#include <string>
#include "AudioEngine.h"
#include "AudioPool.h"
#include "Android.h"

#define NAMA_EXPORT __attribute__ ((visibility ("default")))

extern "C"
{
	NAMA_EXPORT void nama_initAndroidAudio(int maxPlayers)
	{
		nama::AudioEngine::instance().initialize(
				nama::android::nativeAudioSampleRate(),
				nama::android::nativeAudioBufferSize());

		nama::AudioPool::instance().initialize();
	}

	NAMA_EXPORT void nama_shutdownAndroidAudio()
	{
		nama::AudioPool::instance().shutdown();
		nama::AudioEngine::instance().shutdown();
	}

	NAMA_EXPORT void nama_playAudio(int id, float volume)
	{
		nama::AudioPool::instance().play(id, volume);
	}

	NAMA_EXPORT void nama_onPause()
	{
		nama::AudioEngine::instance().onPause();
	}

	NAMA_EXPORT void nama_onResume()
	{
		nama::AudioEngine::instance().onResume();
	}

	NAMA_EXPORT bool nama_audioPoolLoaded()
	{
		return nama::AudioPool::instance().isLoaded();
	}

	NAMA_EXPORT bool nama_loadAudioCache()
	{
		return nama::AudioPool::instance().loadCache();
	}

	NAMA_EXPORT void nama_beginAudioCaching()
	{
        nama::AudioPool::instance().beginLoading();
	}

	NAMA_EXPORT void nama_endAudioCaching()
	{
        nama::AudioPool::instance().endLoading();
	}
#if AUDIO_USE_MPG123 || AUDIO_USE_BUILTIN
	NAMA_EXPORT void nama_loadMp3(int id, void *data, int size)
	{
        nama::AudioPool::instance().loadMp3(id, data, size);
	}

#if AUDIO_LOAD_FROM_ASSETS || AUDIO_USE_BUILTIN
	NAMA_EXPORT void nama_loadMp3Asset(int id, const char *szAsset)
	{
        nama::AudioPool::instance().loadMp3(id, szAsset);
	}
#endif
#endif

#if AUDIO_USE_STB_VORBIS || AUDIO_USE_LIB_VORBIS || AUDIO_USE_BUILTIN
    NAMA_EXPORT void nama_loadOgg(int id, void *data, int size)
	{
        nama::AudioPool::instance().loadOgg(id, data, size);
	}

#if AUDIO_LOAD_FROM_ASSETS || AUDIO_USE_BUILTIN
	NAMA_EXPORT bool nama_loadOggAsset(int id, const char *szAsset)
	{
		return nama::AudioPool::instance().loadOgg(id, szAsset);
	}
#endif
#endif

	NAMA_EXPORT void nama_loadPcm32(
            int id,
            float *data,
            int nChannels,
            int nSamples,
            int sampleRate)
	{
        nama::AudioPool::instance().loadPcm32(id, data, nChannels, nSamples, sampleRate);
	}

	NAMA_EXPORT void nama_loadPcm32Mono(
            int id,
            void* data,
            int nChannels,
            int nSamples,
            int sampleRate)
	{
        float* pcm32 = reinterpret_cast<float*>(data);
        nama::AudioPool::instance().loadPcm32(id, pcm32, nChannels, nSamples, sampleRate);
	}

    NAMA_EXPORT void nama_loadPackedMp3sMono(void* data, int size, bool multithread)
    {
        nama::AudioPool::instance().loadPackedMp3s(data, size, multithread);
    }

    NAMA_EXPORT void nama_loadPackedOggsMono(void* data, int size, bool multithread)
    {
        nama::AudioPool::instance().loadPackedOggs(data, size, multithread);
    }

    NAMA_EXPORT void nama_loadPackedMp3sAssetMono(const char* szAsset, bool multithread)
    {
        nama::AudioPool::instance().loadPackedMp3s(szAsset, multithread);
    }

    NAMA_EXPORT void nama_loadPackedOggsAssetMono(const char* szAsset, bool multithread)
    {
        nama::AudioPool::instance().loadPackedMp3s(szAsset, multithread);
    }

    NAMA_EXPORT void nama_setUnityAudioCallbacks(
            const char* szUnityObject,
            const char* szOnAudioLoaded,
            const char* szOnAudiosLoaded)
    {
        nama::android::setUnityAudioCallbacks(szUnityObject, szOnAudioLoaded, szOnAudiosLoaded);
    }

    NAMA_EXPORT int nama_nativeAudioSampleRate()
    {
        return nama::android::nativeAudioSampleRate();
    }
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_nama_audiotest_MainActivity_stringFromJNI(JNIEnv *env, jobject)
{
	std::string hello = "Hello from C++";
	return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT void JNICALL
Java_com_nama_audiotest_MainActivity_initAudioEngine(JNIEnv*, jobject)
{
	nama_initAndroidAudio(16);
}

extern "C" JNIEXPORT void JNICALL
Java_com_nama_audiotest_MainActivity_playAudio(JNIEnv*, jobject, jint id, jfloat voloume)
{
	nama_playAudio(id, voloume);
}

extern "C" JNIEXPORT void JNICALL
Java_com_nama_audiotest_MainActivity_beginAudioCaching(JNIEnv*, jobject)
{
	nama_beginAudioCaching();
}

extern "C" JNIEXPORT void JNICALL
Java_com_nama_audiotest_MainActivity_endAudioCaching(JNIEnv*, jobject)
{
	nama_endAudioCaching();
}

extern "C" JNIEXPORT jboolean JNICALL
Java_com_nama_audiotest_MainActivity_loadAudioCache(JNIEnv*, jobject)
{
	return (jboolean)(nama_loadAudioCache() ? JNI_TRUE : JNI_FALSE);
}

extern "C" JNIEXPORT void JNICALL
Java_com_nama_audiotest_MainActivity_pause(JNIEnv*, jobject)
{
	nama_onPause();
}

extern "C" JNIEXPORT void JNICALL
Java_com_nama_audiotest_MainActivity_resume(JNIEnv*, jobject)
{
    nama_onResume();
}

static int audios[]
    {
        21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
        39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
        57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74,
        75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92,
        93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108,
    };

extern "C" JNIEXPORT void JNICALL
Java_com_nama_audiotest_MainActivity_loadMp3s(JNIEnv*, jobject, jboolean multithread)
{
    const char* szAsset = nama::android::nativeAudioSampleRate() == 48000 ?
                          "PianoKeysMp3_48000Hz.bytes" : "PianoKeysMp3_44100Hz.bytes";
    nama::AudioPool::instance().loadPackedMp3s(szAsset, multithread);
}

extern "C" JNIEXPORT void JNICALL
Java_com_nama_audiotest_MainActivity_loadOggs(JNIEnv*, jobject, jboolean multithread)
{
    const char* szAsset = nama::android::nativeAudioSampleRate() == 48000 ?
            "PianoKeysOgg_48000Hz.bytes" : "PianoKeysOgg_44100Hz.bytes";
    nama::AudioPool::instance().loadPackedOggs(szAsset, multithread);
}

extern "C" JNIEXPORT jint JNICALL
Java_com_nama_audiotest_MainActivity_audioCount(JNIEnv*, jobject)
{
    return sizeof(audios)/sizeof(audios[0]);
}

extern "C" JNIEXPORT jint JNICALL
Java_com_nama_audiotest_MainActivity_audioId(JNIEnv*, jobject, jint index)
{
    return audios[index];
}

