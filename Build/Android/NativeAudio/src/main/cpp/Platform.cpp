
#pragma once

#include <time.h>

#if defined(__ANDROID__)
#include <cpu-features.h>
#endif

namespace nama {
namespace platform {
    long long nowNs()
    {
        struct timespec res;
        clock_gettime(CLOCK_MONOTONIC, &res);
        return (long long)res.tv_sec * 1000000000LL + res.tv_nsec;
    }

    int cpuCount()
    {
#if defined(__ANDROID__)
        return android_getCpuCount();
#else
        return 1;
#endif
    }
}
}