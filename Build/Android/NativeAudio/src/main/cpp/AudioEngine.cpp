
#include "Config.h"

#include "AudioEngineInternal.h"
#include "AudioPlayer.h"
#include "Audio.h"
#include "Log.h"

#include <algorithm>

namespace nama
{
	AudioEngine& AudioEngine::instance()
	{
		static AudioEngineInternal manager;
		return manager;
	}

	AudioEngineInternal::~AudioEngineInternal()
	{
		shutdown();
	}

	void AudioEngineInternal::initialize(int nativeSampleRate, int nativeBufferSize)
	{
		m_sampleRate = nativeSampleRate ? nativeSampleRate : 44100;
		m_bufferSize = nativeBufferSize ? nativeBufferSize : 1024;
		LOG("Playback sample rate = %d", m_sampleRate);
		LOG("Buffer size = %d", m_bufferSize);

		SLresult result;

		// create engine
		result = slCreateEngine(&engineObject, 0, NULL, 0, NULL, NULL);
		if (SL_RESULT_SUCCESS != result)
		{
			return;
		}
		(void)result;

		while (true)
		{
			// realize the engine
			result = (*engineObject)->Realize(engineObject, SL_BOOLEAN_FALSE);
			if (SL_RESULT_SUCCESS != result)
			{
				break;
			}

			// get the engine interface, which is needed in order to create other objects
			result = (*engineObject)->GetInterface(engineObject, SL_IID_ENGINE, &slEngine);
			if (SL_RESULT_SUCCESS != result)
			{
				break;
			}

			// create output mix, with environmental reverb specified as a non-required interface
			const SLInterfaceID ids[1] = {SL_IID_ENVIRONMENTALREVERB};
			const SLboolean req[1] = {SL_BOOLEAN_FALSE};
			result = (*slEngine)->CreateOutputMix(slEngine, &outputMixObject, 1, ids, req);
			if (SL_RESULT_SUCCESS != result)
			{
				break;
			}

			// realize the output mix
			result = (*outputMixObject)->Realize(outputMixObject, SL_BOOLEAN_FALSE);
			break;
		}

		if (SL_RESULT_SUCCESS != result)
		{
			shutdown();
		}
	}

	void AudioEngineInternal::shutdown()
	{
		destroyAllPlayers();

		// destroy output mix object, and invalidate all associated interfaces
		if (outputMixObject != NULL)
		{
			(*outputMixObject)->Destroy(outputMixObject);
			outputMixObject = NULL;
		}

		// destroy engine object, and invalidate all associated interfaces
		if (engineObject != NULL)
		{
			(*engineObject)->Destroy(engineObject);
			engineObject = NULL;
			slEngine = NULL;
		}
	}

	void AudioEngineInternal::onPause()
	{
		m_appPaused = true;
		destroyAllPlayers();
	}

	void AudioEngineInternal::onResume()
	{

	}

	void AudioEngineInternal::onPlayingDone(AudioPlayer *pPlayer)
	{
	}

	AudioEngine::~AudioEngine()
	{
		destroyAllPlayers();
	}

	void AudioEngine::play(Audio *pAudio, float volume)
	{
		auto pPlayer = idlePlayer();
		if (pPlayer)
		{
			pPlayer->play(pAudio, volume);
		}
		else
		{
			LOG("No player available!");
		}
	}

	AudioPlayer *AudioEngine::idlePlayer()
	{
        lock();
		AudioPlayer* pPlayer = m_players.size() > 0 ? m_players.front() : nullptr;
        if (!pPlayer || (pPlayer->playing() && m_players.size() <= MAX_NUMBER_OF_PLAYERS))
        {
            auto pNew = AudioPlayer::create();
            if (pNew)
            {
                m_players.push_back(pNew);
                LOG("Number of players %d", m_players.size());
                unlock();
                return pNew;
            }
        }
        if (pPlayer)
        {
            m_players.pop_front();
            m_players.push_back(pPlayer);
            if (pPlayer->playing())
            {
                pPlayer->stop();
            }
        }

        unlock();
        return pPlayer;
	}

	void AudioEngine::destroyAllPlayers()
	{
		// Destroy audio players
		for (auto p : m_players)
		{
			AudioPlayer::destroy(p);
		}
		m_players.clear();
	}

    void AudioEngine::lock()
    {
        std::unique_lock<std::mutex> autoLock(m_lock);
        while (m_nWaiting > 0)
        {
            m_condition.wait(autoLock);
        }
        m_nWaiting ++;
    }

    void AudioEngine::unlock()
    {
        m_nWaiting--;
        m_condition.notify_one();
    }
}