
#include "AudioUtil.h"
#include "Audio.h"
#include "Android.h"

#if USE_LIB_SOXR
#include "libsoxr/soxr.h"
#elif USE_LIB_SAMPLE_RATE
#include "libsamplerate/samplerate.h"
#endif

#include <cstdlib>

extern "C" void copy_samples(short *dest, float *src, int len); // stb util

namespace nama
{
    float * resampleAudio(
            float *input,
            int nChannels,
            int inputSamples,
            int inputSampleRate,
            int outputSampleRate,
            int *outputSamples)
    {
        float* output;

#if USE_LIB_SAMPLE_RATE
        int converter = SRC_SINC_FASTEST;
		SRC_STATE	*srcState;
		SRC_DATA	srcData;
		int			error;

		/* Initialize the sample rate converter. */
		if ((srcState = src_new (converter, nChannels, &error)) == nullptr)
		{
			return input;
		}

		srcData.end_of_input = 0; /* Set this later. */

		/* Start with zero to force load in while loop. */
		srcData.input_frames = 0;
		srcData.data_in = input;

		srcData.src_ratio = (float)outputSampleRate / inputSampleRate;

		srcData.output_frames = static_cast<long>(inputSamples * srcData.src_ratio + 0.5);
		srcData.data_out = (float*)malloc(srcData.output_frames * nChannels * sizeof(float));
		if (!srcData.data_out)
		{
			return input;
		}

		srcData.input_frames = inputSamples;

		if ((error = src_process (srcState, &srcData)))
		{
			free(srcData.data_out);
			return input;
		};

		srcData.data_in += srcData.input_frames_used * nChannels;
		srcData.input_frames -= srcData.input_frames_used;
		output = srcData.data_out;
		src_delete (srcState);

		*outputSamples = static_cast<int>(srcData.output_frames_gen);
#elif USE_LIB_SOXR

        float ratio = (float)outputSampleRate / inputSampleRate;
        size_t nOutput = static_cast<size_t>(inputSamples * ratio + 0.5);
        output = (float*)malloc(nOutput * nChannels * sizeof(float));

        size_t nActualOutput;
        soxr_io_spec_t ioSpec;
        ioSpec.itype = SOXR_FLOAT32_I;
        ioSpec.otype = SOXR_INT16_I;

        soxr_quality_spec_t qualitySpec = soxr_quality_spec(SOXR_LSR0Q + 2, 0);

        soxr_error_t error = soxr_oneshot(
                inputSampleRate, outputSampleRate, nChannels,
                input, inputSamples, nullptr,
                output, nOutput, &nActualOutput,
                nullptr, &qualitySpec, nullptr);
        *outputSamples = nActualOutput;
#else
        output = input;
#endif
        return output;
    }

    short* convertAudio(float* pcm32, int nChannels, int nSamples)
    {
        // Convert to short
        int totalSamples = nSamples * nChannels;
        size_t size = totalSamples * sizeof(short);
        short* pcm16 = (short*)malloc(size);
        if (!pcm16)
        {
            return nullptr;
        }
        copy_samples(pcm16, pcm32, totalSamples);
        return pcm16;
    }

    Audio* createAudio(
            float* pcm32,
            int nChannels,
            int nSamples,
            int sampleRate)
    {
        // Resample if necessary
        int nativeSampleRate = android::nativeAudioSampleRate();
        float* nativePcm32;
        if (nativeSampleRate > 0 && nativeSampleRate != sampleRate)
        {
            nativePcm32 = resampleAudio(
                    pcm32,
                    nChannels,
                    nSamples,
                    sampleRate,
                    nativeSampleRate,
                    &nSamples);
        }
        else
        {
            nativePcm32 = pcm32;
        }

        // Convert to short
        short* pcm16 = convertAudio(nativePcm32, nChannels, nSamples);

        if (nativePcm32 != pcm32)
        {
            free(nativePcm32);
        }

#if REMOVE_SILENCE
        // Remove silence
		short* z = pcm16 + nSamples * nChannels;
		bool nonZeroFound = false;
		for (int i = 0; i < nSamples; i++)
		{
			for (int j = 0; j < nChannels; j++)
			{
				if ((--z)[0] != 0)
				{
					nonZeroFound = true;
					break;
				}
			}
			if (nonZeroFound)
			{
				break;
			}
			else
			{
				nSamples--;
			}
		}
#endif

#if 0
        char s[128];
		sprintf(s, "%s/audio%d.pcm", android::externalDirectory(), id);
		FILE* file = fopen(s, "wb");
		fwrite(pcm16, sizeof(short), nSamples * nChannels, file);
		fclose(file);
#endif
        size_t size = nSamples * nChannels * sizeof(short);
        return new Audio(pcm16, size);
    }
}
