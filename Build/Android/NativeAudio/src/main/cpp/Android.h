
#pragma once

#include <jni.h>
#include <sys/types.h>

namespace nama
{
	namespace android
	{
		int nativeAudioSampleRate();
		int nativeAudioBufferSize();

		uint8_t* getAsset(const char* szFile, const char* szDir, int* size);
		void freeAsset(uint8_t* assetData);

		int getFileDescriptor(const char* szAsset, off_t* start, off_t* length);

		const char* externalDirectory();

		void onAudioLoaded();
		void onAudiosLoaded();

		void setUnityAudioCallbacks(
				const char* szUnityObject,
				const char* szOnAudioLoaded,
				const char* szOnAudiosLoaded);
	};
}
