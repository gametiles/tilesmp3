
#pragma once

#include "Config.h"

namespace nama
{
    class Audio;

	class VorbisDecoder
	{
	public:
#if AUDIO_USE_STB_VORBIS
        static VorbisDecoder& instance();
        void initialize();
        void shutdown();
        Audio* decode(void* bytes, int size, int sampleRate);
#elif AUDIO_USE_LIB_VORBIS
		virtual ~VorbisDecoder() {}
		static VorbisDecoder& instance();
		virtual void initialize() = 0;
		virtual void shutdown() = 0;
		virtual Audio* decode(void* bytes, int size, int sampleRate) = 0;
#endif
	};
}
