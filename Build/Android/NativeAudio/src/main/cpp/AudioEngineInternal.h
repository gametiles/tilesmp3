
#pragma once

#include "AudioEngine.h"

// for native audio
#include <SLES/OpenSLES.h>

namespace nama
{
	class AudioEngineInternal : public  AudioEngine
	{
	public:
		~AudioEngineInternal();
		void initialize(int nativeSampleRate, int nativeBufferSize) override;
		void shutdown() override;
		void onPause() override;
		void onResume() override;
		void onPlayingDone(AudioPlayer* pPlayer) override;

		// engine interfaces
		SLObjectItf engineObject = nullptr;
		SLEngineItf slEngine;

		// output mix interfaces
		SLObjectItf outputMixObject = nullptr;
	};
}