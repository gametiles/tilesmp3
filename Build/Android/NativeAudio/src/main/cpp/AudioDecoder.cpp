
#include "AudioDecoder.h"

#if AUDIO_USE_BUILTIN && defined(__ANDROID__)

#include "AudioEngineInternal.h"
#include "Android.h"
#include <SLES/OpenSLES_Android.h>

#include <cstdlib>
#include <cassert>
#include <condition_variable>

namespace nama
{
	enum {
		BUFFER_SIZE_IN_SAMPLES = 1152, // number of samples per MP3 frame
		BUFFER_SIZE_IN_BYTES = (2*BUFFER_SIZE_IN_SAMPLES),
		PCM_BUFFER_SIZE = 1024*1024,
		BUFFERS_IN_QUEUE = 4
	};

	class AudioDecoderInternal : public AudioDecoder
	{
	public:
		AudioDecoderInternal();
		~AudioDecoderInternal();
		void initialize() override;
		void shutdown() override;
		int decode(
				uint8_t* bytes, int size, int* nChannels, int* sampleRate, short** output) override;
		int decodeAsset(const char* szAsset, short** output, AudioFormat format) override;

		void onChunkDecoded();
		void onDecodingDone();
		SLAndroidSimpleBufferQueueItf fdPlayerBufferQueue = nullptr;
		SLMetadataExtractionItf mdExtrItf = nullptr;
		int channelCountKeyIndex;
		int sampleRateKeyIndex;

		short* pcmBuffer = nullptr;
		int decodedSize = 0;
		int chunkSize = 0;
		int pcmBufferSize = 0;
		bool decodingDone = false;
		bool formatQueried = false;

		std::mutex eosLock;
		std::condition_variable eosCondition;
	};

	AudioDecoderInternal::AudioDecoderInternal()
	{

	}

	AudioDecoderInternal::~AudioDecoderInternal()
	{
		shutdown();
	}

	void AudioDecoderInternal::initialize()
	{
		pcmBuffer = (short*)malloc(PCM_BUFFER_SIZE);
		pcmBufferSize = PCM_BUFFER_SIZE;
	}

	void AudioDecoderInternal::shutdown()
	{
		if (pcmBuffer)
		{
			free(pcmBuffer);
			pcmBuffer = nullptr;
		}
	}

	static void callback(SLAndroidSimpleBufferQueueItf bq, void *context)
	{
		//(*fdPlayerBufferQueue)->Enqueue(fdPlayerBufferQueue, pcmBuffer, 1152*2);
		AudioDecoderInternal* pDecoder = (AudioDecoderInternal*)context;
		pDecoder->onChunkDecoded();
	}

	static void decodeProgressCallback(SLPlayItf caller, void *context, SLuint32 event)
	{
		AudioDecoderInternal* pDecoder = (AudioDecoderInternal*)context;
		if (event & SL_PLAYEVENT_HEADATEND)
		{
			pDecoder->onDecodingDone();
		}
	}

	int AudioDecoderInternal::decode(
			uint8_t *bytes, int size, int *nChannels, int *sampleRate, short **output)
	{
		assert(false && "Not supported");
	}

	void AudioDecoderInternal::onChunkDecoded()
	{
		decodedSize += chunkSize;
		SLresult result = (*fdPlayerBufferQueue)->Enqueue(
				fdPlayerBufferQueue,
				pcmBuffer + decodedSize,
				chunkSize);
		assert(SL_RESULT_SUCCESS == result);

		// Quey format
		if (formatQueried)
		{
			return;
		}
		formatQueried = true;

		enum
		{
			PCM_METADATA_VALUE_SIZE = 32,
		};

		union
		{
			char buf[PCM_METADATA_VALUE_SIZE];
			SLMetadataInfo pcmMetaData;
		} info;

		result = (*mdExtrItf)->GetValue(
				mdExtrItf,
				sampleRateKeyIndex,
				PCM_METADATA_VALUE_SIZE,
				&info.pcmMetaData);
		assert(SL_RESULT_SUCCESS == result);

		int sampleRate = *((SLuint32*)info.pcmMetaData.data);
		result = (*mdExtrItf)->GetValue(
				mdExtrItf,
				channelCountKeyIndex,
				PCM_METADATA_VALUE_SIZE,
				&info.pcmMetaData);
		assert(SL_RESULT_SUCCESS == result);
		int nChannels = *((SLuint32*)info.pcmMetaData.data);

		int pcmSize = sampleRate * nChannels;
	}

	void AudioDecoderInternal::onDecodingDone()
	{
		std::unique_lock<std::mutex> autoLock(eosLock);
		decodingDone = true;
		eosCondition.notify_one();
	}

	int AudioDecoderInternal::decodeAsset(const char *szAsset, short **output, AudioFormat format)
	{
		SLObjectItf fdPlayerObject = NULL;
		SLPlayItf fdPlayerPlay;
		auto slEngine = ((AudioEngineInternal&)AudioEngine::instance()).slEngine;

		SLresult result;

		// configure audio source
		off_t  start, length;
		int fd = android::getFileDescriptor(szAsset, &start, &length);
		SLDataLocator_AndroidFD loc_fd = {SL_DATALOCATOR_ANDROIDFD, fd, start, length};
		SLDataFormat_MIME format_mime = {SL_DATAFORMAT_MIME, NULL, SL_CONTAINERTYPE_UNSPECIFIED};
		SLDataSource audioSrc = {&loc_fd, &format_mime};

		// configure audio sink
		SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {
				SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE,
				2};

		SLDataFormat_PCM format_pcm = {
				SL_DATAFORMAT_PCM,
				2,
				(SLuint32)AudioEngine::instance().sampleRate(),
				SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
				SL_SPEAKER_FRONT_LEFT|SL_SPEAKER_FRONT_RIGHT,
				SL_BYTEORDER_LITTLEENDIAN};

		SLDataSink audioSnk = {&loc_bufq, &format_pcm};

		// create audio player
		const SLInterfaceID ids[] =  {
				SL_IID_ANDROIDSIMPLEBUFFERQUEUE,
				SL_IID_PLAY,
				SL_IID_METADATAEXTRACTION};
		const SLboolean req[] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};
		result = (*slEngine)->CreateAudioPlayer(
				slEngine,
				&fdPlayerObject,
				&audioSrc,
				&audioSnk,
				3,
				ids,
				req);
		assert(SL_RESULT_SUCCESS == result);

		// realize the player
		result = (*fdPlayerObject)->Realize(fdPlayerObject, SL_BOOLEAN_FALSE);
		assert(SL_RESULT_SUCCESS == result);

		// get the play interface
		result = (*fdPlayerObject)->GetInterface(fdPlayerObject, SL_IID_PLAY, &fdPlayerPlay);
		assert(SL_RESULT_SUCCESS == result);

		/* Set up the player callback to get events during the decoding */

		result = (*fdPlayerPlay)->SetCallbackEventsMask(
				fdPlayerPlay, SL_PLAYEVENT_HEADATEND);
		assert(SL_RESULT_SUCCESS == result);
		result = (*fdPlayerPlay)->RegisterCallback(fdPlayerPlay, decodeProgressCallback,  this);
		assert(SL_RESULT_SUCCESS == result);

		// get the buffer queue interface
		result = (*fdPlayerObject)->GetInterface(
				fdPlayerObject, SL_IID_BUFFERQUEUE, &fdPlayerBufferQueue);
		assert(SL_RESULT_SUCCESS == result);

		// register callback on the buffer queue
		result = (*fdPlayerBufferQueue)->RegisterCallback(fdPlayerBufferQueue, callback, this);
		assert(SL_RESULT_SUCCESS == result);

		// set the player's state to playing
		result = (*fdPlayerPlay)->SetPlayState(fdPlayerPlay, SL_PLAYSTATE_PLAYING);
		assert(SL_RESULT_SUCCESS == result);

		result = (*fdPlayerObject)->GetInterface(fdPlayerObject, SL_IID_METADATAEXTRACTION, (void*)&mdExtrItf);
		assert(SL_RESULT_SUCCESS == result);

		SLuint32 itemCount;
		result = (*mdExtrItf)->GetItemCount(mdExtrItf, &itemCount);
		SLuint32 i, keySize, valueSize;
		SLMetadataInfo *keyInfo, *value;
		for(i=0 ; i<itemCount ; i++)
		{
			keyInfo = nullptr;
			keySize = 0;
			value = nullptr;
			valueSize = 0;
			result = (*mdExtrItf)->GetKeySize(mdExtrItf, i, &keySize);
			assert(SL_RESULT_SUCCESS == result);
			result = (*mdExtrItf)->GetValueSize(mdExtrItf, i, &valueSize);
			assert(SL_RESULT_SUCCESS == result);
			keyInfo = (SLMetadataInfo*) malloc(keySize);
			if (NULL != keyInfo)
			{
				result = (*mdExtrItf)->GetKey(mdExtrItf, i, keySize, keyInfo);
				assert(SL_RESULT_SUCCESS == result);
				fprintf(stdout, "key[%d] size=%d, name=%s \tvalue size=%d \n",
						i, keyInfo->size, keyInfo->data, valueSize);
				/* find out the key index of the metadata we're interested in */
				if (!strcmp((char*)keyInfo->data, ANDROID_KEY_PCMFORMAT_NUMCHANNELS))
				{
					channelCountKeyIndex = i;
				} else if (!strcmp((char*)keyInfo->data, ANDROID_KEY_PCMFORMAT_SAMPLERATE))
				{
					sampleRateKeyIndex = i;
				}
				free(keyInfo);
			}
		}

		decodingDone = false;
		decodedSize = 0;
		formatQueried = false;

		switch (format)
		{
			default:
			case AudioFormat::OGG: chunkSize = 1024; break;
			case AudioFormat::MP3: chunkSize = BUFFER_SIZE_IN_BYTES; break;
		}
		result = (*fdPlayerBufferQueue)->Enqueue(
				fdPlayerBufferQueue,
				pcmBuffer,
				chunkSize);
		assert(SL_RESULT_SUCCESS == result);

		std::unique_lock<std::mutex> autoLock(eosLock);
		while (!decodingDone)
		{
			eosCondition.wait(autoLock);
		}

		// destroy file descriptor audio player object, and invalidate all associated interfaces
		if (fdPlayerObject != nullptr)
		{
			(*fdPlayerObject)->Destroy(fdPlayerObject);
		}

		i = decodedSize / 2 -1;
		while (i >= 0 && pcmBuffer[i] != 0)
		{
			i--;
		}

		decodedSize = (i + 1) * 2;
		*output = (short*)malloc(decodedSize);
		memcpy(*output, pcmBuffer, decodedSize);
		return decodedSize;
	}

	AudioDecoder &AudioDecoder::instance()
	{
		static AudioDecoderInternal decoder;
		return decoder;
	}

}

#endif