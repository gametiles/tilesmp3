
#include "Audio.h"

namespace nama
{
	Audio::Audio(void *data, int size)
		: m_data(data), m_size(size)
	{

	}

	Audio::~Audio()
	{
		if (m_data != nullptr)
		{
			free(m_data);
		}
	}
}

