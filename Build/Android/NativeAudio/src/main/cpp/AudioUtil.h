
#pragma once

#include "Config.h"

namespace nama
{
    class Audio;

    float* resampleAudio(
            float* input,
            int nChannels,
            int inputSamples,
            int inputSampleRate,
            int outputSampleRate,
            int* outputSamples);

    short* convertAudio(float* input, int nChannels, int nSamples);

    Audio* createAudio(
            float* pcm32,
            int nChannels,
            int nSamples,
            int sampleRate);
}
