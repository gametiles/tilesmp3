
#include "Mp3Decoder.h"

#if AUDIO_USE_MPG123
#include "AudioUtil.h"
#include "Audio.h"
#include "libmpg123/mpg123.h"

#include <cstring>

namespace nama
{
	void Mp3Decoder::initialize()
	{
		mpg123_init();
	}

	void Mp3Decoder::shutdown()
	{
		mpg123_exit();
	}

	Audio* Mp3Decoder::decode(void* bytes, int size, int sampleRate)
	{
		int result, encoding;
		long rate;
		mpg123_handle *mh;

		mh = mpg123_new(nullptr, &result);

		short* pcm16 = nullptr;
		size_t pcm16Size = 0;

		mpg123_open_feed(mh);
		result = mpg123_decode(mh, (uint8_t*)bytes, size, nullptr, 0, nullptr);
		if(result == MPG123_NEW_FORMAT)
		{
			// Get number of nSamples
			mpg123_set_filesize(mh, size);
			int nSamples = mpg123_length(mh);

			// Get format
			int nChannels;
			result = mpg123_getformat(mh, &rate, &nChannels, &encoding);

			pcm16Size = sizeof(short) * nSamples * nChannels;

			if (rate != sampleRate)
			{
				size_t pcm32Size = sizeof(float) * nSamples * nChannels;
				float* pcm32 = (float*)malloc(pcm32Size);

				mpg123_close(mh);
				mpg123_param(mh, MPG123_ADD_FLAGS, MPG123_FORCE_FLOAT, 0.);
				mpg123_open_feed(mh);
				result = mpg123_decode(mh, (uint8_t*)bytes, size, nullptr, 0, nullptr);
				result = mpg123_decode(mh, nullptr, 0, (unsigned char*)pcm32, pcm32Size, &pcm32Size);

				float* pcm32r = resampleAudio(pcm32, nChannels, nSamples, rate, sampleRate, &nSamples);
				pcm16 = convertAudio(pcm32r, nChannels, nSamples);
				free(pcm32);
				free(pcm32r);
			}
			else
			{
				pcm16 = (short*)malloc(pcm16Size);
				result = mpg123_decode(mh, nullptr, 0, (unsigned char*)pcm16, pcm16Size, &pcm16Size);
			}
		}

		mpg123_close(mh);
		mpg123_delete(mh);
		return new Audio(pcm16, pcm16Size);
	}

	Mp3Decoder& Mp3Decoder::instance()
	{
		static Mp3Decoder decoder;
		return decoder;
	}
}

#endif