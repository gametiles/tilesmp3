
#pragma once

#include "Config.h"

#if AUDIO_USE_MPG123

namespace nama
{
    class Audio;

	class Mp3Decoder
	{
	public:
		static Mp3Decoder& instance();
		void initialize();
		void shutdown();
        Audio* decode(void* bytes, int size, int sampleRate);
	};
}

#endif