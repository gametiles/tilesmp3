#include "AudioPool.h"
#include "AudioEngine.h"
#include "Audio.h"
#include "Log.h"
#include "Platform.h"

#if AUDIO_USE_MPG123
#include "Mp3Decoder.h"
#endif

#if AUDIO_USE_STB_VORBIS || AUDIO_USE_LIB_VORBIS
#include "VorbisDecoder.h"
#endif

#if AUDIO_USE_BUILTIN && defined(__ANDROID__)
#include "AudioDecoder.h"
#endif

#include "AudioUtil.h"

#include <cmath>
#include <vector>
#include <thread>
#include <cassert>

#if defined(__ANDROID__)
#include "Android.h"
#endif

namespace nama
{
    struct AudioInfo
    {
        int id, size;
        void* data;
    };

    static bool s_loading = false;
    static std::mutex s_lock;
    static std::condition_variable s_condition;
    static int s_nWaiting = 0;
    static uint8_t* s_packedAudios = nullptr;

    // Number of audios to load
    static int s_nAudios = 0;

#if AUDIO_USE_BUILTIN && defined(__ANDROID__)

	static Audio* loadAudioAsset(int id, const char* szAsset, AudioFormat format)
	{
		short* pcmData;
		int pcmSize = AudioDecoder::instance().decodeAsset(szAsset, &pcmData, format);

		if (!pcmSize)
		{
			return nullptr;
		}
		auto pAudio = new Audio(id, pcmData, pcmSize, false);

		LOG("Done loading %s", szAsset);
		return pAudio;
	}
#endif

#if AUDIO_LOAD_FROM_ASSETS && defined(__ANDROID__)
    static u_int8_t* loadAsset(const char* szAsset, int* size)
    {
        uint8_t* asset;
        std::string path(szAsset);
        int n = path.find_last_of("/\\");

        if (n != std::string::npos)
        {
            std::string filename = path.substr(n+1);
            std::string dirname = path.substr(0,n);
            asset = android::getAsset(filename.c_str(), dirname.c_str(), size);
        }
        else
        {
            asset = android::getAsset(szAsset, "", size);
        }
        return asset;
    }
#endif

#if (AUDIO_USE_STB_VORBIS || AUDIO_USE_LIB_VORBIS) && AUDIO_LOAD_FROM_ASSETS && defined(__ANDROID__)

	static Audio* loadOggAsset(int id, const char* szAsset)
	{
		int size;
		auto oggData = loadAsset(szAsset, &size);
		if (!oggData)
		{
			LOG("File not found %s", szAsset);
			return nullptr;
		}

        int sampleRate = AudioEngine::instance().sampleRate();
		auto pAudio = VorbisDecoder::instance().decode(oggData, size, sampleRate);
		android::freeAsset(oggData);

		LOG("Loaded file %s", szAsset);
		return pAudio;
	}
#endif

#if AUDIO_USE_MPG123
	static Audio* createMp3Audio(void* encoded, int size)
	{
        int sampleRate = AudioEngine::instance().sampleRate();
		return Mp3Decoder::instance().decode(encoded, size, sampleRate);
	}
#if AUDIO_LOAD_FROM_ASSETS && defined(__ANDROID__)
	static Audio* loadMp3Asset(const char* szAsset)
	{
		int size;
		auto data = loadAsset(szAsset, &size);
		if (!data)
		{
			LOG("File not found %s", szAsset);
			return nullptr;
		}

		auto pAudio = createMp3Audio(data, size);
		android::freeAsset(data);

		LOG("Loaded file %s", szAsset);
		return pAudio;
	}
#endif
#endif

	AudioPool::AudioPool()
	{
	}

	AudioPool &AudioPool::instance()
	{
		static AudioPool pool;
		return pool;
	}

	void AudioPool::initialize()
	{
	}

	AudioPool::~AudioPool()
	{
		shutdown();
	}

	void AudioPool::play(int iAudio, float volume)
	{
		auto pAudio = m_audios[iAudio];
		if (pAudio)
		{
			AudioEngine::instance().play(pAudio, volume);
		}
        else
        {
            LOG("audio %d not loaded", iAudio);
        }
	}

	void AudioPool::shutdown()
	{
		for (auto pair : m_audios)
		{
			delete pair.second;
		}
		m_audios.clear();
	}

	bool AudioPool::isLoaded() const
	{
		return m_loaded;
	}

	bool AudioPool::loadCache()
	{
#if AUDIO_POOL_CACHING
		char s[128];
		sprintf(s, "%s/audio.pool", android::externalDirectory());
		FILE* f = fopen(s, "rb");
		if (!f)
		{
			return false;
		}

        // Read version
        int version;
        int n = fread(&version, sizeof(int), 1, f);
        if (n < 1 || version != AUDIO_POOL_VERSION)
        {
            fclose(f);
            return false;
        }

        // Read number of audios
        int nAudios;
        n = fread(&nAudios, sizeof(int), 1, f);
        if (n < 1)
        {
            fclose(f);
            return false;
        }

		int id, size;
		for (;;)
		{
			n = fread(&id, sizeof(int), 1, f);
			if (n < 1)
			{
				break;
			}
			n = fread(&size, sizeof(int), 1, f);
			if (n < 1)
			{
				break;
			}

			void* data = malloc(size);
			if (!data)
			{
				break;
			}

			n = fread(data, 1, size, f);
			if (n != size)
			{
				free(data);
				break;
			}
			Audio* pAudio = new Audio(data, size);
			LOG("Loaded audio %d, size: %d", id, size);
			m_audios[id] = pAudio;
		}

		fclose(f);

		m_loaded = m_audios.size() != nAudios;

#if defined(__ANDROID__)
        android::onAudiosLoaded();
#endif
        if (m_onAudiosLoaded)
        {
            m_onAudiosLoaded();
        }
#else
        m_loaded = false;
#endif
		return m_loaded;
	}

    static int64_t s_time = 0;

	bool AudioPool::beginLoading()
	{
        s_time = platform::nowNs();
        // Destroy all audios
        shutdown();

#if AUDIO_USE_MPG123
		Mp3Decoder::instance().initialize();
#endif

#if AUDIO_USE_LIB_VORBIS || AUDIO_USE_STB_VORBIS
        VorbisDecoder::instance().initialize();
#endif

#if AUDIO_USE_BUILTIN && !AUDIO_USE_LIB_VORBIS && !AUDIO_USE_STB_VORBIS && !AUDIO_USE_MPG123
        AudioDecoder::instance().initialize();
#endif
		return true;
	}

	bool AudioPool::endLoading()
	{
#if defined(__ANDROID__)
        if (s_packedAudios != nullptr)
        {
            android::freeAsset(s_packedAudios);
            s_packedAudios = nullptr;
        }
#endif

#if AUDIO_USE_MPG123
        Mp3Decoder::instance().shutdown();
#endif

#if AUDIO_USE_LIB_VORBIS || AUDIO_USE_STB_VORBIS
        VorbisDecoder::instance().shutdown();
#endif

#if AUDIO_USE_BUILTIN && !AUDIO_USE_LIB_VORBIS && !AUDIO_USE_STB_VORBIS && !AUDIO_USE_MPG123
        AudioDecoder::instance().shutdown();
#endif

        m_loaded = true;

        auto timeElapsed = platform::nowNs() - s_time;
        LOG("Total loading time = %fms", timeElapsed/1000000.0f);

#if defined(__ANDROID__)
        android::onAudiosLoaded();
#endif
        if (m_onAudiosLoaded)
        {
            m_onAudiosLoaded();
        }

#if AUDIO_POOL_CACHING
        if (s_nAudios != m_audios.size())
        {
            return true;
        }

        // Write audio to cache in another thread
        char s[128];
        sprintf(s, "%s/"AUDIO_POOL_CACHE_FILE, android::externalDirectory());
        FILE* f = fopen(s, "wb");
        if (!f)
        {
            LOG("Audio - Initialization for audio caching failed");
            //return false;
        }

        // Write version
        int version = AUDIO_POOL_VERSION;
        int n = fwrite(&version, sizeof(int), 1, f);
        if (n < 1)
        {
            fclose(f);
            return true;
        }

        // Write number of audios
        int nAudios = m_audios.size();
        n = fwrite(&nAudios, sizeof(int), 1, f);
        if (n < 1)
        {
            fclose(f);
            return true;
        }

        for (auto pair : m_audios)
        {
            auto pAudio = pair.second;

            int size = pAudio->size();
            int id = pAudio->id();

            long k = ftell(f);
            bool ok = false;
            while (true)
            {
                n = fwrite(&id, sizeof(int), 1, f);
                if (n < 1)
                {
                    break;
                }

                n = fwrite(&size, sizeof(int), 1, f);
                if (n < 1)
                {
                    break;
                }

                n = fwrite(pAudio->data(), 1, size, f);
                if (n < size)
                {
                    LOG("Audio - Error save audio to cache %d! Expected size: %d, written size: %d", id, size, n);
                    break;
                }

                ok = true;
                break;
            }
            if (!ok)
            {
                LOG("Audio - Cannot cache audio %d", id);
                fseek(f, k, SEEK_SET);
            }
        }

        fclose(f);
#endif
		return true;
	}


	bool AudioPool::loadOgg(int id, void *data, int size)
	{
#if AUDIO_USE_STB_VORBIS || AUDIO_USE_LIB_VORBIS
        int sampleRate = AudioEngine::instance().sampleRate();
        auto pAudio = VorbisDecoder::instance().decode(data, size, sampleRate);
		return storeAudio(pAudio, id);
#else
        return false;
#endif
	}

	bool AudioPool::loadOgg(int id, const char *szAsset)
	{
#if (AUDIO_USE_STB_VORBIS || AUDIO_USE_LIB_VORBIS) && AUDIO_LOAD_FROM_ASSETS && defined(__ANDROID__)
		Audio* pAudio = loadOggAsset(id, szAsset);
#elif AUDIO_USE_BUILTIN && defined(__ANDROID__)
		Audio* pAudio = loadAudioAsset(id, szAsset, AudioFormat::OGG);
#else
		assert(false && "Not supported");
		Audio* pAudio = nullptr;
#endif
		return storeAudio(pAudio, id);
	}


	bool AudioPool::loadMp3(int id, void *data, int size)
	{
#if AUDIO_USE_MPG123
		Audio* pAudio = createMp3Audio(data, size);
		return storeAudio(pAudio, id);
#else
        return false;
#endif
	}


	bool AudioPool::loadMp3(int id, const char *szAsset)
	{
#if AUDIO_USE_MPG123 && AUDIO_LOAD_FROM_ASSETS && defined(__ANDROID__)
		Audio* pAudio = loadMp3Asset(szAsset);
#elif AUDIO_USE_BUILTIN && defined(__ANDROID__)
		Audio* pAudio = loadAudioAsset(id, szAsset, AudioFormat::MP3);
#else
		assert(false && "Not supported");
		Audio* pAudio = nullptr;
#endif
		return storeAudio(pAudio, id);
	}

    bool AudioPool::storeAudio(Audio *pAudio, int id)
    {
        if (!pAudio)
        {
            LOG("Audio - Cannot cache the null audio!");
            return false;
        }

        Audio*& pSlot = m_audios[id];
        if (pSlot)
        {
            LOG("Audio - Duplicate audio? Something went wrong!");
            delete pSlot;
        }
        pSlot = pAudio;
        pAudio->setId(id);

#if defined(__ANDROID__)
        android::onAudioLoaded();
#endif
        if (m_onAudioLoaded)
        {
            m_onAudioLoaded(m_audios.size());
        }
        return true;
    }

	bool AudioPool::loadPcm32(int id, float *data, int nChannels, int nSamples, int sampleRate)
	{
		LOG("Audio - Loading audio %d: %d channels, %d samples", id, nChannels, nSamples);
		auto pAudio = createAudio(data, nChannels, nSamples, sampleRate);
		return storeAudio(pAudio, id);
	}

    static void loadAudio(AudioInfo& audio, int type)
    {
        char s[128];
        switch (type)
        {
            case 1:
                AudioPool::instance().loadOgg(audio.id, audio.data, audio.size);
                break;

            default:
                AudioPool::instance().loadMp3(audio.id, audio.data, audio.size);
                break;
        }
    }

    static void loadAudiosThread(std::vector<AudioInfo>* audios, int type)
    {
        while(audios->size() > 0)
        {
            // Get the next id
            std::unique_lock<std::mutex> autoLock(s_lock);
            while (s_nWaiting > 0)
            {
                s_condition.wait(autoLock);
            }
            s_nWaiting ++;

            AudioInfo audio;
            if (audios->size() > 0)
            {
                audio = audios->back();
                audios->pop_back();
            }
            else
            {
                audio.id = -1;
            }

            s_nWaiting--;
            s_condition.notify_one();

            if (audio.id <= 0)
            {
                continue;
            }

            loadAudio(audio, type);
        }
    }

    static void loadAudiosMT(std::vector<AudioInfo>& audios, int type)
    {
        s_loading = true;
        AudioPool::instance().beginLoading();
        std::thread t1[16];
        int n = platform::cpuCount() - 2;
        LOG("Load audios with %d threads", n + 1);
        for (int i = 0; i < n; i++)
        {
            t1[i] = std::thread(loadAudiosThread, &audios, type);
        }
        loadAudiosThread(&audios, type);
        for (int i = 0; i < n; i++)
        {
            t1[i].join();
        }
        AudioPool::instance().endLoading();
        s_loading = false;
    }

    static void loadAudiosST(std::vector<AudioInfo>& audios, int type)
    {
        s_loading = true;
        AudioPool::instance().beginLoading();
        for (size_t i = 0; i < audios.size(); i++)
        {
            loadAudio(audios[i], type);
        }
        AudioPool::instance().endLoading();
        s_loading = false;
    }

    static void loadPackedAudios(void* data, int size, int type, bool multithread)
    {
        LOG("Loading packed audios");
        std::vector<AudioInfo> audios;
        audios.clear();
        audios.reserve(200);
        AudioInfo audio;
        uint8_t* p = reinterpret_cast<uint8_t*>(data);

        int n = 0;
        for (;;)
        {
            audio.id = *reinterpret_cast<int*>(p);
            p += 4;
            audio.size = *reinterpret_cast<int*>(p);
            p += 4;
            audio.data = p;
            audios.push_back(audio);
            p += audio.size;
            n ++;
            if (p >= reinterpret_cast<uint8_t*>(data) + size)
            {
                break;
            }
        }

        s_nAudios = n;

        if (multithread)
        {
            loadAudiosMT(audios, type);
        }
        else
        {
            loadAudiosST(audios, type);
        }
    }

    static void loadPackedAudiosInAsset(const char *szAsset, int type, bool multithread)
    {
        int assetSize;
        auto asset = loadAsset(szAsset, &assetSize);
        if (!asset)
        {
            s_loading = false;
            return;
        }

        s_packedAudios = asset;

        std::thread(loadPackedAudios, asset, assetSize, type, multithread).detach();
    }

    void AudioPool::loadPackedMp3s(const char *szAsset, bool multithread)
    {
        if (s_loading)
        {
            return;
        }
        s_loading = true;
        loadPackedAudiosInAsset(szAsset, 0, multithread);
    }

    void AudioPool::loadPackedMp3s(void *data, int size, bool multithread)
    {
        if (s_loading)
        {
            return;
        }
        s_loading = true;
        std::thread(loadPackedAudios, data, size, 0, multithread).detach();
    }

    void AudioPool::loadPackedOggs(const char *szAsset, bool multithread)
    {
        if (s_loading)
        {
            return;
        }
        s_loading = true;
        loadPackedAudiosInAsset(szAsset, 1, multithread);
    }

    void AudioPool::loadPackedOggs(void *data, int size, bool multithread)
    {
        if (s_loading)
        {
            return;
        }
        s_loading = true;
        std::thread(loadPackedAudios, data, size, 1, multithread).detach();
    }
}
