
#include "VorbisDecoder.h"
#include "Audio.h"
#include "AudioUtil.h"
#include "Log.h"

#if AUDIO_USE_STB_VORBIS

#include "stb_vorbis/stb_vorbis.h"
#include <cstdlib>

namespace nama
{
    VorbisDecoder &VorbisDecoder::instance()
    {
        static VorbisDecoder decoder;
        return decoder;
    }

    void VorbisDecoder::initialize()
    {

    }

    void VorbisDecoder::shutdown()
    {

    }

    Audio *nama::VorbisDecoder::decode(void *bytes, int size, int sampleRate)
    {
        int nChannels, nSamples, error;
        short* pcmData;

        auto pStream = stb_vorbis_open_memory((unsigned char*)bytes, size, &error, nullptr);
        if (!pStream)
        {
            return nullptr;
        }

        // Get the music parameters
        stb_vorbis_info info = stb_vorbis_get_info(pStream);
        nChannels = info.channels;
        nSamples = stb_vorbis_stream_length_in_samples(pStream);

        int nValues = nSamples * nChannels;
        int pcm16Size;
        short* pcm16;

        if (sampleRate == info.sample_rate)
        {
            pcm16Size = nValues * sizeof(short);
            pcm16 = (short*)malloc(pcm16Size);
            if (!pcm16)
            {
                LOG("Cannot allocate memory");
                stb_vorbis_close(pStream);
                return nullptr;
            }
            stb_vorbis_get_samples_short_interleaved(pStream, nChannels, pcm16, nValues);
        }
        else
        {
            float* pcm32 = (float*)malloc(nChannels * nSamples * sizeof(float));
            if (!pcm32)
            {
                LOG("Cannot allocate memory");
                stb_vorbis_close(pStream);
                return nullptr;
            }
            stb_vorbis_get_samples_float_interleaved(pStream, nChannels, pcm32, nValues);
            stb_vorbis_close(pStream);

            float* pcm32r = resampleAudio(
                    pcm32,
                    nChannels,
                    nSamples,
                    info.sample_rate,
                    sampleRate,
                    &nSamples);

            pcm16 = convertAudio(pcm32r, nChannels, nSamples);
            pcm16Size = nSamples * nChannels * sizeof(short);
            free(pcm32);
            free(pcm32r);
        }

        return new Audio(pcm16, pcm16Size);;
    }
}


#elif AUDIO_USE_LIB_VORBIS

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <cmath>

#include <vorbis/codec.h>

namespace nama
{
	enum {
		INPUT_BUFFER_SIZE = 100 * 1024, // every our audio file is less than 100KB
		OUTPUT_BUFFER_SIZE = 1024 * 1024 * 1024, // and they are less than 1MB when decoded
	};
	class VorbisDecoderInternal : public VorbisDecoder
	{
	public:
		VorbisDecoderInternal();
		~VorbisDecoderInternal();
		void initialize() override;
		void shutdown() override;
		Audio* decode(void* bytes, int size, int sampleRate) override;

		void prepareInputBuffer(int minSize);
		void reallocateOutputBuffer(int newSize);

		ogg_sync_state oy; /* sync and verify incoming physical bitstream */
		char* oggBuffer = nullptr;
		int oggBufferSize = 0;

		char* pcmBuffer = nullptr;
		int pcmBufferSize = 0;
	};

	VorbisDecoderInternal::VorbisDecoderInternal()
	{
		prepareInputBuffer(INPUT_BUFFER_SIZE);
		pcmBuffer = (char*)malloc(OUTPUT_BUFFER_SIZE);
		pcmBufferSize = OUTPUT_BUFFER_SIZE;
	}

	VorbisDecoderInternal::~VorbisDecoderInternal()
	{
		shutdown();
	}

	void VorbisDecoderInternal::initialize()
	{
		ogg_sync_init(&oy); /* Now we can read pages */
	}

	void VorbisDecoderInternal::shutdown()
	{
		if (pcmBuffer)
		{
			free(pcmBuffer);
			pcmBuffer = nullptr;
		}

		/* OK, clean up the framer */
		ogg_sync_clear(&oy);
	}

	int VorbisDecoderInternal::decode(
			uint8_t* bytes, int size, int* nChannels, int* sampleRate, short** output)
	{
		ogg_stream_state os; /* take physical pages, weld into a logical stream of packets */
		ogg_page         og; /* one Ogg bitstream page. Vorbis packets are inside */
		ogg_packet       op; /* one raw packet of data for decode */

		vorbis_info      vi; /* struct that stores all the static vorbis bitstream
						 settings */
		vorbis_comment   vc; /* struct that stores all the bitstream user comments */
		vorbis_dsp_state vd; /* central working state for the packet->PCM decoder */
		vorbis_block     vb; /* local working space for packet->PCM decode */

		prepareInputBuffer(size);
		int nSamples = 0;

		/********** Decode setup ************/

		/* we repeat if the bitstream is chained */
		int eos = 0;
		int i;

		/* grab some data at the head of the stream. We want the first page
		(which is guaranteed to be small and only contain the Vorbis
		stream initial header) We need the first page to get the stream
		serialno. */

		/* Give all data to libvorbis' Ogg layer */
		memcpy(oggBuffer, bytes, size);
		ogg_sync_wrote(&oy, size);

		/* Get the first page. */
		if (ogg_sync_pageout(&oy, &og) != 1)
		{
			/* error case.  Must not be Vorbis data */
			assert(false && "Input does not appear to be an Ogg bitstream.\n");
		}

		/* Get the serial number and set up the rest of decode. */
		/* serialno first; use it to set up a logical stream */
		ogg_stream_init(&os, ogg_page_serialno(&og));

		/* extract the initial header from the first page and verify that the
		Ogg bitstream is in fact Vorbis data */

		/* I handle the initial header first instead of just having the code
		read all three Vorbis headers at once because reading the initial
		header is an easy way to identify a Vorbis bitstream and it's
		useful to see that functionality seperated out. */

		vorbis_info_init(&vi);
		vorbis_comment_init(&vc);
		if (ogg_stream_pagein(&os, &og) < 0)
		{
			/* error; stream version mismatch perhaps */
			assert(false && "Error reading first page of Ogg bitstream data.\n");
		}

		if (ogg_stream_packetout(&os, &op) != 1)
		{
			/* no page? must not be vorbis */
			assert(false && "Error reading initial header packet.\n");
		}

		if (vorbis_synthesis_headerin(&vi, &vc, &op) < 0) {
			/* error case; not a vorbis header */
			assert(false && "This Ogg bitstream does not contain Vorbis audio data.\n");
		}

		/* At this point, we're sure we're Vorbis. We've set up the logical
		(Ogg) bitstream decoder. Get the comment and codebook headers and
		set up the Vorbis decoder */

		/* The next two packets in order are the comment and codebook headers.
		They're likely large and may span multiple pages. Thus we read
		and submit data until we get our two packets, watching that no
		pages are missing. If a page is missing, error out; losing a
		header page is the only place where missing data is fatal. */

		i = 0;
		while (i < 2)
		{
			while (i < 2)
			{
				int result = ogg_sync_pageout(&oy, &og);
				if (result == 0)break; /* Need more data */
				/* Don't complain about missing or corrupt data yet. We'll
				catch it at the packet output phase */
				if (result == 1)
				{
					ogg_stream_pagein(&os, &og); /* we can ignore any errors here
												 as they'll also become apparent
												 at packetout */
					while (i < 2)
					{
						result = ogg_stream_packetout(&os, &op);
						if (result == 0)break;
						if (result < 0)
						{
							/* Uh oh; data at some point was corrupted or missing!
							We can't tolerate that in a header.  Die. */
							assert(false && "Corrupt secondary header.  Exiting.\n");
						}
						result = vorbis_synthesis_headerin(&vi, &vc, &op);
						if (result < 0)
						{
							assert(false && "Corrupt secondary header.  Exiting.\n");
						}
						i++;
					}
				}
			}
		}

		*nChannels = vi.channels;
		int convsize = pcmBufferSize / vi.channels;

		/* OK, got and parsed all three headers. Initialize the Vorbis
		packet->PCM decoder. */
		if (vorbis_synthesis_init(&vd, &vi) != 0)
		{
			assert(false && "Error: Corrupt header during playback initialization.\n");
		}
		/* central decode state */

		// local state for most of the decode so multiple block decodes can proceed in parallel.
		// We could init multiple vorbis_block structures for vd here
		vorbis_block_init(&vd, &vb);

		ogg_int16_t* buffer = (ogg_int16_t*)pcmBuffer;

		/* The rest is just a straight decode loop until end of stream */
		while (!eos)
		{
			while (!eos)
			{
				int result = ogg_sync_pageout(&oy, &og);
				if (result == 0)
				{ /* need more data */
					break;
				}
				if (result < 0)
				{ /* missing or corrupt data at this page position */
					assert(false && "Corrupt or missing data in bitstream; continuing...\n");
				}
				else
				{
					ogg_stream_pagein(&os, &og); /* can safely ignore errors at this point */
					while (1)
					{
						result = ogg_stream_packetout(&os, &op);

						if (result == 0)break; /* need more data */
						if (result < 0)
						{ /* missing or corrupt data at this page position */
							/* no reason to complain; already complained above */
						}
						else
						{
							/* we have a packet.  Decode it */
							float **pcm;
							int samples;

							if (vorbis_synthesis(&vb, &op) == 0) /* test for success! */
								vorbis_synthesis_blockin(&vd, &vb);
							/*

							**pcm is a multichannel float vector.  In stereo, for
							example, pcm[0] is left, and pcm[1] is right.  samples is
							the size of each channel.  Convert the float values
							(-1.<=range<=1.) to whatever PCM format and write it out */

							while ((samples = vorbis_synthesis_pcmout(&vd, &pcm)) > 0)
							{
								int j;
								int clipflag = 0;

								/* convert floats to 16 bit signed ints (host order) and
								interleave */
								for (i = 0; i < vi.channels; i++)
								{
									ogg_int16_t *ptr = buffer + i;
									float  *mono = pcm[i];
									for (j = 0; j < samples; j++) {
#if 1
										int val = floor(mono[j] * 32767.f + .5f);
#else /* optional dither */
										int val = mono[j] * 32767.f + drand48() - 0.5f;
#endif
										/* might as well guard against clipping */
										if (val > 32767) {
											val = 32767;
											clipflag = 1;
										}
										if (val < -32768) {
											val = -32768;
											clipflag = 1;
										}
										*ptr = val;
										ptr += vi.channels;
									}
								}

								if (clipflag)
								{
									//fprintf(stderr, "Clipping in frame %ld\n", (long)(vd.sequence));
								}

								nSamples += samples;
								buffer += vi.channels * samples;

								/* tell libvorbis how many samples we actually consumed */
								vorbis_synthesis_read(&vd, samples);
							}
						}
					}
					if (ogg_page_eos(&og))eos = 1;
				}
			}
		}

		/* ogg_page and ogg_packet structs always point to storage in
		libvorbis.  They're never freed or manipulated directly */

		vorbis_block_clear(&vb);
		vorbis_dsp_clear(&vd);

		/* clean up this logical bitstream; before exit we see if we're
		followed by another [chained] */

		ogg_stream_clear(&os);
		vorbis_comment_clear(&vc);
		vorbis_info_clear(&vi);  /* must be called last */

		int pcmSize = nSamples * (*nChannels) * sizeof(short);
		*output = (short*)malloc(pcmSize);
		memcpy(*output, pcmBuffer, pcmSize);
		return nSamples;
	}

	void VorbisDecoderInternal::prepareInputBuffer(int minSize)
	{
		if (oggBufferSize < minSize)
		{
			oggBufferSize = INPUT_BUFFER_SIZE;
		}
		oggBuffer = ogg_sync_buffer(&oy, oggBufferSize);
	}

	void VorbisDecoderInternal::reallocateOutputBuffer(int newSize)
	{
		if (newSize > oggBufferSize)
		{
			pcmBuffer = (char*)realloc(pcmBuffer, newSize);
			pcmBufferSize = newSize;
		}
	}
}

#endif
