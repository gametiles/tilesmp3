package com.nama.audiotest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

	static MainActivity m_activity;
	SeekBar seekBar = null;

	boolean buttonsCreated = false;
	float volume = 1.0f;
	long beginTime = 0;
	int nLoaded = 0;
	TextView timeText = null;

	public static MainActivity currentActivity() { return m_activity; }

	void loadMp3(boolean multithread)
	{
		nLoaded = 0;
		beginTime = System.currentTimeMillis();
		loadMp3s(multithread);
	}

	void loadOgg(boolean multithread)
	{
		nLoaded = 0;
		beginTime = System.currentTimeMillis();
		loadOggs(multithread);
	}

	void loadCache()
	{
		beginTime = System.currentTimeMillis();
		loadAudioCache();
	}

	void onAudioLoaded()
	{
		nLoaded++;
		if (seekBar != null)
		{
			seekBar.setProgress((nLoaded * 100) / audioCount());
		}
	}

	void onAudiosLoaded()
	{
		if (timeText != null)
		{
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					long timeElapsed = (System.currentTimeMillis() - beginTime);
					timeText.setText("Done in " + timeElapsed + " ms");
					addButtons();
				}
			});
		}
	}

	void addButtons()
	{
		if (buttonsCreated)
		{
			return;
		}
		buttonsCreated = true;

		// Create a LinearLayout element
		GridLayoutExV grid = (GridLayoutExV) findViewById(R.id.spotsView);
		grid.removeAllViews();

		// Add Buttons
		for (int i = 0; i < audioCount(); i++)
		{
			int id = audioId(i);
			Button button = new Button(m_activity);
			button.setText("" + id);
			button.setTag(id);
			button.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					playAudio((int) v.getTag(), volume);
				}
			});
			grid.addView(button);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		m_activity = this;

		setContentView(R.layout.activity_main);

		System.loadLibrary("nama");
		initAudioEngine();

		timeText = ((TextView)findViewById(R.id.timeText));

		findViewById(R.id.buttonLoadMp3).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				loadMp3(false);
			}
		});
		findViewById(R.id.buttonLoadOgg).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				loadOgg(false);
			}
		});
		findViewById(R.id.buttonLoadMp3Mt).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				loadMp3(true);
			}
		});
		findViewById(R.id.buttonLoadOggMt).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				loadOgg(true);
			}
		});
		findViewById(R.id.buttonLoadCache).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				loadCache();
			}
		});

		seekBar = (SeekBar)findViewById(R.id.volumeBar);
		volume = (float)seekBar.getProgress() / seekBar.getMax();
		seekBar.setOnSeekBarChangeListener(
				new SeekBar.OnSeekBarChangeListener(){
					@Override
					public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
						volume = (float)progressValue / seekBar.getMax();
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar)
					{

					}

					@Override
					public void onStopTrackingTouch(SeekBar seekBar)
					{

					}
				}
		);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		pause();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		resume();
	}

	/**
	 * A native method that is implemented by the 'native-lib' native library,
	 * which is packaged with this application.
	 */
	public native void playAudio(int id, float volume);

	public native void initAudioEngine();
	public native boolean loadAudioCache();
	public native void pause();
	public native void resume();
	public native void loadMp3s(boolean multithread);
	public native void loadOggs(boolean multithread);
	public native int audioCount();
	public native int audioId(int index);
}
