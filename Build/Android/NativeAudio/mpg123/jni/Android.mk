LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_MODULE     := mpg123
LOCAL_ARM_MODE   := arm

# common
LOCAL_CFLAGS := -Wno-int-to-pointer-cast \
				-Wno-pointer-to-int-cast -ffast-math \
				-O3 \
				-DHAVE_CONFIG_H \
				-DACCURATE_ROUNDING \
				-DNO_STRING \
				-DNO_EQUALIZER

LOCAL_SRC_FILES +=  libmpg123.c
LOCAL_SRC_FILES +=  compat.c
LOCAL_SRC_FILES +=  frame.c
LOCAL_SRC_FILES +=  format.c
LOCAL_SRC_FILES +=  stringbuf.c
LOCAL_SRC_FILES +=  readers.c
LOCAL_SRC_FILES +=  index.c
LOCAL_SRC_FILES +=  layer3.c
LOCAL_SRC_FILES +=  parse.c
LOCAL_SRC_FILES +=  optimize.c
LOCAL_SRC_FILES +=  synth.c
LOCAL_SRC_FILES +=  ntom.c
LOCAL_SRC_FILES +=  dct64.c
LOCAL_SRC_FILES +=  equalizer.c
LOCAL_SRC_FILES +=  tabinit.c
LOCAL_SRC_FILES +=  feature.c
LOCAL_SRC_FILES +=  id3.c

#enable_misc_features := 1
enable_icy := 1
enable_32bit := 1
enable_real := 1

# layer 1, layer 2, 8 bit, id3
ifdef enable_misc_features
LOCAL_SRC_FILES +=  layer1.c
LOCAL_SRC_FILES +=  layer2.c
LOCAL_SRC_FILES +=  synth_8bit.c
else
LOCAL_CFLAGS +=  -DNO_LAYER1 -DNO_LAYER2 -DNO_8BIT -DNO_ID3V2
endif

#icy
ifdef enable_icy
LOCAL_SRC_FILES +=  icy.c icy2utf8.c
else
LOCAL_CFLAGS += -DNO_ICY
endif

#32 bit
ifdef enable_32bit
LOCAL_SRC_FILES +=  synth_s32.c
else
LOCAL_CFLAGS +=  -DNO_32BIT
endif

#real
ifdef enable_real
LOCAL_SRC_FILES += synth_real.c
LOCAL_CFLAGS +=  -DREAL_IS_FLOAT
else
LOCAL_CFLAGS +=  -DNO_REAL -DREAL_IS_FIXED
endif


ifeq ($(TARGET_ARCH_ABI),armeabi)
LOCAL_CFLAGS    +=  -DOPT_ARM -DASMALIGN_BYTE -DASMALIGN_EXP
LOCAL_SRC_FILES +=  synth_arm.S synth_arm_accurate.S
endif

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_CFLAGS    +=  -DASMALIGN_EXP -DASMALIGN_BYTE
LOCAL_SRC_FILES +=  synth_arm.S synth_arm_accurate.S getcpuflags_arm.c

enable_neon := 1
ifdef enable_neon
	LOCAL_CFLAGS    +=  -DOPT_NEON -mfpu=neon
	LOCAL_SRC_FILES +=  synth_neon.S synth_neon_accurate.S check_neon.S
	LOCAL_SRC_FILES +=  dct36_neon.S synth_stereo_neon_accurate.S

	ifdef enable_32bit
		LOCAL_SRC_FILES +=  synth_neon_s32.S synth_stereo_neon_s32.S
	endif

	ifdef enable_real
		LOCAL_SRC_FILES +=  synth_neon_float.S
		LOCAL_SRC_FILES +=  dct64_neon_float.S synth_stereo_neon_float.S
	endif
else
	LOCAL_CFLAGS    +=  -DOPT_ARM
endif

endif

ifeq ($(TARGET_ARCH_ABI),x86)
LOCAL_CFLAGS    +=  -DABI_ALIGN_FUN -DCCALIGN -DASMALIGN_BALIGN

enable_sse := 1

ifdef enable_sse
	LOCAL_CFLAGS    +=  -DOPT_SSE
	LOCAL_SRC_FILES +=  synth_sse.S synth_sse_accurate.S
	LOCAL_SRC_FILES +=  synth_stereo_sse_accurate.S
	LOCAL_SRC_FILES +=  dct36_sse.S dct64_sse.S
	LOCAL_SRC_FILES +=  tabinit_mmx.S

	ifdef enable_32bit
	LOCAL_SRC_FILES +=  synth_sse_s32.S
	LOCAL_SRC_FILES +=  synth_stereo_sse_s32.S
	endif

	ifdef enable_real
	LOCAL_SRC_FILES +=  synth_sse_float.S
	LOCAL_SRC_FILES +=  synth_stereo_sse_float.S
	LOCAL_SRC_FILES +=  dct64_sse_float.S
	endif
else
	LOCAL_CFLAGS    +=  -DOPT_GENERIC
endif

LOCAL_SRC_FILES +=  dct64_i386.c
endif

ifeq ($(TARGET_ARCH_ABI),x86_64)
LOCAL_CFLAGS    +=  -DOPT_X86_64 -DCCALIGN
LOCAL_SRC_FILES +=  getcpuflags_x86_64.S
LOCAL_SRC_FILES +=  synth_x86_64.S synth_x86_64_accurate.S
LOCAL_SRC_FILES +=  synth_stereo_x86_64.S synth_stereo_x86_64_accurate.S
LOCAL_SRC_FILES +=  dct36_x86_64.S dct64_x86_64.S

ifdef enable_32bit
LOCAL_SRC_FILES +=  synth_x86_64_s32.S synth_stereo_x86_64_s32.S
endif

ifdef enable_real
LOCAL_SRC_FILES +=  synth_x86_64_float.S
LOCAL_SRC_FILES +=  synth_stereo_x86_64_float.S
LOCAL_SRC_FILES +=  dct64_x86_64_float.S
endif

endif

include $(BUILD_STATIC_LIBRARY)
