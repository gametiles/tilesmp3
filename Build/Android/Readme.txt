	I. Quick build: Android Studio is not required (recommended for debugging build)

1. Build directly from Unity using Gradle method
2. Get the apk and cheers

- Note1: Don't use Internal method, it won't work (thanks to the new Facebook SDK)
- Note2: Using method (I) will automatically add READ_PHONE_STATE permission. So method (II)
bellow is recommended

	II. Quick build: Android Studio is not required (recommended for release build)

1. Export Android Project here using Gradle method in Unity
2. Run build.bat to build
2. Get the apk and cheers

	II. Alternative build: Android Studio is required (recommended for advanced customizations)

1. Export Android Project here using Gradle method in Unity
2. Copy everything in Config folder into the folder "Piano Challenges 2" (overwrite everything)
3. Open "Piano Challenges 2" in Android studio
4. Generate signed APK in Android Studio
5. Screw your head before getting the apk
