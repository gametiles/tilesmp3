
-- This script is used to put multiple audio files into a single file

local dir = arg[1]
local command = 'dir "' .. dir  .. '" /b /a:-d' .. ' | findstr /e ".mp3"'
print(command)

local audioFile = assert(io.open("notes.audio", "wb"))

function int2Bytes(x)
	local b4=x%256  x=(x-x%256)/256
	local b3=x%256  x=(x-x%256)/256
	local b2=x%256  x=(x-x%256)/256
	local b1=x%256  x=(x-x%256)/256
	return string.char(b4,b3,b2,b1)
end

function save( fileName )
	local name = fileName:match('(.+)%..+')
	local id = tonumber(name:match('%d+'))
	if id == nil then
		return
	end

	local file = assert(io.open(dir .. '\\' .. fileName, "rb"))
	local size = file:seek("end")
	file:seek("set")
	local data = file:read("*all")

	print('id = ' .. id .. ', size = ' .. size)
	audioFile:write(int2Bytes(id))
	audioFile:write(int2Bytes(size))
	audioFile:write(data)
end

local fileList = io.popen(command)
if fileList then

	local i = 0
	for fileName in fileList:lines() do
		save(fileName)
	end

	fileList:close()
else
	print("failed to read")
end

assert(audioFile:close())
