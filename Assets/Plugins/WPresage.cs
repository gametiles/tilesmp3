﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Collections.Generic;

/**
 * This version of Presage's Wrapper was tested with 2.0.2 Presage lib
 */
public class WPresage
{

    private const string WPRESAGE_ID = "io.presage.Presage";
    private const string WHANDLER_ID = "io.presage.IADHandler";
    private const string WEULA_ID = "io.presage.IEulaHandler";

    // Method used to Initialize Presage SDK
    public static void Initialize()
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
        {
            AndroidJavaClass presageClass = new AndroidJavaClass(WPRESAGE_ID);
            AndroidJavaObject presage = presageClass.CallStatic<AndroidJavaObject>("getInstance");
            presage.Call("setContext", activity);
            presage.Call("start");
        }));
    }

    // Method used to Initialize Presage SDK with apikey
    public static void Initialize(String apiKey)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
        {
            AndroidJavaClass presageClass = new AndroidJavaClass(WPRESAGE_ID);
            AndroidJavaObject presage = presageClass.CallStatic<AndroidJavaObject>("getInstance");
            presage.Call("setContext", activity);
            presage.Call("start", apiKey);
        }));
    }

    /**
	 * Method used to Call AdToServe to show interstitial
	 */
    public static void AdToServe(IADHandler handler)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
        {
            AndroidJavaClass presageClass = new AndroidJavaClass(WPRESAGE_ID);
            AndroidJavaObject presage = presageClass.CallStatic<AndroidJavaObject>("getInstance");
            IADHandlerProxy proxy = new IADHandlerProxy(handler);
            presage.Call("adToServe", proxy);
        }));
    }

    public static void Load(IADHandler handler)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
        {
            AndroidJavaClass presageClass = new AndroidJavaClass(WPRESAGE_ID);
            AndroidJavaObject presage = presageClass.CallStatic<AndroidJavaObject>("getInstance");
            IADHandlerProxy proxy = new IADHandlerProxy(handler);
            presage.Call("load", proxy);
        }));
    }

    public static void Load(int adsToPrecache, IADHandler handler)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
        {
            AndroidJavaClass presageClass = new AndroidJavaClass(WPRESAGE_ID);
            AndroidJavaObject presage = presageClass.CallStatic<AndroidJavaObject>("getInstance");
            IADHandlerProxy proxy = new IADHandlerProxy(handler);
            presage.Call("load", adsToPrecache, proxy);
        }));
    }

    public static bool CanShow()
    {
        AndroidJavaClass presageClass = new AndroidJavaClass(WPRESAGE_ID);
        AndroidJavaObject presage = presageClass.CallStatic<AndroidJavaObject>("getInstance");
        return presage.Call<bool>("canShow");
    }

    public static void Show(IADHandler handler)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
        {
            AndroidJavaClass presageClass = new AndroidJavaClass(WPRESAGE_ID);
            AndroidJavaObject presage = presageClass.CallStatic<AndroidJavaObject>("getInstance");
            IADHandlerProxy proxy = new IADHandlerProxy(handler);
            presage.Call("show", proxy);
        }));
    }

    public static void LaunchWithEula(IEulaHandler handler)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
        {
            AndroidJavaClass presageClass = new AndroidJavaClass(WPRESAGE_ID);
            AndroidJavaObject presage = presageClass.CallStatic<AndroidJavaObject>("getInstance");
            IEulaHandlerProxy proxy = new IEulaHandlerProxy(handler);
            presage.Call("launchWithEula", proxy);
        }));
    }
    /**
	 * Proxy to interface with the IADHandler of the jar
	 */
    public class IADHandlerProxy : AndroidJavaProxy
    {

        delegate void AdNotAvailable();
        delegate void AdAvailable();
        delegate void AdLoaded();
        delegate void AdClosed();

        delegate void AdError(int code);
        delegate void AdDisplayed();

        AdNotAvailable adNotAvailable;
        AdAvailable adAvailable;
        AdLoaded adLoaded;
        AdClosed adClosed;

        AdError adError;
        AdDisplayed adDisplayed;

        public IADHandlerProxy(IADHandler handler) : base(WHANDLER_ID)
        {
            adNotAvailable = handler.OnAdNotAvailable;
            adAvailable = handler.OnAdAvailable;
            adLoaded = handler.OnAdLoaded;
            adClosed = handler.OnAdClosed;
            adError = handler.OnAdError;
            adDisplayed = handler.OnAdDisplayed;
        }

        void onAdNotAvailable()
        {
            // Ad wasn't Available
            adNotAvailable();
        }

        void onAdAvailable()
        {
            // Ad was Available
            adAvailable();
        }

        void onAdLoaded()
        {
            // ads was loaded
            adLoaded();
        }

        void onAdClosed()
        {
            // Ad was closed
            adClosed();
        }

        void onAdError(int code)
        {
            // Ad sent an error
            adError(code);
        }

        void onAdDisplayed()
        {
            // Ad was displayed
            adDisplayed();
        }
    }

    public class IEulaHandlerProxy : AndroidJavaProxy
    {

        delegate void EulaFound();
        delegate void EulaNotFound();
        delegate void EulaClosed();

        EulaFound eulaFound;
        EulaNotFound eulaNotFound;
        EulaClosed eulaClosed;

        public IEulaHandlerProxy(IEulaHandler handler) : base(WEULA_ID)
        {
            eulaFound = handler.OnEulaFound;
            eulaNotFound = handler.OnEulaNotFound;
            eulaClosed = handler.OnEulaClosed;
        }

        void onEulaFound()
        {
            eulaFound();
        }
        void onEulaNotFound()
        {
            eulaNotFound();
        }
        void onEulaClosed()
        {
            eulaClosed();
        }
    }

    /**
	 * Interface to be implemented by the app
	 */
    public interface IADHandler
    {
        void OnAdNotAvailable();
        void OnAdAvailable();
        void OnAdLoaded();
        void OnAdClosed();
        void OnAdError(int code);
        void OnAdDisplayed();
    }

    public interface IEulaHandler
    {
        void OnEulaFound();
        void OnEulaNotFound();
        void OnEulaClosed();
    }
}