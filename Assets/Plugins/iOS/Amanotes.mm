//
//  ViewController.m
//
//
//  Created by Nikunj on 14/09/15.
//
//

@implementation ViewController : UIViewController
    
-(void) shareOnlyTextMethod: (const char *) shareMessage
    {
        
        NSString *message   = [NSString stringWithUTF8String:shareMessage];
        NSArray *postItems  = @[message];
        
        UIActivityViewController *activityVc = [[UIActivityViewController alloc] initWithActivityItems:postItems applicationActivities:nil];
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad &&  [activityVc respondsToSelector:@selector(popoverPresentationController)] ) {
            
            UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVc];
            
            [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)
                                   inView:[UIApplication sharedApplication].keyWindow.rootViewController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:activityVc animated:YES completion:nil];
    }
    
    @end




extern "C"{
    void iOS_ShareText(const char * message){
        ViewController *vc = [[ViewController alloc] init];
        [vc shareOnlyTextMethod: message];
    }
    
    void iOS_CopyTextToPasteBoard(const char * text, const char * message)
    {
        [UIPasteboard generalPasteboard].string = [NSString stringWithUTF8String:text];
        
        UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
                                                        message:[NSString stringWithUTF8String:message]
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:nil, nil];
        [toast show];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC),
                       dispatch_get_main_queue(),
                       ^{
                           [toast dismissWithClickedButtonIndex:0 animated:YES];
                       });
    }
}
