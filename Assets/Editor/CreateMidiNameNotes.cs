﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;


public class CreateMidiNameNotes : MonoBehaviour {
	[MenuItem("Generate Notes/CreateNameNotes")]
	public static void CreateNameNotes ()
	{
		// Try to find an existing file in the project called "UnityConstants.cs"
		string filePath = string.Empty;
		foreach (var file in Directory.GetFiles(Application.dataPath, "*.cs", SearchOption.AllDirectories)) {
			if (Path.GetFileNameWithoutExtension (file) == "MidiNotesName") {
				filePath = file;
				break;
			}
		}

		// If no such file exists already, use the save panel to get a folder in which the file will be placed.
		if (string.IsNullOrEmpty (filePath)) {
			string directory = EditorUtility.OpenFolderPanel ("Choose location for file MidiNotesName.cs", Application.dataPath, "");

			// Canceled choose? Do nothing.
			if (string.IsNullOrEmpty (directory)) {
				return;
			}
			filePath = Path.Combine (directory, "MidiNotesName.cs");
		}

			
			using (var writer = new StreamWriter (filePath)) {
				writer.WriteLine("using System.Collections.Generic;");

				writer.WriteLine ("public static class MidiNotesName{");
				writer.WriteLine ("public static List<string> listNoteName = new List<string> (){");
				Object[] listObj = Resources.LoadAll ("PianoMidiKeys");
				for (int i =0;i< listObj.Length;i++) {
					if(i < listObj.Length -1)
						writer.WriteLine ("\""+listObj[i].name+"\",");
					else
						writer.WriteLine ("\""+listObj[i].name+"\"");
				}
				writer.WriteLine ("};");
				writer.WriteLine ();
				writer.WriteLine ("}");
				writer.WriteLine ();


			}
			AssetDatabase.Refresh();

			Debug.Log("MidiNotesName.cs successfully generated");

	}
}
