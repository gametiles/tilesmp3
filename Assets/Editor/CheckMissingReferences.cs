// Based on http://www.tallior.com/find-missing-references-unity/
// It fixes deprecations and checks for missing references every time a new scene is loaded
// Moreover, it inspects missing references in animators and animation frames

using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Linq;
using System.Collections.Generic;

[InitializeOnLoad]
public static class LatestScenes
{
	static string currentScene;
	static LatestScenes()
	{
		EditorApplication.hierarchyWindowChanged += hierarchyWindowChanged;
	}
	static void hierarchyWindowChanged()
	{
		if (currentScene != UnityEngine.SceneManagement.SceneManager.GetActiveScene().name)
		{
			CheckMissingReferences.FindMissingReferencesInCurrentScene();
			currentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
		}
	}
}

public static class CheckMissingReferences
{
	private static void AddObjectAndItsChildren(GameObject gameObject, List<GameObject> list)
	{
		list.Add(gameObject);

		for (int i = 0; i < gameObject.transform.childCount; i++)
		{
			AddObjectAndItsChildren(gameObject.transform.GetChild(i).gameObject, list);
		}
	}

	public static GameObject[] GetSceneObjects()
	{
		List<GameObject> results = new List<GameObject>();
		var scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
		if (scene.isLoaded)
		{
			var allGameObjects = scene.GetRootGameObjects();
			foreach (GameObject gameObject in scene.GetRootGameObjects())
			{
				AddObjectAndItsChildren(gameObject, results);
			}
		}

		return results.ToArray();
	}

	[MenuItem("Tools/Show Missing Object References in all scenes", false, 51)]
	public static void MissingSpritesInAllScenes()
	{
		foreach (var scene in EditorBuildSettings.scenes.Where(s => s.enabled))
		{
			EditorSceneManager.OpenScene(scene.path);
			FindMissingReferencesInCurrentScene();
		}
	}

	[MenuItem("Tools/Show Missing Object References in scene", false, 50)]
	public static void FindMissingReferencesInCurrentScene()
	{
		var objects = GetSceneObjects();
		FindMissingReferences(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name, objects);
	}

	[MenuItem("Tools/Show Missing Object References in assets", false, 52)]
	public static void MissingSpritesInAssets()
	{
		var allAssets = AssetDatabase.GetAllAssetPaths();
		var objs = allAssets.Select(a => AssetDatabase.LoadAssetAtPath(a, typeof(GameObject)) as GameObject).Where(a => a != null).ToArray();

		FindMissingReferences("Project", objs);
	}

	public static void FindMissingReferences(string sceneName, GameObject[] objects)
	{
		foreach (var go in objects)
		{
			var components = go.GetComponents<Component>();

			foreach (var c in components)
			{
				try
				{
					var so = new SerializedObject(c);
					var sp = so.GetIterator();

					while (sp.Next(true))
					{
						if (sp.propertyType == SerializedPropertyType.ObjectReference && !sp.name.Equals("m_PrefabParentObject"))
						{
							if (sp.objectReferenceValue == null && sp.objectReferenceInstanceIDValue != 0)
							{
								ShowError(FullObjectPath(go), sp.name, sceneName);
							}
						}
					}
					var animator = c as Animator;
					if (animator != null)
					{
						CheckAnimatorReferences(animator);
					}
				}
				catch (System.Exception) { }
			}
		}
	}

	public static void CheckAnimatorReferences(Animator component)
	{
		if (component.runtimeAnimatorController == null)
		{
			return;
		}
		foreach (AnimationClip ac in component.runtimeAnimatorController.animationClips)
		{
			var so = new SerializedObject(ac);
			var sp = so.GetIterator();

			while (sp.Next(true))
			{
				if (sp.propertyType == SerializedPropertyType.ObjectReference && !sp.name.Equals("m_PrefabParentObject"))
				{
					if (sp.objectReferenceValue == null && sp.objectReferenceInstanceIDValue != 0)
					{
						Debug.LogError(
							"Missing reference found in: " + FullObjectPath(component.gameObject) +
							"Animation: " + ac.name +
							", Property : " + sp.name +
							", Scene: " + UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
					}
				}
			}
		}
	}

	static void ShowError(string objectName, string propertyName, string sceneName)
	{
		Debug.LogError("Missing reference found in: " + objectName + ", Property : " + propertyName + ", Scene: " + sceneName);
	}

	static string FullObjectPath(GameObject go)
	{
		return go.transform.parent == null ? go.name : FullObjectPath(go.transform.parent.gameObject) + "/" + go.name;
	}
}
