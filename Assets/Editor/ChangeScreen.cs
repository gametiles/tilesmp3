﻿using UnityEditor;
using UnityEditor.SceneManagement;

public class ChangeScreen {

	[MenuItem("Tools/ChangeScreen/SceneManager #1")]
	public static void ScreenManager()
	{
		EditorSceneManager.OpenScene("Assets/Scenes/ScenesManager.unity");
	}

	[MenuItem("Tools/ChangeScreen/MainGame #2")]
	public static void ScreenMainGame()
	{
        EditorSceneManager.OpenScene("Assets/Scenes/MainGame.unity");
	}
}
