using Parse;
using Amanotes.PianoChallenge;
public class LocalParserInitialize : ParseInitializeBehaviour
{
	private bool isInit = false;
	// Use this for initialization
	new void Awake()
	{
		//nothing
	}
	void Start()
	{
		//nothing
	}

	public void Init()
	{
		serverURL = GameManager.Instance.GameConfigs.PARSE_SERVER_URL;
		serverURL = "http://localhost:1340/parse/";
		applicationID = GameManager.Instance.GameConfigs.PARSE_APPLICATION_ID;
		dotnetKey = GameManager.Instance.GameConfigs.PARSE_DOT_NET_KEY;
		base.Awake();
		isInit = true;
	}

	// Update is called once per frame
	void Update()
	{

	}

	new void OnApplicationPause(bool isPause)
	{
		if (isInit)
		{
			base.OnApplicationPause(isPause);
		}
	}
}
