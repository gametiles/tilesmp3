using UnityEngine;
using System.Collections.Generic;

public class GlobalData : MonoBehaviour
{
	private static Dictionary<string, GameObject> cache = new Dictionary<string, GameObject>();

	// Use this for initialization
	void Awake()
	{
		DontDestroyOnLoad(gameObject);
		if (cache.ContainsKey(name))
		{
			DestroyImmediate(this.gameObject);
		}
		else
		{
			cache[name] = gameObject;
		}
	}
	void Start()
	{

	}
}
