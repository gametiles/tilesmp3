﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PTChapterData{
	public int id;
	public float baseBeats;
	public List<string> scores;
	public string[] instruments;
	public List<string> alternatives;
	public float bpm;
	public PTChapterData(){
	
	}
} 
public class PTAuditionData{
	public List<float> start;
	public List<float> end;
	public PTAuditionData(){
	
	}
}
public class InfoANote{
	public List<List<int>> id;
	public float height;
	public bool isMute;
	public bool isContinuous=false;
	public InfoANote(List<List<int>> _id, float _height,bool _isMute=false){
		id = _id;
		height = _height;
		isMute = _isMute;
	}
	public float GetHeight(){
//		if (height < 0.1) {
//			return 0;
//		} else if (height < 1) {
//			return 1;
//		} else {
//			return (int)height;
//		}
		return height;
	}
}
	
public class PTJsonData {
	public int baseBpm;
	public List<PTChapterData> musics;
	public PTAuditionData audition;
	// Use this for initialization
	public PTJsonData(){
		
	}
//	public int GetCoff(){
//		if (musics [0].baseBeats < 0.5f) {
//			return 2;
//		}
//		return 1;
//	}


//	public List<NoteData> ConvertToTrackView(){
//		
//	}
//	public List<NoteData> ConvertToTrackPlay(){
//		
//	}
}

public class PTDuetNote{
	public int location;
	public List<NoteData> note1=null;
	public List<NoteData> note2=null;

	public int index=0;
	public float lastTimeCheck=0;


	public PTDuetNote(int location){
		this.location = location;
	}
	public void AddNote1(List<NoteData> note1){
		this.note1 = note1;
	}
	public void AddNote2(List<NoteData> note2){
		this.note2 = note2;
	}

	public List<NoteData> GetSuiableSoundPlay(){
		float now = Time.realtimeSinceStartup;
		if(now-lastTimeCheck>3)
		{	
			index = 0;
			//Debug.LogWarning ("Lay Note 1 nha:" + Pathfinding.Serialization.JsonFx.JsonWriter.Serialize (note1));
		}
		lastTimeCheck = now;

		List<NoteData> note = note1;
		if (index % 2 != 0) {
			//Debug.LogWarning ("Lay Note 2 nha:" + Pathfinding.Serialization.JsonFx.JsonWriter.Serialize (note2));
			note = note2;
		}
		index++;
		return note;

	}
}

public class PTConvertData{
	public List<NoteData> trackView;
	public List<NoteData> trackPlay;
	public Dictionary<NoteData,List<NoteData>> dicNotePlay;
	public Dictionary<int,PTDuetNote> dicDuetNote;
	public float timePerTiles;
	public PTConvertData(float _timePerTiles){
		trackView = new List<NoteData> ();
		trackPlay = new List<NoteData> ();
		dicNotePlay = new Dictionary<NoteData, List<NoteData>> ();
		dicDuetNote = new Dictionary<int, PTDuetNote> ();
		this.timePerTiles = _timePerTiles;
	}
	public void AddSubForViewNote(NoteData noteAdd){
		for (int i = 0; i < trackView.Count; i++) {
			if (i < trackView.Count - 1) {
				if (trackView [i].tickAppear == noteAdd.tickAppear) {//okey add
					//Debug.LogError("AAAAAA:"+trackView [i].tickAppear+","+noteAdd.tickAppear);
					AddNote (trackView [i],noteAdd);

					return;
				} else if (trackView [i].tickAppear < noteAdd.tickAppear && trackView [i + 1].tickAppear > noteAdd.tickAppear) {//okey add
					//Debug.LogError("BBBBBB:"+trackView [i].tickAppear+","+noteAdd.tickAppear+","+trackView [i + 1].tickAppear);
					AddNote (trackView [i],noteAdd);

					return;
				}
			} else {
				if (trackView [i].tickAppear <= noteAdd.tickAppear) {
					//Debug.LogError("CCCCCC:"+trackView [i].tickAppear+","+noteAdd.tickAppear);
					AddNote (trackView [i],noteAdd);

					return;
				}
			}
		}
		Debug.LogError ("DDDDD:Super error");
	}

	public void AddNoteDuetCache(int location, int index, List<NoteData> note){
		if (!dicDuetNote.ContainsKey (location)) {
			dicDuetNote [location] = new PTDuetNote (location);
		}
		if (index == 0) {
			dicDuetNote [location].AddNote1 (note);
		} else {
			dicDuetNote [location].AddNote2 (note);
		}
	}

	public void AddNote(NoteData keyNode, NoteData add)
	{
		if (!dicNotePlay.ContainsKey(keyNode)) {
			dicNotePlay [keyNode] = new List<NoteData> ();
		}
		if (add.nodeID != 200) {
//			if (add.nodeID < 200) {
//				add.volume = 1.0f;
//			}
			dicNotePlay[keyNode].Add (add);
		}
	}
	public void AddEmptyKey(NoteData keyNode)
	{
		if (!dicNotePlay.ContainsKey(keyNode)) {
			dicNotePlay [keyNode] = new List<NoteData> ();
		}
	}
}

public class PTInfoData{
	public List<PTTrackData> listTrack;

	public PTInfoData(){
		listTrack = new List<PTTrackData> ();
	}

	public PTInfoData(int numberTrack){
		listTrack = new List<PTTrackData> ();
	}
	public PTTrackData AddTrackData(string[] _instrumentType,string[] content,int coff)
	{
		PTTrackData data = new PTTrackData (_instrumentType, content,coff);
		listTrack.Add (data);
		return data;
	}
}

public class PTTrackData{
	string[] instumentContent;
	string[] instrumentType;
	public int coff;

	public PTTrackData(string[] _instrumentType,string[] _instumentContent, int _coff){
		instrumentType = _instrumentType;
		instumentContent = _instumentContent;
		this.coff = _coff;
	}
	public int ConvertTrackToNote(ref PTConvertData convertData,int startIndex,int typePI)// convert all data in a track (each song have 3 track)
	{
		
		int start=startIndex;
		float backup = start;

		Dictionary<int,float> cacheLineIndex=new Dictionary<int, float>();
		for (int i = 0; i < instumentContent.Length; i++) {
			bool isTrackView = false;
			if (i == 0) {
				isTrackView = true;
			}
			string content_i = instumentContent [i];//info a instrument in track
			string[] content_line=content_i.Split(new string[]{";"},System.StringSplitOptions.RemoveEmptyEntries);// split to each line

			float startNode = startIndex;
			//typePI = 3;
			for(int j=0;j<content_line.Length;j++){
				if (!cacheLineIndex.ContainsKey (j)) {
					cacheLineIndex [j] = Mathf.CeilToInt (startNode);
				} else {
					if (typePI!=3&&typePI!=4) {
						//startNode = Mathf.CeilToInt (startNode);
						startNode = cacheLineIndex [j];
					} else {
						startNode = Mathf.CeilToInt (startNode);
					}

				}

				string[] content_note=content_line[j].Split(new string[]{","},System.StringSplitOptions.RemoveEmptyEntries);// each note
				//string[] noteDataInALine = instructmentTrack[j];//info a line instructment
				for (int x = 0; x < content_note.Length; x++) {
					string aNoteStr = content_note [x];
					//Debug.LogError ("Anote:"+aNoteStr);
//					if (!isTrackView) {
//						Debug.LogError (startNode+","+aNoteStr);
//					}
					bool isEndLine= (x==content_note.Length-1?true:false);
					if (!isEndLine) {
						 isEndLine= (x==0?true:false);
					}
					//Debug.LogError ("isEndLine:" + isEndLine);
					startNode = AnalyticsOneNote (ref convertData, aNoteStr,instrumentType[i], startNode,isTrackView,isEndLine);

				}
				if (i == 0) {
					
					start=Mathf.CeilToInt(startNode);
				}
			}
		}
		return start;
	}

	private float AnalyticsOneNote(ref PTConvertData convertData, string text,string instrument, float nodeIndex,bool isTrackView,bool isEndLine){
//		if (!isTrackView) {
//			Debug.LogError ("TEXT:" + nodeIndex+","+text);
//		}
//		else{	
//			Debug.LogError ("VIEW:" + nodeIndex+","+text);
//		}
		float oldIndex = nodeIndex;
		string[] data = text.Split (new string[]{"="}, System.StringSplitOptions.RemoveEmptyEntries);
		if (data.Length == 2) {
			float currentTime = nodeIndex * convertData.timePerTiles;
			int location = (int)(nodeIndex * 480);
			bool isChange = false;
			float height = 1;
			NoteData firstNote = null;
			if (data [0].Contains ("5")) {//not doi
				
				//Debug.LogError("BeginNote:"+nodeIndex);
				string[] data2 = data [1].Split (new string[]{ "$" }, System.StringSplitOptions.RemoveEmptyEntries);
				for (int i = 0; i < data2.Length; i++) {
					isChange = true;
					string aNoteStr = data2 [i];
					InfoANote aNote = GetNoteInfo (instrument, aNoteStr, this.coff);
					NoteData noteCache = null;
					for (int x = 0; x < aNote.id.Count; x++) {
						isChange = true;
						float heightA = aNote.GetHeight ();
						//Debug.LogError ("heightA:" + heightA+","+convertData.timePerTiles);

						float temp = heightA;
						if (temp < 0.51f) {//0.5
							temp = 1;
						}
						float duration = temp * convertData.timePerTiles;
						int durationInTick = (int)(temp * convertData.timePerTiles * 1000);
						int tickAppear = (int)(currentTime * 1000);
						NoteData noteA = new NoteData (currentTime, aNote.id [x] [0], duration, durationInTick, location, location, 1);
						convertData.trackPlay.Add (noteA);
						if (x == 0 && isTrackView) {
							if (heightA < 1)
								heightA = 1;
							duration = heightA * convertData.timePerTiles;
							durationInTick = (int)(heightA * convertData.timePerTiles * 1000);
							tickAppear = (int)(currentTime * 1000);
							NoteData note = new NoteData (currentTime, aNote.id [x] [0], duration, durationInTick, location, location, 1);
							note.nodeID = 202;// tro thay note sound am
							note.duetNode = 1;
							note.isEndLine = isEndLine;
							convertData.trackView.Add (note);

							if (i > 0) {// note thu 2
								if (firstNote != null) {
									//Debug.LogError ("KHOANG CACH DUET:" + (i * aNote.GetHeight ()/2)+","+noteA.duration+","+temp);
									noteA.timeAppear += i * aNote.GetHeight () / 2;// convertData.timePerTiles;
									convertData.AddNote (firstNote, noteA);
								}
								//note.nodeID = 202;
								convertData.AddNote (note, note);//note thu 2 la rong

							} else {
								convertData.AddNote (note, noteA);
								if (firstNote == null) {
									firstNote = note;
								}
							}

							convertData.AddNoteDuetCache (location, i, convertData.dicNotePlay [note]);
							noteCache = note;
							height = aNote.GetHeight ();
						} 
					}
				}

				if (isChange) {
					if (height < 1) {
						height = 1;
					}
					nodeIndex += height;
				}

				//Debug.LogError("EndNote:"+nodeIndex+","+oldIndex);
			} else if (data [0].Contains ("6")) { 
				bool isAddView = false;
				string[] data2 = data [1].Split (new string[]{ "$" }, System.StringSplitOptions.RemoveEmptyEntries);
				for (int i = 0; i < data2.Length; i++) {
					isChange = true;
					string aNoteStr = data2 [i];
					InfoANote aNote = GetNoteInfo (instrument, aNoteStr, this.coff);
					Debug.LogError ("loai thu 6:" + aNoteStr);

					float duration = aNote.GetHeight () * convertData.timePerTiles;
					int durationInTick = (int)(aNote.GetHeight () * convertData.timePerTiles * 1000);
					int tickAppear = (int)(currentTime * 1000);

					#region old function
					/*
					for (int x = 0; x < aNote.id.Count; x++) {
						isChange = true;
					
					
						NoteData note = new NoteData (currentTime, aNote.id[x], duration, durationInTick, location, location,1);
						if (x == 0 && !isAddView && isTrackView) {
							note.isEndLine = isEndLine;
							height = aNote.GetHeight ();
							isAddView = true;
							//Debug.LogError ("Add to Trackview ne:"+currentTime+","+location);
							convertData.trackView.Add (note);
							convertData.AddNote (note, note);
						} else {
							if (note.nodeID > 3000) {
								note.duration *= 2.5f;
								note.durationInTick = (int)(2.5f * note.durationInTick);
							}
							else if (note.nodeID > 900) {// cho tieng trong hay hon
								note.duration *= 1.5f;
								note.durationInTick = (int)(1.5f * note.durationInTick);
							} 
							convertData.AddSubForViewNote (note);

						}
						convertData.trackPlay.Add (note);
					}
					*/
					#endregion


					for (int x = 0; x < aNote.id.Count; x++) {
						for (int y = 0; y < aNote.id [x].Count; y++) {
							isChange = true;
							NoteData note = new NoteData (currentTime, aNote.id [x] [y], duration, durationInTick, location, location, 1);
							//note.nodeID = 202;
							if (x == 0 && !isAddView && isTrackView) {
								//Debug.LogError (currentTime);
								//Debug.LogError ("AAAAA"+nodeIndex+","+location+","+durationInTick+","+Pathfinding.Serialization.JsonFx.JsonWriter.Serialize (note));
								//Debug.LogError ("Single:" + x + ":" + location+","+aNote.id.Count+","+note.duetNode);
								isAddView = true;
								note.isEndLine = isEndLine;
								height = aNote.GetHeight ();
								convertData.trackView.Add (note);
								if (aNote.isContinuous == false) {
									convertData.AddNote (note, note);
								} else {

								}
							} else {
								if (aNote.isContinuous == false) {
									if (note.nodeID > 3000) {
										note.duration *= 2.5f;
										note.durationInTick = (int)(2.5f * note.durationInTick);
									} else if (note.nodeID > 900) {// cho tieng trong hay hon
										note.duration *= 1f;
										note.durationInTick = (int)(1f * note.durationInTick);
									} 

									convertData.AddSubForViewNote (note);
								}
							}
							convertData.trackPlay.Add (note);
							if (aNote.isContinuous) {
								//Debug.LogError ("Continuous:" + text+","+aNote.GetHeight ());
								float currentTimeA = currentTime + aNote.GetHeight () * convertData.timePerTiles * ((float)x / aNote.id.Count);
								float durationA = aNote.GetHeight () * (1.0f / aNote.id.Count) * convertData.timePerTiles;
								int durationInTickA = (int)(aNote.GetHeight () * (1.0f / aNote.id.Count) * convertData.timePerTiles * 1000);
								int tickAppearA = (int)(currentTime * 1000);
								int locationA = (int)(location + aNote.GetHeight () * ((float)x / aNote.id.Count) * 480);
								NoteData noteContinue = new NoteData (currentTimeA, aNote.id [x] [y], durationA, durationInTickA, locationA, locationA, 1);
								//Debug.LogError ("Continuous:"+nodeIndex+ ","+ text+","+aNote.GetHeight ()+ "," + x + Pathfinding.Serialization.JsonFx.JsonWriter.Serialize (noteContinue));
								convertData.AddSubForViewNote (noteContinue);
							}
						}
					}
				}
				if (isChange) {
					nodeIndex += height;
				}
			} else if (data [0].Contains ("3")) {//dang choi dat biet bam nhieu phim o giua
				//todo later
			}
		} else {
			float currentTime = nodeIndex * convertData.timePerTiles;
		
			int location = (int)nodeIndex * 480;
			if (isTrackView) {
				//Debug.LogWarning ("LOCATION:" + location+","+nodeIndex+","+convertData.coff);
			}
			bool isChange = false;
			string aNoteStr = text;
			//Debug.LogError (nodeIndex + "," + convertData.timePerTiles + "," + currentTime+","+aNoteStr);
			InfoANote aNote = GetNoteInfo (instrument, text, this.coff);
			//Debug.LogError ("XXXX:"+Pathfinding.Serialization.JsonFx.JsonWriter.Serialize (aNote));
//			if (!isTrackView) {
//				Debug.LogError ("TEXT:" + nodeIndex+","+text+","+currentTime+":"+Pathfinding.Serialization.JsonFx.JsonWriter.Serialize (aNote.id));
//			}
			for (int x = 0; x < aNote.id.Count; x++) {
				for (int y = 0; y < aNote.id [x].Count; y++) {
					isChange = true;
					float duration = aNote.GetHeight () * convertData.timePerTiles;
					int durationInTick = (int)(aNote.GetHeight () * convertData.timePerTiles * 1000);
					int tickAppear = (int)(currentTime * 1000);

					NoteData note = new NoteData (currentTime, aNote.id [x][y], duration, durationInTick, location, location, 1);
					//note.nodeID = 202;
					if (x == 0 && isTrackView) {
						//Debug.LogError (currentTime);
						//Debug.LogError ("AAAAA"+nodeIndex+","+location+","+durationInTick+","+Pathfinding.Serialization.JsonFx.JsonWriter.Serialize (note));
						//Debug.LogError ("Single:" + x + ":" + location+","+aNote.id.Count+","+note.duetNode);
						note.isEndLine = isEndLine;
						convertData.trackView.Add (note);
						if (aNote.isContinuous == false) {
							convertData.AddNote (note, note);
						} else {
						
						}
					} else {
						if (aNote.isContinuous == false) {
							if (note.nodeID > 3000) {
								note.duration *= 2.5f;
								note.durationInTick = (int)(2.5f * note.durationInTick);
							} else if (note.nodeID > 900) {// cho tieng trong hay hon
								note.duration *= 1f;
								note.durationInTick = (int)(1f * note.durationInTick);
							} 

							convertData.AddSubForViewNote (note);
						}
					}
					convertData.trackPlay.Add (note);
					if (aNote.isContinuous) {
						float currentTimeA = currentTime + aNote.GetHeight () * convertData.timePerTiles * ((float)x / aNote.id.Count);
						float durationA = aNote.GetHeight () * (1.0f / aNote.id.Count) * convertData.timePerTiles;
						int durationInTickA = (int)(aNote.GetHeight () * (1.0f / aNote.id.Count) * convertData.timePerTiles * 1000);
						int tickAppearA = (int)(currentTime * 1000);
						int locationA = (int)(location + aNote.GetHeight () * ((float)x / aNote.id.Count) * 480);
						NoteData noteContinue = new NoteData (currentTimeA, aNote.id [x][y], durationA, durationInTickA, locationA, locationA, 1);
						convertData.AddSubForViewNote (noteContinue);
					}


				}
			}
			if (isChange) {
				if (isTrackView) {
					//Debug.LogWarning ("LOCATIONBB:" + location + "," + nodeIndex + "," + convertData.coff+","+aNote.GetHeight());
				}
				nodeIndex += aNote.GetHeight ();
			}
		}
		if (nodeIndex == oldIndex) {
			nodeIndex++;
		}
		return nodeIndex;
	}

	private InfoANote GetNoteInfo(string instrucment,string text,int coff)
	{
		if (text.Contains ("R")) {
			return (new InfoANote (new List<List<int>> (){ new List<int>{200} }, 8 * coff, true));
		} else if (text.Contains ("UTS") || text.Contains ("STU")) {
			return new InfoANote (new List<List<int>> (){ new List<int>{200} }, 7 * coff, true);
		} else if (text.Contains ("ST") || text.Contains ("TS")) {
			return new InfoANote (new List<List<int>> (){ new List<int>{200} }, 6 * coff, true);
		} else if (text.Contains ("SU") || text.Contains ("US")) {
			return new InfoANote (new List<List<int>> (){ new List<int>{200} }, 5 * coff, true);
		} else if (text.Contains ("SV")) {
			return new InfoANote (new List<List<int>> (){ new List<int>{200} }, 4.5f * coff, true);
		} 
		else if (text.Contains ("S")) {
			return new InfoANote (new List<List<int>> (){ new List<int>{200} }, 4 * coff, true);
		} else if (text.Contains ("TUV")) {
			return new InfoANote (new List<List<int>> (){ new List<int>{200} }, 3.5f * coff, true);
		} 
		else if (text.Contains ("UT") || text.Contains ("TU")) {
			return new InfoANote (new List<List<int>> (){ new List<int>{200} }, 3 * coff, true);
		} else if (text.Contains ("TV")) {
			return new InfoANote (new List<List<int>> (){ new List<int>{200} }, 2.5f * coff, true);
		}  
		else if (text.Contains ("T")) {
			return new InfoANote (new List<List<int>> (){ new List<int>{200} }, 2 * coff, true);
		} else if (text.Contains ("UV")) {
			return new InfoANote (new List<List<int>> (){ new List<int>{200} }, 1.5f * coff, true);
		} 
		else if (text.Contains ("U")) {
			return new InfoANote (new List<List<int>> (){ new List<int>{200} }, 1 * coff, true);
		} else if (text.Contains ("V")) {//0.5f
			return (new InfoANote (new List<List<int>> (){ new List<int>{200} }, 0.5f * coff, true));
		} else if (text.Contains ("W")) {
			return (new InfoANote (new List<List<int>> (){ new List<int>{200} }, 0.25f * coff, true));
		}
		else if (text.Contains (":")) {
			
			string[] note = text.Split (new string[]{":"},System.StringSplitOptions.RemoveEmptyEntries);
			if (note.Length == 2) {
				int isContinuos = 0;
				if (note [0].Contains ("^")) {
					isContinuos = 1;
				}
				else if (note [0].Contains ("&")) {
					isContinuos = 1;
				}
				else if (note [0].Contains ("@")) {
					isContinuos = 5;
				}
				else if (note [0].Contains ("~")) {
					isContinuos = 2;
				}
				else if (note [0].Contains ("%")) {
					isContinuos = 4;
				}
				//string[] strNote=note[0].Split (new string[]{"."},System.StringSplitOptions.RemoveEmptyEntries);
				List<List<int>> id = new List<List<int>>();
				ConvertToNote (ref id, note [0], instrucment);

				//int node = convertStringToNoteId (instrucment, note [0]);
				float height = 0;
				if (text.Contains ("I")) {
					height = 8;
				}
				else if (text.Contains ("JJ")) {
					height = 8;
				} else if (text.Contains ("JKL") || text.Contains ("LKJ")) {
					height = 7;
				} else if (text.Contains ("JK") || text.Contains ("KJ")) {
					height = 6;
				} else if (text.Contains ("JL") || text.Contains ("LJ")) {
					height = 5;
				}else if (text.Contains ("JM")) {
					height = 4.5f;
				} 
				else if (text.Contains ("J")) {
					height = 4;
				} else if (text.Contains ("KLM")) {
					height = 3.5f;
				} 
				else if (text.Contains ("KL") || text.Contains ("LK")) {
					height = 3;
				} else if (text.Contains ("KM")) {
					height = 2.5f;
				} else if (text.Contains ("K")) {
					height = 2;
				} else if (text.Contains ("LM")) {
					height = 1.5f;
				} else if (text.Contains ("L")) {
					height = 1;
				} else if (text.Contains ("M")) {
					height = 0.5f;
				} else if (text.Contains ("N")) {
					height = 0.25f;
				}
				if (isContinuos==1) {
					
					string[] strNote=note[0].Split (new string[]{"^","&"},System.StringSplitOptions.RemoveEmptyEntries);
					id.Clear();
					int length = (int)(1 + height)/**coff*/;
					//Debug.LogError ("FFFFF:"+length);
					for (int x = 0; x < length; x++) {
						for (int i = 0; i < strNote.Length; i++) {
							ConvertToNote (ref id, strNote [i], instrucment);
						}
					}

					id.Add (new List<int>{200});// cho not cuoi
				}
				else if (isContinuos == 2) {
					string[] strNote=note[0].Split (new string[]{"~",},System.StringSplitOptions.RemoveEmptyEntries);
					id.Clear ();
					for (int i = 0; i < strNote.Length; i++) {
						ConvertToNote (ref id, strNote [i], instrucment);
					}

					id.Add (new List<int>{200});// cho not cuoi
				}
				else if (isContinuos == 4) {
					string[] strNote=note[0].Split (new string[]{"%",},System.StringSplitOptions.RemoveEmptyEntries);
					id.Clear();
					for (int i = 0; i < strNote.Length; i++) {
						ConvertToNote (ref id, strNote [i], instrucment);
					}
					id.Add (new List<int>{200});// cho not cuoi
				}

				else if (isContinuos == 5) {
					int totalHeight = (int)(height * coff);
					string[] strNote=note[0].Split (new string[]{"@"},System.StringSplitOptions.RemoveEmptyEntries);
					id.Clear();
					if (totalHeight <= strNote.Length) {
						for (int i = 0; i < strNote.Length; i++) {
							ConvertToNote (ref id, strNote [i], instrucment);
						}
					} else {
						for (int i = 0; i<totalHeight; i++) {
							//Debug.LogError ("Vo day ne:" + strNote [i]);
							if (i < strNote.Length) {
								ConvertToNote (ref id, strNote [i], instrucment);
							} else {
								int nodeId = 200;
								id.Add (new List<int>{nodeId});
							}
						}
					}

					//id.Add (200);// cho not cuoi
				}
				//Debug.LogError (note [0] +":"+isContinuos+ ": " + Pathfinding.Serialization.JsonFx.JsonWriter.Serialize (id));
				InfoANote aNote = new InfoANote (id, height * coff, false);
				aNote.isContinuous =isContinuos>0?true:false;
				return (aNote);
			}
		}
		Debug.LogError ("Error:" + text);
		return null;
	}
	private void ConvertToNote(ref List<List<int>> id,string text,string instrucment){
		string[] finalStr=text.Split (new string[]{"."},System.StringSplitOptions.RemoveEmptyEntries);
		for (int z = 0; z < finalStr.Length; z++) {
			int nodeId = convertStringToNoteId (instrucment, finalStr [z]);
			id.Add (new List<int>{ nodeId });
		}
	}
	private int convertStringToNoteId(string instrucment,string note){

		#region Bass
		if(instrucment.Contains("bass"))
		{
			switch (note) {
				case "#A-1":
					return 1046;
				case "#a":
					return 1058;
				case "#a1":
					return 1070;
				case "#c":
					return 1049;
				case "#c1":
					return 1061;
				case "#c2":
					return 1073;
				case "#d":
					return 1051;
				case "#d1":
					return 1063;
				case "#d2":
					return 1075;
				case "#F-1":
					return 1042;
				case "#f":
					return 1054;
				case "#f1":
					return 1066;
				case "#G-1":
					return 1044;
				case "#g":
					return 1056;
				case "#g1":
					return 1068;
				case "A-1":
					return 1045;
				case "a":
					return 1057;
				case "a1":
					return 1069;
				case "B-1":
					return 1047;
				case "b":
					return 1059;
				case "b1":
					return 1071;
				case "c":
					return 1048;
				case "c1":
					return 1060;
				case "c2":
					return 1072;
				case "d":
					return 1050;
				case "d1":
					return 1062;
				case "d2":
					return 1074;
				case "E-1":
					return 1040;
				case "e":
					return 1052;
				case "e1":
					return 1064;
				case "F-1":
					return 1041;
				case "f":
					return 1053;
				case "f1":
					return 1065;
				case "G-1":
					return 1043;
				case "g":
					return 1055;
				case "g1":
					return 1067;
				default:
				{
					Debug.Log("Super Error Bass1:"+note);
					return 201;
				}
			}
		}
		#endregion

		#region Bass 2
		else if(instrucment.Contains("bass2"))
		{
			switch (note) {
				case "#A-1":
					return 2046;
				case "#a":
					return 2058;
				case "#a1":
					return 2070;
				case "#C-1":
					return 2037;
				case "#c":
					return 2049;
				case "#c1":
					return 2061;
				case "#c2":
					return 2073;
				case "#D-1":
					return 2039;
				case "#d":
					return 2051;
				case "#d1":
					return 2063;
				case "#d2":
					return 2075;
				case "#F-1": 
					return 2042;
				case "#f":
					return 2054;
				case "#f1":
					return 2066;
				case "#G-1":
					return 2044;
				case "#g":
					return 2056;
				case "#g1": 
					return 2068;
				case "A-1":
					return 2045;
				case "a":
					return 2057;
				case "a1": 
					return 2069;
				case "B-1":
					return 2047;
				case "B-2":
					return 2035;
				case "b":
					return 2059;
				case "b1":
					return 2071;
				case "C-1":
					return 2036;
				case "c":
					return 2048;
				case "c1":
					return 2060;
				case "c2":
					return 2072;
				case "D-1":
					return 2038;
				case "d":
					return 2050;
				case "d1":
					return 2062;
				case "d2":
					return 2074;
				case "E-1":
					return 2040;
				case "e":
					return 2052;
				case "e1":
					return 2064;
				case "F-1":
					return 2041;
				case "f":
					return 2053;
				case "f1":
					return 2065;
				case "G-1":
					return 2043;
				case "g":
					return 2055;
				case "g1":
					return 2067;
				default:
				{
					Debug.Log("Super Error Bass2:"+note);
					return 201;
				}
			}
		}
		#endregion

		#region drum
		else if(instrucment.Contains("drum")||instrucment.Contains("try_drum"))
		{
			switch (note) {
				case "#a1":
					return 3070;
				case "#C-1":
					return 3037;
				case "#c1": 
					return 3061;
				case "#d1":
					return 3063;
				case "#G-1":
					return 3044;
				case "A-1":
					return 3045;
				case "a":
					return 3057;
				case "B-2":
					return 3035;
				case "b":
					return 3059;
				case "C-1":
					return 3036;
				case "c1":
					return 3060;
				case "D-1": 
					return 3038;
				case "d1":
					return 3062;
				case "E-2": 
					return 3028;
				case "e1":
					return 3064;
				case "F-1": 
					return 3041;
				case "F-2":
					return 3029;
				case "f":
					return 3065;
				case "G-1":  
					return 3043;
				case "g":
					return 3067;
				default:
				{
					Debug.Log("Super Error Note Drum:"+note);
					return 201;
				}
			}
		}
		#endregion

		#region piano
		else
		{
			switch (note) {
				//a
				case "A-3":
					return 21;
				case "A-2":
					return 33;
				case "A-1":
					return 45;
				case "a":
					return 57;
				case "a1":
					return 69;
				case "a2":
					return 81;
				case "a3":
					return 93;
				case "a4":
					return 105;

					//a#
				case "#A-3":
					return 22;
				case "#A-2":
					return 34;
				case "#A-1":
					return 46;
				case "#a":
					return 58;
				case "#a1":
					return 70;
				case "#a2":
					return 82;
				case "#a3":
					return 94;
				case "#a4":
					return 106;

				//B
				case "B-3":
					return 23;
				case "B-2":
					return 35;
				case "B-1":
					return 47;
				case "b":
					return 59;
				case "b1":
					return 71;
				case "b2":
					return 83;
				case "b3":
					return 95;
				case "b4":
					return 107;

				//C
				case "C-3":
					return 12;
				case "C-2":
					return 24;
				case "C-1":
					return 36;
				case "c":
					return 48;
				case "c1":
					return 60;
				case "c2":
					return 72;
				case "c3":
					return 84;
				case "c4":
					return 96;
				case "c5":
					return 108;

				//C#
				case "#C-3":
					return 13;
				case "#C-2":
					return 25;
				case "#C-1":
					return 37;
				case "#c":
					return 49;
				case "#c1":
					return 61;
				case "#c2":
					return 73;
				case "#c3":
					return 85;
				case "#c4":
					return 97;


				//D
				case "D-3":
					return 14;
				case "D-2":
					return 26;
				case "D-1":
					return 38;
				case "d":
					return 50;
				case "d1":
					return 62;
				case "d2":
					return 74;
				case "d3":
					return 86;
				case "d4":
					return 98;

				//D#
				case "#D-3":
					return 15;
				case "#D-2":
					return 27;
				case "#D-1":
					return 39;
				case "#d":
					return 51;
				case "#d1":
					return 63;
				case "#d2":
					return 75;
				case "#d3":
					return 87;
				case "#d4":
					return 99;

				//E
				case "E-3":
					return 16;
				case "E-2":
					return 28;
				case "E-1":
					return 40;
				case "e":
					return 52;
				case "e1":
					return 64;
				case "e2":
					return 76;
				case "e3":
					return 88;
				case "e4":
					return 100;

				//F
				case "F-3":
					return 17;
				case "F-2":
					return 29;
				case "F-1":
					return 41;
				case "f":
					return 53;
				case "f1":
					return 65;
				case "f2":
					return 77;
				case "f3":
					return 89;
				case "f4":
					return 101;

				//F#
				case "#F-3":
					return 18;
				case "#F-2":
					return 30;
				case "#F-1":
					return 42;
				case "#f":
					return 54;
				case "#f1":
					return 66;
				case "#f2":
					return 78;
				case "#f3":
					return 90;
				case "#f4":
					return 102;

				//G
				case "G-3":
					return 19;
				case "G-2":
					return 31;
				case "G-1":
					return 43;
				case "g":
					return 55;
				case "g1":
					return 67;
				case "g2":
					return 79;
				case "g3":
					return 91;
				case "g4":
					return 103;

				//G#
				case "#G-3":
					return 20;
				case "#G-2":
					return 32;
				case "#G-1":
					return 44;
				case "#g":
					return 56;
				case "#g1":
					return 68;
				case "#g2":
					return 80;
				case "#g3":
					return 92;
				case "#g4":
					return 104;

				default:
				{
					Debug.Log("Super Error Note:"+note);
					return 201;
				}
			}
		}
		#endregion
	}
} 