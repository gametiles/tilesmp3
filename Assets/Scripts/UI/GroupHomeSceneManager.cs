using UnityEngine;
using System;
using System.Collections.Generic;
using MovementEffects;
using Amanotes.PianoChallenge;
using Amanotes.Utils;

public class GroupHomeSceneManager : SingletonMono<GroupHomeSceneManager>
{
	public GroupPanel[] groupPanel;

	[HideInInspector] public GroupHomeSceneType currentPanel = GroupHomeSceneType.None;
	public GroupHomeSceneType lastPanel = GroupHomeSceneType.None;

	public event Action<GroupHomeSceneType> onPanelChange;

	bool canChangeScene = true;

	public void OpenPanel(GroupHomeSceneType type)
	{
		for (int i = 0; i < groupPanel.Length; i++)
		{
			if (groupPanel[i].type == type)
			{
				lastPanel = currentPanel;
				currentPanel = type;
				SceneManager.Instance.currentGroupHomeUI = type;
				groupPanel[i].gameObject.SetActive(true);
				AnalyticsHelper.Instance.LogOpenScene(type.ToString());
			}
			else if (groupPanel[i].gameObject.activeInHierarchy)
			{
				groupPanel[i].gameObject.SetActive(false);
			}
		}
	}

	public void OpenPanelAnim(GroupHomeSceneType type)
	{
        if (canChangeScene && currentPanel != type)
        {
            AudioEffect.Instance.PlayButtonSoundEffect();
            Timing.RunCoroutine(IEOpenPanel(type));
        }
	}

	private IEnumerator<float> IEOpenPanel(GroupHomeSceneType panel)
	{
		canChangeScene = false;
		if (onPanelChange != null)
			onPanelChange(panel);
		yield return Timing.WaitForSeconds(0.12f);
		OpenPanel(panel);

		canChangeScene = true;
	}
}

public enum GroupHomeSceneType
{
    BattleHome,
    SongList,	
	SettingUI,
	None
}
