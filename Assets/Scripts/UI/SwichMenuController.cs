
using Amanotes.PianoChallenge;

public class SwichMenuController : SSController
{
	public override void OnEnable()
	{
		base.OnEnable();
	}

	public override void OnSet(object data)
	{
		if (data != null)
		{
			try
			{
				GroupHomeSceneType type = (GroupHomeSceneType)data;
				GroupHomeSceneManager.Instance.OpenPanel(type);
			}
			catch (System.Exception)
			{
				throw;
			}
		}
	}

	public override void OnKeyBack()
	{
		if (GroupHomeSceneManager.Instance.currentPanel == GroupHomeSceneType.SettingUI)
		{
			GroupHomeSceneManager.Instance.OpenPanelAnim(GroupHomeSceneType.BattleHome);
		}
		else
		{
			SceneManager.Instance.OpenExitConfirmation();
		}
	}

}
