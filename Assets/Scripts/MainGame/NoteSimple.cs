using UnityEngine;
using Amanotes.PianoChallenge;
using DG.Tweening;
using System.Collections.Generic;
public class NoteSimple : MonoBehaviour
{
	public SpriteRenderer sprite;
	protected const int BASE_COLLIDER_WIDTH = 280;
	protected const int BASE_COLLIDER_HEIGHT = 480;
	protected const float MAX_COLLIDER_EXPAND = 1.2f;
	public bool isLongNote = false;
	public bool isBonus = false;
	private Transform poolRoot;
	[HideInInspector] public TileData data;
	//private int tileIndex;
	public int indexColumn;
	[HideInInspector] public int height;
	protected bool isClickable = true;
	protected TouchCover touchCover;
	protected bool isFinish = false;
	[SerializeField] protected BoxCollider2D box = null;
	[SerializeField] protected Vector2 colliderOffset;

	public Sprite noteTouch;
	public Sprite noteNomal;
	private Transform tf;

	private int tileIndex = 0;

	public bool IsClickable()
	{
		return isClickable;
	}

	public void Setup(TileData _data, Vector3 _spawnPos, int _tileIndex, int _column, int _height)
	{
		if (!isBonus)
		{
			tileIndex = _tileIndex;
			data = _data;
		}

		indexColumn = _column;
		tf = transform;
		sprite.sprite = noteNomal;

		var gameplayUi = InGameUIController.instance;
		Transform parent = gameplayUi.lineRoot;
		tf.SetParent(parent);
		tf.localScale = Vector3.one;
		tf.localPosition = new Vector3(indexColumn * 272, _spawnPos.y + gameplayUi.fixStartHeight, 0);

		if (box == null)
		{
			box = gameObject.GetComponent<BoxCollider2D>();
		}
		if (data.type == TileType.Normal)
		{
			//calculate collider's size
			float speedRatio = Gameplay.instance.GetSpeedRatio();
			float addY = 0;
			float colliderX = BASE_COLLIDER_WIDTH;
			if (speedRatio > 0)
			{
				addY = (speedRatio - 1) * 240;
				if (addY > 240)
				{
					addY = 240;
				}

				addY += GameConsts.ADDY_SIMPLE;

				colliderX = speedRatio < MAX_COLLIDER_EXPAND ? speedRatio * BASE_COLLIDER_WIDTH : BASE_COLLIDER_WIDTH * MAX_COLLIDER_EXPAND;
			}
			box.size = new Vector2(colliderX, 480 + addY);
			box.offset = colliderOffset + new Vector2(0, addY * 0.25f);

			height = 480;
			isClickable = true;
			isFinish = false;

			sprite.DOKill(false);
            Color color = sprite.color;
            color.a = 1;
            sprite.color = color;
        }
	}
	public void SetupInPool(Transform _rootPool)
	{
		poolRoot = _rootPool;
		Reset();
	}
	public virtual void Reset()
	{
		tf = transform;
		tf.SetParent(poolRoot);
		tf.localScale = Vector3.one;
		tf.localPosition = Vector3.zero;
		box.enabled = true;
		isClickable = true;
		isFinish = false;
		isSyncToServer = false;

	}
	public virtual void Press(TouchCover _touchCover)
	{
		touchCover = _touchCover;
		if (!isClickable)
		{
			return;
		}
		Amanotes.Utils.AudioManager.Instance.cancelSound = false;
		isClickable = false;
		isFinish = true;

		var gameplay = Gameplay.instance;

		if (data.duetNode != 0)
		{// duet note
			if (gameplay != null &&
				gameplay.CachedLevelData != null &&
				gameplay.CachedLevelData.dicDuetNote != null)
			{
				PTDuetNote duet = null;
				if (data.notes.Count > 0)
				{
					gameplay.CachedLevelData.dicDuetNote.TryGetValue(data.notes[0].indexId, out duet);
				}
				if (duet != null)
				{
					List<NoteData> list = duet.GetSuiableSoundPlay();
					if (list != null)
					{
						data.notes = list;
					}
				}
			}
		}
		MidiPlayer.Instance.PlayPianoNotes(data.notes, gameplay.GetSpeedRatio() * GameConsts.COFF_SPEED_PT, true, data.soundDelay);
		gameplay.IncreaseAndShowScore();
		gameplay.TileFinished();
		AchievementHelper.Instance.LogAchievement("totalNoteHit");
		EffectWhenFinish();
	}
	public virtual void OnKeepTouch()
	{

	}
	public virtual void OnReleaseTouch()
	{

	}

	protected virtual void EffectWhenFinish()
	{
        //sprite.sprite = noteTouch;
        Color color = sprite.color;
        color.a = 0.2f;
        sprite.color = color;

    }

	public virtual float GetDistanceAcceptPass()
	{
		return 480;
	}
	public float GetTopHeightForPass()
	{
		if (!GameManager.Instance.SessionData.autoplay && !GameManager.Instance.SessionData.listen)
		{
			return tf.localPosition.y + GetDistanceAcceptPass();
		}
		else
		{
			return tf.localPosition.y - 200;
		}
	}


	public float GetTopHeightForReset()
	{
		return tf.localPosition.y + height + 400;
	}
	public bool GetFinish()
	{
		return isFinish;
	}
	public void ResetClickable()
	{
		isClickable = true;
	}

	bool isSyncToServer = false;
	public void SetOnCompleteOutScene()
	{
		if (!isSyncToServer)
		{
			Counter.AddNote(tileIndex.ToString(), data.score);
			isSyncToServer = true;
		}
	}
}
