using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Amanotes.PianoChallenge;
using System;
using Amanotes.Utils;
using Amanotes.Utils.MessageBus;

/// <summary>
/// Touch cover: Minimal data using for Touch
/// </summary>
public class TouchCover
{
	public TouchPhase phase = TouchPhase.Canceled;
	public Vector3 position = Vector3.zero;
	public int fingerId = 0;
	public TouchCover(int _fingerId)
	{
		fingerId = _fingerId;
	}
	public void Reset()
	{
	}
}

public enum GameplayState
	{
		FINISHED,
		LOADING,
		READY,
		PLAYING,
	}

public class InGameUIController : MonoBehaviour
{
	public Camera noteCamera;
	public GameObject touchEffect;
	public GameObject prefabNoteMulti;
	public GameObject prefabNoteSimple;
	public NoteBonus prefabNoteBonus;
	public Transform poolRoot;
	public Transform lineRoot;

	[SerializeField] InfoTile songInfo = null;

	//which note to play when touch the wrong tile?
	private static readonly int PIANO_NOTE_GAME_OVER = 36;
	private int maxCacheDefault = 6;

	private Queue<NoteMulti> poolNoteMulti = new Queue<NoteMulti>();
	private Queue<NoteSimple> poolNoteSimple = new Queue<NoteSimple>();

	public Gameplay gameplay;
	public Transform cameraTouchPos;
	public Transform screenTouchPos;
	public Transform bottomRightLocation;
	public Transform objParentMove;
	public GameObject prefabDie;
	[HideInInspector]
	public Vector3 cacheDeltaMove = Vector3.zero;
	/// <summary>
	/// The list long note played data.Using for long note
	/// </summary>
	[HideInInspector]
	public Dictionary<int, LongNotePlayedData> listLongNotePlayedData = new Dictionary<int, LongNotePlayedData>();

	List<NoteSimple> listNoteActive = new List<NoteSimple>(200);
	List<NoteBonus> listBonusTiles = new List<NoteBonus>(8);

	[HideInInspector]
	public NoteTouchEffectManagement noteTouchEffect;

	public int GetCountNoteActive()
	{
		return listNoteActive.Count;
	}
	[HideInInspector]
	public float distanceCoff = 0;

	// Gameplay state
	[HideInInspector] public GameplayState gameplayState = GameplayState.FINISHED;
	/// <summary>
	/// 
	//sync for Online mode
	private float lastUpdateSync = 0;

	/// <summary>
	/// dicTouchCover cache touch cover by mouse and touch
	/// </summary>
	private Dictionary<int, TouchCover> dicTouchCover = new Dictionary<int, TouchCover>();
	/// The dic touch: using for control stage of touch
	/// </summary>
	Dictionary<int, NoteMulti> dicTouch = new Dictionary<int, NoteMulti>();

	public GameObject startObj;
	public int fixStartHeight = 660 + 480;

	float screenRatioDefault = 9.0f / 16;
	float screenRatioCurrent;
	public static float scale;

	private static int tileWidth = 272;
	//private static int tileHeightRatio169 = 480;

	public bool canTouch = true;
	public static InGameUIController instance = null;

	private float maxRect;
	private float maxSpeedOverlap;
	private float startToOverlap;
	private float distanForCheckMiss;

	private void OnEnable()
	{
		gameplay.OnGameReady -= OnGameplayReady;
		gameplay.OnGameReady += OnGameplayReady;
		gameplay.OnGameStarted -= OnGameplayStarted;
		gameplay.OnGameStarted += OnGameplayStarted;

		screenRatioCurrent = Screen.width * 1.0f / Screen.height;
		scale = screenRatioDefault / screenRatioCurrent;
        if (scale > 1)
            scale = 1;
        noteTouchEffect = new NoteTouchEffectManagement(noteCamera, touchEffect.GetComponent<NoteTouchEffect>());
		maxRect = GameManager.Instance.GameConfigs.maxRectTouch;
		maxSpeedOverlap = GameManager.Instance.GameConfigs.maxSpeedOverlapRect;
		startToOverlap = GameManager.Instance.GameConfigs.startSpeedToOverlap;
		distanForCheckMiss = GameManager.Instance.GameConfigs.distanOverByMissing;
		songInfo.gameObject.SetActive(false);

		if (startObj != null)
		{
			startObj.SetActive(false);
		}
		instance = this;
        AudioBackground.Instance.StopBackgroundMusic();
	}

	private void OnDisable()
	{
		instance = null;
		gameplay.OnGameReady -= OnGameplayReady;
		gameplay.OnGameStarted -= OnGameplayStarted;
	}

	public void PlayAutoGame()
	{
		GameManager.Instance.SessionData.listen = true;
		AnalyticsHelper.Instance.LogClickItem("Auto Play");
		startObj.SetActive(false);
		gameplay.StartGame();
	}

	public void ResetForNewGame()
	{
		distanceCoff = 0;

		noteCamera.transform.localPosition = Vector3.zero;
		startObj.GetComponent<NoteStart>().ResetForNewGame();
		for (int i = 0; i < listNoteActive.Count; i++)
		{
			PushToPool(listNoteActive[i]);
		}

		for (int b = 0; b < listBonusTiles.Count; b++)
		{
			Destroy(listBonusTiles[b].gameObject);
		}
		gameplayState = GameplayState.LOADING;
		listBonusTiles.Clear();
		listNoteActive.Clear();
		dicTouchCover.Clear();
		if (noteTouchEffect == null)
		{
			noteTouchEffect = new NoteTouchEffectManagement(noteCamera, touchEffect.GetComponent<NoteTouchEffect>());
		}
		noteTouchEffect.OnGameReset();
	}

	public void Setup()
	{
		distanceCoff = 0;

		listLongNotePlayedData.Clear();
		CacheDefault();
		System.Random rand = new System.Random();
		int ran = rand.Next() % 4;
		startObj.transform.SetParent(lineRoot);
		startObj.transform.localScale = Vector3.one;
		startObj.transform.localPosition = new Vector3(ran * tileWidth, 0, 0);
	}

	public void PushToPool(NoteSimple objNote)
	{
		if (!objNote.isLongNote)
		{
			NoteSimple simple = objNote;
			simple.Reset();
			poolNoteSimple.Enqueue(simple);
		}
		else
		{
			NoteMulti multi = (NoteMulti)objNote;
			multi.Reset();
			poolNoteMulti.Enqueue(multi);
		}
	}

	public void CacheDefault()
	{
		for (int i = 0; i < maxCacheDefault; i++)
		{
			if (poolNoteSimple.Count < maxCacheDefault)
			{
				GameObject obj = Instantiate(prefabNoteSimple) as GameObject;
				NoteSimple simple = obj.GetComponent<NoteSimple>();
				simple.isLongNote = false;
				simple.SetupInPool(poolRoot);
				obj.name = "simple_buffer";
				poolNoteSimple.Enqueue(simple);
			}
			if (poolNoteMulti.Count < maxCacheDefault)
			{
				GameObject obj = Instantiate(prefabNoteMulti) as GameObject;
				NoteMulti multi = obj.GetComponent<NoteMulti>();
				multi.isLongNote = true;
				multi.SetupInPool(poolRoot);
				obj.name = "multi_buffer";
				poolNoteMulti.Enqueue(multi);
				multi.touchKeepEffect = noteTouchEffect;
			}

		}
	}

	public NoteSimple PopSimple(string name)
	{
		if (poolNoteSimple.Count < 1)
		{
			GameObject obj = Instantiate(prefabNoteSimple) as GameObject;
			NoteSimple simple = obj.GetComponent<NoteSimple>();
			simple.isLongNote = false;
			simple.SetupInPool(poolRoot);
			obj.name = name; // "simple_buffer";
			poolNoteSimple.Enqueue(simple);
		}
		return poolNoteSimple.Dequeue();
	}

	public NoteMulti PopMulti(string name)
	{
		if (poolNoteMulti.Count < 1)
		{
			GameObject obj = Instantiate(prefabNoteMulti) as GameObject;
			NoteMulti multi = obj.GetComponent<NoteMulti>();
			multi.isLongNote = true;
			multi.SetupInPool(poolRoot);
			obj.name = name; // "multi_buffer";
			poolNoteMulti.Enqueue(multi);
			multi.touchKeepEffect = noteTouchEffect;
		}
		return poolNoteMulti.Dequeue();
	}

	public NoteBonus CreateBonusTile(BonusType type)
	{
		NoteBonus bonus = Instantiate(prefabNoteBonus);
		bonus.name = "bonus";

		//NoteBonus bonus = obj.GetComponent<NoteBonus>();
		bonus.isLongNote = false;
		bonus.bonusType = type;

		return bonus;
	}

	public void CreateNewNote(
		TileData data,
		Vector3 spawnPos,
		int tileIndex,
		int column,
		int height,
		bool withBonusTile = false,
		BonusType bonusType = BonusType.Diamond)
	{
		if (data.type == TileType.LongNote)
		{
			NoteMulti multi = PopMulti("multi_buffer");
			multi.Setup(data, spawnPos, tileIndex, column, height);
			if (listNoteActive.Contains(multi))
			{
				listNoteActive.Remove(multi);
			}
			listNoteActive.Add(multi);
		}
		else
		{
			NoteSimple simple = PopSimple("simple_buffer");
			simple.Setup(data, spawnPos, tileIndex, column, height);
			if (listNoteActive.Contains(simple))
			{
				listNoteActive.Remove(simple);
			}
			listNoteActive.Add(simple);
		}

		if (withBonusTile)
		{
			NoteBonus bonus = CreateBonusTile(bonusType);
			int bonusColumn = (column - 2 >= 0) ? column - 2 : column + 2;
			bonus.Setup(data, spawnPos, tileIndex, bonusColumn, height);
			listBonusTiles.Add(bonus);
		}
	}

	public NoteSimple GetLastNoteGenerate()
	{
		if (listNoteActive.Count > 0)
		{
			return listNoteActive[listNoteActive.Count - 1];
		}

		Debug.LogWarning("Could return last generated note, returning null");
		return null;
	}

	void Update()
	{
		if (gameplay != null)
			if (gameplay.currentState == Gameplay.GameState.Playing)
			{
				ProcessWaitPressNotemulti();
			}

	}

	Vector3 oldPos, newPos;
	public void ProcessUpdate(float speed)
	{
		Vector3 translate = Vector3.up * speed * Time.smoothDeltaTime;
		oldPos = noteCamera.transform.localPosition;
		noteCamera.transform.Translate(translate);
		newPos = noteCamera.transform.localPosition;
		cacheDeltaMove = newPos - oldPos;

		Vector3 vecBottom = GetBottomPosition();
		// kiem tra game over
		for (int i = 0; i < listNoteActive.Count; i++)
		{
			if (!listNoteActive[i].GetFinish())
			{
				//check if a tile has gone pass the designated point or not
				float topPassY = listNoteActive[i].GetTopHeightForPass();
				//if yes, continue checking
				if (vecBottom.y - distanForCheckMiss > topPassY)
				{
					//if the game is not on auto play mode, proceed to game over state
					if (!GameManager.Instance.SessionData.autoplay &&
						!GameManager.Instance.SessionData.listen)
					{
						gameplay.ChangeStatusToGameOver();
						StartCoroutine(RoutineGameOverByMissing(listNoteActive[i]));
						return;
					}
					else
					{
						if (dicTouchCover.ContainsKey(0))
						{
							listNoteActive[i].Press(dicTouchCover[0]);
						}
						else
						{
							listNoteActive[i].Press(null);
						}

						if (listNoteActive[i].isLongNote)
						{
							NoteMulti multi = (NoteMulti)listNoteActive[i];
							multi.OnShowUIWhenPress(Vector3.zero);
						}

					}
				}
				else
				{
					break;
				}
			}
		}

		List<NoteSimple> listCanReset = new List<NoteSimple>();
		for (int i = 0; i < listNoteActive.Count; i++)
		{
			if (listNoteActive[i].GetFinish())
			{
				float topPassY = listNoteActive[i].GetTopHeightForReset();

				if (vecBottom.y - distanForCheckMiss > topPassY)
				{
					if (GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.ONLINE)
					{
						listNoteActive[i].SetOnCompleteOutScene();
					}
					listCanReset.Add(listNoteActive[i]);
				}
				else
				{// co phan tu cao hon roi khong can reset
					break;
				}
			}
		}
		for (int i = 0; i < listCanReset.Count; i++)
		{
			listNoteActive.Remove(listCanReset[i]);
			listCanReset[i].Reset();
			PushToPool(listCanReset[i]);// dua vao pool
			gameplay.GenerateNextTile();// generate object khac
		}

	}
	//private int framecount = 0;
	//private int currentTouch = 0;
	bool indexTest = false;
	public void OnProcessInputControl()
	{
		//++framecount;
		if (!canTouch)
		{
			return;
		}
#if UNITY_EDITOR // in editor simulation one touch

		TouchCover touch = null;
		if (!dicTouchCover.TryGetValue(0, out touch))
		{
			touch = new TouchCover(0);
			dicTouchCover[0] = touch;
		}
		bool isUpdate = false;
		if (Input.GetMouseButtonDown(0))
		{
			//press
			touch.position = Input.mousePosition;
			touch.phase = TouchPhase.Began;
			isUpdate = true;
		}
		else if (Input.GetMouseButtonUp(0))
		{
			//release
			touch.position = Input.mousePosition;
			touch.phase = TouchPhase.Ended;
			isUpdate = true;
		}
		else if (Input.GetMouseButton(0))
		{
			//hole
			touch.position = Input.mousePosition;
			touch.phase = TouchPhase.Stationary;
			isUpdate = true;
		}
		if (isUpdate)
		{


			ProcessControlTouch(touch);

		}
#else
		// for multi-touch on device
		for (int i = 0; i < Input.touchCount; i++)
		{
			TouchCover touch = null;

			if (!dicTouchCover.TryGetValue(Input.touches[i].fingerId, out touch))
			{
				touch = new TouchCover(Input.touches[i].fingerId);
				dicTouchCover[Input.touches[i].fingerId] = touch;
			}
			touch.position = Input.touches[i].position;
			touch.phase = Input.touches[i].phase;

			ProcessControlTouch(touch);
		}

		if (Input.touchCount <= 0)
		{
			TouchCover touch = null;
			if (!dicTouchCover.TryGetValue(0, out touch))
			{
				touch = new TouchCover(0);
				dicTouchCover[0] = touch;
			}
		}
#endif
	}

	private Vector2 DiagonPointYWithSpeedNote(Vector2 vec)
	{
		Vector2 diagonPoint = Vector2.zero;
		float percentOverlap = ((gameplay.GetSpeedNotes() - startToOverlap) / (maxSpeedOverlap - startToOverlap));
		if (percentOverlap < 0)
			percentOverlap = 0;
		else if (percentOverlap > 1)
			percentOverlap = 1;
		diagonPoint.x = vec.x + 0.001f;
		diagonPoint.y = vec.y - Mathf.Lerp(0, maxRect, percentOverlap);
		return diagonPoint;
	}

	public void ProcessControlTouch(TouchCover touch)
	{
		if (gameplayState < GameplayState.READY)
		{
			return;
		}

		if (touch == null)
		{
			return;
		}

		//Touch Press
		if (touch.phase == TouchPhase.Began)
		{
			Vector2 rayOrigin = noteCamera.ScreenToWorldPoint(touch.position);

			Vector2 diagonPoint = DiagonPointYWithSpeedNote(rayOrigin);
			Collider2D hit = Physics2D.OverlapArea(rayOrigin, diagonPoint, ProjectConstants.Layers.MainGameMask);

			if (hit && hit.transform != null)
			{
				string hitObjName = hit.transform.name.ToLower();
				if (gameplay.CurrentGameStage != Gameplay.GameState.Playing)
				{
					if (hitObjName.Contains("start"))
					{
						//game start
						NoteStart noteStart = hit.transform.gameObject.GetComponent<NoteStart>();
						if (noteStart != null)
						{
							noteStart.Press(touch);
						}
					}

				}

				if (gameplay.CurrentGameStage == Gameplay.GameState.Playing ||
					gameplay.CurrentGameStage == Gameplay.GameState.Continue ||
					gameplay.CurrentGameStage == Gameplay.GameState.AutoPlay)
				{

					if (gameplay.CurrentGameStage == Gameplay.GameState.Continue)
					{
						gameplay.StartGame();
					}

					if (hitObjName.Contains("simple_"))
					{
						NoteSimple simple = hit.transform.gameObject.GetComponent<NoteSimple>();
						if (simple != null && CanTouchNote(simple))
						{
							simple.Press(touch);
						}
					}
					else if (hitObjName.Contains("multi_"))
					{
						NoteMulti multi = hit.transform.gameObject.GetComponent<NoteMulti>();
						if (multi != null && CanTouchNote(multi))
						{
							multi.Press(touch);
							multi.OnShowUIWhenPress(GetRootPositionHit(touch));
						}
					}
					else if (hitObjName.Contains("bonus"))
					{
						NoteBonus bonus = hit.transform.gameObject.GetComponent<NoteBonus>();
						if (bonus != null)
						{
							bonus.Press(null);
						}
					}
				}
			}
			else
			{
				//if the touch didn't hit any note, check for hit on background
				RaycastHit2D bgCheck = Physics2D.Raycast(rayOrigin, Vector2.zero, 100, ProjectConstants.Layers.BackgroundMask);
				if (bgCheck.transform != null)
				{
					if (gameplay.CurrentGameStage == Gameplay.GameState.Playing ||
						gameplay.CurrentGameStage == Gameplay.GameState.AutoPlay)
					{
						if (CheckGameOver(touch))
						{
							GameOverByPressWrong(touch);
						}
					}
					else if (gameplay.CurrentGameStage == Gameplay.GameState.Continue)
					{
						gameplay.StartGame();
					}
				}
			}
		}
		// Touch Hold
		else if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
		{
			OnHoldTouch(touch);

		}
		else
		{
			ResetTouch(touch);
		}
	}

	public Vector3 GetRootPositionHit(TouchCover touch)
	{
		var cameraPos = noteCamera.ScreenToWorldPoint(touch.position);
		screenTouchPos.transform.position = cameraPos;
		return screenTouchPos.transform.localPosition;
	}

	public Vector3 GetBottomPosition()
	{
		screenTouchPos.transform.position = bottomRightLocation.transform.position;
		return screenTouchPos.transform.localPosition;
	}

	public void CacheTouchForNote(TouchCover touch, NoteMulti multi)
	{
		dicTouch[touch.fingerId] = multi;
	}

	/// <summary>
	/// Resets the touch. When release of cancel touch
	/// </summary>
	/// <param name="touch">Touch.</param>
	public void ResetTouch(TouchCover touch)
	{
		if (dicTouch.ContainsKey(touch.fingerId))
		{
			dicTouch[touch.fingerId].OnKeepTouchEnd();
			dicTouch.Remove(touch.fingerId);
		}
		if (dicTouchCover.ContainsKey(touch.fingerId))
		{
			dicTouchCover.Remove(touch.fingerId);
		}
	}

	public void ClearCacheTouchForNote(TouchCover touch, NoteMulti multi)
	{
		if (dicTouch.ContainsKey(touch.fingerId))
		{
			NoteMulti multiCache = dicTouch[touch.fingerId];
			if (multi == multiCache)
			{
				dicTouch.Remove(touch.fingerId);
			}
		}
	}

	public void OnHoldTouch(TouchCover touch)
	{
		if (dicTouch.ContainsKey(touch.fingerId))
		{
			NoteMulti multi = dicTouch[touch.fingerId];

			//check notes multi, if noteMulti Completed play remove it in dicTouch 
			if (multi.IsMoveCompleted())
			{

				ResetTouch(touch);
			}//

			multi.OnKeepTouch();
			multi.IncreaseActiveHeight(cacheDeltaMove.y);
		}
	}

	#region GameOver by press wrong

	public void GenerateNoteDie(TouchCover touch)
	{
		Vector3 posGenererate = Vector3.zero;
		float heightGenerate = 480;
		NoteSimple note = null;
		Vector3 vec = GetRootPositionHit(touch);
		posGenererate = vec;
		for (int i = 0; i < listNoteActive.Count; i++)
		{
			float min = listNoteActive[i].transform.localPosition.y;
			float max = min + listNoteActive[i].height;
			if (vec.y >= min && vec.y <= max)
			{
				note = listNoteActive[i];
			}
		}
		if (note != null)
		{
			int x = (int)(posGenererate.x / tileWidth);
			posGenererate = note.transform.localPosition;
			posGenererate.x = x * tileWidth;
			heightGenerate = note.height;
		}
		else
		{
			//Should never go to here
			int x = (int)(vec.x / tileWidth);
			posGenererate.x = x * tileWidth;
			heightGenerate = vec.y;
		}

		GameObject objDie = GameObject.Instantiate(prefabDie);
		objDie.transform.parent = lineRoot;
		objDie.transform.localScale = Vector3.one;
		objDie.transform.localPosition = posGenererate;
		objDie.GetComponent<NoteDie>().Setup(heightGenerate);
		GameObject.Destroy(objDie, 2f);
	}

	public void GameOverByPressWrong(TouchCover touch)
	{
		AchievementHelper.Instance.LogAchievement("totalMiss");
		gameplay.ChangeStatusToGameOver();
		StartCoroutine(RoutineGameOverByPressWrong(touch));
	}

	public IEnumerator RoutineGameOverByPressWrong(TouchCover touch)
	{
		//visualize where the touch gone wrong
		GenerateNoteDie(touch);
		MidiPlayer.Instance.PlayPianoNote(PIANO_NOTE_GAME_OVER);
		yield return new WaitForSeconds(0.4f);

		if (listNoteActive.Count > 1)
		{
			Vector3 bottom = GetBottomPosition();
			int stopIndex = 0;
			//if (bottom.y > listNoteActive [1].transform.localPosition.y)
			for (int i = 0; i < listNoteActive.Count; i++)
			{
				if (listNoteActive[i].IsClickable())
				{
					stopIndex = i;
					break;
				}
			}

			//calculate the distance need to move back the camera
			float yMove = bottom.y - listNoteActive[stopIndex].transform.localPosition.y;
			Vector3 vecStart = noteCamera.transform.localPosition;
			Vector3 vecEnd = vecStart;

			vecEnd.y -= yMove * scale;

			//reset the last note
			listNoteActive[stopIndex].ResetClickable();
			//move back camera a bit
			PianoUtils.DoTweenMoveEasyType(noteCamera.gameObject, gameObject, vecStart, vecEnd, "", Framework.iTween.EaseType.easeInOutQuad, 0.5f);
		}

		yield return new WaitForSeconds(0.8f);
		if (GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.NORMAL)
		{
			gameplay.ProcessGameOverEvent();
		}
		else
		{
			OnlineGameOver();
		}

	}

	public IEnumerator RoutineGameOverByMissing(NoteSimple noteDie)
	{
		MidiPlayer.Instance.PlayPianoNote(PIANO_NOTE_GAME_OVER);
		yield return new WaitForSeconds(0.4f);
		Vector3 vecStart = noteCamera.transform.localPosition;
		Vector3 vecEnd = vecStart;

		vecEnd.y -= (noteDie.GetDistanceAcceptPass() * scale + distanForCheckMiss);

		PianoUtils.DoTweenMoveEasyType(noteCamera.gameObject, gameObject, vecStart, vecEnd, "", Framework.iTween.EaseType.easeInOutQuad, 0.5f);
		yield return new WaitForSeconds(0.4f);
		StartCoroutine(EffectDieByMissing(noteDie));
		yield return new WaitForSeconds(0.5f);

		noteDie.ResetClickable();// dung cho replay
		if (GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.NORMAL)
		{
			gameplay.ProcessGameOverEvent();
		}
		else
		{
			OnlineGameOver();
		}
	}

	void OnlineGameOver()
	{
	}

	List<NoteMulti> LnoteMultiwaitpress = new List<NoteMulti>();
	public bool CheckGameOver(TouchCover touch)
	{
		Vector3 posTouch = noteCamera.ScreenToWorldPoint(touch.position);

		int touchIndex = (int)touch.position.x / (Screen.width / 4);


		NoteSimple note = null;
		Vector3 vec = GetRootPositionHit(touch);

		for (int i = 0; i < listNoteActive.Count && i < 4; i++)
		{// so sanh nhung thang cuoi cung thoi
			float bottomPassY = listNoteActive[i].transform.position.y;
			float delta = Math.Abs(posTouch.y - bottomPassY);

			if (listNoteActive[i].indexColumn == touchIndex && delta <= 0.18f && listNoteActive[i].IsClickable())
			{
				if (listNoteActive[i].gameObject.name.Contains("multi_"))
				{
					NoteMulti notem = (NoteMulti)listNoteActive[i];
					notem.posWaitTouch = touch.position;
					notem.touchWait = touch;
					LnoteMultiwaitpress.Add(notem);
				}
				else
				{
					listNoteActive[i].Press(touch);
				}
				return false;
			}

			float min = listNoteActive[i].transform.localPosition.y;
			float max = min + listNoteActive[i].height;
			//check if the touch on background has any tiles with the same Y?
			if (vec.y >= min && vec.y <= max)
			{
				//if yes, check if there is any tile at that exact location?
				float minX = listNoteActive[i].transform.localPosition.x;
				float maxX = minX + tileWidth;
				if (vec.x >= minX && vec.x <= maxX)
				{
					//if yes, then the touch is fine, because may be we touch on an used tile
					return false;
				}
				else
				{
					//if not touch on any tiles, meaning the user has missed it
					note = listNoteActive[i];
					break;
				}
			}
		}
		//if there are no tile next to current touch
		if (note == null)
		{
			//mean the touch is fine
			return false;
		}
		else
		{
			if (!note.IsClickable())
				return false;
		}
		
		return true;
	}

	void ProcessWaitPressNotemulti()
	{
		if (LnoteMultiwaitpress != null)
		{
			int count = LnoteMultiwaitpress.Count;
			for (int i = 0; i < count; i++)
			{
				Vector3 posTouch = noteCamera.ScreenToWorldPoint(LnoteMultiwaitpress[i].posWaitTouch);
				if (LnoteMultiwaitpress[i].transform.position.y <= posTouch.y)
				{
					if (LnoteMultiwaitpress[i].IsClickable())
					{
						LnoteMultiwaitpress[i].Press(LnoteMultiwaitpress[i].touchWait);
						LnoteMultiwaitpress[i].OnShowUIWhenPress(GetRootPositionHit(LnoteMultiwaitpress[i].touchWait));
						LnoteMultiwaitpress.RemoveAt(i);
						count--;
						i--;
					}
				}
			}
		}
	}

	#endregion

	private IEnumerator EffectDieByMissing(NoteSimple noteDie)
	{
		//flash 4 times
		for (int i = 0; i < 8; i++)
		{
			if (i % 2 == 0)
			{
				noteDie.gameObject.SetActive(true);
			}
			else
			{
				noteDie.gameObject.SetActive(false);
			}
			yield return new WaitForSeconds(0.1f);
		}
		noteDie.gameObject.SetActive(true);
	}

	/// <summary>
	/// Check if this note can register touch or not
	/// </summary>
	/// <param name="note">The note to check</param>
	/// <returns>Is the specified note eligible for touching</returns>
	private bool CanTouchNote(NoteSimple note)
	{
		for (int i = 0; i < listNoteActive.Count; i++)
		{
			//get the last clickable note
			if (listNoteActive[i].IsClickable())
			{
				//if that is the same note as we are checking, return true
				if (listNoteActive[i] == note)
				{
					return true;
				}
				else
				{
					//or else, continue to check
					//if the lowest note is a dual note
					if (listNoteActive[i].data.subType == TileType.Dual)
					{
						//special case, if i == 0
						if (i == 0)
						{
							//only check for the next tile, since there are no tile before
							if (listNoteActive[1] == note)
							{
								return true;
							}

							return false;
						}
						//special case, if i is at the end of the list
						else if (i == listNoteActive.Count - 1)
						{
							//only check for the previous tile since there are no tile after
							if (listNoteActive[listNoteActive.Count - 2] == note)
							{
								return true;
							}

							return false;
						}
						else
						{
							//if the note right before or after this clickable note is the one we are checking, return true
							if (listNoteActive[i - 1] == note ||
								listNoteActive[i + 1] == note)
							{
								return true;
							}
						}

						//all else, false
						return false;
					}
					else
					{
						return false;
					}
				}
			}
		}
		return false;
	}

	private NoteSimple GetLastNoteCanTouch()
	{
		for (int i = 0; i < listNoteActive.Count; i++)
		{
			if (listNoteActive[i].IsClickable())
			{
				return listNoteActive[i];
			}
		}
		return null;
	}

	public void CallSync(int gameStatus)
	{
	}

	private void OnGameplayReady()
	{
		songInfo.gameObject.SetActive(true);

		var gm = GameManager.Instance;

		bool premium = false;
		int highScore = 0;
		SongDataModel song;
		if (gm.SessionData.playMode == GAME_PLAY_MODE.ONLINE)
		{
			song = gm.SessionData.song;
			premium = gm.GameConfigs.battlesPremiumSongs.Contains(song.ID);
		}
		else // single play
		{
			song = gm.SessionData.song;
			highScore = HighScoreManager.Instance.GetHighScore(song.storeID);
			startObj.SetActive(true);
		}
		songInfo.SetSongInfo(song.name, highScore, premium);

		gameplayState = GameplayState.READY;
	}

	private void OnGameplayStarted()
	{
		gameplayState = GameplayState.PLAYING;
		songInfo.gameObject.SetActive(false);
	}
}

