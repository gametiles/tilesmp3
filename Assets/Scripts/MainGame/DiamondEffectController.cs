﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Amanotes.PianoChallenge
{
    public class DiamondEffectController : SingletonMono<DiamondEffectController>
    {
       public Transform[] listDiamondIMG;
       public Transform tagetTranform;
       public Animation anmTop;
        [SerializeField]
        UILabel txtDiamond;

        public void PlayEffect(Vector3 diamondPos)
        {
            txtDiamond.text = "+"+GameManager.Instance.GameConfigs.diamondDrop.ToString();
            for (int i = 0; i < listDiamondIMG.Length; i++) 
            {
                if(!listDiamondIMG[i].gameObject.activeSelf)
                {
                    listDiamondIMG[i].gameObject.SetActive(true);
                    anmTop.Play();
                    DOTween.Sequence().Append(listDiamondIMG[i].DOMove(tagetTranform.position,.8f)).Play().OnComplete(() => 
                    { 

                        listDiamondIMG[i].gameObject.SetActive(false);
                        listDiamondIMG[i].localPosition = Vector3.one;

                    });
                    break;
                }
            }
        }
    }
}
