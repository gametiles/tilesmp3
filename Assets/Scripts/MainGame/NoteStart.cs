using UnityEngine;
using Framework;
using Amanotes.PianoChallenge;

public class NoteStart : NoteSimple
{
	public GameObject textStart;
	public GameObject loadingAnimation;

	public override void Press(TouchCover _touchCover)
	{
		if (!isClickable)
		{
			return;
		}
		Gameplay.instance.StartGame();
		isClickable = false;
		// access game
		gameObject.SetActive(false);

		if (GameManager.Instance.SessionData.playMode != GAME_PLAY_MODE.ONLINE)
		{
			Amanotes.Utils.AnalyticsHelper.Instance.LogSongStart(GameManager.Instance.SessionData.song);
		}
	}

	public void ResetForNewGame()
	{
		gameObject.SetActive(true);
		iTween[] listOld = gameObject.GetComponents<iTween>();
		for (int i = 0; i < listOld.Length; i++)
		{
			if (listOld[i] != null)
			{
				Destroy(listOld[i]);
			}
		}
		Color color = sprite.color;
		color.a = 1f;
		sprite.color = color;
		isClickable = true;
		isFinish = false;
	}

	public void SetLoading(bool show)
	{
		if (show)
		{
			isClickable = false;
			if (textStart != null)
				textStart.SetActive(false);
			loadingAnimation.SetActive(true);
		}
		else
		{
			isClickable = true;
			if (textStart != null)
				textStart.SetActive(true);
			loadingAnimation.SetActive(false);
		}
	}
}
