using UnityEngine;
namespace Amanotes.PianoChallenge
{
	public class NoteBonus : NoteSimple
	{
		public BonusType bonusType;
		public Transform objMove;
		public override void Press(TouchCover _touchCover)
		{
			//this.touchCover = _touchCover;
			if (!isClickable)
			{
				return;
			}
			isClickable = false;
			isFinish = true;
			Gameplay.instance.ProcessBonusTile(this);
			//Debug.Log("goeffect");
			DiamondEffectController.Instance.PlayEffect(transform.position);
			EffectWhenFinish();
			AchievementHelper.Instance.LogAchievement("pickDiamondInGame");
		}
	}
}
