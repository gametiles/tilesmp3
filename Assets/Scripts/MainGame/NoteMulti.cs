using UnityEngine;
using System.Collections.Generic;
using Amanotes.PianoChallenge;
using DG.Tweening;

public class NoteMulti : NoteSimple
{
	public const int MAX_COLLIDER_ADDITIONAL_SIZE = 240;
	// Use this for initialization
	[SerializeField] LongNotePlayedData playData = null;
	[SerializeField] SpriteRenderer headSprite = null;
	[SerializeField] SpriteRenderer tailSprite = null;
	[SerializeField] SpriteRenderer lineSprite = null;

	[HideInInspector] public Vector3 posWaitTouch;
	[HideInInspector] public TouchCover touchWait;

	[SerializeField] Animator blinkEffect = null;

	public NoteTouchEffectManagement touchKeepEffect;

	[SerializeField] Sprite sprite43Ratio = null;

	[SerializeField] Sprite[] spriteHead = null;
	[SerializeField] GameObject objActive = null;
	private float cacheYHeight = 0;
	private bool isMoveComplete = false;

	private bool isActiveAuto = false;
	[SerializeField] NumericSprite lbScoreLabel = null;

	private static Dictionary<int, string> listScoreText = new Dictionary<int, string>(100);

	// Update is called once per frame
	void Update()
	{
		if (isActiveAuto)
		{
			if (!isMoveComplete)
			{
				IncreaseActiveHeight(InGameUIController.instance.cacheDeltaMove.y);
			}
			if (!isClickable)
			{
				OnKeepTouch();
			}
		}
	}

	private string GetScoreText(int score)
	{
		if (!listScoreText.ContainsKey(score))
		{
			listScoreText[score] = string.Format("+{0}", score);
		}

		return listScoreText[score];
	}

	public new void Setup(TileData _data, Vector3 _spawnPos, int _tileIndex, int _column, int _height)
	{
		base.Setup(_data, _spawnPos, _tileIndex, _column, _height);
		lbScoreLabel.text = GetScoreText(_data.score);
		lbScoreLabel.gameObject.SetActive(false);

		sprite.sprite = sprite43Ratio;
		height = _height;
		if (box == null)
		{
			box = gameObject.GetComponent<BoxCollider2D>();
		}
		float speedRatio = Gameplay.instance.GetSpeedRatio();
		float addY = 0;
		float colliderX = BASE_COLLIDER_WIDTH;
		if (speedRatio > 0)
		{
			addY = (speedRatio - 1) * MAX_COLLIDER_ADDITIONAL_SIZE;
			if (addY > MAX_COLLIDER_ADDITIONAL_SIZE)
			{
				addY = MAX_COLLIDER_ADDITIONAL_SIZE;
			}

			addY += 100;
			colliderX = speedRatio < MAX_COLLIDER_EXPAND ? speedRatio * BASE_COLLIDER_WIDTH : BASE_COLLIDER_WIDTH * MAX_COLLIDER_EXPAND;
		}
		box.size = new Vector2(colliderX, 700 + addY);
		box.offset = colliderOffset + new Vector2(0, addY * 0.25f);

		Vector3 scale = sprite.transform.localScale;
		scale.y = (int)(_height / 4.80f);
		sprite.transform.localScale = scale;
		lbScoreLabel.transform.localPosition = new Vector3(40, height + 90);
		scale.y = (int)((_height / 3.47f) - 45);
		scale.x = 600;
		lineSprite.transform.localScale = scale;
		lineSprite.gameObject.SetActive(true);
		playData = new LongNotePlayedData();
		playData.timeStampBeginTouch = 0;
		playData.noteDataPlayedIndex = -1;

		objActive.SetActive(false);
		headSprite.sprite = spriteHead[0];
		cacheYHeight = 0;
		isClickable = true;
		isFinish = false;
		isMoveComplete = false;
		isActiveAuto = false;

		posWaitTouch = Vector3.zero;
		touchWait = null;

		sprite.DOKill();
		sprite.color = Color.white;
        sprite.gameObject.SetActive(true);
    }

	public override void Press(TouchCover _touchCover)
	{
		touchCover = _touchCover;
		if (!isClickable)
		{
			return;
		}
		Amanotes.Utils.AudioManager.Instance.cancelSound = false;
		isClickable = false;
		isFinish = true;
		isMoveComplete = false;
		if (touchCover != null)
		{
			InGameUIController.instance.CacheTouchForNote(touchCover, this);
		}

		var gameplay = Gameplay.instance;

		if (data.duetNode != 0)
		{// duet note
			if (gameplay != null && gameplay.CachedLevelData != null && gameplay.CachedLevelData.dicDuetNote != null)
			{

				PTDuetNote duet = null;
				if (data.notes.Count > 0)
				{
					gameplay.CachedLevelData.dicDuetNote.TryGetValue(data.notes[0].indexId, out duet);
				}
				if (duet != null)
				{
					List<NoteData> list = duet.GetSuiableSoundPlay();
					if (list != null)
					{
						data.notes = list;
					}
				}
			}
		}
		MidiPlayer.Instance.PlayPianoNotes(data.notes, gameplay.GetSpeedRatio() * GameConsts.COFF_SPEED_PT, true, data.soundDelay);
		gameplay.IncreaseAndShowScore();
		gameplay.TileFinished();
		AchievementHelper.Instance.LogAchievement("totalNoteHit");
		if (GameManager.Instance.SessionData.autoplay || GameManager.Instance.SessionData.listen)
		{
			isActiveAuto = true;
		}
		blinkEffect.gameObject.SetActive(true);
		blinkEffect.StartPlayback();

		if (!GameManager.Instance.SessionData.listen && !isActiveAuto)
		{
			touchKeepEffect.OnTouchDown(gameObject, touchCover);
		}
	}

	public void OnShowUIWhenPress(Vector3 posTouch)
	{
		posTouch.y += 160;
		float yHeight = posTouch.y - transform.localPosition.y;
		if (yHeight < 320)
		{
			yHeight = 320;
		}
		cacheYHeight = yHeight;
		SetupActiveHeight();
		objActive.SetActive(true);
		headSprite.gameObject.SetActive(true);

	}

	public void IncreaseActiveHeight(float addY)
	{
		cacheYHeight += addY / InGameUIController.scale;
		if (cacheYHeight >= height)
		{
            tailSprite.transform.localScale = new Vector3(sprite.transform.localScale.x, tailSprite.transform.localScale.y, tailSprite.transform.localScale.z);
			OnReleaseTouch();
		}
		else
		{
			SetupActiveHeight();
		}
	}

	public void SetupActiveHeight()
	{
		float heightActive = cacheYHeight;
		if (heightActive >= height)
		{
			heightActive = height;
		}
		Vector3 scale = tailSprite.transform.localScale;
		float scaleY = (heightActive / 4.8f) - 10 / InGameUIController.scale;
		scale.y = scaleY;
		tailSprite.transform.localScale = scale;

		Vector3 posHead = headSprite.transform.localPosition;
		posHead.y = heightActive - 48 / InGameUIController.scale;
		headSprite.transform.localPosition = posHead;

	}

	public bool IsMoveCompleted()
	{
		return isMoveComplete;
	}
	public override float GetDistanceAcceptPass()
	{
		return 700;
	}

	// if the tile is hold, prepare to play its music
	List<NoteData> notesToPlay = new List<NoteData>(15);
	public override void OnKeepTouch()
	{

	}

	public override void OnReleaseTouch()
	{
		if (!isMoveComplete)
		{
			isMoveComplete = true;

			lbScoreLabel.gameObject.SetActive(true);
			//increase tile's score minus one because we have already increased it when first touched
			Gameplay.instance.IncreaseAndShowScore(data.score - 1);
			EffectWhenFinish();

			blinkEffect.StopPlayback();
			blinkEffect.gameObject.SetActive(false);
		}
	}

	public void OnKeepTouchEnd()
	{
		blinkEffect.StopPlayback();
		blinkEffect.gameObject.SetActive(false);
		touchKeepEffect.OnTouchUp(gameObject);

		if (!isMoveComplete)
			Amanotes.Utils.AudioManager.Instance.cancelSound = true;
	}

	protected override void EffectWhenFinish()
	{
        sprite.gameObject.SetActive(false);
        headSprite.gameObject.SetActive(false);
		lineSprite.gameObject.SetActive(false); 
	}

	public override void Reset()
	{
		base.Reset();

	}
}
