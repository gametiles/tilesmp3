using UnityEngine;
using System.Collections;

public class CheckScreenAPI : SingletonMono<CheckScreenAPI>
{

	public int HD_WIDTH = 720;
	public int HD_HEIGHT = 1280;

	public int SD_WIDTH = 720;
	public int SD_HEIGHT = 1280;

	public int TOTAL_PROCESSOR_REQUIRE = 5000;
	public int MAX_TEXTURE_REQUIRE = 2048;

	public void ResolutionHD()
	{
		StartCoroutine(RoutineCheckScreenReference(HD_WIDTH, HD_HEIGHT));

	}
	public void ResolutionSD()
	{
		StartCoroutine(RoutineCheckScreenReference(SD_WIDTH, SD_HEIGHT));
	}

	private void CheckScreenReference(int maxWidth, int defautHeight)
	{
		StartCoroutine(RoutineCheckScreenReference(maxWidth, defautHeight));
	}

	private IEnumerator RoutineCheckScreenReference(int maxWidth, int defautHeight)
	{
		int width = Screen.width;
		int height = Screen.height;

		if (width > maxWidth)
		{
			int newWidth = maxWidth;
			int newHeight = Mathf.RoundToInt(height * 1.0f * newWidth / width);
			if (Mathf.Abs(newHeight - defautHeight) < 4)
			{
				newHeight = defautHeight;
			}
			yield return new WaitForEndOfFrame();
			Screen.SetResolution(newWidth, newHeight, true);
		}
	}

}
