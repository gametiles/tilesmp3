

// Code that runs in development build only
class Development
{
	public static void Log(string s)
	{
#if UNITY_EDITOR || DEVELOPMENT_BUILD || true
		UnityEngine.Debug.Log(s);
#endif
	}
}
