
using Parse;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace fModStudio
{
	public static class Utils
	{
		public static int SecondsSinceEpoch()
		{
			DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			int seconds = (int)(DateTime.UtcNow - epochStart).TotalSeconds;
			return seconds;
		}

		public static string Format2Digit(int n)
		{
			return n > 9 ? n.ToString() : ("0" + n.ToString());
		}

		public static string FormatHHMMSS(int seconds)
		{
			int hours = seconds / 3600;
			int minutes = (seconds % 3600) / 60;
			seconds = seconds % 60;
			return Format2Digit(hours) + ":" + Format2Digit(minutes) + ":" + Format2Digit(seconds);
		}

		public static void DownloadTexture(string url, Action<Texture2D> onFinished)
		{
			AssetDownloader.Instance.DownloadAndCacheAsset(url, 0, null, null, (WWW www) => {
				onFinished(www.texture);
			});
		}

		public static void DownloadSprite(string url, Action<Sprite> onFinished)
		{
			AssetDownloader.Instance.DownloadAndCacheAsset(url, 0, null, null, (WWW www) => {
				Texture2D texture = www.texture;
				Rect rec = new Rect(0, 0, texture.width, texture.height);
				Sprite sprite = Sprite.Create(texture, rec, new Vector2(0, 0));
				try
				{
					onFinished(sprite);
				}
				catch { }
			});
		}
		
		public static void CallParseFunction(
			string funcName,
			Dictionary<string, object> parameters,
			Action<System.Threading.Tasks.Task<object>> callback = null)
		{
			ParseCloud.CallFunctionAsync<object>(funcName, parameters).
			ContinueWith(task =>
			{
				DoOnMainThread.Instance.QueueOnMainThread(() =>
				{
					if (callback != null)
					{
						callback(task);
					}
				});
			});
		}

		public static bool ListsEqual<T>(List<T> a, List<T> b)
		{
			if (ReferenceEquals(a, b))
			{
				return true;
			}

			if (a == null || b == null)
			{
				return false;
			}

			if (a.Count != b.Count)
			{
				return false;
			}

			for (int i = 0; i < a.Count; i++)
			{
				if (!a[i].Equals(b[i]))
				{
					return false;
				}
			}

			return true;
		}
	}

}
