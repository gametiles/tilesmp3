﻿
namespace Amanotes.Analytics {
    public interface IAnalyticService {
        void Initialize(params object[] args);
        void LogGameStarted(params object[] args);
        void LogUserNavigation(params object[] args);
        void LogUserInteraction(params object[] args);
        void LogUserProgression(params object[] args);
        void LogFinishLevel(params object[] args);
    }
}