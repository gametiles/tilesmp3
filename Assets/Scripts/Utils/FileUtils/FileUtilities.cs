using UnityEngine;
using System.IO;
using FullSerializer;

/// <summary>
/// Helper class to deal with every task related to files and folder
/// </summary>
public class FileUtilities
{
	public static readonly fsSerializer JSONSerializer = new fsSerializer();
	private static System.Security.Cryptography.RijndaelManaged rijndael = new System.Security.Cryptography.RijndaelManaged();

	/// <summary>
	/// Read a text file from local storage and decrypt it as needed
	/// </summary>
	/// <param name="filePath">Where the file is saved</param>
	/// <param name="password">If not null, will be used to decrypt the file</param>
	/// <param name="isAbsolutePath">Is the file path an absolute one?</param>
	/// <returns></returns>
	public static string LoadFileWithPassword(string filePath, string password = null, bool isAbsolutePath = false)
	{
		var bytes = LoadFile(filePath, isAbsolutePath);
		if (bytes != null)
		{
			string text = System.Text.Encoding.UTF8.GetString(bytes);
			if (!string.IsNullOrEmpty(password))
			{
				string decrypt = Encryption.Decrypt(text, password);
				if (string.IsNullOrEmpty(decrypt))
				{
					return null;
				}
				else
				{
					return decrypt;
				}
			}
			else
			{
				return text;
			}
		}
		else
		{
			return null;
		}
	}

	public static byte[] DecryptBinaryFile(string filePath, byte[] password = null, bool isAbsolutePath = false)
	{
		var bytes = LoadFile(filePath, isAbsolutePath);
		rijndael.Key = password;

		if (bytes != null)
		{
			if (password != null)
			{
				byte[] result = Encryption.Decrypt(bytes, rijndael);
				return result;
			}
		}

		return null;
	}

	public static string EncryptBinaryFile(byte[] content, string filePath, byte[] password = null, bool isAbsolutePath = false)
	{
		if (password != null)
		{
			rijndael.Key = password;
		}
		return SaveFile(content, filePath, isAbsolutePath);
	}
	/// <summary>
	/// Read a file at specified path
	/// </summary>
	/// <param name="filePath">Path to the file</param>
	/// <param name="isAbsolutePath">Is this path an absolute one?</param>
	/// <returns>Data of the file, in byte[] format</returns>
	public static byte[] LoadFile(string filePath, bool isAbsolutePath = false)
	{
		if (filePath == null || filePath.Length == 0)
		{
			return null;
		}

		string absolutePath = filePath;
		if (!isAbsolutePath) { absolutePath = GetWritablePath(filePath); }

		if (File.Exists(absolutePath))
		{
			return File.ReadAllBytes(absolutePath);
		}
		else
		{
			return null;
		}
	}

	/// <summary>
	/// Check if a file is existed or not
	/// </summary>
	public static bool IsFileExist(string filePath, bool isAbsolutePath = false)
	{
		if (filePath == null || filePath.Length == 0)
		{
			return false;
		}

		string absolutePath = filePath;
		if (!isAbsolutePath) { absolutePath = GetWritablePath(filePath); }

		return (File.Exists(absolutePath));
	}

	public static string SaveFileWithPassword(string content, string filePath, string password = null, bool isAbsolutePath = false)
	{
		var bytes = System.Text.Encoding.UTF8.GetBytes(content);
		if (!string.IsNullOrEmpty(password))
		{
			string encrypt = Encryption.Encrypt(bytes, password);
			bytes = System.Text.Encoding.UTF8.GetBytes(encrypt);
		}
		return SaveFile(bytes, filePath, isAbsolutePath);
	}



	/// <summary>
	/// Save a byte array to storage at specified path and return the absolute path of the saved file
	/// </summary>
	/// <param name="bytes">Data to write</param>
	/// <param name="filePath">Where to save file</param>
	/// <param name="isAbsolutePath">Is this path an absolute one or relative</param>
	/// <returns>Absolute path of the file</returns>
	public static string SaveFile(byte[] bytes, string filePath, bool isAbsolutePath = false)
	{
		if (filePath == null || filePath.Length == 0)
		{
			return null;
		}

		//path to the file, in absolute format
		string path = filePath;
		if (!isAbsolutePath)
		{
			path = GetWritablePath(filePath);
		}
		//create a directory tree if not existed
		string folderName = Path.GetDirectoryName(path);
		if (!Directory.Exists(folderName))
		{
			Directory.CreateDirectory(folderName);
		}

		//write file to storage
		File.WriteAllBytes(path, bytes);

		return path;
	}

	/// <summary>
	/// Return a path to a writable folder on a supported platform
	/// </summary>
	/// <param name="relativeFilePath">A relative path to the file, from the out most writable folder</param>
	/// <returns></returns>
	public static string GetWritablePath(string relativeFilePath)
	{
		string result = "";

#if UNITY_EDITOR
		result = Application.dataPath.Replace("Assets", "DownloadedData") + Path.DirectorySeparatorChar + relativeFilePath;
#elif UNITY_ANDROID
		result = Application.persistentDataPath + Path.DirectorySeparatorChar + relativeFilePath;
#elif UNITY_IPHONE
		result = Application.persistentDataPath + Path.DirectorySeparatorChar + relativeFilePath;
#elif UNITY_WP8 || NETFX_CORE
		result = Application.persistentDataPath + "/" + relativeFilePath;
#endif

		return result;
	}

	/// <summary>
	/// Delete a file from storage using default setting
	/// </summary>
	/// <param name="filePath">The path to the file</param>
	/// <param name="isAbsolutePath">Is this file path an absolute path or relative one?</param>
	public static void DeleteFile(string filePath, bool isAbsolutePath = false)
	{
		if (filePath == null || filePath.Length == 0)
			return;

		if (isAbsolutePath)
		{
			if (System.IO.File.Exists(filePath))
			{
				System.IO.File.Delete(filePath);
			}
		}
		else
		{
			string file = GetWritablePath(filePath);
			DeleteFile(file);
		}
	}

	/// <summary>
	/// Get a "safe" file name for current platform so that it can be access without problem
	/// </summary>
	/// <param name="fileName">File name to sanitize</param>
	/// <returns></returns>
	public static string SanitizeFileName(string fileName)
	{
		string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(Path.GetInvalidFileNameChars()));
		string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

		return StripVietnameseAccent(System.Text.RegularExpressions.Regex.Replace(fileName, invalidRegStr, "_"));
	}


	//string to be replaced to sanitize Vietnamese strings
	private static readonly string[] VietnameseSigns = new string[]{
		"aAeEoOuUiIdDyY",
		"áàạảãâấầậẩẫăắằặẳẵ",
		"ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
		"éèẹẻẽêếềệểễ",
		"ÉÈẸẺẼÊẾỀỆỂỄ",
		"óòọỏõôốồộổỗơớờợởỡ",
		"ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
		"úùụủũưứừựửữ",
		"ÚÙỤỦŨƯỨỪỰỬỮ",
		"íìịỉĩ",
		"ÍÌỊỈĨ",
		"đ",
		"Đ",
		"ýỳỵỷỹ",
		"ÝỲỴỶỸ"
	};
	/// <summary>
	/// Remove all accent in Vietnamese and convert to normal text
	/// </summary>
	public static string StripVietnameseAccent(string str)
	{
		for (int i = 1; i < VietnameseSigns.Length; i++)
		{
			for (int j = 0; j < VietnameseSigns[i].Length; j++)
				str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
		}

		return str;
	}


	/// <summary>
	/// Serialize an object into JSON string and write it into file
	/// </summary>
	/// <typeparam name="T">Type of object</typeparam>
	/// <param name="data">Object to serialize</param>
	/// <param name="fileName">Filename to write to</param>
	public static void SerializeObjectToFile<T>(T data, string fileName, string password = null, bool isAbsoluteFilePath = false)
	{
		if (data != null)
		{
			fsData jsonData;
			FileUtilities.JSONSerializer.TrySerialize<T>(data, out jsonData);
			string json = jsonData.ToString();
			var bytes = System.Text.Encoding.UTF8.GetBytes(json);
			if (!string.IsNullOrEmpty(password))
			{
				string encrypt = Encryption.Encrypt(bytes, password);
				bytes = System.Text.Encoding.UTF8.GetBytes(encrypt);
			}
			SaveFile(bytes, fileName, isAbsoluteFilePath);
		}
	}

	/// <summary>
	/// Deserialize an object from JSON file
	/// </summary>
	/// <typeparam name="T">Type of result object</typeparam>
	/// <param name="fileName">Json file content the serialized object</param>
	/// <returns>Object serialized in json file, if the file is not existed or invalid, the result will be default(T)</returns>
	public static T DeserializeObjectFromFile<T>(string fileName, string password = null, bool isAbsolutePath = false)
	{
		T data = default(T);
		byte[] localSaved = LoadFile(fileName, isAbsolutePath);
		if (localSaved == null)
		{
			Debug.Log(fileName + " not exist, returning null");
		}
		else
		{
			string json = System.Text.Encoding.UTF8.GetString(localSaved);
			if (!string.IsNullOrEmpty(password))
			{
				string decrypt = Encryption.Decrypt(json, password);
				if (string.IsNullOrEmpty(decrypt))
				{
					Debug.LogWarning("Can't decrypt file " + fileName);
					return data;
				}
				else
				{
					json = decrypt;
				}
			}
			fsData jsonData = fsJsonParser.Parse(json);
			fsResult result = JSONSerializer.TryDeserialize(jsonData, ref data);
			if (result.Failed)
			{
				Debug.Log("Failed to de-serialize object for file " + fileName);
			}
		}
		return data;
	}


	public static string EncodeData(string rawText)
	{
		if (Amanotes.PianoChallenge.GameConsts.IsEncypt)
		{
			string encode = Encryption.Encrypt(rawText, Amanotes.PianoChallenge.GameConsts.SecretKey);
			return encode;
		}
		else
		{
			return rawText;
		}
	}

	public static string DecodeData(string encodeText)
	{
		if (Amanotes.PianoChallenge.GameConsts.IsEncypt)
		{
			string final = "";
			string decode = Encryption.Decrypt(encodeText, Amanotes.PianoChallenge.GameConsts.SecretKey, res =>
			{
				// trong truong hop Tap Challeger game da publish du lieu chua duoc ma hoa
				// => cho load theo phuong phap cu sau doforce ve du lieu cu sau do ma hoa du lieu do.
				// de dam bao User cu van vao choi binh thuong.
				if (res == false)
				{
					final = encodeText;
				}
			});
			if (final.Length > 0)
				return final;
			else
				return decode;
		}
		else
		{
			return encodeText;
		}
	}

}
