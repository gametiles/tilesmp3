using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
// using Framework
using Framework;
using Amanotes.Utils;

//Log
public class LogInfo {
	public string logString;
	public string logTrace;
	public UnityEngine.LogType logType;

	public LogInfo (string logString, string logTrace, UnityEngine.LogType logType) {
		this.logString = logString;
		this.logTrace = logTrace;
		this.logType = logType;
	}
}



public class GMTool : SingletonMono<GMTool> {
	//public Text infoText;
	public InputField inputField;
	public int limitLog=20;
	public GameObject objShowGM;
	public GameObject cameraGM;
	public GameObject objHideGM;

	public GameObject objShowLogFull;
	public GameObject objShowLogNormal;

	public Text textGeneration;
	public GameObject contentRoot;
	public bool isUseGM=true;

	private bool isShowLogFull=false;
	private int cacheLength=0;
	private bool isShow=false;
	private int timePress=0;
	private float lastTimePress=0;
	public enum LogInfoFilter
	{
		VERBOSE,
		INFO,
		WARNING,
		ERROR,
		EXCEPTION
	}
	private LogInfoFilter logInfo=LogInfoFilter.VERBOSE;


	//GM Command
	private static GMCommand cmdInstance=null;

	// Logger
	public List<LogInfo> listLog = new List<LogInfo> ();

	public GMCommand GetCommands()
	{
		return cmdInstance;
	}
	public void ShowGM(bool _isShow)
	{
		objShowGM.SetActive (_isShow);
		objHideGM.SetActive (!_isShow);
		this.isShow = _isShow;
	}
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (this.gameObject);
		// Initial GM Console
		if (cmdInstance == null) {
			cmdInstance = new GMCommand ();
			ConsoleCommands.AddCommandProvider (cmdInstance, null);
		}
		// dang ky log
		Application.logMessageReceived += (OnLogCallback);
	}

	public void Command()
	{
		if (inputField.text.Length < 1) {
			Debug.Log("Input command are empty");
		} else {
			string command = inputField.text;
			string ret=ConsoleCommands.ExecuteCommand(command);
			Debug.LogWarning (ret);
		}
	}

	public void Add1000Exp()
	{
		ConsoleCommands.ExecuteCommand("exp 1000");
	}

	public void Add1000Diamonds()
	{
		ConsoleCommands.ExecuteCommand("gem 1000");
	}

	public void Autoplay()
	{
		ConsoleCommands.ExecuteCommand("auto");
	}

	public void OpenInterstitial()
	{
		ConsoleCommands.ExecuteCommand("ad");
	}

	public void Back()
	{
		ShowGM (false);
	}
	public void Open()
	{
		ShowGM (true);
		ShowLogType (isShowLogFull);
	}


	public void Update()
	{
		if (isShow) {
			int count = listLog.Count;
			if (count != cacheLength) {
				cacheLength = count;
				UpdateLog ();
			}
		}

	}
	private void UpdateLog() {
		/*string log = "";
		int count =listLog.Count;
		for (int i = count-1; i > count - limitLog; --i) {
			if (i >= 0) {
				LogInfo info = listLog[i];
				log += info.logString + "\n";
				if (string.IsNullOrEmpty (info.logTrace) == false) {
					if (isShowLogFull) {
						log += "<= " + info.logTrace;
					}
				}
				log += "\n";
			}
		}
		if (log.Length > 2000) {
			log = log.Substring (log.Length - 2000, 1999);
		}
		infoText.text = log;
		Debug.Log("1:"+LayoutUtility.GetPreferredHeight(infoText.rectTransform) + " vs " + infoText.rectTransform.rect.height);
		infoText.rectTransform.SetHeight(LayoutUtility.GetPreferredHeight (infoText.rectTransform));
		Debug.Log("2:"+LayoutUtility.GetPreferredHeight(infoText.rectTransform) + " vs " + infoText.rectTransform.rect.height);
		*/
		FillToScreen ();
	}

	public void FillToScreen()
	{
        Application.logMessageReceived -= (OnLogCallback);
        Application.logMessageReceived += OnLogCallbackNotWrite;
		for (int i = 0; i < contentRoot.transform.childCount; i++) {
			GameObject objTemp=contentRoot.transform.GetChild (i).gameObject;
			if (objTemp != null) {
				GameObject.Destroy (objTemp);
			}
		}
		float cacheHeight = 0;
		int count =listLog.Count;
		int countLog = 0;
		for (int i = count-1; i > 0; --i) {
			if (i >= 0) {
				LogInfo info = listLog[i];
				string log = "";
				if (countLog > limitLog) {
					break;
				} else {
					bool ignore = true;
					switch (logInfo) {
					case LogInfoFilter.VERBOSE:
						{
							ignore = false;
						}
						break;
					case LogInfoFilter.INFO:
						{
							if (info.logType == UnityEngine.LogType.Log) {
								ignore = false;
							}
						}
						break;
					case LogInfoFilter.WARNING:
						{
							if (info.logType == UnityEngine.LogType.Warning) {
								ignore = false;
							}
						}
						break;
					case LogInfoFilter.EXCEPTION:
						{
							if (info.logType == UnityEngine.LogType.Exception) {
								ignore = false;
							}
						}
						break;
					case LogInfoFilter.ERROR:
						{
							if (info.logType == UnityEngine.LogType.Error) {
								ignore = false;
							}
						}
						break;
					}
					if (ignore) {
						continue;
					} else {
						countLog++;
					}
				}
				if (info.logType == UnityEngine.LogType.Error) {
					log+="<color=red>"; 
				}
				else if (info.logType == UnityEngine.LogType.Exception) {
					log+="<color=magenta>"; 
				}
				else if (info.logType == UnityEngine.LogType.Warning) {
					log+="<color=yellow>"; 
				}
				else {
					log+="<color=white>"; 
				}
				log += info.logString + "\n";
				if (log.Length> 1000) {
					log = log.Substring (log.Length - 1000, 999);
					log += "\n... (limit show)";
				}
				if (string.IsNullOrEmpty (info.logTrace) == false) {
					if (isShowLogFull) {
						log += "<= " + info.logTrace;
					}
				}
				log+="</color>"; 
				log+= "\n-------------------------";
				// generate
				GameObject obj=GameObject.Instantiate(textGeneration.gameObject) as GameObject;
				obj.transform.SetParent(contentRoot.transform);
				obj.transform.localScale = Vector3.one;
				obj.transform.localPosition = new Vector3 (320, -cacheHeight, 0);
				Text text = obj.GetComponent<Text> ();
				if(log.Length<1)
					log=" ";
				text.text = log;
				text.rectTransform.SetHeight(LayoutUtility.GetPreferredHeight (text.rectTransform));
				float height = LayoutUtility.GetPreferredHeight (text.rectTransform);
				cacheHeight += height;
			}
		}
		if (cacheHeight < 10)
			cacheHeight = 10;
		contentRoot.GetComponent<RectTransform>().SetHeight(cacheHeight);
		Application.logMessageReceived += (OnLogCallback);
        Application.logMessageReceived -= OnLogCallbackNotWrite;
    }

	public void ProcessGM()
	{
		//Debug.LogError ("ProcessGM");
		if(isUseGM)
		{

			float now=Time.realtimeSinceStartup;
			if(now-lastTimePress<0.5f)
			{

				lastTimePress=now;
				timePress++;
			}
			else
			{
				timePress=0;
				lastTimePress=now;
			}
			if(timePress>1)
			{
				lastTimePress=0;
				Open ();
			}
		}
	}
	public void Clear()
	{
		listLog.Clear ();
	}
	public void ShowFull()
	{
		ShowLogType (true);
	}
	public void ShowNormal()
	{
		ShowLogType (false);
	}
	public void ShowLogType(bool isFull)
	{
		isShowLogFull = isFull;
		objShowLogFull.SetActive (isShowLogFull);
		objShowLogNormal.SetActive (!isShowLogFull);

		UpdateLog();
	}
	/// <summary>
	/// Strong D: Ghi Log tu callback
	/// </summary>
	public void OnLogCallback (string logString, string stackTrace, UnityEngine.LogType type) {
		//if (type != UnityEngine.LogType.Log) 
		{
			listLog.Add (new LogInfo (logString, stackTrace, type));
		}
	}

	public void OnLogCallbackNotWrite (string logString, string stackTrace, UnityEngine.LogType type) {
	}
	public void OnLogFilterChange(int value)
	{
		logInfo = (LogInfoFilter)value;
		UpdateLog();
	}
}
