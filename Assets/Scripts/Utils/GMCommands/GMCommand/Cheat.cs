using UnityEngine;

namespace Amanotes.PianoChallenge
{
	public class Cheat : MonoBehaviour
	{
#if !DEVELOPMENT_BUILD // Cheat button is already shown in development build
		private int step = 0;
		private GameObject[] steps = new GameObject[10];
		private void Start()
		{
			for (int i = 0, k = 0; i < 4; i++)
			{
				for (int j = 0; j < i + 1; j++, k++)
				{
					steps[k] = transform.GetChild(i).gameObject;
				}
			}
		}

		private void OnEnable()
		{
			step = 0;
		}
#endif

		public void OnCheatButtonPressed(GameObject button)
		{
#if !DEVELOPMENT_BUILD // Cheat button is already shown in development build
			if (step < steps.Length && button == steps[step] && GameManager.Instance.GameConfigs.cheatEnabled)
			{
				step++;
				if (step == 10)
				{
					SceneManager.Instance.ShowGmCommand();
				}
			}
			else
			{
				step = button == steps[0] ? 1 : 0;
				SceneManager.Instance.HideGmCommand();
			}
#endif
		}
	}
}
