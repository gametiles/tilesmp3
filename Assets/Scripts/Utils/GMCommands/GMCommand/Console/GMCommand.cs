using System.ComponentModel;
using UnityEngine;

using Amanotes.Utils;
using Amanotes.PianoChallenge;

namespace Framework
{
	public partial class GMCommand
	{
		// Main logger
		protected static readonly GMLogger log = GMLogger.GetLogger(typeof(GMCommand));

		public GMCommand()
		{

		}

		#region [ System Commands ]

		[Description("Display all available command")]
		public static string Help()
		{
			string str = ConsoleCommands.GetAllCommandHelps();
			Debug.LogError(str);
			return str;
		}

		[Description("Display a specific command")]
		public static string Help(string command)
		{
			string str = ConsoleCommands.GetCommandHelp(command);
			Debug.LogError(str);
			return str;
		}

		[Description("Change current quality settings.")]
		[CmdDetail("Params> 0: Fastest, " +
			"1: Fast, " +
			"2: Simple, " +
			"3: Good, " +
			"4: Beautiful, " +
			"5: Fantastic.")]
		public string Quality(int value)
		{
			QualitySettings.SetQualityLevel(value);
			return "Quality now set to: " + CheckQuality();
		}

		[Description("Get current quality settings.")]
		public string CheckQuality()
		{
			return QualitySettings.names[QualitySettings.GetQualityLevel()] + " (" + QualitySettings.GetQualityLevel() + ")";
		}

		#endregion

		public string Auto(int auto)
		{
			bool isAuto = auto > 0 ? true : false;
			GameManager.Instance.SessionData.autoplay = isAuto;
			return "Is AutoPlay:" + auto;
		}
		public string Auto()
		{
			return Auto(1);
		}
		public string NotAuto()
		{
			return Auto(0);
		}

		public string PauseGame(int pause)
		{
			if (pause > 0)
			{
				Gameplay.GMPauseGame();
				return "Pausing game";
			}
			else
			{
				Gameplay.GMContinueGame();
				return "Continuing game";
			}
		}

		public static void Unsync()
		{
			Parse.ParseUser.LogOutAsync();
			Debug.Log("Logging out from Parse...");
		}

		public void UA(string ach)
		{
			AchievementHelper.GMUnlockAchievement(ach);
		}
		// upto propety
		public void UP(string p, int value)
		{
			AchievementHelper.GMIncreaseAchievementProperty(p, value);
		}

		public void Ad()
		{
			AdHelper.Instance.TestInterstitial();
		}

#if DEVELOPMENT_BUILD

		public void initOg()
		{
			AdsOgury.Instance.TestInit();
		}

		public void showOg()
		{
			AdsOgury.Instance.TestShowAds();
		}

		public void loadOg()
		{
			AdsOgury.Instance.TestRequestAds();
		}

		public void adOg()
		{
			AdsOgury.Instance.TestAdServe();
		}
#endif

		public void Rads()
		{
			AdHelper.Instance.SetNoAds(false);
		}

		public void gem(int diamonds)
		{
			ProfileHelper.Instance.CurrentDiamond += diamonds;
		}

		public void exp(int exp)
		{
			ProfileHelper.Instance.AddExpForUser(exp, GameManager.Instance.configUserLevel);
		}

		// Show AB test info
		public void ab()
		{
			ABTestManager.Instance.LogInfo();
		}

		public void ag(int type)
		{
			if (type < 0)
			{
				type = 0;
			}
			else if (type > 1)
			{
				type = 1;
			}
			ABTestManager.Instance.InGameAchievementOption = type;
		}

		public void day0pn(int type)
		{
			if (type < 0)
			{
				type = 0;
			}
			else if (type > 2)
			{
				type = 2;
			}
			ABTestManager.Instance.Day0PnOption = type;
		}

		public void itp(int type)
		{
			if (type < 0)
			{
				type = 0;
			}
			else if (type > 3)
			{
				type = 3;
			}
			ABTestManager.Instance.InterstitialPolicy = type;
		}

		public void sc(int type)
		{
			if (type < 0)
			{
				type = 0;
			}
			else if (type > 4)
			{
				type = 4;
			}
			ABTestManager.Instance.SongConfigOption = type;
		}

		public void lc(int type)
		{
			if (type < 0)
			{
				type = 0;
			}
			else if (type > 2)
			{
				type = 2;
			}
			ABTestManager.Instance.LevelConfigOption = type;
		}

#if UNITY_EDITOR
		public void parse()
		{
			SongLoader.Instance.DownloadAndParseSongs ();
		}

		public void info()
		{
			
		}
#endif
	}
}


