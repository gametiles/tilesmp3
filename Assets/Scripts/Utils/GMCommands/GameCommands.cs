﻿using UnityEngine;
using System.Collections;

public partial class GMCommands {
    [CUDLR.Command("set gamespeed", "Set tiles running speed, received only 1 float argument ")]
    public static void SetGameSpeed(string[] arg) {
        if(arg.Length != 1) {
            CUDLR.Console.Log("Wrong command format, ONE float argument is expected in this command");
            return;
        }

        float f;
        if(float.TryParse(arg[0], out f)) {
            CUDLR.Console.Log("Setting game speed to " + arg[0]);
            Amanotes.PianoChallenge.Gameplay.GMSetGameSpeed(f);
        }
        else {
            CUDLR.Console.Log("Wrong float format, some example: 0.01, 0.005, 1.5, 2.8");
        }
    }

    [CUDLR.Command("set gameautoplay", "Set tiles running speed, received only 1 integer argument ")]
    public static void SetGameAutoPlay(string[] arg) {
        if (arg.Length != 1) {
            CUDLR.Console.Log("Wrong command format, ONE integer argument is expected in this command");
            return;
        }

        int autoplay;
        if (int.TryParse(arg[0], out autoplay)) {
            CUDLR.Console.Log("Setting game speed to " + arg[0]);
            Amanotes.PianoChallenge.Gameplay.GMSetAutoPlay(autoplay != 0);
        }
        else {
            CUDLR.Console.Log("Wrong argument format, use 0 for false, any other value for true");
        }
    }
    //GMSetAutoPlay
}
