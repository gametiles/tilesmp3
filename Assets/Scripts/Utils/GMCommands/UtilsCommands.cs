using UnityEngine;

public partial class GMCommands
{
	[CUDLR.Command("show fps", "Display current FPS")]
	public static void ShowFPS()
	{
		FPS.SetVisible(true);
		CUDLR.Console.Log("FPS is shown");
	}

	[CUDLR.Command("hide fps", "Hide current FPS")]
	public static void HideFPS()
	{
		FPS.SetVisible(false);
		CUDLR.Console.Log("FPS is hidden");
	}

	[CUDLR.Command("logout parse", "Logout from Parse")]
	public static void LogoutParse()
	{
		Parse.ParseUser.LogOutAsync();
		Debug.Log("Logging out from Parse...");
	}

	[CUDLR.Command("increase achievementproperty", "Increase achievement property")]
	public static void IncreaseAchievementProperty(string[] args)
	{
		if (args.Length != 2)
		{
			CUDLR.Console.Log("Wrong command format, need 1 id and 1 value as integer");
			return;
		}

		int value;
		if (int.TryParse(args[1], out value))
		{
			CUDLR.Console.Log("Increasing achievement property by " + args[1]);
			AchievementHelper.GMIncreaseAchievementProperty(args[0], value);
		}
		else
		{
			CUDLR.Console.Log("Wrong argument format, use 0 for false, any other value for true");
		}
	}
}
