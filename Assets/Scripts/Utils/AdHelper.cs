using UnityEngine;
using System;
using System.Collections;
using Amanotes.PianoChallenge;
using System.Collections.Generic;

namespace Amanotes.Utils
{
	public enum RewardType
	{
		Ruby,
		Energy,
		Continue,
		MultiplyRewards,
		Custom,
	}

	public class AdHelper : MonoBehaviour
	{
		[SerializeField] bool adsEnabled;

		public bool isAdsEnabled
		{
			get { return adsEnabled; }
			set { adsEnabled = value; }
		}

		public void SetNoAds(bool isNoAds)
		{
			isAdsEnabled = !isNoAds;
		}

		public int RewardVideoProviderCount { get { return rewardVideoProviders.Count; } }

		private Dictionary<string, AdsProvider> adsProviders = new Dictionary<string, AdsProvider>();
		private int nInitializedAdsProviders = 0;

		private List<AdsProvider> interstitialProviders = new List<AdsProvider>();
		private int iInterstitalProvider = 0; // Index of the ads provider used to show interstitial
		private float interstitialShownTime = 0;

		private List<AdsProvider> rewardVideoProviders = new List<AdsProvider>();
		private int iRewardVideoProvider = 0; // Index of the ads provider used to show rewarded video

		private RewardType typeReward = RewardType.Ruby;
		private bool openingRewardVideo = false;
		private int indexTimeout = 0;

		private Action onRewardVideoFailed = null;
		private Action onRewardVideoWatched = null;

		private bool skipNextInterstitial = false;
		private int nIntersitialTimes = 0;

		private static AdHelper instance;
		public static AdHelper Instance
		{
			get
			{
				if (instance == null)
				{
					Debug.LogError("Adhelper instance is null, is this an error?");
				}

				return instance;
			}
		}

		public bool LoadingRewardVideo
        {
            get { return openingRewardVideo; }
            set { openingRewardVideo = value; }
        }


		void Awake()
		{
			if (instance == null)
			{
				instance = this;
			}
		}

		// Use this for initialization
		void Start()
		{
			if (instance == null)
			{
				instance = this;
				DontDestroyOnLoad(gameObject);
			}
			else
			{
				if (this != instance)
				{
					Destroy(this);
					return;
				}
			}
		}

		void OnApplicationPause(bool isPaused)
		{
			foreach (AdsProvider adsProvider in adsProviders.Values)
			{
				adsProvider.OnApplicationPaused(isPaused);
			}
		}

		public void OnEnable()
		{
			// Wake up ads providers from long lasting sleep
			foreach (AdsProvider adsProvider in adsProviders.Values)
			{
				adsProvider.OnEnabled();
			}
		}

		private bool initialized = false;
		public void Initialize()
		{
			if (initialized)
			{
				return;
			}
			initialized = true;

			if (adsProviders != null && adsProviders.Count > 0)
			{
				return;
			}

#if DEVELOPMENT_BUILD
			GameManager.Instance.GameConfigs.listRegionOgury.Add("VN");
#endif
        }

		public void TestInterstitial()
		{
			if (interstitialProviders.Count <= 0)
			{
				Development.Log("Ads - There is no interstitial provider");
				return;
			}

			Development.Log("Ads - Test interstitial from " + interstitialProviders[0].Name());
			iInterstitalProvider = 0;
			interstitialProviders[0].ShowInterstitial();
		}

		public void SkipNextInterstitialTrigger()
		{
			skipNextInterstitial = true;
		}

		public void ShowInterstitial()
		{
			if (SceneManager.Instance.CurrentScene == ProjectConstants.Scenes.MainGame || SceneManager.Instance.CurrentScene == ProjectConstants.Scenes.OnlineMainGame)
			{
				Development.Log("Ads - Avoid showing ad in gameplay");
				return;
			}

            if (skipNextInterstitial)
            {
                skipNextInterstitial = false;
                Development.Log("Ads - Skip interstitial");
                return;
            }

            float timeRemainingToShowInterstitial = 60 - (Time.realtimeSinceStartup - interstitialShownTime);
            if (timeRemainingToShowInterstitial > 0)
            {
                Development.Log("Ads - Intersitial should wait for " + timeRemainingToShowInterstitial + "s more");
                return;
            }

#if !UNITY_EDITOR

            AdsMob.Instance.ShowInterstitial();
			AnalyticsHelper.Instance.LogFullscreenAdTriggered();
#else
            OnInterstitialClosed();
#endif
		}

		/// <summar
		public void ShowRewardVideo(
			RewardType type = RewardType.Ruby,
			Action onWatched = null,
			Action onFailed = null)
		{
            if(openingRewardVideo)
                Debug.LogError("AAA: adhelper opening: TRUE" );
            else
                Debug.LogError("AAA: adhelper opening: FALSE");
            if (openingRewardVideo)
			{
				return;
			}

			Development.Log("Ads - Trigger rewarded video for: " + type);

			onRewardVideoFailed = onFailed;
			onRewardVideoWatched = onWatched;
			typeReward = type;
#if UNITY_EDITOR
			openingRewardVideo = true;
			OnRewardVideoWatched();

			
			if (openingRewardVideo)
			{
				Development.Log("Ads - Rewarded video is already being loaded");
				AnalyticsHelper.Instance.LogRewardVideoFailed("Another loading");
				return;
			}
#else
            
            AdsMob.Instance.ShowRewardVideo();
            //openingRewardVideo = true;
			//ShowVideoRewardAdsProgress();           
			//StopCoroutine("TimeOutVideoReward");
			//indexTimeout++;
			//StartCoroutine(TimeOutVideoReward(indexTimeout));
#endif
        }

        private void OnInterstitialClosed()
		{
			Development.Log("Ads - Interstitial closed");
			interstitialShownTime = Time.realtimeSinceStartup;

			if (iInterstitalProvider < interstitialProviders.Count)
			{
				string providerName = interstitialProviders[iInterstitalProvider].Name();
				AnalyticsHelper.Instance.LogFullscreenAdShown(providerName);
			}
		}

		private void OnAdsProviderInitialized()
		{
			nInitializedAdsProviders++;
			Development.Log("Ads - " + nInitializedAdsProviders + "/" + adsProviders.Keys.Count + " provider(s) Initialized");

			if (nInitializedAdsProviders < adsProviders.Keys.Count)
			{
				return;
			}

			Development.Log("Ads - Making lists of interstitial and rewarded video providers in preferred order");
			foreach (string name in GameManager.Instance.GameConfigs.adsProviders)
			{
				if (!adsProviders.ContainsKey(name))
				{
					Development.Log("Ads - Ads provider " + name + " won't be used");
					continue;
				}

				AdsProvider provider = adsProviders[name];
				if (provider.IsInterstitialReady())
				{
					Development.Log("Ads - Listed " + name + " as an interstitial provider");
					interstitialProviders.Add(provider);
				}
				if (provider.IsRewardVideoReady())
				{
					Development.Log("Ads - Listed " + name + " as a rewarded video provider");
					rewardVideoProviders.Add(provider);
				}
			}
		}

		private void OnInterstitialFailed()
		{
			Development.Log("Ads - Cannot open interstitial from " + interstitialProviders[iInterstitalProvider].Name());
			iInterstitalProvider++;

			if (iInterstitalProvider < interstitialProviders.Count)
			{ // Try with another provider
				Development.Log("Ads - Try to open interstitial from " + interstitialProviders[iInterstitalProvider].Name());
				interstitialProviders[iInterstitalProvider].ShowInterstitial();
			}
			else
			{ // Completely failed with all providers
				Development.Log("Ads - Opening interstitial completely failed");
			}
		}

		/// <summary>
		/// Add Ruby reward for user when finish watching video ad
		/// </summary>
		public void OnRewardVideoWatched()
		{
            if (openingRewardVideo)
                Debug.LogError("AAA: adhelper OnRewardVideoWatched: TRUE");
            else
                Debug.LogError("AAA: adhelper OnRewardVideoWatched: FALSE");

            if (!openingRewardVideo)
			{
				return;
			}

			openingRewardVideo = false;

			SSSceneManager.Instance.HideLoading();

			AnalyticsHelper.Instance.LogRewardVideoWatched("Admobbb");

			AchievementHelper.Instance.LogAchievement("watchVideoAds");
			skipNextInterstitial = true;

			switch (typeReward)
			{
				case RewardType.Ruby:
					ProfileHelper.Instance.CurrentDiamond += GameManager.Instance.GameConfigs.videoAdsReward;
					break;
			}

			if (onRewardVideoWatched != null)
			{
				onRewardVideoWatched();
			}
		}

		public void OnRewardVideoClosed()
		{
            if (openingRewardVideo)
                Debug.LogError("AAA: adhelper OnRewardVideoClosed: TRUE");
            else
                Debug.LogError("AAA: adhelper OnRewardVideoClosed: FALSE");

            if (!openingRewardVideo)
			{
				return;
			}

			openingRewardVideo = false;
			SSSceneManager.Instance.HideLoading();
		}

		public void OnRewardVideoFailed()
		{
            if (openingRewardVideo)
                Debug.LogError("AAA: adhelper OnRewardVideoFailed: TRUE");
            else
                Debug.LogError("AAA: adhelper OnRewardVideoFailed: FALSE");

            Development.Log("Ads - Cannot open rewarded video");
			iRewardVideoProvider++;

			// Completely failed with all providers
			SSSceneManager.Instance.HideLoading();

			//if (!openingRewardVideo)
			//{
			//	return;
			//}

			openingRewardVideo = false;
			SceneManager.Instance.SetLoadingVisible(false);
			MessageBoxDataModel msg = new MessageBoxDataModel();
			msg.message = Localization.Get("pu_rewardloadfailed_title");
			msg.messageYes = Localization.Get("pu_rewardloadfailed_btn_ok");
			msg.OnYesButtonClicked = delegate {
				if (onRewardVideoFailed != null)
				{
					onRewardVideoFailed();
				}
			};

			Development.Log("Ads - Opening rewarded video completely failed ");
			AnalyticsHelper.Instance.LogRewardVideoFailed ("Cannot load");
			SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.MessageBoxPopup, msg);
		}

		private IEnumerator TimeOutVideoReward(int countIndex)
		{
			AnalyticsHelper.Instance.LogRewardVideoFailed("Time-out");

			indexTimeout = countIndex;
			yield return new WaitForSeconds(10);
			if (indexTimeout != countIndex)
			{ // loai bo function cu
				yield break;
			}

			Development.Log("Ads - Loading rewarded video time out " + openingRewardVideo);
			if (!openingRewardVideo)
			{
				yield break;
			}

			openingRewardVideo = false;

			SceneManager.Instance.SetLoadingVisible(false);
			MessageBoxDataModel msg = new MessageBoxDataModel();
			msg.message = Localization.Get("pu_rewardloadfailed_title");
			msg.messageYes = Localization.Get("pu_rewardloadfailed_btn_ok");
			msg.OnYesButtonClicked = delegate { };

			SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.MessageBoxPopup, msg);
			SceneManager.Instance.SetLoadingVisible(false);
		}

		private void ShowVideoRewardAdsProgress()
		{
#if UNITY_EDITOR
			MessageBoxDataModel msg = new MessageBoxDataModel();
			msg.message = "Khong chay tren Editor";
			msg.messageYes = Localization.Get("pu_rewardloadfailed_btn_ok");
			msg.OnYesButtonClicked = delegate {};

			SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.MessageBoxPopup, msg);
			openingRewardVideo = false;
#else
			SceneManager.Instance.SetLoadingVisible (true);
#endif
		}

	}//end class
}//end namespace
