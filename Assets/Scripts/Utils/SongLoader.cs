using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullSerializer;
using Amanotes.Utils;

namespace Amanotes.PianoChallenge
{
	public class SongLoader : SingletonMono<SongLoader>
	{
		// an error-tolerance to determine if a note in midi should be treat
		// as a normal tile, or just a part of a long tile. if a note has duration
		// of 119, when ticksPerTile is 120, that note will be a normal tile if
		// tickTolerance is more than 0.01 (because 120 * 0.99 <= 119 <= 120 * 1.01)
		private static readonly float tickTolerance = 0.125f;

		//Track of midi file to store note data
		private static readonly int NoteTrack = 1;
		//Track of midi file used when play back song data
		private static readonly int PlaybackTrack = 0;

		public void DownloadAndParseSongs()
		{
			StoreDataModel storeData = GameManager.Instance.StoreData;
			foreach (var song in storeData.listAllSongs)
			{
				Load(song, null, null, null, null, null, null);
			}
		}

		public void Load(
			SongDataModel song,
			Action<string> OnDownloadError,
			Action<float> OnDownloadProgress,
			Action OnDownloadComplete,
			Action<string> OnParseError,
			Action<float> OnParseProgress,
			Action<LevelDataModel, List<TileData>> OnParseComplete)
		{
			LevelDataModel levelData = new LevelDataModel();
			levelData.songData = song;

			// Look for parsed data in Resources first
			SongTileData tileData = FileSystem.LoadResource<SongTileData>("songs/" + song.storeID);

			// Look for parsed data in previously parsed data
			if (tileData == null)
			{
				tileData = FileSystem.Load<SongTileData>(song.storeID + ".nuna");
			}

			if (tileData != null && tileData.songDataModel.version == levelData.songData.version)
			{
				Helpers.Callback(OnDownloadComplete); // No need to download
				levelData.denominator = tileData.denominator;
				levelData.BPM = tileData.BPM;
				levelData.tickPerQuarterNote = tileData.tickPerQuarterNote;
				levelData.dicDuetNote = tileData.dicDuetNote;
				levelData.dicNotePlay = tileData.dicNotePlay;

				// No need to compile
				Helpers.CallbackWithValue(OnParseComplete, levelData, tileData.tileList);
				return;
			}

			Helpers.CallbackWithValue(OnDownloadProgress, 0);

			// Look for Piano Tiles song in Resources
			if (song.isPianoTiles > 0)
			{
				Helpers.Callback(OnDownloadComplete); // No need to download
				PIDecodeData(song, song.isPianoTiles, OnParseProgress, OnParseComplete, OnParseError);
				return;
			}

			// Download and compile song
			DownloadAndParse(
				song,
				OnDownloadError,
				OnParseError,
				OnDownloadProgress,
				OnParseProgress,
				OnDownloadComplete,
				OnParseComplete);
		}

		// Load Piano Tiles song from Resources
		private void PIDecodeData
		(
			SongDataModel song,
			int typePI,
			Action<float> onParseProgress = null,
			Action<LevelDataModel, List<TileData>> onParseComplete = null,
			Action<string> onParseError = null
		)
		{
			PTJsonData ptJsonData = null;
			string filename = "json/" + song.name;
			UnityEngine.Object obj = Resources.Load(filename);
			TextAsset textAsset = (TextAsset)obj;
			string json = textAsset.ToString();
			fsData jsonData = fsJsonParser.Parse(json);
			fsResult result = FileUtilities.JSONSerializer.TryDeserialize<PTJsonData>(jsonData, ref ptJsonData);
			if (result.Failed)
			{
				Debug.Log("Failed to de-serialize object for file " + filename);
				return;
			}

			#region phan tich theo track
			List<string[]> infoPath = new List<string[]>();
			PTInfoData ptInfo = new PTInfoData();
			for (int i = 0; i < ptJsonData.musics.Count; i++)
			{// phan tich ra tung track
				if (ptJsonData.musics[i].instruments == null)
				{
					ptJsonData.musics[i].instruments = new string[ptJsonData.musics[i].scores.Count];
					for (int j = 0; j < ptJsonData.musics[i].instruments.Length; j++)
					{
						ptJsonData.musics[i].instruments[j] = "piano";
					}
				}
				string[] fixContent = new string[ptJsonData.musics[i].instruments.Length];
				for (int j = 0; j < ptJsonData.musics[i].scores.Count; j++)
				{// phan tich theo nhac cu tung track -> toi da 6 nhac cu
					string orginal = ptJsonData.musics[i].scores[j];
					string fixData = FixString(orginal);// fix cac ky tu dac biet de phan tich
					fixContent[j] = fixData;
				}
				int coff = 1;
				float baseBeat = ptJsonData.musics[i].baseBeats;
				if (baseBeat < 0.5f)
				{
					coff = 2;
				}

				PTTrackData trackData = ptInfo.AddTrackData(ptJsonData.musics[i].instruments, fixContent, coff);

			}
			#endregion

			#region convert from PT Json to midi data
			PTConvertData convertData = new PTConvertData(0.5f);//  60.0f / ptJsonData.baseBpm);
			int indexNote = 0;
			for (int i = 0; i < ptInfo.listTrack.Count; i++)
			{
				indexNote = ptInfo.listTrack[i].ConvertTrackToNote(ref convertData, indexNote, typePI);
			}
			#endregion

			#region xoa cac noi thua
			for (int i = 0; i < convertData.trackView.Count; i++)
			{
				bool isSkip = true;

				List<NoteData> listNote = null;
				if (convertData.dicNotePlay.TryGetValue(convertData.trackView[i], out listNote))
				{
					if (listNote != null)
					{
						for (int cx = 0; cx < listNote.Count; cx++)
						{
							if (listNote[cx].nodeID != 200)
							{
								isSkip = false;
								break;
							}
						}
					}
				}
				if (typePI == 1 || typePI == 3)
				{
					if (convertData.trackView[i].isEndLine)
					{
						isSkip = false;
					}
				}
				if (isSkip)
				{
					convertData.trackView.RemoveAt(i);
				}
			}
			#endregion

			#region chinh lai khoang cach cac not
			if (typePI == 1 || typePI == 3)
			{
				int totalSub = 0;
				int next = 0;
				while (next < convertData.trackView.Count - 1)
				{
					//remove empty space
					NoteData note1 = convertData.trackView[next];
					NoteData note2 = convertData.trackView[next + 1];

					List<NoteData> nextNote = new List<NoteData>();
					nextNote.Add(note2);
					for (int j = next + 2; j < convertData.trackView.Count; j++)
					{// tinh het cac not doi mot luot
						if (convertData.trackView[j].indexId == note2.indexId)
						{
							nextNote.Add(convertData.trackView[j]);
						}
						else
						{

							break;
						}
					}

					next += nextNote.Count;

					int endNote1 = (int)(note1.timeAppear * 1000) + note1.durationInTick;
					int startNote2 = (int)(note2.timeAppear * 1000);
					if (startNote2 - endNote1 >= 500)
					{
						int sub = startNote2 - endNote1;
						totalSub = sub;
					}
					for (int x = 0; x < nextNote.Count; x++)
					{

						nextNote[x].timeAppear -= totalSub / 1000.0f;
						nextNote[x].tickAppear -= (totalSub * 960) / 1000;

						List<NoteData> listNote = null;
						if (convertData.dicNotePlay.TryGetValue(nextNote[x], out listNote))
						{
							if (listNote != null)
							{
								for (int cx = 0; cx < listNote.Count; cx++)
								{
									if (listNote[cx] != nextNote[x])
									{
										listNote[cx].timeAppear -= totalSub / 1000.0f;
										listNote[cx].tickAppear -= (totalSub * 960) / 1000;
									}
								}
							}
						}

					}
				}

				for (int i = 0; i < convertData.trackView.Count; i++)
				{
					bool isSkip = true;

					List<NoteData> listNote = null;
					if (convertData.dicNotePlay.TryGetValue(convertData.trackView[i], out listNote))
					{
						if (listNote != null)
						{
							for (int cx = 0; cx < listNote.Count; cx++)
							{
								if (listNote[cx].nodeID != 200)
								{
									isSkip = false;
									break;
								}
							}
						}
					}
					if (isSkip)
					{
						convertData.trackView.RemoveAt(i);
						i--;
					}
					else
					{
					}
				}
			}
			#endregion

			LevelDataModel levelData = new LevelDataModel();
			levelData.songData = song;
			levelData.noteData = convertData.trackView;
			levelData.playbackData = convertData.trackView;
			levelData.dicNotePlay = convertData.dicNotePlay;
			levelData.dicDuetNote = convertData.dicDuetNote;
			levelData.BPM = ptJsonData.baseBpm;
			levelData.denominator = 2;
			levelData.tickPerQuarterNote = (int)(60 / levelData.BPM * 1000) * 4 / 3;
			float timeToFinish = indexNote * (60 / levelData.BPM);
			float baseSpeed = timeToFinish / indexNote;

			StartCoroutine(Parse(levelData, onParseProgress, onParseComplete, onParseError, false));
		}

		private string FixString(string fixString)
		{
			fixString = fixString.ToString();
			fixString = fixString.Replace("[", ":");
			fixString = fixString.Replace("]", "");

			fixString = fixString.Replace('[', ':');
			fixString = fixString.Replace(']', '\0');

			fixString = fixString.Replace("(", "");
			fixString = fixString.Replace(")", "");

			fixString = fixString.Replace('(', '\0');
			fixString = fixString.Replace(')', '\0');

			int count = 0;
			while (true)
			{
				int indexBegin = fixString.IndexOf('<');
				int indexEnd = fixString.IndexOf('>');
				if (indexBegin >= 0 && indexEnd > indexBegin)
				{

					string sub = fixString.Substring(indexBegin - 1, indexEnd - indexBegin + 2);
					string orginal = sub;

					sub = sub.Replace("<", "=");
					sub = sub.Replace(">", "");
					sub = sub.Replace(",", "$");

					sub = sub.Replace('<', '=');
					sub = sub.Replace('>', '\0');
					sub = sub.Replace(',', '$');
					fixString = fixString.Replace(orginal, sub);

				}
				else
				{
					break;
				}
				count++;
				if (count > 100)
				{
					Debug.LogError("error Fix String");
					break;
				}
			}
			return fixString;
		}

		private void DownloadAndParse(
			SongDataModel song,
			Action<string> onDownloadError,
			Action<string> onParseError,
			Action<float> onDownloadProgress,
			Action<float> onParseProgress,
			Action onDownloadComplete,
			Action<LevelDataModel, List<TileData>> onParseComplete)
		{
			AssetDownloader.Instance.DownloadAndCacheAsset(
				song.songURL,
				song.version,
				onDownloadProgress,
				onDownloadError,
				(midi) =>
				{
					Helpers.Callback(onDownloadComplete);

					LevelDataModel levelData = new LevelDataModel();
					levelData.songData = song;

					var data = new MidiData();
					MidiParser.ParseNotesData(midi.bytes, ref data);
					levelData.BPM = data.beatsPerMinute;
					levelData.denominator = data.denominator;
					levelData.tickPerQuarterNote = (int)data.deltaTickPerQuarterNote;
					levelData.noteData = data.notesData[NoteTrack];
					levelData.playbackData = data.notesData[PlaybackTrack];

					StartCoroutine(Parse(levelData, onParseProgress, onParseComplete, onParseError));
				});
		}

		/// <summary>
		/// From provided note data, prepare tile data for the game. After this methods is call, the game can be played
		/// </summary>
		private IEnumerator Parse(
			LevelDataModel levelData,
			Action<float> onProgress = null,
			Action<LevelDataModel, List<TileData>> onComplete = null,
			Action<string> onError = null,
			bool cachingEnabled = true)
		{
			int ticksPerTile = levelData.songData.ticksPerTile;

			int minTickPerTile = Mathf.FloorToInt(ticksPerTile * (1 - tickTolerance));
			int maxTickPerTile = Mathf.CeilToInt(ticksPerTile * (1 + tickTolerance));

			levelData.playbackData.Sort((x, y) => (x.timeAppear.CompareTo(y.timeAppear)));
			levelData.noteData.Sort((x, y) => (x.timeAppear.CompareTo(y.timeAppear)));

			List<TileData> tileList = new List<TileData>(1000);

			//we know that note data will always less or equals to playback data
			//so we will start by traverse through list note data
			NoteData currentNote, nextNote;
			int currentNoteIndex;
			float lastReleaseThreadTime = Time.realtimeSinceStartup;

			//this variable is used to reduce number of cast operation
			float noteDataCount = levelData.noteData.Count;

			int countLock = 0;
			//for each note in view list
			for (int i = 0; i < levelData.noteData.Count; i++)
			{
				currentNoteIndex = i;

				//set up range for checking song data
				currentNote = levelData.noteData[currentNoteIndex];
				nextNote = null;
				countLock++;
				if (countLock % 10 == 0)
				{
					yield return null;
					lastReleaseThreadTime = Time.realtimeSinceStartup;
					Helpers.CallbackWithValue(onProgress, ((i / noteDataCount)));
				}

				//try to get next view note (must be different at timestamp with current note)
				while (++i < levelData.noteData.Count)
				{
					//++i;
					nextNote = levelData.noteData[i];
					//stop the loop right when next note is found
					if (nextNote.timeAppear != currentNote.timeAppear)
					{
						//decrease i so that at the end of the loop, it can be increased gracefully
						--i;
						break;
					} // if
				} // while

				if (i >= levelData.noteData.Count)
				{
					i = levelData.noteData.Count - 1;
				} // if

				//how many notes existed at the same timestamp
				int numConcurrentNotes = i - currentNoteIndex + 1;

				//for each note, create a tile
				for (int j = currentNoteIndex; j <= i; j++)
				{
					//with each note data, there is a tile
					TileData tileData = new TileData();

					tileData.type = TileType.Normal;
					tileData.notes = new List<NoteData>();
					tileData.startTime = currentNote.timeAppear;
					tileData.startTimeInTicks = currentNote.tickAppear;
					tileData.soundDelay = 0;
					tileData.duetNode = currentNote.duetNode;

					int startTime, endTime;
					startTime = endTime = -1;
					switch (numConcurrentNotes)
					{
						//only 1 tile
						case 1:
							tileData.subType = TileType.Normal;
							startTime = currentNote.tickAppear;
							endTime = ((nextNote == null) ? currentNote.tickAppear + currentNote.durationInTick : nextNote.tickAppear);
							break;
						//dual tile
						case 2:
							tileData.subType = TileType.Dual;
							if (j % 2 == 0)
							{
								startTime = currentNote.tickAppear;
								endTime = currentNote.tickAppear + (int)(currentNote.durationInTick * 0.5f);
							}
							else
							{
								if (levelData.dicNotePlay == null)
								{// khong tinh delay cho piano tile
									tileData.soundDelay = currentNote.duration * 0.5f;
								}
								startTime = currentNote.tickAppear + (int)(currentNote.durationInTick * 0.5f);
								endTime = ((nextNote == null) ? currentNote.tickAppear + currentNote.durationInTick : nextNote.tickAppear);
							}

							break;
						//big tile
						case 3:
							tileData.subType = TileType.Big;
							if (tileList.Count > 1)
							{
								TileData lastTileData = tileList[tileList.Count - 1];
								if (lastTileData.startTimeInTicks != currentNote.tickAppear)
								{
									startTime = currentNote.tickAppear;
									endTime = ((nextNote == null) ? currentNote.tickAppear + currentNote.durationInTick : nextNote.tickAppear);
								}
								else
								{
									startTime = endTime = -1;
								}
							}
							break;
					} // switch


					if (startTime < 0 || endTime < 0)
					{
						continue;
					}

					int _endTimeTmp = i < levelData.noteData.Count - 1 ? levelData.noteData[i + 1].tickAppear : endTime;

					if (levelData.dicNotePlay == null)
					{// cach cu midieditor
						AddConcurrentMidiDataByTick(
							ref tileData,
							levelData.playbackData,
							startTime,
							endTime,
							j
						);
					} // if
					else
					{// cach cua piano tile
						maxTickPerTile = 500;
						minTickPerTile = 100;
						List<NoteData> listNote = null;
						if (levelData.dicNotePlay.TryGetValue(levelData.noteData[i], out listNote))
						{
							if (listNote != null)
							{
								for (int cx = 0; cx < listNote.Count; cx++)
								{
									tileData.notes.Add(listNote[cx]);
								} // for
							} // if
						} // if
					} // else

					tileData.durationInTicks = currentNote.durationInTick;
					tileData.duration = currentNote.duration;
					//if a tile has duration belong to the normal tile's range
					if (minTickPerTile <= tileData.durationInTicks && tileData.durationInTicks <= maxTickPerTile)
					{
						//set it as so
						tileData.type = TileType.Normal;
						tileList.Add(tileData);
					}
					else
					{
						//else, it is either a long note...
						if (maxTickPerTile < tileData.durationInTicks)
						{
							tileData.type = TileType.LongNote;
							tileList.Add(tileData);
						}
						else
						{
							//... or just an error note, fuck that shit
						}
					} // else

					// calculate score
					if (tileData.type == TileType.LongNote)
					{
						tileData.score = levelData.dicNotePlay != null ?
							Mathf.RoundToInt(tileData.durationInTicks / 500) :
							Mathf.RoundToInt(tileData.durationInTicks * 1.0f / levelData.songData.ticksPerTile);
					} // if
					else
					{
						tileData.score = 1;
					} // else

				} // for
			} // for

			//easy, start render in the next frame
			if (cachingEnabled)
			{
				SongTileData b_data = new SongTileData();
				b_data.BPM = levelData.BPM;
				b_data.denominator = levelData.denominator;
				b_data.tickPerQuarterNote = levelData.tickPerQuarterNote;
				b_data.tileList = tileList;
				b_data.songDataModel = levelData.songData;
				FileSystem.Save(b_data, b_data.songDataModel.storeID + ".nuna");
			}
			yield return null;
			Helpers.CallbackWithValue(onComplete, levelData, tileList);
		}

		/// <summary>
		/// Auto add all midi note that has the same time as specified
		/// </summary>
		/// <param name="tile">Tile object to add note into</param>
		/// <param name="playbackData">The list of note data to add</param>
		/// <param name="tickAppear">Begin to check note data from this time stamp (inclusive)</param>
		/// <param name="tickEnd">Note data before this timestamp will be added (exclusive)</param>
		/// <param name="startSearchIndex">Index of playbackData list to start searching</param>
		/// <returns></returns>
		private int AddConcurrentMidiDataByTick(
			ref TileData tile,
			List<NoteData> playbackData,
			int tickAppear,
			int tickEnd,
			int startSearchIndex)
		{
			if (playbackData.Count <= 0)
			{
				Debug.LogError("playbackData null super error");
				return 0;
			}

			for (int i = startSearchIndex; i < playbackData.Count; i++)
			{
				if (playbackData[i].tickAppear == tickAppear)
				{
					tile.notes.Add(playbackData[i]);
				}
				else if (playbackData[i].tickAppear > tickAppear)
				{
					if (playbackData[i].tickAppear < tickEnd)
					{
						tile.notes.Add(playbackData[i]);
					}
					else
					{
						return i;
					}
				}
			}


			return -1;
		}
	}
}
