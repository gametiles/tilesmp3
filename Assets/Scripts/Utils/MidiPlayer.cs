using UnityEngine;
using System.Collections.Generic;
using Amanotes.Utils;
using MovementEffects;

[RequireComponent(typeof(AudioSource))]
public class MidiPlayer : SingletonMono<MidiPlayer>
{
	MidiData data;
	List<NoteData> notesData;
	Queue<List<int>> queueNoteData = new Queue<List<int>>();

	public TextAsset backgroundMidiFile;

	public void Initialize()
	{
	}

#if UNITY_EDITOR
	public bool isTest = false;
	// Update is called once per frame
	void Update()
	{
		if (isTest)
		{
			if (queueNoteData.Count != 0)
			{
				GetNotesInTapHint();
			}
			else
			{
			}
		}
	}
	private void GetNotesInTapHint()
	{
		if (Input.GetMouseButtonDown(0))
		{
			PlayNextNote();
		}
	}
#endif

	/// <summary>
	/// From midi data, parse and edit to make sure it is ready to be played
	/// </summary>
	private void BuildNoteData()
	{
		//		print ("parse");
		SortedDictionary<float, List<int>> listNotes = new SortedDictionary<float, List<int>>();
		for (int i = 0; i < notesData.Count; i++)
		{
			float timeAppear = notesData[i].timeAppear;
			if (!listNotes.ContainsKey(timeAppear))
			{
				List<int> listNotesID = new List<int>();
				listNotesID.Add(notesData[i].nodeID);
				listNotes.Add(timeAppear, listNotesID);
			}
			else
			{
				listNotes[timeAppear].Add(notesData[i].nodeID);
			}
		}
		queueNoteData.Clear();
		foreach (float f in listNotes.Keys)
		{
			queueNoteData.Enqueue(listNotes[f]);
		}
	}

	/// <summary>
	/// Play a single note using piano player
	/// </summary>
	/// <param name="noteID"></param>
	public void PlayPianoNote(int noteID, float volume = 1)
	{
		AudioManager.Instance.PlaySound(noteID, volume);
	}

	private void PlayAudioNotes(List<int> noteID)
	{
		if (noteID != null && noteID.Count > 0)
		{
			for (int i = 0; i < noteID.Count; i++)
			{
				AudioManager.Instance.PlaySound(noteID[i], 0.8f);
			}
		}
	}

	public static List<NoteData> lastNodePlay = new List<NoteData>();

	/// <summary>
	/// Play every note in this list, with a choice to respect different in start time
	/// </summary>
	/// <param name="listNotes">List of note data to play</param>
	/// <param name="timeRatio">How much the time stamp different being modified? Only has effect if the respectTimeStampDifferent is true</param>
	/// <param name="respectTimeStampDifferent">If the note datas have different time stamp, should them be played after each other or the same? </param>
	public void PlayPianoNotes(List<NoteData> listNotes, float timeRatio = 1, bool respectTimeStampDifferent = true, float delay = 0)
	{
		if (listNotes == null || listNotes.Count <= 0)
		{
			return;
		}

		float startTime = listNotes[0].timeAppear;
		if (respectTimeStampDifferent)
		{
			for (int i = 0; i < listNotes.Count; i++)
			{
				//in case user want to play notes in sequence
				float timeDelayed = (listNotes[i].timeAppear - startTime + delay) / timeRatio;
				if (timeDelayed < 0.02f)
				{
					AudioManager.Instance.PlaySound(listNotes[i].nodeID, listNotes[i].volume);
				}
				else
				{
					Timing.RunCoroutine(CoroutinePlaySoundDelayed(listNotes[i].nodeID, timeDelayed, listNotes[i].volume));
				}
			}
		}
		else
		{
			for (int i = 0; i < listNotes.Count; i++)
			{
				AudioManager.Instance.PlaySound(listNotes[i].nodeID, listNotes[i].volume);
			}
		}
	}

	IEnumerator<float> CoroutinePlaySoundDelayed(int noteID, float delay, float volume)
	{
		yield return Timing.WaitForSeconds(delay);
		AudioManager.Instance.PlaySound(noteID, volume);
	}

	/// <summary>
	/// Play next note in queue
	/// </summary>
	public void PlayNextNote()
	{
		//if (!AudioManager.Instance.MuteSFX && AudioManager.Instance.Initialized)
		//{
  //          if (queueNoteData.Count > 0)
		//	{
  //              List<int> noteID = queueNoteData.Dequeue();
		//		//loop song
		//		queueNoteData.Enqueue(noteID);
		//		PlayAudioNotes(noteID);
		//	}
		//}
	}

	public void LoadSong(string filePath, bool isAbsoluteFilePath = false)
	{
		byte[] midiData = FileUtilities.LoadFile(filePath, isAbsoluteFilePath);
		LoadSong(midiData);
	}

	public void LoadSong(byte[] midiData)
	{
		data = new MidiData();
		if (midiData == null)
		{
			return;
		}
		MidiParser.ParseNotesData(midiData, ref data);
		notesData = data.notesData[1];
		BuildNoteData();
	}
}
