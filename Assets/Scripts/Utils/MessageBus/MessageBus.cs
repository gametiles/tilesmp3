using UnityEngine;
using System.Collections.Generic;
using System;
using MovementEffects;

namespace Amanotes.Utils.MessageBus
{
	public class MessageBus : SingletonMono<MessageBus>
	{
		private Dictionary<MessageBusType, List<Action<Message>>> listeners = new Dictionary<MessageBusType, List<Action<Message>>>(50);
		private Timing timer;
		private Queue<Message> normalMSGs;
		private float lastBreakTime = 0;
		private float MAX_FRAME_TIME = 1f / 60f;

		public void Initialize()
		{
			timer = gameObject.AddComponent<Timing>();
			normalMSGs = new Queue<Message>(25);
			timer.RunCoroutineOnInstance(ProcessMessage(), Segment.Update);
		}

		IEnumerator<float> ProcessMessage()
		{
			while (true)
			{
				yield return 0f;
				lastBreakTime = Time.realtimeSinceStartup;
				while (normalMSGs.Count > 0)
				{
					if (Time.realtimeSinceStartup - lastBreakTime >= MAX_FRAME_TIME) break;
					DispatchMessage(normalMSGs.Dequeue());
				}
			}
		}

		public void Subscribe(MessageBusType type, Action<Message> handler)
		{
			if (listeners.ContainsKey(type))
			{
				if (listeners[type] == null)
				{
					listeners[type] = new List<Action<Message>>(20);
				}
			}
			else
			{
				listeners.Add(type, new List<Action<Message>>(20));
			}

			listeners[type].Add(handler);
		}

		public void Unsubscribe(MessageBusType type, Action<Message> handler)
		{
			if (listeners.ContainsKey(type))
			{
				if (listeners[type] != null)
				{
					listeners[type].Remove(handler);
				}
			}
		}

		/// <summary>
		/// Send a message to all subscribers listening to this certain message type
		/// </summary>
		/// <param name="message"></param>
		public void SendMessage(Message message, bool immidiately = false)
		{
			if (immidiately)
			{
				DispatchMessage(message);
			}
			else
			{
				normalMSGs.Enqueue(message);
			}
		}

		private void DispatchMessage(Message message)
		{
			if (listeners.ContainsKey(message.messageType))
			{
				var listHandlers = listeners[message.messageType];
				for (int i = 0; i < listHandlers.Count; i++)
				{
					if (listHandlers[i] != null)
					{
						listHandlers[i](message);
					}
				}
			}
		}

		/// <summary>
		/// Short hand for calling send message with normal priority (will be delay if the target framerate is not reached)
		/// </summary>
		public static void Annouce(Message message)
		{
			Instance.SendMessage(message);
		}
	}
}
