﻿using UnityEngine;
using System.Collections; 

public class TextArea : MonoBehaviour {

    public UILabel lable;
    Animator  animator;


    void Start() {
        animator = GetComponent<Animator>();
        lable.gameObject.SetActive(false);
    }
     
    public void Show(string text) {
        lable.text = text;
        lable.gameObject.SetActive(true);
        animator.Play("TextAreaShow", 0, 0.0f);
    }

    public void Hide() {
        lable.gameObject.SetActive(false);
    }

    public void OnShowCompelete() {
        lable.gameObject.SetActive(false);
    }
}
