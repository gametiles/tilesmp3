using UnityEngine;
using System.Collections;
using Amanotes.Utils;
using Amanotes.PianoChallenge;

/// <summary>
/// Capture screen and send to social app
/// NOTE: edit link down game, subject, content before 
/// </summary>

public class SocialPost : MonoBehaviour
{
	public GameObject shareIcon;

	[HideInInspector]
	public bool isTakingScreenshot = false;
	private bool isProcessing = false;
	private byte[] screenShot;

	private void OnEnable()
	{
		shareIcon.SetActive(true);
	}

	/// <summary>
	/// share screen shot on facebook
	/// show sharing result if user in screen result only
	/// </summary>
	/// <param name="status"></param>
	public void OnShareScreanShotToFacebookClick()
	{
		ShareScreenshot();
	}

	public void ShareScreenshot()
	{
		if (isProcessing)
			return;

		isProcessing = true;
		isTakingScreenshot = true;
		StartCoroutine(ShareScreanShotToFacebook());
	}

	IEnumerator ShareScreanShotToFacebook()
	{
		yield return new WaitForEndOfFrame();

		int width = Screen.width;
		int height = Screen.height;
		Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
		// Read screen contents into the texture
		tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		tex.Apply();
		screenShot = tex.EncodeToPNG();
		isTakingScreenshot = false;
		shareIcon.SetActive(false);
		SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.ShareFacebookPopUp, screenShot);
	}

	public void ShareProgress(string status)
	{
		if (FacebookManager.Instance.IsLogin())
		{
			if (FacebookManager.Instance.CheckHavePublishActionPermision())
			{
				ShareScreen(status);
			}
			else
			{
				LoginThenShare(status);
			}
		}
		else
		{
			LoginThenShare(status);
		}
	}
	void LoginThenShare(string message)
	{
		string status = message;
		FacebookManager.Instance.RequestPublishAction(res2 =>
		{
			bool isLogined = FacebookManager.Instance.IsLogin();

			if (isLogined && FacebookManager.Instance.CheckHavePublishActionPermision())
			{
				ShareScreen(status);
			}
			else
			{
				EndProcess();
			}
		});
	}

	void ShareScreen(string status)
	{
		FacebookManager.Instance.ShareFeedWithScreenShot(status, screenShot, shareRusult =>
		{
			EndProcess();
		});
	}

	void EndProcess()
	{
		shareIcon.SetActive(true);
		isProcessing = false;
	}

	public void SendCancel()
	{
		isTakingScreenshot = false;
		isProcessing = false;
		shareIcon.SetActive(true);
	}
}
