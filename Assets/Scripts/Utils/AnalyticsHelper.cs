using UnityEngine;
using System.Collections.Generic;
using Facebook.Unity;
using Amanotes.PianoChallenge;

namespace Amanotes.Utils
{
	public class AnalyticsHelper : SingletonMono<AnalyticsHelper>
	{
		[Header("Which analytics service to use?")]
		[SerializeField] bool useFacebook = true;
		[SerializeField] bool useUnityAnalytics = true;

		private static Dictionary<string, object> parameters = new Dictionary<string, object>();

		public const string EVENT_START_GAME = "Start Level";
		public const string EVENT_LEVEL_FISNIHED = "Level Finished";
		public const string EVENT_BUY_INGAME_ITEM = "Buy Ingame Item";
		public const string EVENT_ABTEST = "AB TEST";
		public const string EVENT_ITEM_CLICKED = "Click";
		public const string EVENT_DAILYREWARD_CLAIMED = "Daily reward";
		public const string EVENT_ACHIEVEMENT_CLAIMED = "Claimed";
		public const string EVENT_ACHIEVEMENT_UNLOCKED = "Unlocked";

		public const string EVENTTYPE_TRANSACTION = "Transaction";
		public const string EVENTTYPE_NAVIGATION = "Navigation";
		public const string EVENTTYPE_INTERACTION = "Interaction";

		//new event
		public const string EVENT_SINGLE_PLAY_CLICKED = "Single Play Clicked";
		public const string EVENT_PREMIUM_SONG_PURCHASE = "Premium song purchase";
		public const string EVENT_SONG_START = "Song start";
		public const string EVENT_SONG_END = "Song end";

		public const string EVENT_FB_LOGIN = "FB login";
		public const string EVENT_FB_LOGGED_IN = "FB logged in";
		public const string EVENT_GAME_STARTED = "game started";
		public const string EVENT_INAPP_PURCHASE = "Inapp purchase";

		public const string EVENT_ACHIEVEMENT_CLAIMED2 = "Achievement claimed";
		public const string EVENT_ACHIEVEMENT_CLAIMABLE = "Achievement claimable";

		//online Event
		public const string EVENT_CLICK_ICON_BATTLE = "MP Player Battle Icon";
		public const string EVENT_ONLINE_OPEN = "MP Open Online";

		public const string EVENT_ONLINE_LOGIN = "MP Login";
		public const string EVENT_ONLINE_JOINROOM = "MP Player Join Room";
		public const string EVENT_ONLINE_LEAVEROOM = "MP Player Leave Room";
		public const string EVENT_ONLINE_MOVETOGAMEPLAY = "MP MoveTo Gameplay";
		public const string EVENT_ONLINE_PLAYCLICK = "MP Play Click";
		public const string EVENT_ONLINE_DOWNLOAD_FINISH = "MP Player Song Ready";
		public const string EVENT_ONLINE_READY_PLAY = "MP Player Ready Play";
		public const string EVENT_ONLINE_BATTLE_START = "MP Player Battle Start";
		public const string EVENT_ONLINE_BATTLE_FAIL = "MP Battle Fail";
		public const string EVENT_ONLINE_BATTLE_FINISH = "MP Battle Finish";
		public const string EVENT_ONLINE_USER_FINISH = "MP Player Finish";
		public const string EVENT_ONLINE_WIN = "MP Win";
		public const string EVENT_ONLINE_LOSE = "MP lose";
		public const string EVENT_ONLINE_DIE = "MP die";
		public const string EVENT_ONLINE_CONNECTERROR = "MP OnConnectionError";
		public const string EVENT_ONLINE_DISCONECT = "MP OnServerDisconnect";
		public const string EVENT_ONLINE_ROOMCREATE = "MP Room";
		public const string EVENT_ONLINE_START = "MP start "; // behavior

		public const string EVENT_ONLINE_RESULT_CLOSE = "MP Result Exit";
		public const string EVENT_ONLINE_REPLAY_ROOM = "MP Result replay";

		public const string EVENT_ONLINE_TIME_WAIT = "MP Player Time Wating";

		public const string EVENT_ONLINE_TIME_BEGIN_TO_MOVETO = "MP Step1  Begin To Moveto";
		public const string EVENT_ONLINE_TIME_MOVETO_TO_READYSONG = "MP Step2 Moveto To ReadySong";
		public const string EVENT_ONLINE_TIME_READYSONG_TO_BATTLESTART = "MP Step3 ReadySong To BattleStart";

		public const string EVENT_ONLINE_SCORE_RANGE = "MP Player Score Range";
		public const string EVENT_ONLINE_ROOM_RANGE_USER = "MP Room Range Level";
		public const string EVENT_ONLINE_ROOM_RANGE_SONG = "MP Room Range Song";
		public const string EVENT_ONLINE_BATTLE_COUNT = "MP Battle Count";
		public const string EVENT_ONLINE_DAILY_BATTLE_COUNT = "MP Daily Battle Count";

		public const string EVENT_BATTLE_ON_FIRST_DATE = "MP Player Day 1";

		public const string EVENT_LEADERBOARD_OPEN = "Leaderboard Open";
		public const string EVENT_REWARD_VIDEO_BUTTON_SHOWN = "Reward Video Button Shown";
		public const string EVENT_REWARD_VIDEO_BUTTON_CLICKED = "Reward Video Button Clicked";
		public const string EVENT_REWARD_VIDEO_WATCHED = "Reward Video Watched";
		public const string EVENT_REWARD_VIDEO_FAILED = "Reward Video Failed";
		public const string EVENT_FULLSCREEN_AD_TRIGGERED = "Fullscreen Ad Triggered";
		public const string EVENT_FULLSCREEN_AD_SHOWN = "Fullscreen Ad Shown";
		public const string EVENT_BUY_PREMIUM_SONG_CLICKED = "Buy Premium Song Clicked";
		public const string EVENT_PREMIUM_SONG_START = "Premium Song Start";
		public const string EVENT_PREMIUM_SONG_END = "Premium Song End";
		public const string EVENT_FACEBOOK_SLIDE_TEXT = "Facebook Slide Text Shown";
		public const string EVENT_NO_ADS_BUTTON_SHOWN = "No Ads Button Shown";
		public const string EVENT_NO_ADS_BUTTON_CLICKED = "No Ads Button Shown";
		public const string EVENT_NO_ADS_PURCHASED = "No Ads Purchased";
		public const string EVENT_LEVEL_UP = "Level_Up";
		public const string EVENT_LEVEL_UP_NUM = "Level Up - ";
		public const string EVENT_DEVICE_INFO = "Device Info";
		public const string EVENT_SUBMIT_RATING = "Submit Rating";
		public const string EVENT_GOTO_STORE_RATING = "Goto Store Rating";
		public const string EVENT_GAME_LAUNCH = "Game Launch";
		public const string EVENT_LOCAL_NOTIFICATION_COUNT = "Local Notification Count";
		public const string EVENT_LOCAL_NOTIFICATION_CLICKED = "Local Notification Clicked";
		public const string EVENT_CROSS_PROMO_ITEM_CLICKED = "Coss Promotion Item Clicked";
		public const string EVENT_IAP_OPEN = "IAP Open";
		public const string EVENT_SONG_LEADERBOARD_FAILED = "Song Leaderboard Load Failed";

		public const string EVENT_SURVEY = "SURVEY EVENT";

		public const string ONBOARDING = "OnBoarding";

		private static string GetMpEventName(string s)
		{
			return BattleManager.Instance.PlayingWithFriends ? "Friend " + s : s;
		}

		/// <summary>
		/// Start logging session
		/// </summary>
		public void Initialize()
		{
		}

		public void LogEvent(string eventName)
		{
			if (useFacebook && FB.IsInitialized)
			{
				FB.LogAppEvent(eventName);
			}

			if (useUnityAnalytics)
			{
				UnityEngine.Analytics.Analytics.CustomEvent(eventName);
			}
		}

		void LogEventWithParams(string eventName)
		{
			if (useFacebook && FB.IsInitialized)
			{
				FB.LogAppEvent(eventName, 1, parameters);
			}

			if (useUnityAnalytics)
			{
				UnityEngine.Analytics.Analytics.CustomEvent(eventName, parameters);
			}
		}

		/// <summary>
		/// Log event when player open the game
		/// </summary>
		public void LogGameLaunch(int times)
		{
			parameters.Clear();
			parameters.Add("times", times);
			LogEvent(EVENT_GAME_LAUNCH);
		}

		public void LogLevelUp(int level, int plays, int days)
		{
			parameters.Clear();
			parameters.Add("level", level);
			parameters.Add("plays", plays);
			parameters.Add("days", days);
			LogEventWithParams(EVENT_LEVEL_UP);
		}

		public void LogLevelUp(
			int level, int plays, int days,
			int normalSongs, int premiumSongs, int normalBattles, int friendBattles,
			int paidForPremium, int paidForLives, int paidForContinue)
		{
#if SEND_SEPARATE_LEVEL_UP_EVENT || true
			string paid4PremiumCatetory, paid4LivesCategory, paidForContinueCategory, diamondsCategory;
			if (paidForPremium == 0)
			{
				paid4PremiumCatetory = "0";
			}
			else if (paidForPremium < 100)
			{
				paid4PremiumCatetory = "<100";
			}
			else if (paidForPremium < 200)
			{
				paid4PremiumCatetory = "100-200";
			}
			else if (paidForPremium < 300)
			{
				paid4PremiumCatetory = "200-300";
			}
			else if (paidForPremium < 400)
			{
				paid4PremiumCatetory = "300-400";
			}
			else if (paidForPremium < 500)
			{
				paid4PremiumCatetory = "400-500";
			}
			else if (paidForPremium < 600)
			{
				paid4PremiumCatetory = "500-600";
			}
			else if (paidForPremium < 700)
			{
				paid4PremiumCatetory = "600-700";
			}
			else if (paidForPremium < 800)
			{
				paid4PremiumCatetory = "700-700";
			}
			else
			{
				paid4PremiumCatetory = "800+";
			}

			if (paidForLives == 0)
			{
				paid4LivesCategory = "0";
			}
			else if (paidForLives < 20)
			{
				paid4LivesCategory = "<20";
			}
			else if (paidForLives < 50)
			{
				paid4LivesCategory = "20-50";
			}
			else if (paidForLives < 100)
			{
				paid4LivesCategory = "50-100";
			}
			else if (paidForLives < 150)
			{
				paid4LivesCategory = "100-150";
			}
			else if (paidForLives < 200)
			{
				paid4LivesCategory = "150-200";
			}
			else if (paidForLives < 250)
			{
				paid4LivesCategory = "200-250";
			}
			else if (paidForLives < 300)
			{
				paid4LivesCategory = "250-300";
			}
			else if (paidForLives < 350)
			{
				paid4LivesCategory = "300-350";
			}
			else
			{
				paid4LivesCategory = "350+";
			}

			if (paidForContinue == 0)
			{
				paidForContinueCategory = "0";
			}
			else if (paidForContinue < 10)
			{
				paidForContinueCategory = "<10";
			}
			else if (paidForContinue < 20)
			{
				paidForContinueCategory = "10-20";
			}
			else if (paidForContinue < 30)
			{
				paidForContinueCategory = "20-30";
			}
			else if (paidForContinue < 40)
			{
				paidForContinueCategory = "30-40";
			}
			else if (paidForContinue < 50)
			{
				paidForContinueCategory = "40-50";
			}
			else if (paidForContinue < 60)
			{
				paidForContinueCategory = "50-60";
			}
			else if (paidForContinue < 70)
			{
				paidForContinueCategory = "60-70";
			}
			else if (paidForContinue < 80)
			{
				paidForContinueCategory = "70-80";
			}
			else
			{
				paidForContinueCategory = "80+";
			}

			int diamonds = ProfileHelper.Instance.CurrentDiamond;
			if (diamonds < 20)
			{
				diamondsCategory = "0-20";
			}
			else if (diamonds < 50)
			{
				diamondsCategory = "20-50";
			}
			else if (diamonds < 100)
			{
				diamondsCategory = "50-100";
			}
			else if (diamonds < 150)
			{
				diamondsCategory = "100-150";
			}
			else if (diamonds < 200)
			{
				diamondsCategory = "150-200";
			}
			else if (diamonds < 250)
			{
				diamondsCategory = "200-250";
			}
			else if (diamonds < 300)
			{
				diamondsCategory = "250-300";
			}
			else if (diamonds < 350)
			{
				diamondsCategory = "300-350";
			}
			else if (diamonds < 400)
			{
				diamondsCategory = "350-400";
			}
			else
			{
				diamondsCategory = "400+";
			}

			parameters.Clear();
			parameters.Add("level", level);
			parameters.Add("plays", plays);
			parameters.Add("days", days);
			parameters.Add("plays_normal", normalSongs);
			parameters.Add("plays_premium", premiumSongs);
			parameters.Add("battles_normal", normalBattles);
			parameters.Add("battles_friend", friendBattles);
			parameters.Add("paid_for_premium", paid4PremiumCatetory);
			parameters.Add("paid_for_lives", paid4LivesCategory);
			parameters.Add("paid_for_continue", paidForContinueCategory);
			parameters.Add("diamonds", diamondsCategory);

			LogEventWithParams(EVENT_LEVEL_UP_NUM + level.ToString());
#endif
		}

		/// <summary>
		/// Log event when user start playing a level
		/// </summary>
		public void LogLevelStarted(string levelName)
		{
			parameters.Clear();
			parameters.Add("Song name", levelName);
			LogEventWithParams(EVENT_START_GAME);
		}

		/// <summary>
		/// Log event and measurement when player finished a level
		/// </summary>
		public void LogLevelFinished(string levelName, int score, int numStar, int crown)
		{
			parameters.Clear();
			parameters.Add("stars", numStar);
			parameters.Add("crowns", crown);
			LogEventWithParams(EVENT_LEVEL_FISNIHED);
		}

		public const string SONG_LIST_NORMAL = "SongList Normal";
		public const string SONG_LIST_PREMIUM = "SongList Premium";

		List<string> scenesToLog = new List<string> {
			ProjectConstants.Scenes.HomeUI.ToString(),
			ProjectConstants.Scenes.IAP.ToString(),
			ProjectConstants.Scenes.ContinuePopup.ToString(),
			ProjectConstants.Scenes.ResultUI.ToString(),
			ProjectConstants.Scenes.SongLeaderboardPopup.ToString(),
			SONG_LIST_NORMAL,
			SONG_LIST_PREMIUM
		};

		List<string> scenesNotToLog = new List<string> { GroupHomeSceneType.SongList.ToString() };

		public void LogOpenScene(string sceneName, bool userAction = false)
		{
			// Only send event "HomeUI" if player intentionally tap Home
			if (sceneName.Equals(ProjectConstants.Scenes.HomeUI.ToString()) && !userAction)
				return;

			if (useFacebook && FB.IsInitialized)
			{
				parameters.Clear();
				parameters.Add("Scene name", sceneName);
				FB.LogAppEvent(AppEventName.ViewedContent, 1, parameters);
			}

			if (useUnityAnalytics && !scenesNotToLog.Contains(sceneName))
			{
				if (!scenesToLog.Contains(sceneName))
				{
					sceneName = "Others";
				}

				parameters.Clear();
				parameters.Add("Scene name", sceneName);
				UnityEngine.Analytics.Analytics.CustomEvent("Open Scene", parameters);
			}
		}

		public void LogFavorite(string songName, bool active)
		{
			parameters.Clear();
			parameters.Add("Song name " + active, songName);
			LogEventWithParams("Song Favorite " + active);
		}

		public void LogClaimDailyReward(string index)
		{
			parameters.Clear();
			parameters.Add("Day", index);
			LogEventWithParams(EVENT_DAILYREWARD_CLAIMED);
		}

		public void LogClickItem(string itemID)
		{
			parameters.Clear();
			parameters.Add("item", itemID);
			LogEventWithParams(EVENT_ITEM_CLICKED);
		}

		public void LogClaimAchievement(string achievementID)
		{
			parameters.Clear();
			parameters.Add("achievement", achievementID);
			LogEventWithParams(EVENT_ACHIEVEMENT_CLAIMED);
		}

		public void LogUnlockAchievement(string achievementID)
		{
			parameters.Clear();
			parameters.Add("achievement", achievementID);
			LogEventWithParams(EVENT_ACHIEVEMENT_UNLOCKED);
		}

		public void LogBuyMarketItem(string itemID, string itemName, string itemType)
		{

			LogBuyInAppPurchase(itemName);

			parameters.Clear();
			parameters.Add("Item ID", itemID);
			parameters.Add("Item name", itemName);
			parameters.Add("Item Type", itemType);

			if (useFacebook)
			{
				FB.LogPurchase(0, "", parameters);
			}

			if (useUnityAnalytics)
			{
				UnityEngine.Analytics.Analytics.Transaction(itemID, 0, "USD");
			}
		}

		public void LogBuyInGameItem(string itemType, int quantity, int price)
		{
			parameters.Clear();
			parameters.Add("Item Type", itemType);
			parameters.Add("Quantity", quantity);
			parameters.Add("Price", price);
			LogEventWithParams(EVENT_BUY_INGAME_ITEM);
		}

		public void LogRewardVideoWatched(string provider)
		{
			parameters.Clear();
			parameters.Add("provider", provider);
			LogEventWithParams(EVENT_REWARD_VIDEO_WATCHED);
		}

		public void LogRewardVideoFailed(string reason)
		{
			parameters.Add("reason", reason);
			LogEventWithParams(EVENT_REWARD_VIDEO_FAILED);
		}

		public void LogFBLogin()
		{
			parameters.Clear();
			parameters.Add("Place", FacebookManager.Instance.loginPlace);
			parameters.Add("TextChoice", FacebookManager.Instance.incentiveTextChoice);
			LogEventWithParams(EVENT_FB_LOGIN);
		}

		private static string GetSingleMode()
		{
			return GameManager.Instance.SessionData.endless ? "Endless" : "Normal";
		}

		public void LogSongStart(SongDataModel song)
		{
			parameters.Clear();
			parameters.Add("Song_name", song.name);
			parameters.Add("mode", GetSingleMode());
			string eventName = song.pricePrimary > 0 ? EVENT_PREMIUM_SONG_START : EVENT_SONG_START;
			LogEventWithParams(eventName);
		}

		public void LogSongEnd(SongDataModel song, int stars, int crowns)
		{
			parameters.Clear();
			parameters.Add("Song_name", song.name);
			string eventName = song.pricePrimary > 0 ? EVENT_PREMIUM_SONG_END : EVENT_SONG_END;

			if (crowns > 0)
			{
				parameters.Add("result", crowns.ToString() + " crown");
			}
			else
			{
				parameters.Add("result", stars < 1 ? "<1 star" : stars.ToString() + " star");
			}
			parameters.Add("mode", GetSingleMode());
			LogEventWithParams(eventName);
		}

		public void LogPremiumSongPurchased(string songName, string place)
		{
			parameters.Clear();
			parameters.Add("song_name", songName);
			parameters.Add("place", place);
			LogEventWithParams(EVENT_PREMIUM_SONG_PURCHASE);
		}

		public void LogGameStarted()
		{
			LogEvent(EVENT_GAME_STARTED);
		}

		public void LogBuyInAppPurchase(string itemName)
		{
			parameters.Clear();
			parameters.Add("name_of_the_item", itemName);
			LogEventWithParams(EVENT_INAPP_PURCHASE);
		}

		public void LogClaimedAchievement(string achievementName)
		{
			parameters.Clear();
			parameters.Add("Name of the achievement", achievementName);
			LogEventWithParams(EVENT_ACHIEVEMENT_CLAIMED2);
		}

		public void LogClamiableAchievement(string achievementName)
		{
			parameters.Clear();
			parameters.Add("Name of the achievement", achievementName);
			LogEventWithParams(EVENT_ACHIEVEMENT_CLAIMABLE);
		}

		public void LogOnline(string eventName)
		{
			eventName = GetMpEventName(eventName);
			LogEvent(eventName);
		}


		public void LogSurvey(string survey)
		{
			parameters.Clear();
			parameters.Add(EVENT_SURVEY, survey);
			LogEventWithParams(EVENT_SURVEY);
		}


		public void LogOnBoarding(string param)
		{
			parameters.Clear();
			parameters.Add(ONBOARDING, param);
			LogEventWithParams(ONBOARDING);
		}

		public void LogOnlineSongStart(string s)
		{
			parameters.Clear();
			parameters.Add("MP Song Start", s);
			LogEventWithParams("Online Song Start");
		}

		public float mPTimeBeginBattle = 0;

		public void LogTimeBeginBattle()
		{
			mPTimeBeginBattle = Time.realtimeSinceStartup;
		}
		public void LogTimeBeginToMovetoGP()
		{
			float now = Time.realtimeSinceStartup;
			float timeReady = now - mPTimeBeginBattle;
			int rangeScore = GetRangeTime(timeReady);
			mPTimeBeginBattle = now;

			parameters.Clear();
			parameters.Add("Time_Range", rangeScore);

			LogEventWithParams(GetMpEventName(EVENT_ONLINE_TIME_BEGIN_TO_MOVETO));
		}
		public void LogTimeMoveToGPToReadySong()
		{
			float now = Time.realtimeSinceStartup;
			float timeReady = now - mPTimeBeginBattle;
			int rangeScore = GetRangeTime(timeReady);
			mPTimeBeginBattle = now;

			parameters.Clear();
			parameters.Add("Time_Range", rangeScore);

			LogEventWithParams(GetMpEventName(EVENT_ONLINE_TIME_MOVETO_TO_READYSONG));
		}
		public void LogTimeReadySongToBattleReady()
		{
			float now = Time.realtimeSinceStartup;
			float timeReady = now - mPTimeBeginBattle;
			int rangeScore = GetRangeTime(timeReady);
			mPTimeBeginBattle = now;

			parameters.Clear();
			parameters.Add("Time_Range", rangeScore);
			LogEventWithParams(GetMpEventName(EVENT_ONLINE_TIME_READYSONG_TO_BATTLESTART));
		}

		public int GetRangeTime(float time)
		{
			int rangeScore = 0;
			if (time <= 3)
			{
				rangeScore = 3;
			}
			else if (time <= 5)
			{
				rangeScore = 5;
			}
			else if (time <= 10)
			{
				rangeScore = 10;
			}
			else if (time <= 15)
			{
				rangeScore = 15;
			}
			else if (time <= 20)
			{
				rangeScore = 20;
			}
			else if (time <= 30)
			{
				rangeScore = 30;
			}
			else if (time <= 40)
			{
				rangeScore = 40;
			}
			else if (time <= 50)
			{
				rangeScore = 50;
			}
			else
			{
				rangeScore = 60;
			}
			return rangeScore;
		}

		public void LogBattlePlayerInfo(int userLevel, int songId)
		{
			SongDataModel songModel = GameManager.Instance.StoreData.GetSongDataModelById(songId);
			if (songModel == null)
			{
				return;
			}

			parameters.Clear();
			int distanceSong = songModel.LvToUnlock - userLevel;
			if (distanceSong < 0)
			{
				distanceSong = 0;
			}
			parameters.Add(EVENT_ONLINE_ROOM_RANGE_SONG, distanceSong);
			LogEventWithParams(EVENT_ONLINE_ROOM_RANGE_SONG);
		}

		public void LogBattlePointRange(int score)
		{
			string rangeTime;

			if (score == 0)
			{
				rangeTime = "0";
			}
			else if (score < 50)
			{
				rangeTime = "<50";
			}
			else if (score <= 100)
			{
				rangeTime = "50-100";
			}
			else if (score <= 200)
			{
				rangeTime = "100-200";
			}
			else if (score <= 500)
			{
				rangeTime = "200-500";
			}
			else if (score <= 1000)
			{
				rangeTime = "500-1000";
			}
			else if (score <= 2000)
			{
				rangeTime = "1000-2000";
			}
			else if (score <= 3000)
			{
				rangeTime = "2000-3000";
			}
			else
			{
				rangeTime = "3000+";
			}

			parameters.Clear();
			parameters.Add("Song", GameManager.Instance.SessionData.song.name);
			LogEventWithParams(EVENT_ONLINE_SCORE_RANGE + rangeTime);
		}

		public void LogBattleLobbyOpen(string from)
		{
			parameters.Clear();
			parameters.Add("From", from);
			LogEventWithParams(GetMpEventName(EVENT_ONLINE_OPEN));
		}

		public void LogBattleStart(bool host)
		{
			parameters.Clear();
			parameters.Add("Role", host ? "Host" : "Guest");
			LogEventWithParams(GetMpEventName(EVENT_ONLINE_BATTLE_START));
		}

		public void LogBattleCount(int battles)
		{
			parameters.Clear();
			parameters.Add("Count", battles);
			LogEventWithParams(GetMpEventName(EVENT_ONLINE_BATTLE_COUNT));
		}

		public void LogDailyBattleCount(int battles)
		{
			parameters.Clear();
			parameters.Add("Count", battles);
			LogEventWithParams(GetMpEventName(EVENT_ONLINE_DAILY_BATTLE_COUNT));
		}

		public void LogBattleDisconnected()
		{
			parameters.Clear();
			parameters.Add("Scene", BattleManager.Instance.sceneName);
			parameters.Add("Step", BattleManager.Instance.stepName);
			LogEventWithParams(GetMpEventName(EVENT_ONLINE_DISCONECT));
		}

		public void LogFacebookTextShown(string place)
		{
			parameters.Clear();
			parameters.Add("Text", FacebookManager.Instance.incentiveTextChoice);
			parameters.Add("Place", place);
			LogEventWithParams(EVENT_FACEBOOK_SLIDE_TEXT);
		}

		public void LogLeaderboardOpen(string place)
		{
			parameters.Clear();
			parameters.Add("Place", place);
			LogEventWithParams(EVENT_LEADERBOARD_OPEN);
		}

		public void LogFacebookLoggedIn()
		{
			parameters.Clear();
			parameters.Add("Place", FacebookManager.Instance.loginPlace);
			parameters.Add("TextChoice", FacebookManager.Instance.incentiveTextChoice);
			LogEventWithParams(EVENT_FB_LOGGED_IN);
		}

		public const string IAP_TO_BUY_SONG = "To Buy Song";
		public const string IAP_TO_CONTINUE = "To Continue";
		public const string IAP_MORE_LIVES = "To Get More Lives";
		public const string IAP_TO_BATTLE = "To Battle";
		public const string IAP_MORE_DIAMONDS = "Menu Bar";

		public void LogIapOpen(string reason)
		{
			parameters.Clear();
			parameters.Add("reason", reason);
			LogEventWithParams(EVENT_IAP_OPEN);
		}

		public const string LACK_DIAMONDS_FOR_BATTLE = "Lack Diamonds For Battle";
		public void LogRewardVideoButtonShown(string place, string text)
		{
			parameters.Clear();
			parameters.Add("place", place);
			parameters.Add("text", text);
			LogEventWithParams(EVENT_REWARD_VIDEO_BUTTON_SHOWN);
		}
		public void LogRewardVideoButtonClicked(string place, string text)
		{
			parameters.Clear();
			parameters.Add("place", place);
			parameters.Add("text", text);
			LogEventWithParams(EVENT_REWARD_VIDEO_BUTTON_CLICKED);
		}

		public void LogFullscreenAdTriggered()
		{
			LogEvent(EVENT_FULLSCREEN_AD_TRIGGERED);
		}

		public void LogFullscreenAdShown(string provider)
		{
			parameters.Clear();
			parameters.Add("provider", provider);
			LogEventWithParams(EVENT_FULLSCREEN_AD_SHOWN);
		}

		public void LogBuyPremiumSongClicked(string place)
		{
			parameters.Clear();
			parameters.Add("place", place);
			LogEventWithParams(EVENT_BUY_PREMIUM_SONG_CLICKED);
		}

		public void LogNoAdsButtonShown(string place)
		{
			parameters.Clear();
			parameters.Add("place", place);
			LogEventWithParams(EVENT_NO_ADS_BUTTON_SHOWN);
		}

		public void LogNoAdsButtonClicked(string place)
		{
			parameters.Clear();
			parameters.Add("place", place);
			LogEventWithParams(EVENT_NO_ADS_BUTTON_CLICKED);
		}

		public void LogNoAdsPurchased(string place)
		{
			parameters.Clear();
			parameters.Add("place", place);
			LogEventWithParams(EVENT_NO_ADS_PURCHASED);
		}

		public void LogABTest(string eventName, string option)
		{
			parameters.Clear();
			parameters.Add("option", option);
			LogEventWithParams(eventName);
		}

		public void LogSinglePlayClicked(string place)
		{
			parameters.Clear();
			parameters.Add("place", place);
			LogEventWithParams(EVENT_SINGLE_PLAY_CLICKED);
		}

		public void LogDeviceInfo(int nativeAudioSampleRate)
		{
			parameters.Clear();
			parameters.Add("native_audio_sample_rate", nativeAudioSampleRate);
			LogEventWithParams(EVENT_DEVICE_INFO);
		}

		public void LogSubmitRating(int rating)
		{
			parameters.Clear();
			parameters.Add("rating", rating);
			LogEventWithParams(EVENT_SUBMIT_RATING);
		}

		public void LogGotoStoreRating(int rating)
		{
			parameters.Clear();
			parameters.Add("rating", rating);
			LogEventWithParams(EVENT_GOTO_STORE_RATING);
		}

		public void LogLocalPNCount(int count, int id)
		{
			parameters.Clear();
			parameters.Add("count", count);
			parameters.Add("id", id);
			LogEventWithParams(EVENT_LOCAL_NOTIFICATION_COUNT);
		}

		public void LogLocalPNClicked(int id)
		{
			parameters.Clear();
			parameters.Add("id", id);
			LogEventWithParams(EVENT_LOCAL_NOTIFICATION_CLICKED);
		}

		public void LogCrossPromotionClicked(string trackingCode)
		{
			parameters.Clear();
			parameters.Add("item", trackingCode);
			LogEventWithParams(EVENT_CROSS_PROMO_ITEM_CLICKED);
		}

		public void LogSongLeaderboardLoadFailed(string type, string reason)
		{
			parameters.Clear();
			parameters.Add("type", type);
			parameters.Add("reason", reason);
			LogEvent(EVENT_SONG_LEADERBOARD_FAILED);
		}
	}
}
