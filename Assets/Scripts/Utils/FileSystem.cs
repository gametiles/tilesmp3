using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Amanotes.PianoChallenge
{
	public static class FileSystem
	{
		public static bool Save<T>(T data, string fileName) where T : class
		{
			try
			{
				BinaryFormatter formatter = new BinaryFormatter();
				using (FileStream stream = new FileStream(SavePath(fileName), FileMode.Create))
				{
					try
					{
						formatter.Serialize(stream, data);
					}
					catch (Exception e)
					{
						Debug.LogWarning(e.Message);
						return false;
					}
				}
			}
			catch {
				return false;
			}

			return true;
		}

		public static T Load<T>(string fileName) where T : class
		{
			if (!FileExisting (fileName))
			{
				return default(T);
			}

			BinaryFormatter formatter = new BinaryFormatter ();

			using (FileStream stream = new FileStream (SavePath (fileName), FileMode.Open))
			{
				try
				{
					return formatter.Deserialize (stream) as T;
				}
				catch (Exception e)
				{
					Debug.Log (e.Message);
					return default(T);
				}
			}
		}

		public static T LoadResource<T>(string resourceName) where T : class
		{
			TextAsset asset = Resources.Load (resourceName) as TextAsset;
			if (asset == null)
			{
				Development.Log ("Cannot find resource: " + resourceName);
				return default(T);
			}
				
			BinaryFormatter formatter = new BinaryFormatter ();
			using (Stream s = new MemoryStream (asset.bytes))
			{
				try
				{
					return formatter.Deserialize (s) as T;
				}
				catch (Exception ex)
				{
					Development.Log ("Could not de-serialize level data with exception: " + ex);
					return default(T);
				}
			}
		}

		public static bool SaveTileData (SongTileData saveGame, string name)
		{
			BinaryFormatter formatter = new BinaryFormatter ();

			using (FileStream stream = new FileStream (SavePath (name), FileMode.Create))
			{
				try
				{
					formatter.Serialize (stream, saveGame);
				}
				catch (Exception e)
				{
					Debug.LogWarning (e.Message);
					return false;
				}
			}

			return true;
		}

		public static bool Delete (string fileName)
		{
			try
			{
				File.Delete (SavePath (fileName));
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}

		public static bool FileExisting (string name)
		{
			return FileUtilities.IsFileExist (name, false);
		}

		private static string SavePath (string name)
		{
			return FileUtilities.GetWritablePath (name);
		}
	}
}
