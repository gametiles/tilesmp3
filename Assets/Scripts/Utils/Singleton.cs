/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Mateusz Majchrzak
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

using UnityEngine;

//========================================================
// class BaseSingleton ThuanTQ Add 2016-02-19
//========================================================
// - for making singleton object
// - usage
//      + declare class(derived )
//          public class OnlyOne : BaseSingleton< OnlyOne >
//      + client
//          OnlyOne.Instance.[method]
//========================================================
public abstract class Singleton<T> where T : new()
{
	private static T singleton;

	public static T Instance {
		get {
			if (singleton == null)
			{
				singleton = new T ();
			}
			return singleton;
		}
	}
}

//========================================================
// class BaseSingleton ThuanTQ Add 2016-02-19
//========================================================
// - for making singleton object mono in scene
// - usage
//      + declare class(derived )
//          public class OnlyOne : BaseSingleton< OnlyOne >
//      + client
//          OnlyOne.Instance.[method]
//      + if scene don't have animation instance, it will be auto generate with name prefix '@'
//      + if have 2 instance, new instance will auto remove
//      + singleton mono just exist in one scene, call don't detroy on load to keep it forever
//========================================================
public class SingletonMono<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T singleton;

	public static bool IsInstanceValid ()
	{
		return singleton != null;
	}

	void Reset ()
	{
		gameObject.name = typeof(T).Name;
	}

	public static T Instance {
		get {
			if (singleton == null)
			{
				singleton = (T)FindObjectOfType (typeof(T));
				if (singleton == null)
				{
					GameObject obj = new GameObject ();
					obj.name = "[@" + typeof(T).Name + "]";
					singleton = obj.AddComponent<T> ();
				}
			}

			return singleton;
		}
	}

}
