
using Amanotes.PianoChallenge;
using Amanotes.Utils;
using System.Collections.Generic;
using UnityEngine;

// I hate to do this but I was forced to
class AbusedAnalyticsHelper
{
	static private AbusedAnalyticsHelper instance = null;
	static private readonly string BATTLE_COUNT_KEY = "Battle Count";
	static private readonly string BATTLE_FRIEND_COUNT_KEY = "Friend Battle Count";

	private static Dictionary<string, object> parameters = new Dictionary<string, object>();

	private int battles = 0;
	private int friendBattles = 0;

	static public AbusedAnalyticsHelper Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new AbusedAnalyticsHelper();
			}
			return instance;
		}
	}

	public void OnBattlePlayed()
	{
		if (BattleManager.Instance.PlayingWithFriends)
		{
			friendBattles++;
			PlayerPrefs.SetInt(BATTLE_FRIEND_COUNT_KEY, friendBattles);

			if (friendBattles >= 50)
			{
				friendBattles = 50;
			}
			AnalyticsHelper.Instance.LogBattleCount(friendBattles);
		}
		else
		{
			battles++;
			PlayerPrefs.SetInt(BATTLE_COUNT_KEY, battles);

			if (battles >= 50)
			{
				battles = 50;
			}
			AnalyticsHelper.Instance.LogBattleCount(battles);
		}
	}

	private AbusedAnalyticsHelper()
	{
		battles = PlayerPrefs.GetInt(BATTLE_COUNT_KEY);
		friendBattles = PlayerPrefs.GetInt(BATTLE_FRIEND_COUNT_KEY);
	}
}
