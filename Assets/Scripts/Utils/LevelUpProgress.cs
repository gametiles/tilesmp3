
using UnityEngine;
using Amanotes.Utils;
using System;
using Amanotes.PianoChallenge;
using System.Collections.Generic;
using Amanotes.Utils.MessageBus;
using ProjectConstants;

class LevelUpProgress
{
	static private LevelUpProgress instance = null;

	private int songs = 0; // Number of songs played since last level up
	private int normalSongs = 0; // Number of normal songs played since last level up
	private int premiumSongs = 0; // Number of premium songs played since last level up
	private int normalBattles = 0; // Number of normal battles played since last level up
	private int friendBattles = 0; // Number of friend battles played since last level up
	private int paid4Songs = 0; // Number of diamonds spent for premium songs
	private int paid4Lives = 0; // Number of diamonds spent for lives
	private int paid4Continue = 0; // Number of diamonds spent to continue

	private DateTime lastLevelUpDate = new DateTime(1970, 1, 1);

	private const string LEVEL_UP_PROGRESS_DATE = "Level Up Progress - Days";
	private const string LEVEL_UP_PROGRESS_SONGS = "Level Up Progress - Songs";
	private const string LEVEL_UP_PROGRESS_NORMAL_SONGS = "Level Up Progress - Normal Songs";
	private const string LEVEL_UP_PROGRESS_PREMIUM_SONGS = "Level Up Progress - Premium Songs";
	private const string LEVEL_UP_PROGRESS_NORMAL_BATTLES = "Level Up Progress - Normal Battles";
	private const string LEVEL_UP_PROGRESS_FRIEND_BATTLES = "Level Up Progress - Friend Battles";
	private const string LEVEL_UP_PROGRESS_PAID_4_SONGS = "Level Up Progress - Paid 4 Songs";
	private const string LEVEL_UP_PROGRESS_PAID_4_LIVES = "Level Up Progress - Paid 4 Lives";
	private const string LEVEL_UP_PROGRESS_PAID_4_CONTINUE = "Level Up Progress - Paid 4 Continue";

	static public LevelUpProgress Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new LevelUpProgress();
			}
			return instance;
		}
	}

	public int AddExp(SongDataModel song, float exp)
	{
		int oldLevel = ProfileHelper.Instance.Level;
		ConfigUserLevelItem configLv = GameManager.Instance.configUserLevel.GetConfigUserByLevel(oldLevel);
		GameConfigModel gameConfig = GameManager.Instance.GameConfigs;

		float expNextLevel = configLv.ExpNeedtoNextLevel;
		float expPlus = exp * expNextLevel * configLv.MultiExp;

		if (GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.NORMAL)
		{
			float expIfNotEnoughStar =
				((Counter.GetQuantity(Counter.KeyPercenToNextStar) / 100f) *
				GameManager.Instance.GameConfigs.receivedExpForStar) * expNextLevel * configLv.MultiExp; // + exp neu user choi chua dat star 
			expPlus += MathRoundUp(expIfNotEnoughStar);
		}

		float DeductPercen = song.LvToUnlock > 0 && oldLevel > song.LvToUnlock ? (oldLevel - song.LvToUnlock) * gameConfig.expDeductPerLevel : 0;

		DeductPercen = DeductPercen > gameConfig.MaxexpDeductPerLevel ? gameConfig.MaxexpDeductPerLevel : DeductPercen;

		float expDeduct = DeductPercen * expPlus;

		expPlus -= expDeduct;

		List<int> listLevelUp = ProfileHelper.Instance.AddExpForUser(MathRoundUp(expPlus), GameManager.Instance.configUserLevel);

		int newLevel = ProfileHelper.Instance.Level;
		ConfigUserLevelItem configLvNext = GameManager.Instance.configUserLevel.GetConfigUserByLevel(newLevel + 1);
		int EXPInTotalNextLevel = configLvNext != null ? configLvNext.EXPInTotal : 0;

		if (expPlus >= 0 && ProfileHelper.Instance.TotalExp < EXPInTotalNextLevel)
		{
			MessageBus.Annouce(new Message(MessageBusType.ExpIncreased, MathRoundUp(expPlus)));
		}

		for (int i = 0; i < listLevelUp.Count; i++)
		{
			ProfileHelper.Instance.CurrentDiamond += GameManager.Instance.configUserLevel.GetConfigUserByLevel(listLevelUp[0]).RewardRuby;
		}

		if (listLevelUp.Count > 0)
		{
			SceneManager.Instance.OpenPopup(Scenes.LevelUpPopUp, listLevelUp);
		}

		return listLevelUp.Count;
	}

	// Handle level up event and return the premium song unlocked for this level
	public void OnLevelUp(int level)
	{
		int days = (int)(DateTime.Now - lastLevelUpDate).TotalDays;

		AnalyticsHelper.Instance.LogLevelUp(level, songs, days);

		AnalyticsHelper.Instance.LogLevelUp(
			level, songs, days,
			normalSongs, premiumSongs, normalBattles, friendBattles,
			paid4Songs, paid4Lives, paid4Continue);

		Reset();
	}

	public void OnFriendBattlePlayed()
	{
		friendBattles++;
		songs++;

		Save();
	}

	public void OnNormalBattlePlayed()
	{
		normalBattles++;
		songs++;

		Save();
	}

	public void OnSongPurchased(int diamonds)
	{
		paid4Songs += diamonds;

		Save();
	}

	public void OnLivesPurchased(int diamonds)
	{
		paid4Lives += diamonds;

		Save();
	}

	public void OnContinuePurchased(int diamonds)
	{
		paid4Continue += diamonds;

		Save();
	}

	public void OnNormalSongPlayed()
	{
		normalSongs++;
		songs++;

		Save();
	}

	public void OnPremiumSongPlayed(SongDataModel song)
	{
		premiumSongs++;
		songs++;

		Save();
	}

	public void Reload()
	{
		Load ();
	}

	private LevelUpProgress()
	{
		Load();
	}

	private void Reset()
	{
		songs = 0;
		normalSongs = 0;
		premiumSongs = 0;
		normalBattles = 0;
		friendBattles = 0;

		lastLevelUpDate = DateTime.Today;
		Save();
	}

	private void Save()
	{
		PlayerPrefs.SetInt(LEVEL_UP_PROGRESS_SONGS, songs);
		PlayerPrefs.SetInt(LEVEL_UP_PROGRESS_NORMAL_SONGS, normalSongs);
		PlayerPrefs.SetInt(LEVEL_UP_PROGRESS_PREMIUM_SONGS, premiumSongs);
		PlayerPrefs.SetInt(LEVEL_UP_PROGRESS_NORMAL_BATTLES, normalBattles);
		PlayerPrefs.SetInt(LEVEL_UP_PROGRESS_FRIEND_BATTLES, friendBattles);
		PlayerPrefs.SetInt(LEVEL_UP_PROGRESS_PAID_4_SONGS, paid4Songs);
		PlayerPrefs.SetInt(LEVEL_UP_PROGRESS_PAID_4_LIVES, paid4Lives);
		PlayerPrefs.SetInt(LEVEL_UP_PROGRESS_PAID_4_CONTINUE, paid4Continue);

		PlayerPrefs.SetString(LEVEL_UP_PROGRESS_DATE, lastLevelUpDate.ToString("yyyy-MM-dd"));
	}

	private void Load()
	{
		songs = PlayerPrefs.GetInt(LEVEL_UP_PROGRESS_SONGS);
		normalSongs = PlayerPrefs.GetInt(LEVEL_UP_PROGRESS_NORMAL_SONGS);
		premiumSongs = PlayerPrefs.GetInt(LEVEL_UP_PROGRESS_PREMIUM_SONGS);
		normalBattles = PlayerPrefs.GetInt(LEVEL_UP_PROGRESS_NORMAL_BATTLES);
		friendBattles = PlayerPrefs.GetInt(LEVEL_UP_PROGRESS_FRIEND_BATTLES);
		paid4Songs = PlayerPrefs.GetInt(LEVEL_UP_PROGRESS_PAID_4_SONGS);
		paid4Lives = PlayerPrefs.GetInt(LEVEL_UP_PROGRESS_PAID_4_LIVES);
		paid4Continue = PlayerPrefs.GetInt(LEVEL_UP_PROGRESS_PAID_4_CONTINUE);

		lastLevelUpDate = DateTime.Parse(PlayerPrefs.GetString(LEVEL_UP_PROGRESS_DATE, DateTime.Today.ToString()));
	}

	private static int MathRoundUp(float n)
	{
		return n > (int)n ? (int)n + 1 : (int)n;
	}
}
