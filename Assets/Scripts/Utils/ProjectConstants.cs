// This file is auto-generated. Modifications are not saved.

namespace ProjectConstants
{
    public static class Keys
    {
        public const string LastSongIDPlayed = "LastSongIDPlayed";
        public const string canBackgroundMusic = "canBackgroundMusic";
    }

    public enum Scenes
    {
        ScenesManager,
        SSEmpty,
        ContinuePopup,
        Loading,
        MainGame,
        HomeUI,
        JoinRoom,
        MainMenu,
        ResultUI,
        SplashScreen,
        AchievementListUI,
        FeedbackPopup,
        MessageBoxPopup,
        LivesPopup,
        IAP,
        DailyRewardPopUp,
        ChooseLanguagePopUp,
        MessageInviteFriends,
        ShareFacebookPopUp,
        RemoveAdsPopUp,
        RatePopUp,
        RatePopupNew,
        OnlineMainGame,
        OnBoarding,
        LevelUpPopUp,
        MessageForceLevelPopup,
        UserInfoPopup,
        CrossPromotionPopup,
        LeaderBoardPopup,
        BattleLobbyFriends,
        DialogInput,
        ExitPopup,
        ProfileConflict,
        UpdatePopup,
        DailyFreeSongPopup,
        SongLeaderboardPopup,
        PSGameplay,
        PSResultUI,
        PSContinuePopup,
        END
    }

    public static class Tags
    {
        /// <summary>
        /// Name of tag 'Untagged'.
        /// </summary>
        public const string Untagged = "Untagged";
        /// <summary>
        /// Name of tag 'Respawn'.
        /// </summary>
        public const string Respawn = "Respawn";
        /// <summary>
        /// Name of tag 'Finish'.
        /// </summary>
        public const string Finish = "Finish";
        /// <summary>
        /// Name of tag 'EditorOnly'.
        /// </summary>
        public const string EditorOnly = "EditorOnly";
        /// <summary>
        /// Name of tag 'MainCamera'.
        /// </summary>
        public const string MainCamera = "MainCamera";
        /// <summary>
        /// Name of tag 'Player'.
        /// </summary>
        public const string Player = "Player";
        /// <summary>
        /// Name of tag 'GameController'.
        /// </summary>
        public const string GameController = "GameController";
    }

    public static class SortingLayers
    {
        /// <summary>
        /// ID of sorting layer 'Default'.
        /// </summary>
        public const int Default = 0;
        /// <summary>
        /// ID of sorting layer 'TopLayer'.
        /// </summary>
        public const int TopLayer = -1893982671;
    }

    public static class Layers
    {
        /// <summary>
        /// Index of layer 'Default'.
        /// </summary>
        public const int Default = 0;
        /// <summary>
        /// Index of layer 'TransparentFX'.
        /// </summary>
        public const int TransparentFX = 1;
        /// <summary>
        /// Index of layer 'Ignore Raycast'.
        /// </summary>
        public const int Ignore_Raycast = 2;
        /// <summary>
        /// Index of layer 'Water'.
        /// </summary>
        public const int Water = 4;
        /// <summary>
        /// Index of layer 'UI'.
        /// </summary>
        public const int UI = 5;
        /// <summary>
        /// Index of layer 'UI2D'.
        /// </summary>
        public const int UI2D = 8;
        /// <summary>
        /// Index of layer 'MovingBackground'.
        /// </summary>
        public const int MovingBackground = 9;
        /// <summary>
        /// Index of layer 'MenuUI'.
        /// </summary>
        public const int MenuUI = 10;
        /// <summary>
        /// Index of layer 'MainGame'.
        /// </summary>
        public const int MainGame = 11;
        /// <summary>
        /// Index of layer 'Background'.
        /// </summary>
        public const int Background = 12;

        /// <summary>
        /// Bitmask of layer 'Default'.
        /// </summary>
        public const int DefaultMask = 1 << 0;
        /// <summary>
        /// Bitmask of layer 'TransparentFX'.
        /// </summary>
        public const int TransparentFXMask = 1 << 1;
        /// <summary>
        /// Bitmask of layer 'Ignore Raycast'.
        /// </summary>
        public const int Ignore_RaycastMask = 1 << 2;
        /// <summary>
        /// Bitmask of layer 'Water'.
        /// </summary>
        public const int WaterMask = 1 << 4;
        /// <summary>
        /// Bitmask of layer 'UI'.
        /// </summary>
        public const int UIMask = 1 << 5;
        /// <summary>
        /// Bitmask of layer 'UI2D'.
        /// </summary>
        public const int UI2DMask = 1 << 8;
        /// <summary>
        /// Bitmask of layer 'MovingBackground'.
        /// </summary>
        public const int MovingBackgroundMask = 1 << 9;
        /// <summary>
        /// Bitmask of layer 'MenuUI'.
        /// </summary>
        public const int MenuUIMask = 1 << 10;
        /// <summary>
        /// Bitmask of layer 'MainGame'.
        /// </summary>
        public const int MainGameMask = 1 << 11;
        /// <summary>
        /// Bitmask of layer 'Background'.
        /// </summary>
        public const int BackgroundMask = 1 << 12;
    }

    public static class SceneIDs
    {
        /// <summary>
        /// ID of scene 'ScenesManager'.
        /// </summary>
        public const int ScenesManager = 0;
        /// <summary>
        /// ID of scene 'SSEmpty'.
        /// </summary>
        public const int SSEmpty = 1;
        /// <summary>
        /// ID of scene 'ContinuePopup'.
        /// </summary>
        public const int ContinuePopup = 2;
        /// <summary>
        /// ID of scene 'Loading'.
        /// </summary>
        public const int Loading = 3;
        /// <summary>
        /// ID of scene 'MainGame'.
        /// </summary>
        public const int MainGame = 4;
        /// <summary>
        /// ID of scene 'HomeUI'.
        /// </summary>
        public const int HomeUI = 5;
        /// <summary>
        /// ID of scene 'JoinRoom'.
        /// </summary>
        public const int JoinRoom = 6;
        /// <summary>
        /// ID of scene 'MainMenu'.
        /// </summary>
        public const int MainMenu = 7;
        /// <summary>
        /// ID of scene 'ResultUI'.
        /// </summary>
        public const int ResultUI = 8;
        /// <summary>
        /// ID of scene 'SplashScreen'.
        /// </summary>
        public const int SplashScreen = 9;
        /// <summary>
        /// ID of scene 'AchievementListUI'.
        /// </summary>
        public const int AchievementListUI = 10;
        /// <summary>
        /// ID of scene 'FeedbackPopup'.
        /// </summary>
        public const int FeedbackPopup = 11;
        /// <summary>
        /// ID of scene 'MessageBoxPopup'.
        /// </summary>
        public const int MessageBoxPopup = 12;
        /// <summary>
        /// ID of scene 'LivesPopup'.
        /// </summary>
        public const int LivesPopup = 13;
        /// <summary>
        /// ID of scene 'IAP'.
        /// </summary>
        public const int IAP = 14;
        /// <summary>
        /// ID of scene 'DailyRewardPopUp'.
        /// </summary>
        public const int DailyRewardPopUp = 15;
        /// <summary>
        /// ID of scene 'ChooseLanguagePopUp'.
        /// </summary>
        public const int ChooseLanguagePopUp = 16;
        /// <summary>
        /// ID of scene 'MessageInviteFriends'.
        /// </summary>
        public const int MessageInviteFriends = 17;
        /// <summary>
        /// ID of scene 'ShareFacebookPopUp'.
        /// </summary>
        public const int ShareFacebookPopUp = 18;
        /// <summary>
        /// ID of scene 'RemoveAdsPopUp'.
        /// </summary>
        public const int RemoveAdsPopUp = 19;
        /// <summary>
        /// ID of scene 'RatePopUp'.
        /// </summary>
        public const int RatePopUp = 20;
        /// <summary>
        /// ID of scene 'RatePopupNew'.
        /// </summary>
        public const int RatePopupNew = 21;
        /// <summary>
        /// ID of scene 'OnlineMainGame'.
        /// </summary>
        public const int OnlineMainGame = 22;
        /// <summary>
        /// ID of scene 'OnBoarding'.
        /// </summary>
        public const int OnBoarding = 23;
        /// <summary>
        /// ID of scene 'LevelUpPopUp'.
        /// </summary>
        public const int LevelUpPopUp = 24;
        /// <summary>
        /// ID of scene 'MessageForceLevelPopup'.
        /// </summary>
        public const int MessageForceLevelPopup = 25;
        /// <summary>
        /// ID of scene 'UserInfoPopup'.
        /// </summary>
        public const int UserInfoPopup = 26;
        /// <summary>
        /// ID of scene 'CrossPromotionPopup'.
        /// </summary>
        public const int CrossPromotionPopup = 27;
        /// <summary>
        /// ID of scene 'LeaderBoardPopup'.
        /// </summary>
        public const int LeaderBoardPopup = 28;
        /// <summary>
        /// ID of scene 'BattleLobbyFriends'.
        /// </summary>
        public const int BattleLobbyFriends = 29;
        /// <summary>
        /// ID of scene 'DialogInput'.
        /// </summary>
        public const int DialogInput = 30;
        /// <summary>
        /// ID of scene 'ExitPopup'.
        /// </summary>
        public const int ExitPopup = 31;
        /// <summary>
        /// ID of scene 'ProfileConflict'.
        /// </summary>
        public const int ProfileConflict = 32;
        /// <summary>
        /// ID of scene 'UpdatePopup'.
        /// </summary>
        public const int UpdatePopup = 33;
        /// <summary>
        /// ID of scene 'DailyFreeSongPopup'.
        /// </summary>
        public const int DailyFreeSongPopup = 34;
        /// <summary>
        /// ID of scene 'SongLeaderboardPopup'.
        /// </summary>
        public const int SongLeaderboardPopup = 35;
        public const int PSGameplay = 36;
        public const int PSResultUI = 37;
        public const int PSContinuePopup = 38;
    }

    public static class SceneNames
    {
          public const string INVALID_SCENE = "InvalidScene";
    public static readonly string[] ScenesNameArray = {
	        "ScenesManager",
	        "SSEmpty",
	        "ContinuePopup",
	        "Loading",
	        "MainGame",
	        "HomeUI",
	        "JoinRoom",
	        "MainMenu",
	        "ResultUI",
	        "SplashScreen",
	        "AchievementListUI",
	        "FeedbackPopup",
	        "MessageBoxPopup",
	        "LivesPopup",
	        "IAP",
	        "DailyRewardPopUp",
	        "ChooseLanguagePopUp",
	        "MessageInviteFriends",
	        "ShareFacebookPopUp",
	        "RemoveAdsPopUp",
	        "RatePopUp",
	        "RatePopupNew",
	        "OnlineMainGame",
	        "OnBoarding",
	        "LevelUpPopUp",
	        "MessageForceLevelPopup",
	        "UserInfoPopup",
	        "CrossPromotionPopup",
	        "LeaderBoardPopup",
	        "BattleLobbyFriends",
	        "DialogInput",
	        "ExitPopup",
	        "ProfileConflict",
	        "UpdatePopup",
	        "DailyFreeSongPopup",
	        "SongLeaderboardPopup",
            "PSGameplay",
            "PSResultUI",
            "PSContinuePopup"
        };
        /// <summary>
        /// Convert from enum to string
        /// </summary>
        public static string GetSceneName(Scenes scene) {
              int index = (int)scene;
              if(index > 0 && index < ScenesNameArray.Length) {
                  return ScenesNameArray[index];
              } else {
                  return INVALID_SCENE;
              }
        }
    }

    public static class Axes
    {
        /// <summary>
        /// Input axis 'Horizontal'.
        /// </summary>
        public const string Horizontal = "Horizontal";
        /// <summary>
        /// Input axis 'Vertical'.
        /// </summary>
        public const string Vertical = "Vertical";
        /// <summary>
        /// Input axis 'Fire1'.
        /// </summary>
        public const string Fire1 = "Fire1";
        /// <summary>
        /// Input axis 'Fire2'.
        /// </summary>
        public const string Fire2 = "Fire2";
        /// <summary>
        /// Input axis 'Fire3'.
        /// </summary>
        public const string Fire3 = "Fire3";
        /// <summary>
        /// Input axis 'Jump'.
        /// </summary>
        public const string Jump = "Jump";
        /// <summary>
        /// Input axis 'Mouse X'.
        /// </summary>
        public const string Mouse_X = "Mouse X";
        /// <summary>
        /// Input axis 'Mouse Y'.
        /// </summary>
        public const string Mouse_Y = "Mouse Y";
        /// <summary>
        /// Input axis 'Mouse ScrollWheel'.
        /// </summary>
        public const string Mouse_ScrollWheel = "Mouse ScrollWheel";
        /// <summary>
        /// Input axis 'Submit'.
        /// </summary>
        public const string Submit = "Submit";
        /// <summary>
        /// Input axis 'Cancel'.
        /// </summary>
        public const string Cancel = "Cancel";
    }


    public static class ExtentionHelpers {
        /// <summary>
        /// Shortcut to change enum to string
        /// </summary>
        public static string GetName(this Scenes scene) {
              return SceneNames.GetSceneName(scene);
        }
    }
}

