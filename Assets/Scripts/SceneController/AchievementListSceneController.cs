using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Amanotes.PianoChallenge
{
	public class AchievementListSceneController : SSController
	{
		[SerializeField] private AchievementListView[] achievementListView;

		[SerializeField] GameObject[] activeTabs;
		[SerializeField] GameObject[] inactiveTabs;

		[SerializeField] private List<GameObject> tabContents;
		private int currentTab = 0;

		public void OnEnable()
		{
			Refresh();
		}

		public void OpenDailyTab()
		{
			currentTab = 0;
			Refresh();
		}

		public void OpenQuestTab()
		{
			currentTab = 1;
			Refresh();
		}

		private void Refresh()
		{
			for (int i = 0; i < 2; i++)
			{
				activeTabs[i].SetActive(i == currentTab);
				inactiveTabs[i].SetActive(i != currentTab);
				tabContents[i].gameObject.SetActive(i == currentTab);
			}
			achievementListView[currentTab].RefreshAchievementList();
		}

		public void ClosePopUp()
		{
			SceneManager.Instance.CloseScene();
		}
	}
}
