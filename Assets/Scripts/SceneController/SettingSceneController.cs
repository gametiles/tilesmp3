using UnityEngine;
using Amanotes.Utils;
using System.Collections.Generic;
using ProjectConstants;

namespace Amanotes.PianoChallenge
{
	public class SettingSceneController : MonoBehaviour
	{
		[SerializeField] Animator anmController = null;
		[SerializeField] UITable compTable = null;
        [SerializeField] GameObject sprON = null;
        [SerializeField] GameObject sprOFF = null;
        [SerializeField] UILabel lbON_OFF = null;

        private FeedbackDataModel feedback = null;
		private FeedbackDataModel requestSong = null;

		public void OnEnable()
		{
			//SceneManager.Instance.MainMenu.SetMenuState(MainMenuState.ShowBGOnly);

			anmController.SetTrigger("showtoleft");
			GroupHomeSceneManager.Instance.onPanelChange -= OnPanelChange;
			GroupHomeSceneManager.Instance.onPanelChange += OnPanelChange;
            CheckMusic();
        }

		public void OnDisable()
		{
			//SceneManager.Instance.MainMenu.SetMenuState(MainMenuState.ShowAll);
			GroupHomeSceneManager.Instance.onPanelChange -= OnPanelChange;
		}

        private void CheckMusic()
        {
            if (AudioBackground.Instance.canMusic == true)
            {
                sprON.SetActive(true);
                sprOFF.SetActive(false);
                lbON_OFF.text = "ON";
            }
            else
            {
                sprON.SetActive(false);
                sprOFF.SetActive(true);
                lbON_OFF.text = "OFF";
            }
        }

        public void OnBackgroundMusicClick()
        {
            AudioBackground.Instance.SetCanMusic();
            CheckMusic();
        }

		public void OnSoundTogglePressed(bool enableSound)
		{
			AnalyticsHelper.Instance.LogClickItem("Mute sound: " + !enableSound);
			AudioManager.Instance.MuteSFX = !enableSound;
		}

		public void OnHDTogglePressed(GameObject objOn, GameObject objOff)
		{
			GameManager.Instance.option.HDGraphicOn = !GameManager.Instance.option.HDGraphicOn;
			AnalyticsHelper.Instance.LogClickItem("HD: " + GameManager.Instance.option.HDGraphicOn);

			if (GameManager.Instance.option.HDGraphicOn)
			{
				objOn.SetActive(true);
				objOff.SetActive(false);
			}
			else
			{
				objOn.SetActive(false);
				objOff.SetActive(true);
			}
			GameManager.Instance.SettingEffect();
		}

		public void OpenCrossPromotion()
		{
			SceneManager.Instance.OpenPopup(Scenes.CrossPromotionPopup);
		}

		private void OnPanelChange(GroupHomeSceneType scene)
		{
			anmController.SetTrigger("hidetoright");
		}

		public void OnFeedbackButtonClicked()
		{
            //AnalyticsHelper.Instance.LogClickItem("Feedback in SETTING");
            //feedback = new FeedbackDataModel()
            //{
            //	title = Localization.Get("pu_feedback_title"),
            //	buttonLabel = Localization.Get("pu_feedback_btnsend"),
            //	subtitle = Localization.Get("pu_feedback_subject"),
            //	type = "UserFeedback"
            //};
            //SceneManager.Instance.OpenPopup(Scenes.FeedbackPopup, feedback);
            Application.OpenURL(GameManager.Instance.GameConfigs.fanpageLink);
        }

		public void OnRequestButtonClicked()
		{
            //AnalyticsHelper.Instance.LogClickItem("Request in SETTING");
            //requestSong = new FeedbackDataModel()
            //{
            //	title = Localization.Get("pu_requestsong_title"),
            //	buttonLabel = Localization.Get("pu_requestsong_btn"),
            //	subtitle = Localization.Get("pu_requestsong_subject"),
            //	type = "SongRequest"
            //};
            //SceneManager.Instance.OpenPopup(Scenes.FeedbackPopup, requestSong);
            Application.OpenURL(GameManager.Instance.GameConfigs.fanpageLink);
        }

		public void OnChooseLanuageClicked()
		{
			AnalyticsHelper.Instance.LogClickItem("ChooseLanguage");
			SceneManager.Instance.OpenPopup(Scenes.ChooseLanguagePopUp);
		}

		public void OnNoAdsClicked()
		{
		}

		public void LikeFanpage()
		{
			AchievementHelper.Instance.LogAchievement("likefanpage");
			Application.OpenURL(GameManager.Instance.GameConfigs.fanpageLink);
		}

		public void GoBack()
		{
			GroupHomeSceneManager.Instance.OpenPanelAnim(GroupHomeSceneType.BattleHome);
		}
	}
}
