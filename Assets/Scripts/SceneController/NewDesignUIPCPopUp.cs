﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Amanotes.Utils;
using Parse;
using Amanotes.PianoChallenge;

public class NewDesignUIPCPopUp : SSController {
	public UICenterOnChild centerChild;
	public UIToggle[] listToggle;
	private string PARSE_TABLE_ANALYTICS = "SURVEY";
	private const string PARSE_DATANAME_ROW = "dataName";
	private const string PARSE_COUNTERS_ROW = "counters";
	public override void Start ()
	{
		centerChild.onCenter += OncenterFinished;
	}

	void OncenterFinished(GameObject go){
		int index = int.Parse (go.name);
		if (!listToggle [index].value) {
			listToggle [index].Set (true);
		}
	}

	public void YesTap(){
		AnalyticsHelper.Instance.LogSurvey ("YES");
		AnalyticsParse ("Yes");
		ClosePopUp ();
	}
	public void SosoTap(){
		AnalyticsHelper.Instance.LogSurvey ("SOSO");
		AnalyticsParse ("SOSO");
		ClosePopUp ();
	}
	public void NoTap(){
		AnalyticsHelper.Instance.LogSurvey ("NO");
		AnalyticsParse ("NO");
		ClosePopUp ();
	}

	public void ClosePopUp(){
		SceneManager.Instance.CloseScene ();
	}

	public void AnalyticsParse(string fileName) 
	{
		var query = new ParseQuery<ParseObject>(PARSE_TABLE_ANALYTICS)
			.WhereEqualTo(PARSE_DATANAME_ROW, fileName);

		query.FindAsync().ContinueWith(
			t =>
			{
				IEnumerator<ParseObject> obj = t.Result.GetEnumerator();
				obj.MoveNext();
				if (obj.Current == null)
				{
					//Debug.Log("null");
					ParseObject analyticsData = new ParseObject(PARSE_TABLE_ANALYTICS);
					analyticsData[PARSE_DATANAME_ROW] = fileName;
					analyticsData[PARSE_COUNTERS_ROW] = 1;
					analyticsData.SaveAsync();
				}
				else
				{
					//Debug.Log("not null");
					obj.Current.Increment(PARSE_COUNTERS_ROW, 1);
					obj.Current.SaveAsync();
				}
			});
	}
}
