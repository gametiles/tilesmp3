﻿using UnityEngine;
using System.Collections;
using Amanotes.PianoChallenge;

public class HomeLeaderBoardItem : MonoBehaviour
{
	public UILabel lbIndex;
	public UILabel lbCrowns;
	public UILabel lbStars;
	public Sprite defaultAvatar;

	public Material _maskAvatar;

	public GameObject avatarSpriter;

	public void InitUI (int _index, GlobalRankingModel _userTop)
	{
//		avatarParent.sprite2D = defaultAvatar;
		lbCrowns.text = _userTop.totalCrown.ToString ();
		lbStars.text = _userTop.totalStar.ToString ();
		lbIndex.text = _index.ToString ();
		DownloadFacebookAvatar (_userTop.avatar);
	}


//	private void DownloadFacebookAvatar (string url)
//	{
//		ResourceLoaderManager.Instance.DownloadSprite (url, res => {
//			if (res == null) {
//				//load default
//			} else {
//				if (avatarParent != null)
//					avatarParent.sprite2D = res;
//			}
//		});
//
//
//	}

	private void DownloadFacebookAvatar (string url)
	{
		Debug.LogError("start down");
		ResourceLoaderManager.Instance.DownLoadTexture (url, texture => {
			//Debug.Log("DownloadFacebookAvatar finish");
			if (texture != null) {
				//Debug.LogError("Why");
				Material m = Instantiate (_maskAvatar);
				m.SetTexture ("_MainTex", texture);
				avatarSpriter.GetComponent<UITexture> ().material = m;
			}
		});
	}
}
