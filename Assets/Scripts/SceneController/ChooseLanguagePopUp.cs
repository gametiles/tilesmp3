﻿using UnityEngine;
using System.Collections.Generic;
using Amanotes.Utils.MessageBus;
using Amanotes.PianoChallenge;
using Amanotes.Utils;

public class ChooseLanguagePopUp : SSController
{
	List<ChooseLanguageItemView> listLanguage;

	[SerializeField]
	ChooseLanguageItemView itemLanguage = null;

	[SerializeField]
	Transform contan = null;

	public override void OnEnable ()
	{
		base.OnEnable ();
		MessageBus.Instance.Subscribe (MessageBusType.LanguageChanged, OnLanguageChanged);
		Init ();
	}

	void Init ()
	{
		if (listLanguage == null)
		{
			listLanguage = new List<ChooseLanguageItemView> ();
		}
		string currLanguage = Localization.language;
		List<string> languages = GameManager.Instance.GameConfigs.localizeConfig;
		Debug.LogError ("languages " + languages.Count);
		for (int i = 0; i < languages.Count; i++)
		{
			string[] dataLg = languages [i].Split ('#');
			if (dataLg.Length >= 2)
			{
				string language = dataLg [0];
				string url = dataLg [1];
				ChooseLanguageItemView itm = i >= listLanguage.Count ? Instantiate (itemLanguage) : listLanguage [i];
				itm.transform.SetParent (contan);
				itm.transform.localScale = Vector3.one;
				itm.InitUI (currLanguage, language, url);
				listLanguage.Add (itm);
			}
		}
	}

	public override void OnDisable ()
	{
		base.OnDisable ();
		MessageBus.Instance.Unsubscribe (MessageBusType.LanguageChanged, OnLanguageChanged);
	}

	private void OnLanguageChanged (Message msg)
	{
		string language = (string)msg.data;
		SelectLanguageController (language);
		PlayerPrefs.SetString (GameConsts.CACHE_LANGUAGE, language);
		PlayerPrefs.Save ();
		AnalyticsHelper.Instance.LogClickItem ("Change language " + language);
	}

	public void SelectLanguageController (string currLanguage)
	{
		for (int i = 0; i < listLanguage.Count; i++)
		{
			listLanguage [i].Refesh (currLanguage);
		}
	}

	public void ClosePopUp ()
	{
		SceneManager.Instance.CloseScene ();
	}
}
