using Amanotes.Utils.MessageBus;
using Facebook.Unity;
using UnityEngine;

class SongLeaderboardView : MonoBehaviour
{
	[SerializeField] UIWrapContent listWrap = null;
	[SerializeField] GameObject loadingAnimation = null;
	[SerializeField] GameObject listView = null;
	[SerializeField] GameObject errorPanel = null;

	string songId = null;
	SongLeaderboardList leaderboardList = null;
	SongLeaderboard leaderboard = null;

	private void OnEnable()
	{
		errorPanel.SetActive(false);

		if (leaderboardList != null)
		{
			MessageBus.Instance.Unsubscribe(
				leaderboardList.OnLeaderboardLoadedMessage,
				OnLeaderboardLoaded);
			MessageBus.Instance.Subscribe(
				leaderboardList.OnLeaderboardLoadedMessage,
				OnLeaderboardLoaded);
		}
	}

	private void OnDisable()
	{
		if (leaderboardList != null)
		{
			MessageBus.Instance.Unsubscribe(
				leaderboardList.OnLeaderboardLoadedMessage,
				OnLeaderboardLoaded);
		}
	}

	public void Init(string songId, SongLeaderboardList leaderboardList)
	{
		this.songId = songId;
		this.leaderboardList = leaderboardList;

		if (leaderboardList != null)
		{
			MessageBus.Instance.Unsubscribe(
				leaderboardList.OnLeaderboardLoadedMessage,
				OnLeaderboardLoaded);
			MessageBus.Instance.Subscribe(
				leaderboardList.OnLeaderboardLoadedMessage,
				OnLeaderboardLoaded);
		}
	}

	public void Show()
	{
		if (leaderboardList == null || songId == null)
		{ // This should never happen
			return;
		}

		gameObject.SetActive(true);
		errorPanel.SetActive(false);

		bool done = leaderboardList.RequestData(songId);
		if (done)
		{
			leaderboard = leaderboardList.GetLeaderboard(songId);
			listView.SetActive(true);
			loadingAnimation.SetActive(false);
			Refresh();
		}
		else
		{
			listView.SetActive(false);
			loadingAnimation.SetActive(true);
		}
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	private void OnLeaderboardLoaded(Message messge)
	{
		loadingAnimation.SetActive(false);

		if (leaderboardList == null || songId == null)
		{
			return;
		}

		leaderboard = leaderboardList.GetLeaderboard(songId);
		Refresh();
	}

	private void Refresh()
	{
		listWrap.maxIndex = 0;
		if (leaderboard == null)
		{
			listWrap.minIndex = 0;
			errorPanel.SetActive(true);
			listView.SetActive(false);
			return;
		}

		listView.SetActive(true);
		listWrap.minIndex = -leaderboard.records.Count;
		listWrap.onInitializeItem -= OnItemNeedInitialized;
		listWrap.onInitializeItem += OnItemNeedInitialized;

		listWrap.ResetChildrenPositions();
		listView.GetComponent<UIScrollView>().ResetPosition();
	}

	private void OnItemNeedInitialized(GameObject gameObj, int wrapIndex, int realIndex)
	{
		int index = Mathf.Abs(realIndex);
		if (leaderboardList == null ||
			songId == null ||
			leaderboard == null ||
			index >= leaderboard.records.Count)
		{
			gameObj.SetActive(false);
			return;
		}

		gameObj.SetActive(true);
		gameObj.GetComponent<SongLeaderboardItemView>().InitUI(
			leaderboard.records[index], leaderboard.rank + index);
	}
}
