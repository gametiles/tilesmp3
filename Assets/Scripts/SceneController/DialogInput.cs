using UnityEngine;

namespace Amanotes.PianoChallenge
{
	public class DialogInput : SSController
	{
		DialogInputDataModel mess;

		[SerializeField] UILabel lbtitle;
		[SerializeField] GameObject yesBtn;
		[SerializeField] GameObject noBtn;
		[SerializeField] UILabel yesLb;
		[SerializeField] UILabel noLb;
		[SerializeField] UILabel input;

		public override void OnSet(object data)
		{
			if (data != null)
			{
				mess = (DialogInputDataModel)data;
				lbtitle.text = mess.message;

				yesBtn.SetActive(true);
				noBtn.SetActive(true);
				yesLb.text = mess.messageYes;
				noLb.text = mess.messageNo;
				input.text = "";
			}
		}

		public void OnYesClick()
		{
			Helpers.CallbackWithValue(mess.OnYesButtonClicked, input.text);
		}
		public void OnNoClick()
		{
			Helpers.Callback(mess.OnNoButtonClicked);
		}
	}
}
