﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;
using ProjectConstants;
using Amanotes.Utils;
using Nama;
using DG.Tweening;

namespace Amanotes.PianoChallenge
{
    public class PSResultUI : SSController
    {
        [SerializeField] Animator animatorBestScoreEffect = null;
        [SerializeField] Animation anmEffectMoveButton = null;
        [SerializeField] AudioClip clip_Money = null;
        [SerializeField] AudioClip result_BG_Music = null;
        [SerializeField] AudioClip crow_Win = null;
        [SerializeField] UILabel lbScore = null;
        [SerializeField] UILabel lbSongName = null;
        [SerializeField] UILabel lbHighScore = null;
        [SerializeField] UILabel lbDiamondReward = null;
        [SerializeField] GameObject btnX2Diamond = null;
        [SerializeField] GameObject newBest = null;
        [SerializeField] List<GameObject> listStar = null;
        [SerializeField] GameObject btnPlay = null;
        [SerializeField] GameObject btnPlayAds = null;
        [SerializeField] GameObject favoriteActive = null;

        private PSGameplay psGamePlay = null;
        private int numDiamondReward = 0;

        public override void OnDisable()
        {
            base.OnDisable();
            anmEffectMoveButton.Stop();
        }

        public override void OnSet(object data)
        {
            base.OnSet(data);
            psGamePlay = data as PSGameplay;
            DisplaySongResult();
        }

        public override void OnEnable()
        {
            base.OnEnable();
            ResetScene();
            SongManager.Instance.LoadingSong = false;
        }

        private void DisplaySongResult()
        {
            showStar(psGamePlay.nStars);
            lbSongName.text = psGamePlay.currentSongInfo.title;
            lbScore.text = "0";
            lbDiamondReward.text = "+0";
            numDiamondReward = 0;
            animatorBestScoreEffect.gameObject.SetActive(false);
            //btnPlay.SetActive(psGamePlay.songData.pricePrimary >= 0);
            //btnPlayAds.SetActive(psGamePlay.songData.pricePrimary < 0);
            float delaySceneAnimation = 1f;
            StartCoroutine(C_ShowSceneAnimation(delaySceneAnimation));
            SongDataModel song = GameManager.Instance.SessionData.song;
            SetFavorite(SongManager.Instance.IsFavorite(song));
            int animatingScore = 0;
            int score = psGamePlay.score;
            if(result_BG_Music != null)
                AudioEffect.Instance.PlaySound(result_BG_Music);
            DOTween.To(() => animatingScore, x => animatingScore = x, score, .7f)
                .OnUpdate(() =>
                    {
                        lbScore.text = animatingScore.ToString();
                    })
                .SetDelay(0.2f)
                .Play();

            SongEndReward();
            UpdateHightScore();
            SaveLastSongIdPlayed();
        }

        private void SongEndReward()
        {
            int totalTiles = psGamePlay.song.notes.Length;
            int tilesEarn = psGamePlay.nTiles;
            int percentFinish = (int)(((float)tilesEarn/totalTiles)*100);

            if (percentFinish >= 90)
            {
                numDiamondReward += 54;
                ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                lbDiamondReward.text = "+" + numDiamondReward;
            }
            else if (percentFinish >= 80)
            {
                numDiamondReward += 48;
                ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                lbDiamondReward.text = "+" + numDiamondReward;
            }
            else if (percentFinish >= 70)
            {
                numDiamondReward += 42;
                ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                lbDiamondReward.text = "+" + numDiamondReward;
            }
            else if (percentFinish >= 60)
            {
                numDiamondReward += 36;
                ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                lbDiamondReward.text = "+" + numDiamondReward;
            }
            else if (percentFinish >= 50)
            {
                numDiamondReward += 30;
                ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                lbDiamondReward.text = "+" + numDiamondReward;
            }
            else if (percentFinish >= 40)
            {
                numDiamondReward += 24;
                ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                lbDiamondReward.text = "+" + numDiamondReward;
            }
            else if (percentFinish >= 30)
            {
                numDiamondReward += 18;
                ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                lbDiamondReward.text = "+" + numDiamondReward;
            }
            else if (percentFinish >= 20)
            {
                numDiamondReward += 12;
                ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                lbDiamondReward.text = "+" + numDiamondReward;
            }
            else if (percentFinish >= 10)
            {
                numDiamondReward += 6;
                ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                lbDiamondReward.text = "+" + numDiamondReward;
            }

            if (numDiamondReward > 0)
                btnX2Diamond.SetActive(true);
            else
                btnX2Diamond.SetActive(false);
        }

        public void DoubleDiamondReward()
        {
            AdHelper.Instance.ShowRewardVideo(RewardType.Custom, () =>
            {
                int animatingScore = numDiamondReward;
                ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                numDiamondReward = numDiamondReward * 2;
                int score = numDiamondReward;
                btnX2Diamond.SetActive(false);
                if(clip_Money != null)
                    AudioEffect.Instance.PlaySound(clip_Money);
                DOTween.To(() => animatingScore, x => animatingScore = x, score, .7f)
                    .OnUpdate(() =>
                    {
                        lbDiamondReward.text = animatingScore.ToString();
                    })
                    .SetDelay(0.2f)
                    .Play();
            });            
        }

        private void UpdateHightScore()
        {
            string songID = GameManager.Instance.SessionData.song.storeID;
            bool isNewBestScore = false;
            int score = psGamePlay.score;
            int star = psGamePlay.nStars;

            //update highscore
            if (HighScoreManager.Instance.UpdateHighScore(songID, score))
            {
                isNewBestScore = true;
                //only update  star if getting a new highscore

                if (star > 0)
                {
                    HighScoreManager.Instance.UpdateHighScore(songID, star, ScoreType.Star);
                }
            }

            if (!isNewBestScore)
            {
                lbHighScore.text = Localization.Get("maingame_highscore") + ": " + HighScoreManager.Instance.GetHighScore(songID).ToString();
            }
            else
            {
                lbHighScore.text = "";
                if (score > 0)
                {
                    if(crow_Win != null)
                        AudioEffect.Instance.PlaySound_2(crow_Win);
                    Invoke("ShowNewBest",0.7f);
                }
            }

            ProfileHelper.Instance.MarkUserDataChanged();
            ProfileHelper.Instance.PushUserData();
        }

        private void ShowNewBest()
        {
            animatorBestScoreEffect.gameObject.SetActive(true);
            animatorBestScoreEffect.SetTrigger("NewBest");
        }

        private void SaveLastSongIdPlayed()
        {
            PlayerPrefs.SetInt(Keys.LastSongIDPlayed, GameManager.Instance.SessionData.song.ID);
        }

        private void ResetScene()
        {
            Static.countPSContinuePopup = 0;
            anmEffectMoveButton.Play("resetAnimations");
        }

        public void PlayAgaint()
        {
            AudioBackground.Instance.StopBackgroundMusic();
            AudioEffect.Instance.StopPlaySound_2();
            SceneManager.Instance.OpenScene(Scenes.PSGameplay, psGamePlay.songData);
        }

        public void PlayAgaintAds()
        {
            AdHelper.Instance.ShowRewardVideo(RewardType.Custom, ()=>
                {
                    AudioEffect.Instance.StopPlaySound_2();
                    AudioBackground.Instance.StopBackgroundMusic();
                    SceneManager.Instance.OpenScene(Scenes.PSGameplay, psGamePlay.songData);
                });
        }

        private void showStar(int index)
        {
            switch (index)
            {
                case 0:
                    listStar[0].SetActive(false);
                    listStar[1].SetActive(false);
                    listStar[2].SetActive(false);
                    break;
                case 1:
                    listStar[0].SetActive(true);
                    listStar[1].SetActive(false);
                    listStar[2].SetActive(false);
                    break;
                case 2:
                    listStar[0].SetActive(true);
                    listStar[1].SetActive(true);
                    listStar[2].SetActive(false);
                    break;
                case 3:
                    listStar[0].SetActive(true);
                    listStar[1].SetActive(true);
                    listStar[2].SetActive(true);
                    break;
                default:
                    break;
                    
            }            
        }

        void SetFavorite(bool isFavorite)
        {
            if (isFavorite)
                favoriteActive.SetActive(true);
            else
                favoriteActive.SetActive(false);
        }

        public void FavoriteClicked()
        {
            SongDataModel model = GameManager.Instance.SessionData.song;
            bool favoriteAfter = !SongManager.Instance.IsFavorite(model);
            SongManager.Instance.SetFavorite(model, favoriteAfter);
            SetFavorite(favoriteAfter);
        }

        private IEnumerator C_ShowSceneAnimation(float delay)
        {
            yield return new WaitForSeconds(delay);
            anmEffectMoveButton.Play("controlButtons");
        }

        public void OpenSceneMenu()
        {
            SceneManager.Instance.OpenScene(Scenes.HomeUI, GroupHomeSceneType.SongList);
        }

        public void ShowVideoAds()
        {
            AnalyticsHelper.Instance.LogRewardVideoButtonClicked(Scenes.IAP.GetName(), string.Empty);
            AdHelper.Instance.ShowRewardVideo();
        }

        public override void OnKeyBack()
        {
            SceneManager.Instance.OpenScene(Scenes.HomeUI, GroupHomeSceneType.SongList);
        }
    }
}
