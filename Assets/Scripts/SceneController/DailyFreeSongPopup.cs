using UnityEngine;
using System;

namespace Amanotes.PianoChallenge
{
	public class DailyFreeSongPopup : SSController
	{
		[SerializeField] UILabel titleLabel = null;
		[SerializeField] UILabel authorLabel = null;
		[SerializeField] UILabel timeLabel = null;
		[SerializeField] UIButton playButton = null;
		[SerializeField] UILabel title = null;

		float secondsRemaining = 0;
		int secondsDisplay = 0;
		bool red = false;

		public override void OnEnableFS()
		{
			var song = DailyRewardManager.Instance.FreeSongLimitedTime;
			title.text = Localization.Get ("357").Replace (
				"{0}", GameManager.Instance.GameConfigs.freeSongHours.ToString());
			titleLabel.text = song.name;
			authorLabel.text = song.author;
			secondsRemaining = DailyRewardManager.Instance.FreeSongSecondsRemaining;
			timeLabel.color = new Color(0.329f, 0.737f, 0.431f);
			playButton.enabled = true;
			red = false;
			UpdateTime ();
		}

		static string format2digit(int n)
		{
			return n > 9 ? n.ToString () : ("0" + n.ToString ());
		}

		void UpdateTime()
		{
			int n = (int)secondsRemaining;
			if (n == secondsDisplay)
			{
				return;
			}
			secondsDisplay = n;
			timeLabel.text = fModStudio.Utils.FormatHHMMSS(n);
			if (n < 600 && !red) // 10 minutes
			{
				red = true;
				timeLabel.color = new Color (0.949f, 0.314f, 0.314f);
			}
		}

		private void Update()
		{
			secondsRemaining -= Time.deltaTime;
			if (secondsRemaining < 0)
			{
				if (playButton.enabled)
				{
					playButton.enabled = false;
					playButton.state = UIButtonColor.State.Disabled;
					timeLabel.text = "00:00:00";
				}
				return;
			}
			UpdateTime ();
		}

		public void Play()
		{
			var song = DailyRewardManager.Instance.FreeSongLimitedTime;
			GameManager.Instance.SessionData.endless = false;
			SongManager.Instance.TryToPlay(song, ProjectConstants.Scenes.DailyFreeSongPopup.ToString());
		}

		public void Close()
		{
			SceneManager.Instance.CloseScene ();
		}
	}
}
