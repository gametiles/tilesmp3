using UnityEngine;

public class RateItemView : MonoBehaviour
{
	[SerializeField] GameObject starActive;
	private int index;
	public System.Action<int> onStarClick;

	public void RateActive(bool isActive)
	{
		starActive.SetActive(isActive);
	}

	public void SetIndex(int _index)
	{
		index = _index;
	}
	public void OnClickStar()
	{
		if (onStarClick != null)
			onStarClick(index);
	}
}
