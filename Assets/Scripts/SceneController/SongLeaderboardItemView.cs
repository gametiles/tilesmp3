using UnityEngine;
using Amanotes.PianoChallenge;
using Facebook.Unity;
using Amanotes.Utils;

public class SongLeaderboardItemView : MonoBehaviour
{
	[SerializeField] UILabel lbIndex = null;
	[SerializeField] UILabel lbUserName = null;
	[SerializeField] UILabel lbScore = null;
	[SerializeField] UI2DSprite avatar = null;
	[SerializeField] GameObject[] stars = null;
	[SerializeField] GameObject meBg = null;
	[SerializeField] Sprite yourDefaultAvatar = null;
	[SerializeField] Sprite otherDefaultAvatar = null;

	public void InitUI(SongRecord record, int rank)
	{
		gameObject.SetActive(true);
		if (record == null)
		{
			return;
		}

		bool isMe = record.IsMe;

		if (record.avatar != null)
		{
			avatar.sprite2D = record.avatar;
		}
		else if (record.userId != null)
		{
			avatar.sprite2D = isMe ? yourDefaultAvatar : otherDefaultAvatar;
			DownloadFacebookAvatar(record);
		}
		else
		{
			avatar.sprite2D = yourDefaultAvatar;
		}

		meBg.SetActive(isMe);

		lbIndex.text = rank.ToString();
		lbUserName.text = record.userName;
		lbScore.text = record.score > 0 ? record.score.ToString() : "-";

		int n = record.stars + record.crowns;
		for (int i = 0; i < 3; i++)
		{
			stars[i].SetActive(i < record.stars && record.crowns <= 0);
		}
		for (int i = 0; i < 3; i++)
		{
			stars[i+3].SetActive(i < record.crowns);
		}
	}

	private void DownloadFacebookAvatar(SongRecord record)
	{
		string linkAvatar = "https://graph.facebook.com/" + record.userId + "/picture?type=normal";
        fModStudio.Utils.DownloadSprite(linkAvatar, sprite => {
			avatar.sprite2D = sprite;
			record.avatar = sprite;
		});
	}
}
