using UnityEngine;
using Amanotes.Utils.MessageBus;

public class ChooseLanguageItemView : MonoBehaviour
{
	[SerializeField] UILabel lbLanguage = null;
	[SerializeField] UISprite selected = null;
	[SerializeField] GameObject chooseLanuage = null;
	[SerializeField] UIButton btnChoose = null;
	[SerializeField] string language = null;

	[SerializeField] UI2DSprite flagIcon = null;

	public void InitUI(string currLanguage, string myLanguage, string url)
	{
		language = myLanguage;
		lbLanguage.text = myLanguage;
		if (currLanguage.Equals(myLanguage, System.StringComparison.OrdinalIgnoreCase))
		{
			LanguageSelected(true);
		}
		else
		{
			LanguageSelected(false);
		}

		if (url.Contains("http"))
		{
			DownloadIcon(url);
		}
		else
		{
			try
			{
				Object texture = Resources.Load("Flags/" + url);
				UpdateFlagIcon(texture as Texture2D);
			}
			catch(System.Exception)
			{

			}
		}
	}
	public void Refesh(string currLanguage)
	{
		if (currLanguage.Equals(language, System.StringComparison.OrdinalIgnoreCase))
		{
			LanguageSelected(true);
		}
		else
		{
			LanguageSelected(false);
		}
	}
	private void LanguageSelected(bool isSelected)
	{
		selected.cachedGameObject.SetActive(isSelected);
		chooseLanuage.SetActive(!isSelected);
	}
	public void ChooseLanguageClick()
	{
		Localization.language = language;
		MessageBus.Annouce(new Message(MessageBusType.LanguageChanged, Localization.language));
	}

	void UpdateFlagIcon(Texture2D texture)
	{
		Rect rec = new Rect(0, 0, texture.width, texture.height);
		flagIcon.sprite2D = Sprite.Create(texture, rec, new Vector2(0, 0));
	}

	void DownloadIcon(string url)
	{
		AssetDownloader.Instance.DownloadAndCacheAsset(url, 0,
			((float p) =>
			{
				//notthing
			}),
			((string fail) =>
			{
				Debug.Log("Error download img pack: " + fail);
			}),
			(WWW www) =>
			{
				UpdateFlagIcon(www.texture);
			});
	}
}
