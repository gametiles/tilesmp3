using UnityEngine;
using Amanotes.PianoChallenge;

public class CrossPromotionPopup : SSController
{
	[SerializeField] CrossPromotionButton itemCross = null;
	[SerializeField] Transform grid = null;
	[SerializeField] UIScrollView scrollView = null;
	[SerializeField] CrossPromotionButton[] pool = null;

	public override void Start()
	{
		InitListItemCross();
	}

	void InitListItemCross()
	{
		var items = CrossPromotionManager.Instance.Items;

		for (int i = 0; i < items.Count; i++)
		{
			CrossPromotionButton item;
			if (i < pool.Length)
			{
				item = pool[i];
				item.gameObject.SetActive(true);
			}
			else
			{
				item = Instantiate(itemCross);
			}

			item.Init(items[i], scrollView);
			item.transform.SetParent(grid);
			item.transform.localPosition = Vector3.zero;
			item.transform.localScale = Vector3.one;
		}

		grid.GetComponent<UIGrid>().enabled = true;
	}

	public void OnClickClose()
	{
		SceneManager.Instance.CloseScene();
	}
}
