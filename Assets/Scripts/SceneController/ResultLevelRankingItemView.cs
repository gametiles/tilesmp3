using UnityEngine;

namespace Amanotes.PianoChallenge
{
	public class ResultLevelRankingItemView : MonoBehaviour
	{
		[SerializeField] UITexture sprAvatar = null;
		[SerializeField] UILabel lbFbName = null;
		[SerializeField] UILabel lbScore = null;

		public void InitUI(SongRecord record)
		{
			lbFbName.text = record.userName;
			lbScore.text = record.score.ToString();
			if (record.avatar == null && record.userId != null)
			{
                fModStudio.Utils.DownloadSprite(
					"https://graph.facebook.com/" + record.userId + "/picture?type=normal",
					sprite => {
						sprAvatar.mainTexture = sprite.texture;
						record.avatar = sprite;
					});
			}
			else
			{
				sprAvatar.mainTexture = record.avatar.texture;
			}
		}
	}
}
