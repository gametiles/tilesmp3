using UnityEngine;

namespace Amanotes.PianoChallenge
{
	public class MessageInviteFriends : SSController
	{
		MessageBoxDataModel mess;

		[SerializeField] UILabel lbtitle = null;
		[SerializeField] GameObject okBtn = null;
		[SerializeField] GameObject yesBtn = null;
		[SerializeField] GameObject noBtn = null;
		[SerializeField] UILabel yesLb = null;
		[SerializeField] UILabel noLb = null;
		[SerializeField] UILabel okLb = null;

		public override void OnSet(object data)
		{
			if (data != null)
			{
				mess = (MessageBoxDataModel)data;
				lbtitle.text = mess.message;
				if (mess.style == PopUpStyle.YES_NO)
				{
					okBtn.SetActive(false);
					yesBtn.SetActive(true);
					noBtn.SetActive(true);
					yesLb.text = mess.messageYes;
					noLb.text = mess.messageNo;
				}
				else
				{
					okLb.text = mess.messageYes;
					okBtn.SetActive(true);
					yesBtn.SetActive(false);
					noBtn.SetActive(false);
				}
			}
		}

		public void Close()
		{
			Helpers.Callback(mess.OnYesButtonClicked);
			SceneManager.Instance.CloseScene();
		}

		public void OnYesClick()
		{
			Helpers.Callback(mess.OnYesButtonClicked);
		}
		public void OnNoClick()
		{
			Helpers.Callback(mess.OnNoButtonClicked);
		}
	}
}
