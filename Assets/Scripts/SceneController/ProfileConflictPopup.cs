using UnityEngine;
using System;

namespace Amanotes.PianoChallenge
{
	public class ConflictInfo
	{
		public UserDataModel localSave = null;
		public UserDataModel cloudSave = null;
		public Action onLocalChosen = null;
		public Action onCloudChosen = null;
	}

	// This popup ask if player wants to load cloud save or the local one
	public class ProfileConflictPopup : SSController
	{
		[SerializeField] UILabel localLevelText = null;
		[SerializeField] UILabel localDiamondsText = null;
		[SerializeField] UILabel localCrownsText = null;
		[SerializeField] UILabel localStarsText = null;
		[SerializeField] UILabel localSongsText = null;

		[SerializeField] UILabel cloudLevelText = null;
		[SerializeField] UILabel cloudDiamondsText = null;
		[SerializeField] UILabel cloudCrownsText = null;
		[SerializeField] UILabel cloudStarsText = null;
		[SerializeField] UILabel cloudSongsText = null;

		Action onLocalChosen;
		Action onCloudChosen;

		static int CountStars(UserDataModel save)
		{
			int n = 0;
			foreach (var item in save.listHighscore)
			{
				n += item.highestStar;
			}
			return n;
		}

		static int CountCrowns(UserDataModel save)
		{
			int n = 0;
			foreach (var item in save.listHighscore)
			{
				n += item.highestCrown;
			}
			return n;
		}

		static int CountSongs(UserDataModel save)
		{
			int n = 0;
			foreach (var song in GameManager.Instance.StoreData.listNormalSongs)
			{
				if (song.LvToUnlock <= save.level)
				{
					n++;
				}
			}
			return n + save.listBoughtSongs.Count;
		}

		public override void OnSet(object data)
		{
			var conflictInfo = data as ConflictInfo;
			onLocalChosen = conflictInfo.onLocalChosen;
			onCloudChosen = conflictInfo.onCloudChosen;

			UserDataModel cloudSave = conflictInfo.cloudSave;
			int nCrowns = CountCrowns (cloudSave);
			int nStars = CountStars (cloudSave);
			int nSongs = CountSongs (cloudSave);

			cloudLevelText.text = cloudSave.level.ToString();
			cloudDiamondsText.text = cloudSave.diamond.ToString();
			cloudCrownsText.text = nCrowns.ToString();
			cloudStarsText.text = nStars.ToString ();
			cloudSongsText.text = nSongs.ToString ();

			UserDataModel localSave = conflictInfo.localSave;
			nCrowns = CountCrowns (localSave);
			nStars = CountStars (localSave);
			nSongs = CountSongs (localSave);

			localLevelText.text = localSave.level.ToString();
			localDiamondsText.text = localSave.diamond.ToString();
			localCrownsText.text = nCrowns.ToString();
			localStarsText.text = nStars.ToString ();
			localSongsText.text = nSongs.ToString ();
		}

		public void ChooseLocal()
		{
			if (onLocalChosen != null)
			{
				onLocalChosen ();
			}
			SceneManager.Instance.CloseScene ();
		}

		public void ChooseCloud()
		{
			if (onCloudChosen != null)
			{
				onCloudChosen();
			}
			SceneManager.Instance.CloseScene ();
		}

		public override void OnKeyBack()
		{
#if UNITY_ANDROID && !UNITY_EDITOR
			AndroidJavaObject activity =
				new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");

			string message = "Cannot go back at this stage";
			AndroidJavaObject toastClass = new AndroidJavaClass("android.widget.Toast");
			toastClass.CallStatic<AndroidJavaObject>("makeText", activity, message, toastClass.GetStatic<int>("LENGTH_SHORT")).Call("show");
#elif UNITY_IPHONE && !UNITY_EDITOR

#endif
		}
	}
}
