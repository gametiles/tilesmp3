using UnityEngine;

namespace Amanotes.PianoChallenge
{
	public class ExitPopup : SSController
	{
		public void Exit()
		{
			Application.Quit();
		}

		public void Stay()
		{
			SceneManager.Instance.CloseScene();
		}
	}
}
