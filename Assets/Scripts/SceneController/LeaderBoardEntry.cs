using UnityEngine;
using Facebook.Unity;

namespace Amanotes.PianoChallenge
{
	public class LeaderBoardEntry : MonoBehaviour
	{
		[SerializeField] private UILabel lblIndex;
		[SerializeField] private UIBasicSprite avatar;
		[SerializeField] private UILabel lblUserName;
		[SerializeField] private UILabel lblTotalGrown;
		[SerializeField] private UILabel lblTotalStar;
		[SerializeField] private UILabel lblBattlesWon;
		[SerializeField] private Sprite defaultSprite;
		[SerializeField] private GameObject[] medals;

		public void InitUI(int rank, GlobalRankingModel topPlayer = null)
		{
			if (topPlayer != null)
			{
				lblIndex.text = (rank).ToString();
				if (string.IsNullOrEmpty(topPlayer.user_name))
					topPlayer.user_name = "Unknown";
				lblUserName.text = topPlayer.user_name;
				lblTotalGrown.text = topPlayer.totalCrown.ToString();
				lblTotalStar.text = topPlayer.totalStar.ToString();
				((UI2DSprite)avatar).sprite2D = defaultSprite;
				DownloadFacebookAvatar(topPlayer.avatar);
				lblBattlesWon.text = "0";

				rank--;
				for (int i = 0; i < 3; i++)
				{
					medals[i].SetActive(i == rank);
				}
			}
			else
			{
				lblIndex.text = Localization.Get("314").Replace("{0}", rank.ToString());
				lblUserName.text = "You";
				lblTotalGrown.text = ProfileHelper.Instance.TotalCrowns.ToString();
				lblTotalStar.text = ProfileHelper.Instance.TotalStars.ToString();
				lblBattlesWon.text = ProfileHelper.Instance.Wins.ToString();
				((UITexture)avatar).mainTexture = defaultSprite.texture;
				DownloadFacebookAvatar(
					"https://graph.facebook.com/" + AccessToken.CurrentAccessToken.UserId + "/picture?type=normal",
					true);
			}
		}

		private void DownloadFacebookAvatar(string url, bool isMe = false)
		{
			ResourceLoaderManager.Instance.DownloadSprite(url, res =>
				{
					Sprite sprite = res != null ? res : defaultSprite;
					if (isMe)
					{
						((UITexture)avatar).mainTexture = sprite.texture;
					}
					else
					{
						((UI2DSprite)avatar).sprite2D = sprite;
					}
				});

		}
	}
}
