using UnityEngine;
using Parse;
using Amanotes.PianoChallenge;
using System.Collections.Generic;
using MovementEffects;

public class RatingPopUpController : SSController
{
	[SerializeField] RateItemView[] rateItem = null;
	[SerializeField] UILabel starLabel = null;
	[SerializeField] RatingOptionFeedbackItem[] listRatingOption = null;

	//public RateItemView emptyRate;
	[SerializeField] GameObject ratePanel = null;
	[SerializeField] GameObject feedbackPanel = null;
	[SerializeField] GameObject btnSendRating = null;
	[SerializeField] TweenAlpha animButtonSendRating = null;
	[SerializeField] GameObject thankyou = null;

	//Parse config
	private const string TABLE_NAME = "RatingFeedbacks";
	private const string STAR_COLUMN = "StarNum";
	private const string DIVICE_NAME = "DiviceName";
	private const string FEEDBACK = "FeedbackUser";

	private int currentRate;

#if UNITY_IOS
	string linkGame = "https://play.google.com/store/apps/details?id=com.pianotiles.magictilesvocal";
#else // UNITY_ANDROID
    string linkGame = "https://play.google.com/store/apps/details?id=com.pianotiles.magictilesvocal";
#endif

	public override void OnEnableFS()
	{
		base.OnEnableFS();

		ratePanel.SetActive(true);
		feedbackPanel.SetActive(false);
		for (int i = 0; i < rateItem.Length; i++)
		{
			RateItemView rate = rateItem[i];
			rate.SetIndex(i + 1);
			rate.onStarClick -= OnStarClick;
			rate.onStarClick += OnStarClick;
		}

		for (int i = 0; i < listRatingOption.Length; i++)
		{
			RatingOptionFeedbackItem item = listRatingOption[i];
			item.OnCheckedValueChanged -= OnFeedbackOptionChanged;
			item.OnCheckedValueChanged += OnFeedbackOptionChanged;
		}
	}

	private void OnFeedbackOptionChanged()
	{
		Timing.RunCoroutine(C_ShowHideSendButton());
	}

	private void OnStarClick(int index)
	{
		if (currentRate == index)
			return;
		Rating(index);
	}

	public override void OnEnable()
	{
		base.OnEnable();
		//btnRating.SetActive(false);
		for (int i = 0; i < listRatingOption.Length; i++)
		{
			listRatingOption[i].UnCheckToggle();
			//listRatingOption[i].SetTextOption("ratingoption_" + i);
		}
		Rating(0);
	}

	// this variable is used only for this coroutine, do not delete
	private bool isProcessingShowHideButton = false;
	/// <summary>
	/// Show 'send rating button' only if there is rating option checked
	/// </summary>
	private IEnumerator<float> C_ShowHideSendButton()
	{
		if (isProcessingShowHideButton)
		{
			yield break;
		}
		else
		{
			isProcessingShowHideButton = true;
			yield return 1;
		}

		bool shouldShowRateButton = false;
		for (int i = 0; i < listRatingOption.Length; i++)
		{
			if (listRatingOption[i].IsChecked() == true)
			{
				shouldShowRateButton = true;
				break;
			}
		}

		if (shouldShowRateButton)
		{
			btnSendRating.SetActive(true);
			animButtonSendRating.PlayForward();
		}
		else
		{
			animButtonSendRating.PlayReverse();
		}
		yield return Timing.WaitForSeconds(animButtonSendRating.duration);
		btnSendRating.SetActive(shouldShowRateButton);

		yield return 1;
		isProcessingShowHideButton = false;
	}

	//show/hide star for rating
	private void Rating(int rateNum)
	{
		for (int i = 0; i < rateItem.Length; i++)
		{
			if (i <= rateNum - 1)
			{
				rateItem[i].RateActive(true);
			}
			else
			{
				rateItem[i].RateActive(false);
			}
		}
		currentRate = rateNum;
		if (rateNum > 0)
		{
			if (currentRate >= 4)
			{
				Timing.RunCoroutine(C_OpenStoreLinkAndClosePopup());
			}
			else
			{
				ShowFeedBackOption();
			}

			starLabel.text = Localization.Get("pu_rate_star" + rateNum);
		}
		else
		{
			starLabel.text = "";
		}
	}

	IEnumerator<float> C_OpenStoreLinkAndClosePopup()
	{
		yield return Timing.WaitForSeconds(0.5f);
		Application.OpenURL(linkGame);
		SceneManager.Instance.CloseScene();
	}

	//click "Rate" btn after choose star
	public void RatingClick()
	{
		//<=4 star open game in AppStore or GoogleStore.
		if (currentRate >= 4)
		{
#if UNITY_ANDROID
			string linkGame = "https://play.google.com/store/apps/details?id=com.musicgame.pianomusictiles";
#else
			string linkGame = "https://play.google.com/store/apps/details?id=com.musicgame.pianomusictiles";
#endif
            Application.OpenURL(linkGame);
			SceneManager.Instance.CloseScene();
		}
		else
		{
			//>=3 star show feedback
			ShowFeedBackOption();
		}
	}

	public void ShowFeedBackOption()
	{
		//play Animation show feedback popup
		ratePanel.GetComponent<TweenAlpha>().PlayForward();
		feedbackPanel.SetActive(true);
		feedbackPanel.GetComponent<TweenAlpha>().PlayForward();
	}

	// save feedback in Parse
	public void SendFeedbackClick()
	{
		animButtonSendRating.PlayReverse();
		Timing.RunCoroutine(C_DelayVisibleRateButton(animButtonSendRating.duration, false));


		thankyou.SetActive(true);
		thankyou.GetComponent<TweenAlpha>().PlayForward();

		Timing.RunCoroutine(IECloseScene());
	}

	private IEnumerator<float> IECloseScene()
	{
		yield return Timing.WaitForSeconds(1.5f);
		CloseScene();
	}

	private IEnumerator<float> C_DelayVisibleRateButton(float delay, bool visible)
	{
		yield return Timing.WaitForSeconds(delay);
		btnSendRating.SetActive(visible);
	}

	public void CloseScene()
	{
		SceneManager.Instance.CloseScene();

	}
}
