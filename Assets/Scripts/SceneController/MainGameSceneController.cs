using System;
using UnityEngine;

namespace Amanotes.PianoChallenge
{
	public class MainGameSceneController : SSController
	{
		public Gameplay gameplay;
		[SerializeField] private LevelLoader levelLoader;

		private string command = null;

		/// <summary>
		/// Will be called when opened with not-null parameter
		/// </summary>
		/// <param name="data"></param>
		public override void OnSet(object data)
		{
			if (data != null)
			{
				command = (string)data;

				//if the game scene is brought up with additional command, check it
				if (command.Contains("replay"))
				{
					AchievementHelper.Instance.LogAchievement("numTimeReplay");
				}
				else if (command.Contains("continue"))
				{
					AchievementHelper.Instance.LogAchievement("numTimeContinue");
					ContinueGame();
				}

				command = string.Empty;
			}
		}

		public override void OnEnable()
		{
			GameManager.Instance.SessionData.playMode = GAME_PLAY_MODE.NORMAL;

			gameplay.OnGameOver -= OnGameOver;
			gameplay.OnGameOver += OnGameOver;

			levelLoader.OnLevelLoaded -= OnLevelLoaded;
			levelLoader.OnLevelLoaded += OnLevelLoaded;
			levelLoader.gameObject.SetActive(true);
			levelLoader.LoadLevel(GameManager.Instance.SessionData.song);
		}

		public override void OnDisable()
		{
			levelLoader.OnLevelLoaded -= OnLevelLoaded;
			gameplay.OnGameOver -= OnGameOver;
			base.OnDisable();
		}

		public void OnRetryButtonClicked()
		{
			levelLoader.LoadLevel(GameManager.Instance.SessionData.song);
		}

		public void OnHomeButtonClicked()
		{
			SceneManager.Instance.OpenScene(ProjectConstants.Scenes.HomeUI, GroupHomeSceneType.SongList);
		}

		internal void EndGame()
		{
			InGameUIController.instance.gameplayState = GameplayState.FINISHED;
			SceneManager.Instance.OpenScene(ProjectConstants.Scenes.ResultUI);
		}

		private void OnGameOver()
		{
			if (!GameManager.Instance.SessionData.listen && gameplay.CanContinue())
			{
                EndGame();
                //SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.ContinuePopup, this);
            }
			else
			{
				EndGame();
			}
		}

		internal void ContinueGame(bool afterPause = true)
		{
			gameplay.ContinueGame(afterPause);
		}

		private void OnLevelLoaded()
		{
			levelLoader.OnReady();
			levelLoader.gameObject.SetActive(false);
		}
	}
}
