using System;
using UnityEngine;
using Amanotes.Utils;
using System.Collections.Generic;

namespace Amanotes.PianoChallenge
{
	public class BattleHomeController : MonoBehaviour
	{
        [SerializeField] UILabel lbLastSongName = null;
        [SerializeField] GameObject btnPlayFree = null;
        [SerializeField] GameObject btnPlayAds = null;
        [SerializeField] AudioClip clip_Money = null;
        private SongDataModel lastSongPlayed;

        [SerializeField] List<int> idFree = null;
        [SerializeField] List<SongItemView> listItemViewFree = null;
  
        [SerializeField] List<int> idPremium = null;
        [SerializeField] List<SongItemView> listItemViewPremium = null;
        public void OnEnable()
		{
			Animator animator = GetComponent<Animator>();
            animator.SetTrigger("showtoright");
            GroupHomeSceneManager.Instance.onPanelChange -= OnSceneChange;
			GroupHomeSceneManager.Instance.onPanelChange += OnSceneChange;
			Init();
		}

		private void OnSceneChange(GroupHomeSceneType panel)
		{
			Animator animator = GetComponent<Animator>();
            animator.SetTrigger("hidetoleft");
        }

		private void Init()
		{
            lastSongPlayed = GetLastSongPlayedById();
            lbLastSongName.text = lastSongPlayed.name;
            if (lastSongPlayed.pricePrimary < 0)
            {
                btnPlayFree.SetActive(false);
                btnPlayAds.SetActive(true);
            }
            else
            {
                btnPlayFree.SetActive(true);
                btnPlayAds.SetActive(false);
            }

            if (idFree.Count > 0)
            {
                for (int i = 0; i < idFree.Count; i++)
                {
                     listItemViewFree[i].Song = GetSongDataModelById(idFree[i]);
                }
            }

            if (idPremium.Count > 0)
            {
                for (int i = 0; i < idPremium.Count; i++)
                {
                    listItemViewPremium[i].Song = GetSongDataModelById(idPremium[i]);
                }
            }
        }

        private SongDataModel GetSongDataModelById(int id)
        {
            List<SongDataModel> listAllSongs = GameManager.Instance.StoreData.listAllSongs;
            for (int i = 0; i < listAllSongs.Count; i++)
            {
                if (listAllSongs[i].ID == id)
                    return listAllSongs[i];
            }
            Debug.LogError("ERROR: GetSongDataModelById() = NULL , BattleHomeController.cs");
            return null;
        }

        private SongDataModel GetLastSongPlayedById()
        {
            //Try to get ID last played, if not exits we set default is first song
            int id = PlayerPrefs.GetInt(ProjectConstants.Keys.LastSongIDPlayed, GameManager.Instance.StoreData.listNormalSongs[0].ID);
            List<SongDataModel> listAllSongs = GameManager.Instance.StoreData.listAllSongs;
            for (int i = 0; i < listAllSongs.Count; i++)
            {
                if (listAllSongs[i].ID == id)
                    return listAllSongs[i];
            }
            return GameManager.Instance.StoreData.listAllSongs[0];
        }

        public void AdsForRuby()
        {
            AdHelper.Instance.ShowRewardVideo(RewardType.Ruby, ()=> 
            {
                AudioEffect.Instance.PlaySound(clip_Money);
            });
        }

        public void PlaySong()
        {
            SongManager.Instance.TryToPlay(lastSongPlayed, "HOME");
        }

        public void PlayAds()
        {
            AdHelper.Instance.ShowRewardVideo(RewardType.Custom, () =>
            {
                GameManager.Instance.SessionData.endless = false;
                SongManager.Instance.TryToPlay(lastSongPlayed, "HOME");
            });
        }

        private void OnDisable()
        {
            GroupHomeSceneManager.Instance.onPanelChange -= OnSceneChange;
        }
    }
}
