using UnityEngine;
using Amanotes.Utils;
using Amanotes.Utils.MessageBus;
using ProjectConstants;

namespace Amanotes.PianoChallenge
{
	public class InAppPurchasePopUp : SSController
	{
        [SerializeField] AudioClip clip_Money = null;
		[Header("Current Diamond")]
		[SerializeField] UILabel lbCurrentDiamond = null;
		[Header("Reward Diamond")]
		[SerializeField] UILabel lblDiamondReward = null;
		[SerializeField] Animator diamondpackageEffect = null;

		[Header("NO Ads")]
		[SerializeField] UILabel priceNoAds = null;
		[SerializeField] GameObject isPuschased = null;
		[SerializeField] GameObject btnBuyNoAds = null;

		public override void OnEnableFS()
		{

			if (GameManager.Instance.GameConfigs.videoAdsReward != 0)
			{
				lblDiamondReward.text = "+" + GameManager.Instance.GameConfigs.videoAdsReward.ToString();
			}
		}

		public override void OnEnable()
		{
			if (EventManager.Instance.IapNotifiable)
			{ // Same for this effect
				EventManager.Instance.IapNotifiable = false;
				SceneManager.Instance.MainMenu.StopIapNotification ();
			}

			//check Remove ads
			isPuschased.SetActive(!AdHelper.Instance.isAdsEnabled);
			btnBuyNoAds.SetActive(AdHelper.Instance.isAdsEnabled);

			lbCurrentDiamond.text = ProfileHelper.Instance.CurrentDiamond.ToString();
			MessageBus.Instance.Subscribe(MessageBusType.DiamondChanged, OnDiamondChanged);
			MessageBus.Instance.Subscribe(MessageBusType.ProductPurchased, OnProductPurchased);
			MessageBus.Instance.Subscribe(MessageBusType.PurchaseFailed, OnProductPurchasedFailed);

			AnalyticsHelper.Instance.LogRewardVideoButtonShown(Scenes.IAP.GetName(), string.Empty);

		}

		private void OnProductPurchasedFailed(Message msg)
		{
			SceneManager.Instance.SetLoadingVisible(false);
		}

		private void OnViewedAds()
		{
			diamondpackageEffect.SetTrigger("d0");
		}

		private void OnProductPurchased(Message msg)
		{

		}

		public override void OnDisable()
		{

		}
		/// <summary>
		/// close popup when tap btn close
		/// </summary>
		public void CloseThisPopUp()
		{
			SceneManager.Instance.CloseScene();
		}

		public void ShowVideoAds()
		{
			AnalyticsHelper.Instance.LogRewardVideoButtonClicked(Scenes.IAP.GetName(), string.Empty);
            AdHelper.Instance.ShowRewardVideo(RewardType.Ruby, () =>
            {
                AudioEffect.Instance.PlaySound(clip_Money);
            });
		}

		public void NoAdsItemClick()
		{
		}

		private void OnDiamondChanged(Message obj)
		{
			lbCurrentDiamond.text = ProfileHelper.Instance.CurrentDiamond.ToString();
		}
	}
}
