using UnityEngine;
using Amanotes.PianoChallenge;

public class RatePopupNew : SSController
{
	[Header("Rating")]
	[SerializeField] GameObject ratePanel = null;
	[SerializeField] RateItemView[] rateItem = null;
	[SerializeField] GameObject submitButton = null;

	[Header("Store rating redirect")]
	[SerializeField] GameObject feedbackPanel = null;
	[SerializeField] UILabel gotoStoreText = null;

	private int currentRate;

#if UNITY_IOS
	const string STORE_LINK = "https://play.google.com/store/apps/details?id=com.pianotiles.magictilesvocal";
#else // UNITY_ANDROID
    const string STORE_LINK = "https://play.google.com/store/apps/details?id=com.pianotiles.magictilesvocal";
#endif

	public override void OnEnableFS()
	{
		base.OnEnableFS();

		for (int i = 0; i < rateItem.Length; i++)
		{
			RateItemView rate = rateItem[i];
			rate.SetIndex(i + 1);
			rate.onStarClick -= OnStarClick;
			rate.onStarClick += OnStarClick;
		}

#if UNITY_ANDROID
		gotoStoreText.text = Localization.Get("344");
#else
		gotoStoreText.text = Localization.Get("345");
#endif
		Rating(0);

		ratePanel.GetComponent<TweenAlpha>().enabled = false;
		ratePanel.GetComponent<TweenAlpha>().value = 1;
		ratePanel.GetComponent<UIWidget>().alpha = 1;
		feedbackPanel.GetComponent<TweenAlpha>().enabled = false;
		feedbackPanel.GetComponent<TweenAlpha>().value = 0;
		ratePanel.SetActive(true);
		feedbackPanel.SetActive(false);
	}

	private void OnStarClick(int index)
	{
		if (currentRate == index)
		{
			return;
		}
		Rating(index);
	}

	//show/hide star for rating
	private void Rating(int rateNum)
	{
		for (int i = 0; i < rateItem.Length; i++)
		{
			bool active = (i < rateNum);
			rateItem[i].gameObject.transform.GetChild(0).gameObject.SetActive(active);
			rateItem[i].gameObject.transform.GetChild(1).gameObject.SetActive(!active);
		}
		currentRate = rateNum;
		submitButton.SetActive(currentRate > 0);
	}

	//click "Rate" btn after choose star
	public void SubmitRating()
	{
		//<=4 star open game in AppStore or GoogleStore.
		if (currentRate >= 5)
		{
            //ratePanel.GetComponent<TweenAlpha>().PlayForward();
            //feedbackPanel.SetActive(true);
            //feedbackPanel.GetComponent<TweenAlpha>().PlayForward();
            GotoStore();

        }
		else
		{
			SceneManager.Instance.CloseScene();
		}
		SongManager.Instance.GameRating = currentRate;
		Amanotes.Utils.AnalyticsHelper.Instance.LogSubmitRating(currentRate);
	}

	// save feedback in Parse
	public void GotoStore()
	{
		Amanotes.Utils.AnalyticsHelper.Instance.LogGotoStoreRating(currentRate);
		Application.OpenURL(STORE_LINK);
		SceneManager.Instance.CloseScene();
	}

	public void Close()
	{
		SceneManager.Instance.CloseScene();
		if (SongManager.Instance.GameRating == 0)
		{
			SongManager.Instance.GameRating = 1;
		}
	}
}
