using UnityEngine;
using System.Collections;
using ProjectConstants;
using System;
using System.Collections.Generic;
using Amanotes.Utils;

namespace Amanotes.PianoChallenge
{
	public class SplashScreenController : SSController
	{
		[Header("Message to communicate with player")]
		[SerializeField] UILabel lbMessage = null;
		[SerializeField] UISprite retryButton = null;
		[SerializeField] GameObject loadingContainer = null;

		[Header("Animation duration when fade in, fade out the elements")]
		[SerializeField] float durationFadeIn = 0.5f;
		[SerializeField] float durationFadeOut = 0.25f;
		[SerializeField] GameInitializer gameInit = null;

		private Dictionary<string, UIToggle> listToggles;

		public override void OnEnableFS()
		{
			base.OnEnableFS();
			retryButton.cachedGameObject.SetActive(false);
			StartCoroutine(IEInitData());
		}

		public void Initialize()
		{
			lbMessage.cachedGameObject.SetActive(false);
			retryButton.cachedGameObject.SetActive(false);

			var toggles = gameObject.GetComponentsInChildren<UIToggle>();
			if (toggles != null)
			{
				listToggles = new Dictionary<string, UIToggle>(10);
				for (int i = 0; i < toggles.Length; i++)
				{
					listToggles.Add(toggles[i].gameObject.name.Substring(6), toggles[i]);
				}
			}
		}

		IEnumerator IEInitData()
		{
			yield return new WaitForSeconds(0.01f);
			gameInit.StartInitData();
		}
		public void SplashScreenFinished()
		{
			var prompt = GameManager.Instance.GameConfigs.updatePrompt;
			if (prompt == "optional" || prompt == "mandatory")
			{
				Action action = StartGame;
				SceneManager.Instance.OpenPopup (Scenes.UpdatePopup, action);
			}
			else
			{
				StartGame ();
			}
		}

		private void StartGame()
		{
			SSSceneManager.Instance.LoadMenu(Scenes.MainMenu.GetName());
			//if (PlayerPrefs.GetInt(GameConsts.OnBoarding, -1) == -1)
			//{
			//	StartCoroutine(WaitShowOnBoarding());
			//	if (ABTestManager.Instance.InterstitialPolicy == 1 ||
			//		ABTestManager.Instance.InterstitialPolicy == 2)
			//	{
			//		AdHelper.Instance.SkipNextInterstitialTrigger();
			//	}
			//}
			//else
			//{
			//	SongManager.Instance.LoadFirstSong();
			//	var scene = GroupHomeSceneType.SongList;
			//	SceneManager.Instance.OpenScene(Scenes.HomeUI, scene);
			//}
            SongManager.Instance.LoadFirstSong();
            var scene = GroupHomeSceneType.BattleHome;
            SceneManager.Instance.OpenScene(Scenes.HomeUI, scene);
        }

		public IEnumerator WaitShowOnBoarding()
		{
			yield return new WaitForSeconds(0.5f);
			SceneManager.Instance.OpenScene(Scenes.OnBoarding);
		}

		internal void EnterGame()
		{
			SplashScreenFinished();
		}

		public void ShowLoading()
		{
			loadingContainer.SetActive(true);
		}

		public void ShowMessage(string message)
		{
			lbMessage.text = message;
			lbMessage.cachedGameObject.SetActive(true);
			lbMessage.alpha = 0;
			TweenAlpha.Begin(lbMessage.cachedGameObject, durationFadeIn, 1);
		}

		public void HideMessage()
		{
			lbMessage.alpha = 1;
			TweenAlpha.Begin(lbMessage.cachedGameObject, durationFadeOut, 0);
		}

		public void ShowRetryButton()
		{
			retryButton.cachedGameObject.SetActive(true);
			retryButton.alpha = 0;
			TweenAlpha.Begin(retryButton.cachedGameObject, durationFadeIn, 1);
		}

		public void HideRetryButton()
		{
			retryButton.alpha = 1;
			TweenAlpha.Begin(retryButton.cachedGameObject, durationFadeOut, 0);
			retryButton.cachedGameObject.SetActive(false);
		}
	}
}
