using UnityEngine;
using System.Collections.Generic;
using Facebook.Unity;
using Amanotes.Utils.MessageBus;
using Amanotes.Utils;

namespace Amanotes.PianoChallenge
{
	public class LeaderBoardPopup : SSController
	{
		[SerializeField] UIWrapContent itemScrollList;
		[SerializeField] GameObject[] tabs; // 0: friends tab, 1: local tab, 2: global tab
		[SerializeField] LeaderBoardEntry myEntry;
		[SerializeField] GameObject[] activeTabs;
		[SerializeField] GameObject[] inactiveTabs;
		[SerializeField] GameObject rankinglist;
		[SerializeField] UIScrollView scrollView;
		[SerializeField] GameObject facebookBox;
		[SerializeField] GameObject facebookLoginBox;
		[SerializeField] GameObject waitingAnimation;

		[SerializeField] GameObject failText;
		[SerializeField] GameObject comingSoon;

		private Dictionary<GameObject, LeaderBoardEntry> listItem = new Dictionary<GameObject, LeaderBoardEntry>(10);
		private int currentTab = 2; // 0 - Friends, 1 - Loca, 2 - Global

		private bool needReload = true;
		private bool needRefreshListView = true;

		public override void OnEnable()
		{
			base.OnEnable();
			MessageBus.Instance.Subscribe(MessageBusType.UserDataChanged, OnUserDataChanged);
			MessageBus.Instance.Subscribe(MessageBusType.FacebookLoggedIn, OnFacebookLoggedIn);
			needReload = true; // force reload leaderboard
			HandleLeaderBoard();
		}

		public override void OnDisable()
		{
			MessageBus.Instance.Unsubscribe(MessageBusType.UserDataChanged, OnUserDataChanged);
			MessageBus.Instance.Unsubscribe(MessageBusType.FacebookLoggedIn, OnFacebookLoggedIn);
			base.OnDisable();
		}

		private void HandleLeaderBoard()
		{
			// Update tabs status
			for (int i = 0; i < 3; i++)
			{
				activeTabs[i].SetActive(i == currentTab);
				inactiveTabs[i].SetActive(i != currentTab);
			}

			failText.SetActive(false);
			comingSoon.SetActive(false);
			waitingAnimation.SetActive(false);
			myEntry.gameObject.SetActive(false);

			// Check facebook login
			if (AccessToken.CurrentAccessToken == null)
			{
				facebookBox.SetActive(true);
				facebookLoginBox.SetActive(true);
				rankinglist.SetActive(false);
			}
			else
			{
				facebookBox.SetActive(false);
				if (currentTab == 2) // global ranking
				{
					if (needReload || !LeaderboardManager.Instance.RankingLoaded)
					{
						needReload = false;
						rankinglist.SetActive(false);
						waitingAnimation.SetActive(true);
						LeaderboardManager.Instance.RequestRankingData(CompletedRequest);
					}
					else
					{
						myEntry.gameObject.SetActive(true);
						myEntry.InitUI(LeaderboardManager.Instance.MyRank);
						rankinglist.SetActive(true);
					}
				}
				else
				{
					rankinglist.SetActive(false);
					comingSoon.SetActive(true);
				}
			}
		}

		private void RefreshListView()
		{
			scrollView.UpdatePosition();
			itemScrollList.maxIndex = 0;
			itemScrollList.minIndex = -(LeaderboardManager.Instance.Ranks.Count - 1);

			itemScrollList.onInitializeItem -= OnItemNeedInitialized;
			itemScrollList.onInitializeItem += OnItemNeedInitialized;
		}

		private void CompletedRequest(bool result)
		{
			waitingAnimation.SetActive(false);
			if (result && LeaderboardManager.Instance.RankingLoaded)
			{
				rankinglist.SetActive(true);
				myEntry.gameObject.SetActive(true);

				myEntry.InitUI(LeaderboardManager.Instance.MyRank);

				RefreshListView();
			}
			else
			{
				failText.SetActive(true);
				Debug.Log("fail when get global ranking");
			}

			// Update tabs status
			for (int i = 0; i < 3; i++)
			{
				activeTabs[i].SetActive(i == currentTab);
				inactiveTabs[i].SetActive(i != currentTab);
			}
		}

		private void OnItemNeedInitialized(GameObject itemObject, int wrapIndex, int realIndex)
		{
			int index = Mathf.Abs(realIndex);
			if (index >= LeaderboardManager.Instance.Ranks.Count)
			{
				itemObject.SetActive(false);
				return;
			}

			itemObject.SetActive(true);
			var entry = itemObject.GetComponent<LeaderBoardEntry>();
			entry.InitUI(index + 1, LeaderboardManager.Instance.Ranks[index]);
		}

		private void OnUserDataChanged(Message msg)
		{
			if (!gameObject.activeInHierarchy)
				return;

			currentTab = 2; // For now, auto switch to global tab
			HandleLeaderBoard();
		}

		private void OnFacebookLoggedIn(Message msg)
		{
			if (AccessToken.CurrentAccessToken == null)
			{
				facebookBox.SetActive(true);
				facebookLoginBox.SetActive(true);
				rankinglist.SetActive(false);
				waitingAnimation.SetActive(false);
			}
		}

		/// <summary>
		/// close popup when tap btn close
		/// </summary>
		public void CloseThisPopUp()
		{
			SceneManager.Instance.CloseScene();
		}

		public void LoginFacebook()
		{
			facebookLoginBox.SetActive(false);
			waitingAnimation.SetActive(true);
			FacebookManager.Instance.loginPlace = "Leaderboard";
			FacebookManager.Instance.incentiveTextChoice = "308";
			ProfileHelper.Instance.LoginFacebook();
		}

		public void GotoFriendsTab()
		{
			currentTab = 0;
			HandleLeaderBoard();
		}

		public void GotoLocalTab()
		{
			currentTab = 1;
			HandleLeaderBoard();
		}

		public void GotoGlobalTab()
		{
			currentTab = 2;
			HandleLeaderBoard();
		}
	}
}
