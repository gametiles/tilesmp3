using UnityEngine;

namespace Amanotes.PianoChallenge
{
	public class DailyRewardPopUp : SSController
	{
		[SerializeField] UILabel diamondsTodayText = null;
		[SerializeField] UILabel livesTodayText = null;
		[SerializeField] UILabel diamondsTomorrowText = null;
		[SerializeField] UILabel livesTomorrowText = null;
		[SerializeField] GameObject tomorrow = null;

		public override void OnEnableFS()
		{
			int diamondsToday = DailyRewardManager.Instance.DiamondsToday;
			int livesToday = DailyRewardManager.Instance.LivesToday;
			int diamondsTomorrow = DailyRewardManager.Instance.DiamondsTomorrow;
			int livesTomrrow = DailyRewardManager.Instance.LivesTomorrow;

			diamondsTodayText.text = "+" + diamondsToday;

			livesTodayText.gameObject.SetActive(livesToday > 0);
			if (livesToday > 0)
			{
				livesTodayText.text = "+" + livesToday;
			}

			tomorrow.SetActive(diamondsTomorrow > 0);
			if (diamondsTomorrow > 0)
			{
				diamondsTomorrowText.text = "+" + diamondsTomorrow;

				livesTomorrowText.gameObject.SetActive(livesTomrrow > 0);
				if (livesTomrrow > 0)
				{
					livesTomorrowText.text = "+" + livesTomrrow;
				}
			}
		}

		public void Claim()
		{
			SceneManager.Instance.CloseScene();
			DailyRewardManager.Instance.ClaimToday();
			DailyRewardManager.Instance.CheckOffersToday ();
		}

		public override void OnKeyBack()
		{
			Claim ();
		}
	}
}
