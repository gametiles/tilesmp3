using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Amanotes.Utils.MessageBus;
using Amanotes.Utils;
using Facebook.Unity;
using MovementEffects;
using DG.Tweening;
using ProjectConstants;

namespace Amanotes.PianoChallenge
{
	public class ResultSceneController : SSController
	{
		[SerializeField] Animator animatorBestScoreEffect = null;
		[SerializeField] UILabel lbScore = null;
		[SerializeField] UILabel lbHighScore = null;
		[SerializeField] SocialPost socialPostScript = null;

		[SerializeField] Animation anmEffectMoveButton = null;

		[SerializeField] GameObject contentLoggedIn = null;
		[SerializeField] GameObject contentNotLoggedIn = null;
		[SerializeField] GameObject loadingObj = null;

		[SerializeField] UILabel facebookInstruction = null;
		[SerializeField] UILabel lbWatchAd = null;
		[SerializeField] UILabel lbDiamond = null;
        [SerializeField] UILabel lbDiamondReward = null;
        [SerializeField] TweenRotation tweenRotate = null;

		[Header("Cross promotion")]
		[SerializeField] CrossPromotionItemView itemCross = null;

		[SerializeField] GameObject cross = null;
		[SerializeField] GameObject reward = null;

		[Header("Retry")]
		[SerializeField] GameObject retryButton = null;
		[SerializeField] GameObject retryAdButton = null;
		[SerializeField] GameObject retryBuyButton = null;
		[SerializeField] UILabel timesText = null;
		[SerializeField] UILabel priceText = null;

		[Header("Song info")]
		[SerializeField] UILabel songName = null;
		[SerializeField] GameObject starImages = null;
		[SerializeField] GameObject crownImages = null;

		[Header("Endless")]
		[SerializeField] GameObject endlessGroup = null;

		[Header("Free song limited time")]
		[SerializeField] UILabel timeText = null;

		[Header("Favorite")]
		[SerializeField] GameObject songLiked = null;
		[SerializeField] GameObject songNotLiked = null;
		[SerializeField] UIButton likeButton = null;

		[Header("Song ranking")]
		[SerializeField] GameObject rankUpIcon = null;
		[SerializeField] float facebookTextDuration = 3.0f;

		private int score, star, crown;

		public const int SOUND_PIANO_FINISHED_SCORING = 88;
		public const string TEXT_0 = "0";
		private bool hasNewSongsUnlocked = false;

		private int watchAdTextChoice = 1;
		private string watchAdText = null;

		private float facebookTextTimer = 0;
		private int facebookTextChoice = 1;
		private string facebookText = null;
		private List<string> facebookTexts = null;

		bool inFreePeriod = false;
		float freeSecondsRemaining = 0;
		int freeSecondsDisplay = 0;
		bool freeExpireSoon = false;

		bool loggingIn = false;

		new private void Start()
		{
			facebookTextChoice = Random.Range(0, 2); // Random 0 or 1
			watchAdTextChoice = Random.Range(0, 2); // Random 0 or 1
		}

		public override void OnDisable()
		{
			base.OnDisable();

			SceneManager.Instance.MainMenu.HideResultBackground ();

			anmEffectMoveButton.Stop();
		}



		private void OnUserDataLoaded(Message obj)
		{
			loggingIn = true;
			var song = GameManager.Instance.SessionData.song;

			if (song.CanViewGlobalLeaderboard)
			{
				SongLeaderboardManager.Instance.GlobalLeaderboards.RequestData(song.storeID);
			}
			SongLeaderboardManager.Instance.FriendLeaderboards.RequestData(song.storeID);

		}

		private void OnFacebookLoggedIn(Message obj)
		{
			if (AccessToken.CurrentAccessToken != null)
			{
				return;
			}

			loggingIn = false;
		}

		private void RefreshRetryOptions()
		{
			// Retry with premium songs
			var song = GameManager.Instance.SessionData.song;

			bool bought = song.pricePrimary != 0 && SongManager.IsBought(song);
			bool normal = song.pricePrimary == 0;
			bool premium = song.pricePrimary > 0;
			bool promo = song.pricePrimary < 0;
			bool endlessUnlocked = HighScoreManager.Instance.GetHighScore(song.storeID, ScoreType.Star) >= 3;

			if (bought || normal)
			{
				likeButton.gameObject.GetComponent<Collider2D>().enabled = true;
				likeButton.state = UIButtonColor.State.Normal;
			}
			else
			{
				likeButton.gameObject.GetComponent<Collider2D>().enabled = false;
				likeButton.state = UIButtonColor.State.Disabled;
			}

			int nTries = (song.pricePrimary != 0) ? SongManager.Instance.PlayTimesRemaining(song) : 0;

			if (song == DailyRewardManager.Instance.FreeSongLimitedTime)
			{
				freeSecondsRemaining = DailyRewardManager.Instance.FreeSongSecondsRemaining;
				inFreePeriod =
					freeSecondsRemaining > 0 &&
					freeSecondsRemaining < DailyRewardManager.Instance.FreeSongMaxSeconds;
			}
			else
			{
				inFreePeriod = false;
			}

			if (inFreePeriod)
			{
				timeText.color = new Color(0.329f, 0.737f, 0.431f);
				freeExpireSoon = false;
				UpdateFreeSongTime();
			}

			timeText.gameObject.SetActive(inFreePeriod);
			bool canRetry = normal || nTries > 0 || bought || inFreePeriod;
			endlessGroup.SetActive(canRetry && endlessUnlocked);
			retryButton.SetActive(canRetry && !endlessUnlocked);
			retryAdButton.SetActive(!canRetry && promo);
			retryBuyButton.SetActive(!canRetry && premium);

			if (nTries > 0)
			{
				timesText.gameObject.SetActive(true);
				timesText.text = nTries.ToString();
			}
			else if (promo && !bought)
			{
				AnalyticsHelper.Instance.LogRewardVideoButtonShown(Scenes.ResultUI.GetName(), song.storeID);
				timesText.gameObject.SetActive(true);
				timesText.text = (-song.pricePrimary).ToString();
			}
			else
			{
				timesText.gameObject.SetActive(false);
			}

			if (!canRetry && premium)
			{
				priceText.text = "x" + song.pricePrimary;
			}
		}


		void RefreshFacebookTexts()
		{
			facebookTexts = new List<string>(3);

			var song = GameManager.Instance.SessionData.song;
			var leaderboard = SongLeaderboardManager.Instance.FriendLeaderboards.GetLeaderboard(song.storeID);

			rankUpIcon.SetActive(false);
			if (leaderboard != null && leaderboard.MyRankUp > 0)
			{
				facebookTexts.Add(Localization.Get("373").Replace("{0}", leaderboard.RecordBelowMe.userName));
				rankUpIcon.SetActive(true);
			}

			leaderboard = SongLeaderboardManager.Instance.GlobalLeaderboards.GetLeaderboard(song.storeID);
			if (leaderboard != null)
			{
				int myRankUp = leaderboard.MyRankUp;
				if (myRankUp > 0)
				{
					facebookTexts.Add(Localization.Get("372").Replace("{0}", myRankUp.ToString()));
					rankUpIcon.SetActive(true);
				}
				else if (leaderboard.records != null && leaderboard.records.Count > 0)
				{
					facebookTexts.Add(Localization.Get("376").Replace("{0}", leaderboard.myRank.ToString()));
				}
			}

			if (AccessToken.CurrentAccessToken != null || !song.CanViewGlobalLeaderboard)
			{
				facebookTexts.Add(Localization.Get("383"));
			}
			else
			{
				facebookTexts.Add(Localization.Get("374"));
			}

			facebookTextChoice = facebookTexts.Count;
			NextFacebookText();
			facebookInstruction.gameObject.SetActive(true);
		}

		void ShowFacebookText(TweenAlpha tween)
		{
			if (facebookTextChoice >= facebookTexts.Count)
			{
				tween.enabled = false;
				facebookInstruction.gameObject.SetActive(false);
				return;
			}
			facebookInstruction.text = facebookTexts[facebookTextChoice];
			tween.PlayForward();
		}

		void NextFacebookText()
		{
			if (facebookTexts == null || facebookTexts.Count <= 0)
			{
				return;
			}

			if (facebookTexts.Count == 1)
			{
				facebookInstruction.gameObject.SetActive(true);
				facebookInstruction.text = facebookTexts[0];
				return;
			}

			facebookTextChoice = (facebookTextChoice + 1) % facebookTexts.Count;
			var tween = facebookInstruction.gameObject.GetComponent<TweenAlpha>();

			tween.enabled = true;
			if (facebookInstruction.gameObject.activeSelf)
			{
				tween.PlayReverse();
				tween.SetOnFinished(() => {
					ShowFacebookText(tween);
				});
			}
			else
			{
				facebookInstruction.gameObject.SetActive(true);
				ShowFacebookText(tween);
			}
		}

		public override void OnEnable()
		{
			base.OnEnable();
			SongManager.Instance.LoadingSong = false;

			SceneManager.Instance.MainMenu.ShowResultBackground ();
            Amanotes.Utils.AudioManager.Instance.cancelSound = false;
            bool isFirstPlay = PlayerPrefs.GetInt(GameConsts.OnBoarding, -1) == -1;
			DailyRewardManager.Instance.IsFirstTime = isFirstPlay;

			if (isFirstPlay)
			{
				PlayerPrefs.SetInt(GameConsts.OnBoarding, 1);
				cross.SetActive(false);
				reward.SetActive(false);
			}
			else
			{
				lbDiamond.text = "x" + GameManager.Instance.GameConfigs.videoAdsReward;
				watchAdTextChoice = (watchAdTextChoice + 1) % 2;
				watchAdText = watchAdTextChoice == 0 ? "resultscene_videoreward" : "unityadstitle";
				lbWatchAd.text = Localization.Get(watchAdText);
				AnalyticsHelper.Instance.LogRewardVideoButtonShown(Scenes.ResultUI.GetName(), watchAdText);
				cross.SetActive(true);
				reward.SetActive(true);
			}

			ResetScene();
			ResetCrossMotion();


			DisplaySongResult();

			AchievementHelper.Instance.LogAchievement("songFinish");

			// Favorite
			var song = GameManager.Instance.SessionData.song;
			bool isFavorite = SongManager.Instance.IsFavorite(song);
			songLiked.SetActive(isFavorite);
			songNotLiked.SetActive(!isFavorite);

			loggingIn = false;

			RefreshRetryOptions();

			GameManager.Instance.SessionData.autoplay = false;
			GameManager.Instance.SessionData.endless = false;
			GameManager.Instance.SessionData.listen = false;
		}

		private void ResetScene()
		{
			anmEffectMoveButton.Play("resetAnimations");
			hasNewSongsUnlocked = false;
			lbScore.text = TEXT_0;
		}

		void AddExp()
		{
			if (!GameManager.Instance.sessionLevel) return;

			GameManager.Instance.sessionLevel = false;
			SongDataModel songItem = GameManager.Instance.SessionData.song;

			float expPlus = 0;
			int star, crown;
			int newStar, newCrown;
			star = HighScoreManager.Instance.GetHighScore(songItem.storeID, ScoreType.Star);
			crown = HighScoreManager.Instance.GetHighScore(songItem.storeID, ScoreType.Crown);
			newStar = Mathf.Clamp(Counter.GetStarQuantity(), 0, 3);
			newCrown = Counter.GetStarQuantity() - 3;

			newStar = newStar > 0 ? newStar : 0;
			newCrown = newCrown > 0 ? newCrown : 0;
            StartCoroutine(songEndReward(newStar, newCrown));
            if (newStar - star > 0) // dat star lan dau
			{
				expPlus += (newStar - star) * GameManager.Instance.GameConfigs.FirstExpForStar;

				expPlus += star * GameManager.Instance.GameConfigs.receivedExpForStar;
			}
			else // dat star lan sau
			{
				expPlus += newStar * GameManager.Instance.GameConfigs.receivedExpForStar;
			}

			if (newCrown - star > 0)
			{
				expPlus += (newCrown - star) * GameManager.Instance.GameConfigs.FirstExpForCrown;

				expPlus += crown * GameManager.Instance.GameConfigs.receivedExpForCrown;
			}
			else
			{
				expPlus += newCrown * GameManager.Instance.GameConfigs.receivedExpForCrown;
			}

			int n = LevelUpProgress.Instance.AddExp(songItem, expPlus);

			if (n <= 0)
			{ // Show daily reward for the first time, but only when there is no level up popup
				DailyRewardManager.Instance.CheckFirstReward();
			}
		}

        IEnumerator songEndReward(int newStar, int newCrow)
        {
            lbDiamondReward.text = "+0";
            int totalStar = newStar + newCrow;
            int numDiamondReward = 0;
            tweenRotate.enabled = false;
            if (totalStar <= 0)
                yield return null;
            else
            {
                tweenRotate.enabled = true;
                switch (totalStar)
                {
                    case 1:
                        numDiamondReward += 20;
                        ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                        lbDiamondReward.text = "+" + numDiamondReward;
                        break;
                    case 2:
                        numDiamondReward += 30;
                        ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                        lbDiamondReward.text = "+" + numDiamondReward;
                        break;
                    case 3:
                        numDiamondReward += 40;
                        ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                        lbDiamondReward.text = "+" + numDiamondReward;
                        break;
                    case 4:
                        numDiamondReward += 50;
                        ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                        lbDiamondReward.text = "+" + numDiamondReward;
                        break;
                    case 5:
                        numDiamondReward += 60;
                        ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                        lbDiamondReward.text = "+" + numDiamondReward;
                        break;
                    case 6:
                        numDiamondReward += 70;
                        ProfileHelper.Instance.CurrentDiamond += numDiamondReward;
                        lbDiamondReward.text = "+" + numDiamondReward;
                        break;
                }
            }
            yield return null;
        }

        void ShowAchievements(int n, GameObject obj)
		{
			for (int i = 0; i < 3; i++)
			{
				obj.transform.GetChild(i * 2 + 0).gameObject.SetActive(i < n);
				obj.transform.GetChild(i * 2 + 1).gameObject.SetActive(i >= n);
			}
		}

		public void ShowStarsOrCrowns(int stars, int crowns)
		{
			bool showCrowns = GameManager.Instance.SessionData.endless || crowns > 0;
			starImages.SetActive(!showCrowns);
			crownImages.SetActive(showCrowns);

			if (!showCrowns)
			{
				ShowAchievements(stars, starImages);
			}
			else
			{
				ShowAchievements(crowns, crownImages);
			}
		}

		private void DisplaySongResult()
		{
			AddExp();

			animatorBestScoreEffect.gameObject.SetActive(false);

			score = Counter.GetQuantity(Counter.KeyScore);
			star = Mathf.Clamp(Counter.GetStarQuantity(), 0, 3);
			crown = Counter.GetStarQuantity() - 3;
			if (crown < 0) crown = 0;

			//set song title
			songName.text = GameManager.Instance.SessionData.song.name;

			int animatingScore = 0;
			//update score with a little animation
			DOTween.To(() => animatingScore, x => animatingScore = x, score, .7f)
				.OnUpdate(() =>
				{
					lbScore.text = animatingScore.ToString();
				})
				.SetDelay(0.2f)
				.Play();

			ShowStarsOrCrowns(star, crown);

			string songID = GameManager.Instance.SessionData.song.storeID;
			bool isNewBestScore = false;
            SaveLastSongIdPlayed();
            //update highscore
            if (HighScoreManager.Instance.UpdateHighScore(songID, score))
			{
				isNewBestScore = true;
				//only update crown and star if getting a new highscore
				if (crown > 0)
				{
					if (HighScoreManager.Instance.UpdateHighScore(songID, crown, ScoreType.Crown))
					{
						//if this is the first time reach 3 crowns, also set achievement
						if (crown == 3)
						{
							AchievementHelper.Instance.LogAchievement("song6Stars");
						}
					};
				}
				if (star > 0)
				{
					if (HighScoreManager.Instance.UpdateHighScore(songID, star, ScoreType.Star))
					{
						//if this is the first time reach 3 stars, also set achievement
						if (star == 3)
						{
							AchievementHelper.Instance.LogAchievement("song3Stars");
						}
					}
				}
			}

			//MidiPlayer.Instance.ShouldPlay = false;
			AnalyticsHelper.Instance.LogLevelFinished(GameManager.Instance.SessionData.song.name, score, star, crown);
			ProfileHelper.Instance.MarkUserDataChanged();
			ProfileHelper.Instance.PushUserData();

			if (!isNewBestScore)
			{
				lbHighScore.text = Localization.Get("maingame_highscore") + " " + HighScoreManager.Instance.GetHighScore(songID).ToString();
			}
			else
			{
				lbHighScore.text = "";
				if (score > 0)
				{
					Timing.RunCoroutine(C_ShowNewBest(0f));
				}
			}

			//achievement logging
			if (crown == 3) AchievementHelper.Instance.LogAchievement("turn6Stars");
			if (star == 3) AchievementHelper.Instance.LogAchievement("turn3Stars");
			ProfileHelper.Instance.SaveAchievementValue();

			float delaySceneAnimation = 0.2f;//isNewBestScore ? 3f : 2f;
			Timing.RunCoroutine(C_ShowSceneAnimation(delaySceneAnimation));

			var song = GameManager.Instance.SessionData.song;
			if (!GameManager.Instance.SessionData.listen)
			{
				if (song.pricePrimary != 0)
				{
					LevelUpProgress.Instance.OnPremiumSongPlayed(song);
					SongManager.Instance.OnPremiumSongPlayed(song, crown);
				}
				else
				{
					LevelUpProgress.Instance.OnNormalSongPlayed();
				}
			}

			AnalyticsHelper.Instance.LogSongEnd(
				GameManager.Instance.SessionData.song, star, crown);

#if TEMPORARY_CODE
			AdHelper.Instance.ShowInterstitial();
#endif
		}

		private IEnumerator ShowInterstitial()
		{
			float TIMEOUT = 5;
			var t0 = Time.realtimeSinceStartup;
			yield return new WaitUntil(() => Time.realtimeSinceStartup - t0 > TIMEOUT);
			
			if (Time.realtimeSinceStartup - t0 < TIMEOUT)
			{
				AdHelper.Instance.ShowInterstitial();
			}
		}

		public void ResetCrossMotion()
		{
			if (!itemCross.Randomize())
			{
				itemCross.gameObject.SetActive(false);
			}
			else
			{
				itemCross.gameObject.SetActive(true);
			}
		}

		private IEnumerator<float> C_ShowSceneAnimation(float delay)
		{
			yield return Timing.WaitForSeconds(delay);

			//wait for the "new songs unlocked" popup to be closed before showing more control
			if (hasNewSongsUnlocked)
			{
				yield return Timing.WaitForSeconds(1.5f);
				while (!SceneManager.Instance.LastOpenSceneName.Contains(Scenes.ResultUI.GetName()))
				{
					yield return Timing.WaitForSeconds(0.1f);
				}
				yield return Timing.WaitForSeconds(0.8f);
			}

			anmEffectMoveButton.Play("ResultSceneTranlateButton");
			//wait more here to allow the ads to be loaded
			yield return Timing.WaitForSeconds(anmEffectMoveButton["ResultSceneTranlateButton"].length + .1f);
			anmEffectMoveButton.Play("controlButtons");

		}

		private IEnumerator<float> C_ShowNewBest(float delay)
		{
			yield return Timing.WaitForSeconds(delay);
			animatorBestScoreEffect.gameObject.SetActive(true);
			animatorBestScoreEffect.SetTrigger("NewBest");
			//TODO: play new best sound here
		}

		private bool IsBought(string storeID)
		{
			if (ProfileHelper.Instance.ListBoughtSongs == null)
			{
				return false;
			}

			for (int i = 0; i < ProfileHelper.Instance.ListBoughtSongs.Count; i++)
			{
				if (ProfileHelper.Instance.ListBoughtSongs[i].CompareTo(storeID) == 0)
				{
					return true;
				}
			}

			return false;
		}

		//Testing purpose
		public void ShowScoreAnimation()
		{
			int animatingScore = 0;
			//update score with a little animation
			DOTween.To(() => animatingScore, x => animatingScore = x, Random.Range(1000, 5000), 0.8f)
				.OnUpdate(() =>
				{
					lbScore.text = animatingScore.ToString();
				})
				.Play();
		}

		public void ReplayEndless()
		{
			if (socialPostScript.isTakingScreenshot)
				return;

			GameManager.Instance.SessionData.endless = true;
			SongManager.Instance.TryToPlay(
				GameManager.Instance.SessionData.song,
				Scenes.ResultUI.ToString(),
				"replay");
		}

		public void Replay()
		{
			if (socialPostScript.isTakingScreenshot)
				return;

			SongManager.Instance.TryToPlay(
				GameManager.Instance.SessionData.song,
				Scenes.ResultUI.ToString(),
				"replay");
		}

		public void ReplayAd()
		{
			if (socialPostScript.isTakingScreenshot)
				return;

			SongManager.Instance.TryToBuy(
				GameManager.Instance.SessionData.song,
				Scenes.ResultUI.ToString(),
				() =>
				{
					Replay();
				});
		}

		public void ReplayBuy() // for premium songs
		{
			if (socialPostScript.isTakingScreenshot)
				return;

			var song = GameManager.Instance.SessionData.song;
			if (SongManager.Instance.TryToBuy(song, Scenes.ResultUI.ToString()))
			{
				Replay();
			}
		}

        private void SaveLastSongIdPlayed()
        {
            PlayerPrefs.SetInt(Keys.LastSongIDPlayed, GameManager.Instance.SessionData.song.ID);
        }

        public void ShowVideoReward()
		{
			AnalyticsHelper.Instance.LogRewardVideoButtonClicked(Scenes.ResultUI.GetName(), watchAdText);
			AdHelper.Instance.ShowRewardVideo();
		}

		public void Menu()
		{
			if (socialPostScript.isTakingScreenshot)
				return;

			SceneManager.Instance.OpenScene(Scenes.HomeUI, GroupHomeSceneType.SongList);
		}


		public void Share()
		{
			AnalyticsHelper.Instance.LogClickItem("Share Facebook in RESULT");
			socialPostScript.ShareScreenshot();
		}

		private void OnCompletedGetStatus(Message message)
		{
			string status = (string)message.data;
			socialPostScript.ShareProgress(status);
		}

		private void OnCancelPostStatus(Message obj)
		{
			socialPostScript.SendCancel();
		}

		public void LoginFacebook()
		{
			contentNotLoggedIn.SetActive(false);
			contentLoggedIn.SetActive(false);
			loadingObj.SetActive(true);
			facebookInstruction.gameObject.SetActive(false);
			AnalyticsHelper.Instance.LogClickItem("Login Facebook in RESULT");

			FacebookManager.Instance.loginPlace = "ResultSingle";
			FacebookManager.Instance.incentiveTextChoice = facebookText;
			ProfileHelper.Instance.LoginFacebook();
		}

		public void OnNoAdsClicked()
		{
		}

		public void LikeSong()
		{
			var song = GameManager.Instance.SessionData.song;
			bool isFavorite = SongManager.Instance.IsFavorite(song);

			isFavorite = !isFavorite;
			SongManager.Instance.SetFavorite(song, isFavorite);

			songLiked.SetActive(isFavorite);
			songNotLiked.SetActive(!isFavorite);
		}

		void UpdateFreeSongTime()
		{
			int n = (int)freeSecondsRemaining;
			if (n == freeSecondsDisplay)
			{
				return;
			}
			freeSecondsDisplay = n;
			timeText.text = fModStudio.Utils.FormatHHMMSS(n);
			if (n < 600 && !freeExpireSoon) // 10 minutes
			{
				freeExpireSoon = true;
				timeText.color = new Color(0.949f, 0.314f, 0.314f);
			}
		}

		private void Update()
		{
			if (!loadingObj.activeSelf)
			{
				facebookTextTimer -= Time.deltaTime;
				if (facebookTextTimer < 0)
				{
					facebookTextTimer = facebookTextDuration;
					NextFacebookText();
				}
			}

			if (!inFreePeriod)
			{
				return;
			}
			freeSecondsRemaining -= Time.deltaTime;
			if (freeSecondsRemaining < 0)
			{
				RefreshRetryOptions();
				return;
			}
			UpdateFreeSongTime();
		}

		public override void OnKeyBack()
		{
			Menu();
		}

		public void ShowSongLeaderboard()
		{
			SceneManager.Instance.OpenPopup(
				Scenes.SongLeaderboardPopup,
				GameManager.Instance.SessionData.song);
		}
	}
}
