using UnityEngine;
using System.Collections;
using ProjectConstants;
using Amanotes.Utils.MessageBus;
using MovementEffects;
using System.Collections.Generic;
using Amanotes.Utils;

namespace Amanotes.PianoChallenge
{
	public enum MainMenuState
	{
		HideAll,
		ShowTopBar,
		ShowBottomBar,
		ShowAll,
		ShowBGAndTop,
		ShowBGOnly
	}

	public class MainMenuController : SSController
	{
		[SerializeField] GameObject liveBG = null;

		[Tooltip("The camera to set clear flag to NONE, reduce draw call")]
		[SerializeField] Camera menuCamera = null;

		[Header("Menu bars")]
		[SerializeField] UIWidget topBar = null;
		[SerializeField] TweenPosition tweenTopBar = null;
		[SerializeField] UIWidget bottomBar = null;
		[SerializeField] TweenPosition tweenBottomBar = null;
		[SerializeField] float menubarFloatDistance = 250.0f;

		[Header("User's metrics")]
		[SerializeField] UILabel lbLife = null;
		[SerializeField] UILabel lbLifeCountdown = null;
		[SerializeField] GameObject goLifeCountdown = null;
		[SerializeField] UILabel lbDiamond = null;
		[SerializeField] UILabel lbStar = null;

		[SerializeField] List<UIToggle> btns = null;

		[Header("Background")]
		[SerializeField] GameObject mainBackground = null;
		[SerializeField] GameObject resultBackground = null;

		[Header("Animation ")]
		[Tooltip("Minimum value of diamond metric to start playing the warning animation")]
		[SerializeField] int minMetricDiamonds = 3;
		[Tooltip("Minimum value of life metric to start playing the warning animation")]
		[SerializeField] int minMetricLife = 3;

		[SerializeField] GameObject glowEffectMP = null;
		[SerializeField] Animation[] lifeAnims = null;

		private const string WARNING_ANIMATION_NAME = "PulsingAnimation";
		private const string ADD_ANIMATION_NAME = "DiamondEffectInMainMenu";

		private AnimationState asWarnLife, asWarnDiamond;
		private const int IndexAnimationAdd = 0;
		private const int IndexAnimationWarning = 1;

		private MainMenuState currentState = MainMenuState.HideAll;
		private string[] int2string;

		void Update()
		{
			CheckAttentionTabs();
		}

		/// <summary>
		/// Well lol this one is absolutely garbage. But consider this one "room for improvement". Delete and alter as you wish
		/// </summary>
		public void CheckAttentionTabs()
		{
		}

		public override void OnEnableFS()
		{
		}

		public override void Start()
		{
			base.Start();
			glowEffectMP.SetActive (EventManager.Instance.SocialNotifiable);

			//tweenTopBar.to = topBar.cachedTransform.localPosition;
			//tweenTopBar.from = new Vector3(tweenTopBar.to.x, tweenTopBar.to.y + menubarFloatDistance, tweenTopBar.to.y);

			//tweenBottomBar.to = bottomBar.cachedTransform.localPosition;
			//tweenBottomBar.from = new Vector3(tweenBottomBar.to.x, tweenBottomBar.to.y - menubarFloatDistance, tweenBottomBar.to.y);

			tweenBottomBar.PlayForward();
			tweenTopBar.PlayForward();
			currentState = MainMenuState.ShowAll;

			SceneManager.Instance.RegisterMenu(this);
			goLifeCountdown.SetActive(false);
			int2string = new string[60];
			//pre cache all posible ToString() for timer countdown
			for (int i = 0; i < 60; i++)
			{
				int2string[i] = i.ToString("00");
			}
            AudioBackground.Instance.PlaySoundBackground(0f);
        }

		public override void OnEnable()
		{
			base.OnEnable();

			MessageBus.Instance.Subscribe(MessageBusType.TimeLifeCountDown, OnLifeGenerateTimeCountdown);
			MessageBus.Instance.Subscribe(MessageBusType.LifeChanged, OnUserLifeChanged);
			MessageBus.Instance.Subscribe(MessageBusType.DiamondChanged, OnDiamondChanged);
			MessageBus.Instance.Subscribe(MessageBusType.SongRecordChanged, OnUserPlayRecordChanged);
			MessageBus.Instance.Subscribe(MessageBusType.PlaySongAction, PlayAnmMinusLifeWhenPlaySong);
			StartCoroutine(RefreshUserView());
            AudioBackground.Instance.PlaySoundBackground(4f);
		}


		public override void OnDisable()
		{
			base.OnDisable();

			MessageBus.Instance.Unsubscribe(MessageBusType.TimeLifeCountDown, OnLifeGenerateTimeCountdown);
			MessageBus.Instance.Unsubscribe(MessageBusType.LifeChanged, OnUserLifeChanged);
			MessageBus.Instance.Unsubscribe(MessageBusType.DiamondChanged, OnDiamondChanged);
			MessageBus.Instance.Unsubscribe(MessageBusType.SongRecordChanged, OnUserPlayRecordChanged);
			MessageBus.Instance.Unsubscribe(MessageBusType.PlaySongAction, PlayAnmMinusLifeWhenPlaySong);
		}

		public void SetMenuState(MainMenuState nextState)
		{
			if (nextState == currentState)
			{
				return;
			}

			switch (nextState)
			{
				case MainMenuState.ShowBGAndTop:
					if (!gameObject.activeInHierarchy)
					{
						gameObject.SetActive(true);
					}
					liveBG.SetActive(true);
					if (currentState == MainMenuState.ShowBottomBar)
					{
						tweenTopBar.PlayForward();
						tweenBottomBar.PlayReverse();
					}
					else if (currentState == MainMenuState.ShowAll)
					{
						tweenBottomBar.PlayReverse();
					}
					else
					{
						tweenTopBar.PlayForward();
					}
					break;
				case MainMenuState.HideAll:
					liveBG.SetActive(false);
					if (currentState == MainMenuState.ShowTopBar)
					{
						tweenTopBar.PlayReverse();
					}
					else if (currentState == MainMenuState.ShowBottomBar)
					{
						tweenBottomBar.PlayReverse();
					}
					else
					{
						tweenTopBar.PlayReverse();
						tweenBottomBar.PlayReverse();
					}

					Timing.RunCoroutine(DelayedDisable(1f));
					break;

				case MainMenuState.ShowBGOnly:
					liveBG.SetActive(true);

					if (currentState == MainMenuState.ShowTopBar || currentState == MainMenuState.ShowBGAndTop)
					{
						tweenTopBar.ResetToBeginning();
						tweenTopBar.PlayReverse();
					}
					else if (currentState == MainMenuState.ShowBottomBar)
					{
						tweenBottomBar.PlayReverse();
					}
					else if (currentState == MainMenuState.ShowAll)
					{
						tweenTopBar.PlayReverse();
						tweenBottomBar.PlayReverse();
					}

					break;
				case MainMenuState.ShowTopBar:
					if (!gameObject.activeInHierarchy)
					{
						gameObject.SetActive(true);
					}
					liveBG.SetActive(false);
					if (currentState == MainMenuState.ShowBottomBar)
					{
						tweenTopBar.PlayForward();
						tweenBottomBar.PlayReverse();
					}
					else if (currentState == MainMenuState.ShowAll)
					{
						tweenBottomBar.PlayReverse();
					}
					else
					{
						tweenTopBar.PlayForward();
					}
					break;
				case MainMenuState.ShowBottomBar:

					if (!gameObject.activeInHierarchy)
					{
						gameObject.SetActive(true);
					}
					liveBG.SetActive(true);
					if (currentState == MainMenuState.ShowTopBar)
					{
						tweenBottomBar.PlayForward();
						tweenTopBar.PlayReverse();
					}
					else if (currentState == MainMenuState.ShowAll)
					{
						tweenTopBar.PlayReverse();
					}
					else
					{
						tweenBottomBar.PlayForward();
					}
					break;
				case MainMenuState.ShowAll:

					if (!gameObject.activeInHierarchy)
					{
						gameObject.SetActive(true);
					}
					liveBG.SetActive(true);
					if (currentState == MainMenuState.ShowTopBar)
					{
						tweenBottomBar.PlayForward();
					}
					else if (currentState == MainMenuState.ShowBottomBar)
					{
						tweenTopBar.PlayForward();
					}
					else
					{
						tweenTopBar.PlayForward();
						tweenBottomBar.PlayForward();
					}
					break;
			}

			currentState = nextState;
		}

		private IEnumerator<float> DelayedDisable(float delay)
		{
			yield return Timing.WaitForSeconds(delay);
			gameObject.SetActive(false);
		}

		public void ResetToggleMainMenu(Scenes scene)
		{
			if (scene != Scenes.HomeUI)
			{
				return;
			}

			int index = (int)SceneManager.Instance.currentGroupHomeUI;
			if (index < btns.Count && !btns[index].value)
			{
				btns[index].Set(true);
			}
		}

		public void ShowResultBackground()
		{
			mainBackground.SetActive (false);
			resultBackground.SetActive (true);
		}

		public void HideResultBackground()
		{
			mainBackground.SetActive (true);
			resultBackground.SetActive (false);
		}

		private void OnLifeGenerateTimeCountdown(Message obj)
		{
			if (obj.data != null)
			{
				int numSeconds = (int)(obj.data);
				if (numSeconds <= 0)
				{
					goLifeCountdown.SetActive(false);
				}
				else
				{
					if (!goLifeCountdown.activeInHierarchy)
					{
						goLifeCountdown.SetActive(true);
					}

					int minutes = Mathf.FloorToInt(numSeconds * 1.0f / 60);
					int seconds = Mathf.Clamp(numSeconds - minutes * 60, 0, 59);
					lbLifeCountdown.text = string.Format("{0}:{1}", int2string[minutes], int2string[seconds]);
				}
			}
		}

		private void OnUserDataChanged(Message obj)
		{
			//updating view should wait for 1 frame to ensure everything has finished
			StartCoroutine(RefreshUserView());
		}

		private IEnumerator RefreshUserView()
		{
			yield return null;
			menuCamera.clearFlags = CameraClearFlags.Nothing;
            lbStar.text = ProfileHelper.Instance.TotalStars.ToString();
			lbLife.text = ProfileHelper.Instance.CurrentLife.ToString();
			lbDiamond.text = ProfileHelper.Instance.CurrentDiamond.ToString();

			ShowWarningDiamondAnimation(ProfileHelper.Instance.CurrentDiamond);
			ShowWarningLifeAnimation(ProfileHelper.Instance.CurrentLife);
		}

		private void OnUserPlayRecordChanged(Message obj)
		{
			if (gameObject.activeInHierarchy)
			{
				StartCoroutine(RefreshUserRecordView());
			}
		}


		private IEnumerator RefreshUserRecordView()
		{
			yield return null;
			if (gameObject.activeInHierarchy)
			{
                lbStar.text = ProfileHelper.Instance.TotalStars.ToString();
			}
		}

		private void OnUserLifeChanged(Message obj)
		{
			int currLife = int.Parse(lbLife.text);
			lbLife.text = ProfileHelper.Instance.CurrentLife.ToString();

			if (currLife < ProfileHelper.Instance.CurrentLife)
			{
				lifeAnims[IndexAnimationAdd].Play(ADD_ANIMATION_NAME);
			}

			ShowWarningLifeAnimation(currLife);
		}

		private void ShowWarningLifeAnimation(int currentLife)
		{
			if (currentLife <= minMetricLife)
			{
				if (!lifeAnims[IndexAnimationWarning].IsPlaying(WARNING_ANIMATION_NAME))
				{
					lifeAnims[IndexAnimationWarning][WARNING_ANIMATION_NAME].wrapMode = WrapMode.Loop;
					asWarnLife = lifeAnims[IndexAnimationWarning].PlayQueued(WARNING_ANIMATION_NAME);
				}
			}
			else
			{
				if (lifeAnims[IndexAnimationWarning].IsPlaying(WARNING_ANIMATION_NAME))
				{
					lifeAnims[IndexAnimationWarning][WARNING_ANIMATION_NAME].wrapMode = WrapMode.Once;
				}
			}
		}


		private void OnDiamondChanged(Message obj)
		{
			int currDiamond = int.Parse(lbDiamond.text);
			lbDiamond.text = ProfileHelper.Instance.CurrentDiamond.ToString();
		}

		private void ShowWarningDiamondAnimation(int currentDiamond)
		{

		}

		public void PlayAnmMinusLifeWhenPlaySong(Message mess)
		{
			if (!LivesManager.Instance.IsNextPlayFree)
			{
				lifeAnims[IndexAnimationAdd].Play("minuslifewhenplaysong");
			}
		}

		public void OpenLeaderboard()
		{
			AnalyticsHelper.Instance.LogLeaderboardOpen("MenuBar");
			SceneManager.Instance.OpenPopup(Scenes.LeaderBoardPopup);
		}

		public void OpenIAP()
		{
            //AnalyticsHelper.Instance.LogIapOpen(AnalyticsHelper.IAP_MORE_DIAMONDS);
            SceneManager.Instance.OpenPopup(Scenes.IAP);
		}

		public void OpenSongListUI()
		{
            GroupHomeSceneManager.Instance.OpenPanelAnim(GroupHomeSceneType.SongList);
		}

		public void OpenSettingtUI()
		{
            GroupHomeSceneManager.Instance.OpenPanelAnim(GroupHomeSceneType.SettingUI);
		}

		public void OpenBattleHome()
		{
			GroupHomeSceneManager.Instance.OpenPanelAnim(GroupHomeSceneType.BattleHome);
		}

		public void OpenLivesPopup()
		{
			SceneManager.Instance.OpenPopup(Scenes.LivesPopup);
		}

		public void OpenUserInfoPopup()
		{
			SceneManager.Instance.OpenPopup(Scenes.UserInfoPopup);
		}

		public void StopIapNotification()
		{
		}

        public void ShowRewardVideo()
        {
            AdHelper.Instance.ShowRewardVideo();
        }
	}
}
