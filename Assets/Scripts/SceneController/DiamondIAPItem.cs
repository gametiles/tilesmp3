using UnityEngine;
using Amanotes.Utils;

namespace Amanotes.PianoChallenge
{
	public class DiamondIAPItem : MonoBehaviour
	{
		[SerializeField] InAppPurchasePopUp IPAControl = null;
		[SerializeField] UILabel lblQuantityDiamond = null;
		[SerializeField] GameObject lblDouble = null;
		[SerializeField] UILabel lblAmount = null;

		/// <summary>
		/// "BuyDiamond" btn TAP
		/// </summary>
		public void BuyDiamond()
		{

		}

		void OnEnable()
		{
			InitUI();
		}

		private void InitUI()
		{
		}
	}
}
