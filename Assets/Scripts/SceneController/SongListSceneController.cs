using UnityEngine;
using Amanotes.Utils.MessageBus;
using System.Collections.Generic;

namespace Amanotes.PianoChallenge
{
	public class SongListSceneController : MonoBehaviour
	{
		[SerializeField] SongListView[] LviewSongList = null;
		[SerializeField] GameObject[] tabButtons = null;
		[SerializeField] GameObject[] tabActives = null;
		[SerializeField] GameObject[] tabContents = null;
        [SerializeField] UILabel[] tabTitles = null;
		[SerializeField] Animator anmController = null;
		[SerializeField] GameObject newIndicator = null;
        [SerializeField] UILabel lbLastSongName = null;

        private Color yellow = new Color32(218,202,143,255);

        static int currentTab = 0; // 1, 2
        private SongDataModel lastSongPlayed;

        public void OnEnable()
		{
			MessageBus.Instance.Subscribe(MessageBusType.UserDataChanged, OnSaveLoaded);
            if (GroupHomeSceneManager.Instance.lastPanel == GroupHomeSceneType.SettingUI)
                anmController.SetTrigger("showtoright");
            else
                anmController.SetTrigger("showtoleft");

			GroupHomeSceneManager.Instance.onPanelChange -= OnpanelChange;
			GroupHomeSceneManager.Instance.onPanelChange += OnpanelChange;

			RefeshUI();

			newIndicator.SetActive(SongManager.Instance.TotalPremiumTries > 0);
		}

		void RefeshUI()
		{
			for (int i = 0; i < LviewSongList.Length; i++)
			{
				if (LviewSongList[i].type != SongListType.Favorite)
				{
					LviewSongList[i].Refresh();
				}
			}

			ShowTab(currentTab);
            lastSongPlayed = GetLastSongPlayedById();
            if(lastSongPlayed != null && lastSongPlayed.name != null)
                lbLastSongName.text = lastSongPlayed.name;
        }

		void OnSaveLoaded(Message msg)
		{
			for (int i = 0; i < LviewSongList.Length; i++)
			{
				LviewSongList[i].Refresh();
			}
		}

		public void OnDisable()
		{
			MessageBus.Instance.Unsubscribe(MessageBusType.UserDataChanged, OnSaveLoaded);
			GroupHomeSceneManager.Instance.onPanelChange -= OnpanelChange;
		}

		public void ShowNormalTab()
		{
			ShowTab(0);
		}

		public void ShowPremiumTab()
		{
			ShowTab(1);
		}

		public void ShowFavoriteTab()
		{
			ShowTab(2);
		}

		void ShowTab(int tab)
		{
			currentTab = tab;
			for (int i = 0; i < LviewSongList.Length; i++)
			{
				tabButtons[i].SetActive(i != currentTab);
				tabActives[i].SetActive(i == currentTab);
				tabContents[i].SetActive(i == currentTab);
			}

			if (LviewSongList[currentTab].type == SongListType.Favorite)
			{
				LviewSongList[currentTab].Refresh();
			}
			else
			{
				LviewSongList[currentTab].RefreshItems();
			}

            for (int j = 0; j < tabTitles.Length; j++)
            {
                tabTitles[j].color = Color.white;
            }
            tabTitles[tab].color = yellow;
		}

		private void OnpanelChange(GroupHomeSceneType panel)
		{
            if (panel == GroupHomeSceneType.SettingUI)
                anmController.SetTrigger("hidetoleft");
            else
                anmController.SetTrigger("hidetoright");
		}

        private SongDataModel GetLastSongPlayedById()
        {
            int id = PlayerPrefs.GetInt(ProjectConstants.Keys.LastSongIDPlayed, GameManager.Instance.StoreData.listNormalSongs[0].ID);
            List<SongDataModel> listAllSongs = GameManager.Instance.StoreData.listAllSongs;
            for (int i = 0; i < listAllSongs.Count; i++)
            {
                if (listAllSongs[i].ID == id)
                    return listAllSongs[i];
            }
            return GameManager.Instance.StoreData.listAllSongs[0];
        }

        public void PlayLastSongID()
        {
            if (lastSongPlayed != null)
            {
                SongManager.Instance.TryToPlay(lastSongPlayed, GroupHomeSceneType.SongList.ToString(), "replay");
            }
        }
    }
}
