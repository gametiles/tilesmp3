using UnityEngine;
using Amanotes.Utils;
using ProjectConstants;
using Amanotes.Utils.MessageBus;

namespace Amanotes.PianoChallenge
{
	public class ContinuePopUpController : SSController
	{
		public CountdownView countdown;

		[Header("Shown when user can reach new highscore")]
		[SerializeField] RecordView starView = null;
		[SerializeField] RecordView crownView = null;
		[SerializeField] UILabel lbSupportiveMessage = null;

		[Header("Shown when user already has new highscore")]
		[SerializeField] UILabel lbEncourageMessage = null;

		[Header("Price to continue")]
		[SerializeField] UILabel lbPriceToContinue = null;
		[SerializeField] GameObject insufficientDiamonds = null;

		private int priceToContinue = 0;
		private MainGameSceneController maingame;
		private bool decisionMade = false;

		private string watchAdText = "continue";

		public override void OnKeyBack()
		{
			FinishGame();
		}

		public override void OnEnable()
		{
			base.OnEnable();

			decisionMade = false;

			InGameUIController.instance.canTouch = false;
			StartCountDown();

			MessageBus.Instance.Subscribe(MessageBusType.DiamondChanged, OnDiamondsChanged);
		}

		public override void OnDisable()
		{
			base.OnDisable();
			InGameUIController.instance.canTouch = true;

			MessageBus.Instance.Unsubscribe(MessageBusType.DiamondChanged, OnDiamondsChanged);
		}

		public void StartCountDown()
		{
			//count down for 5 seconds, then finish the game
			countdown.Restart(5f, 1, FinishGame);
		}

		public override void OnSet(object data)
		{
			base.OnSet(data);

			maingame = data as MainGameSceneController;

			OnDiamondsChanged();

			starView.gameObject.SetActive(false);
			crownView.gameObject.SetActive(false);
			lbEncourageMessage.gameObject.SetActive(false);
			lbSupportiveMessage.gameObject.SetActive(false);

			//get current star
			int star = Counter.GetQuantity(Counter.KeyStar);
			int currentScore = Counter.GetQuantity(Counter.KeyScore);
			int highScore = HighScoreManager.Instance.GetHighScore(GameManager.Instance.SessionData.song.storeID, ScoreType.Score);
			int scoreTillNextHighScore = highScore - currentScore;
			int tilesTillNextStar = maingame.gameplay.GetNumTileTillNextStar(star);

			int choice = 0;
			if (PlayerPrefs.GetInt(GameConsts.OnBoarding, -1) == -1)
			{
				choice = 0;
			}
			else if (scoreTillNextHighScore > 0)
			{
				choice = tilesTillNextStar > scoreTillNextHighScore ? 1 : 0;
			}
			else
			{
				choice = 2;
			}

			switch (choice)
			{
				case 0:
					if (star <= 5)
					{
						++star;
						lbSupportiveMessage.gameObject.SetActive(true);
						lbSupportiveMessage.text = Localization.Get("pu_continue_supportive").Replace("{0}", tilesTillNextStar.ToString());
						if (star <= 3)
						{
							starView.gameObject.SetActive(true);
							starView.SetVisible(true);
							starView.ShowNumRecord(star);
						}
						else
						{
							crownView.gameObject.SetActive(true);
							crownView.SetVisible(true);
							crownView.ShowNumRecord(star - 3);
						}
					}
					break;

				case 1:
					lbEncourageMessage.gameObject.SetActive(true);
					lbEncourageMessage.text = Localization.Get("pu_continue_encourage_1").Replace("{0}", scoreTillNextHighScore.ToString());
					break;

				case 2:
					lbEncourageMessage.gameObject.SetActive(true);
					lbEncourageMessage.text = Localization.Get("pu_continue_encourage_2").Replace("{0}", currentScore.ToString());
					break;
			}

			AnalyticsHelper.Instance.LogRewardVideoButtonShown(Scenes.ContinuePopup.GetName(), watchAdText);
		}

		/// <summary>
		/// Finish current game and go to result scene
		/// </summary>
		public void FinishGame()
		{
			Utils.AnalyticsHelper.Instance.LogClickItem("Finish Game");
			countdown.Stop();
			SceneManager.Instance.CloseScene();
			maingame.EndGame();
		}

		/// <summary>
		/// Continue to play this game, at current progress
		/// </summary>
		public void ContinueGame()
		{
			if (ProfileHelper.Instance.CurrentDiamond >= priceToContinue)
			{
				decisionMade = true;

				ProfileHelper.Instance.CurrentDiamond -= priceToContinue;
				LevelUpProgress.Instance.OnContinuePurchased(priceToContinue);
				AnalyticsHelper.Instance.LogClickItem("Continue Game");
				countdown.Stop();
				SceneManager.Instance.CloseScene();
				maingame.ContinueGame(false);
			}
			else
			{
				countdown.Pause();
				AnalyticsHelper.Instance.LogIapOpen(AnalyticsHelper.IAP_TO_CONTINUE);
				SceneManager.Instance.OpenPopup(Scenes.IAP);
			}
		}

		public void WatchRewardVideo()
		{
			AnalyticsHelper.Instance.LogRewardVideoButtonClicked(Scenes.ContinuePopup.GetName(), watchAdText);
			AdHelper.Instance.ShowRewardVideo(RewardType.Continue, OnRewardVideoWatched);
#if !UNITY_EDITOR
			countdown.Pause();
#endif
		}

		private void OnRewardVideoWatched()
		{
			decisionMade = true;
			countdown.Stop();
			SceneManager.Instance.CloseScene();
			maingame.ContinueGame(false);
		}

		private void OnRewardVideoFailed()
		{
			countdown.Resume();
		}

		private void OnDiamondsChanged(Message msg = null)
		{
			if (decisionMade)
			{
				return;
			}

			priceToContinue = maingame.gameplay.GetPriceForContinuePlaying();

			bool enough = priceToContinue <= ProfileHelper.Instance.CurrentDiamond;

			insufficientDiamonds.SetActive(!enough);

			//how much does it cost to continue playing
			lbPriceToContinue.text = "x " + priceToContinue.ToString();
		}
	}
}
