using UnityEngine;
using Amanotes.PianoChallenge;
using Facebook.Unity;
using Amanotes.Utils;
using ProjectConstants;
using Amanotes.Utils.MessageBus;

public class SongLeaderboardPopup : SSController
{
	[SerializeField] UILabel lbNameSong = null;
	[SerializeField] SongLeaderboardView friendTab = null;
	[SerializeField] SongLeaderboardView globalTab = null;
	[SerializeField] GameObject[] activeTabs;
	[SerializeField] GameObject[] inactiveTabs;
	[SerializeField] GameObject facebookPanel = null;
	[SerializeField] GameObject friendLoginText = null;
	[SerializeField] GameObject globalLoginText = null;
	[SerializeField] GameObject loggingIn = null;
	[SerializeField] GameObject notLoggedIn = null;
	[SerializeField] UILabel requireText = null;

	SongDataModel song = null;

	public override void OnEnable()
	{
		base.OnEnable();

		MessageBus.Instance.Subscribe(MessageBusType.FriendsLoaded, OnFriendsLoaded);
		MessageBus.Instance.Subscribe(MessageBusType.FacebookLoggedIn, OnFacebookLoggedIn);
	}

	public override void OnDisable()
	{
		MessageBus.Instance.Unsubscribe(MessageBusType.FriendsLoaded, OnFriendsLoaded);
		MessageBus.Instance.Unsubscribe(MessageBusType.FacebookLoggedIn, OnFacebookLoggedIn);

		base.OnDisable();
	}

	public override void OnSet(object data)
	{
		notLoggedIn.SetActive(true);
		loggingIn.SetActive(false);

		if (data == null)
		{
			friendTab.Init(null, null);
			globalTab.Init(null, null);
			return;
		}

		song = (data as SongDataModel);
		friendTab.Init(song.storeID, SongLeaderboardManager.Instance.FriendLeaderboards);
		globalTab.Init(song.storeID, SongLeaderboardManager.Instance.GlobalLeaderboards);

		lbNameSong.text = song.name;

		if (AccessToken.CurrentAccessToken == null)
		{
			OpenGlobalTab();
			return;
		}

		var friendLeaderboard = SongLeaderboardManager.Instance.FriendLeaderboards.GetLeaderboard(song.storeID);
		bool friendLeaderboardAvailable = friendLeaderboard != null && friendLeaderboard.Populated;

		if (song.CanViewGlobalLeaderboard && !friendLeaderboardAvailable)
		{
			OpenGlobalTab();
			return;
		}
		OpenFriendTab();
	}

	public void CloseScene()
	{
		SceneManager.Instance.CloseScene();
	}

	public void OpenFriendTab()
	{
		friendLoginText.SetActive(true);
		globalLoginText.SetActive(false);
		activeTabs[0].SetActive(true);
		activeTabs[1].SetActive(false);
		inactiveTabs[0].SetActive(false);
		inactiveTabs[1].SetActive(true);
		requireText.gameObject.SetActive(false);

		bool loggedIn = AccessToken.CurrentAccessToken != null;
		facebookPanel.SetActive(!loggedIn);

		globalTab.Hide();

		if (loggedIn)
		{
			friendTab.Show();
		}
		else
		{
			friendTab.Hide();
		}
	}

	public void OpenGlobalTab()
	{
		friendLoginText.SetActive(false);
		globalLoginText.SetActive(true);
		activeTabs[1].SetActive(true);
		activeTabs[0].SetActive(false);
		inactiveTabs[1].SetActive(false);
		inactiveTabs[0].SetActive(true);

		if (CheckGlobalLeaderboardRequirement())
		{
			globalTab.Show();
			facebookPanel.SetActive(AccessToken.CurrentAccessToken == null);
		}
		else
		{
			globalTab.Hide();
			facebookPanel.SetActive(false);
		}

		friendTab.Hide();
	}

	public void LoginFacebook()
	{
		notLoggedIn.SetActive(false);
		loggingIn.SetActive(true);
		FacebookManager.Instance.loginPlace = Scenes.SongLeaderboardPopup.ToString();
		FacebookManager.Instance.incentiveTextChoice = "308";
		ProfileHelper.Instance.LoginFacebook();
	}

	bool CheckGlobalLeaderboardRequirement()
	{
		var eligibility = song.GlobalLeaderboardEligibility;
		switch(song.GlobalLeaderboardEligibility)
		{
			case SongGlobalLeaderboardStatus.Eligible:
				requireText.gameObject.SetActive(false);
				return true;

			case SongGlobalLeaderboardStatus.LackStars:
				requireText.text = Localization.Get("378").Replace("{0}", GameManager.Instance.GameConfigs.songLeaderboardStarsRequired.ToString());
				break;

			case SongGlobalLeaderboardStatus.LackScore:
				requireText.text = Localization.Get("378").Replace("{0}", GameManager.Instance.GameConfigs.songLeaderboardScoreRequired.ToString());
				break;
		}

		requireText.gameObject.SetActive(true);
		return false;
	}

	private void OnFacebookLoggedIn(Message msg)
	{
		if (AccessToken.CurrentAccessToken != null)
		{
			return;
		}

		// Log in failed
		notLoggedIn.SetActive(true);
		loggingIn.SetActive(false);
	}

	private void OnFriendsLoaded(Message msg)
	{
		facebookPanel.SetActive(false);

		if (globalTab.gameObject.activeSelf)
		{ // Global tab is active, nothing to do
			return;
		}

		friendTab.Show();
	}
}
