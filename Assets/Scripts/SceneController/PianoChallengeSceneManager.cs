
namespace Amanotes.PianoChallenge
{
	public class PianoChallengeSceneManager : SSSceneManager
	{
		protected override void OnFirstSceneLoad()
		{
			base.OnFirstSceneLoad();
			SceneManager.Instance.OpenScene(ProjectConstants.Scenes.SplashScreen);
		}

		public void LogoutParseUser()
		{
			Parse.ParseUser.LogOutAsync();
		}
	}
}
