using UnityEngine;
using System;

namespace Amanotes.PianoChallenge
{
	public class UpdatePopup : SSController
	{
		[SerializeField] GameObject continueButton = null;
		[SerializeField] UILabel instruction = null;

		#if UNITY_IOS
		const string STORE_LINK = "https://itunes.apple.com/ye/app/piano-challenge-2/id1077947558";
		#else // UNITY_ANDROID
		const string STORE_LINK = "https://play.google.com/store/apps/details?id=com.musicheroesrevenge.pianochallenge2";
		#endif
		Action onContinue = null;
		public override void OnSet(object data)
		{
			
			onContinue = data as Action;

			var prompt = GameManager.Instance.GameConfigs.updatePrompt;
			continueButton.SetActive (prompt != "mandatory");

			instruction.text = Localization.Get ( prompt == "mandatory" ? "356" : "355");
		}

		public void Continue()
		{
			SceneManager.Instance.CloseScene();
			if (onContinue != null)
			{
				onContinue ();
			}
		}

		public void GotoStore()
		{
			Application.OpenURL(STORE_LINK);
		}
	}
}
