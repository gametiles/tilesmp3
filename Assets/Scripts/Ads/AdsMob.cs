﻿using UnityEngine;
using GoogleMobileAds.Api;
using System;
using Amanotes.Utils;
using Amanotes.PianoChallenge;

public class AdsMob : SingletonMono<AdsMob>
{
    private InterstitialAd interstitial;
    [HideInInspector] public RewardBasedVideoAd rewardBasedVideo;
    private string deviceTest = "88FB34577B8AD24D94D8DF0C48A8DE24";
    private bool isInited = false;

    void Start()
    {
        Init();
    }

    private void Init()
    {
        rewardBasedVideo = RewardBasedVideoAd.Instance;
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;

        MobileAds.Initialize(initStatus =>
        {
            isInited = true;
            Invoke("RequestInterstitial", 1f);
            Invoke("RequestRewardVideo", 1f);
            Debug.LogError("Ads Init thanh cong");
        });

    }

    public void RequestInterstitial()
    {
        this.interstitial = new InterstitialAd("ca-app-pub-6343462558308394/3399384844");
        AdRequest request = new AdRequest.Builder().Build();
        this.interstitial.LoadAd(request);
    }

    public void ShowInterstitial()
    {
        if (this.interstitial.IsLoaded())
        {
            this.interstitial.Show();
            Invoke("RequestInterstitial", 3f);
        }
        else if (isInited == false)
            Init();
        else
            Invoke("RequestInterstitial", 3f);
    }

    public void RequestRewardVideo()
    {
        string adUnitId = "ca-app-pub-6343462558308394/6962870119";
        AdRequest request = new AdRequest.Builder().Build();
        rewardBasedVideo.LoadAd(request, adUnitId);
    }

    public void ShowRewardVideo()
    {
        if (rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.Show();
            Debug.LogError("AAA: rw loaded");
            Invoke("RequestRewardVideo", 2f);
        }
        else if (isInited == false)
        {
            Debug.LogError("AAA: rw init");
            Init();
            AdHelper.Instance.OnRewardVideoFailed();
        }
        else
        {
            Debug.LogError("AAA: rw fail");
            Invoke("RequestRewardVideo", 1f);
            AdHelper.Instance.OnRewardVideoFailed();
        }
  
    }

    #region Handle
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        Debug.LogError("AAA: handle watched");
        AdHelper.Instance.OnRewardVideoWatched();
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        Debug.LogError("AAA: handle closed");
        SceneManager.Instance.SetLoadingVisible(false);
        AdHelper.Instance.OnRewardVideoClosed();
        Invoke("RequestRewardVideo", 1f);
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.LogError("AAA: handle faild to load");
    }
    #endregion

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        Debug.LogError("AAA: handle Video Started");
        AdHelper.Instance.LoadingRewardVideo = true;
        SceneManager.Instance.SetLoadingVisible(true);
    }

    void OnGUI()
    {
        //if (GUI.Button(new Rect(0, 0, Screen.width / 5, Screen.height / 10), "Init_1"))
        //{
        //    ShowRewardVideo();
        //}
    }
}
