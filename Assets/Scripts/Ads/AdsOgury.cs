
#if UNITY_ANDROID

using Amanotes.PianoChallenge;
using Amanotes.Utils;
using System.Collections;
using UnityEngine;

public class WHandlerImpl : WPresage.IADHandler
{
	public delegate void onAdNotFound();
	public delegate void onAdFound();
	public delegate void onAdClosed();
	public delegate void onAdError(int code);
	public delegate void onAdDisplayed();

	public onAdNotFound _onAdNotFound;
	public onAdFound _onAdFound;
	public onAdClosed _onAdClosed;
	public onAdError _onAdError;
	public onAdDisplayed _onAdDisplayed;

	public void OnAdNotAvailable()
	{
		Development.Log("Ads - OguryOnAdNotAvailable");
		if (_onAdNotFound != null)
			_onAdNotFound();
	}

	public void OnAdAvailable()
	{
		Development.Log("Ads - OguryOnAdAvailable");
	}

	public void OnAdLoaded()
	{
		Development.Log("Ads - (Ogury) an ad in loaded, ready to be shown");
		if (_onAdFound != null)
			_onAdFound();
	}

	public void OnAdClosed()
	{
		Development.Log("Ads - OguryonAdClosed");
		if (_onAdClosed != null)
			_onAdClosed();
	}

	public void OnAdError(int code)
	{
		Development.Log("Ads - OguryonAdError");
		if (_onAdError != null)
			_onAdError(code);
	}

	public void OnAdDisplayed()
	{
		Development.Log("Ads - OguryonAdDisplayed");
		if (_onAdDisplayed != null)
			_onAdDisplayed();
	}
}

public class AdsOgury : AdsProvider
{
	public const string NAME = "Ogury";

	private delegate void onAdNotFound();
	private delegate void onAdFound();
	private delegate void onAdClosed();
	private delegate void onAdError();
	private delegate void onAdDisplayed();

	private bool adsLoaded = false;

	bool active = false;

	WHandlerImpl handlerImpl;
	WHandlerImpl handlerImplLoad;

	public override IEnumerator Initialize()
	{
		Development.Log("Ads - Initializing Ogury");
		active = false;

		// Wait until geolocation is initialized
		while (!GeoLocationManager.Instance.IsLoaded)
		{
			yield return new WaitForSeconds(0.5f);
		}

		try
		{
			// Check for the supported countries
			string countryCode = GeoLocationManager.Instance.CountryCode.countryCode.ToUpper();
			active = GameManager.Instance.GameConfigs.listRegionOgury.Contains(countryCode);

			if (active)
			{
				Development.Log("Ads - Ogury is active for " + countryCode);
				Init();
			}
			else
			{
				Development.Log("Ads - Ogury won't be used for " + countryCode);
			}
		}
		catch (System.Exception)
		{
			active = false;
		}

		Development.Log("Ads - Ogury Initialized");
		OnInitialized();

		yield break;
	}

	private void Init()
	{
		handlerImplLoad = new WHandlerImpl();
		handlerImplLoad._onAdNotFound += OnAdNotFound;
		handlerImplLoad._onAdFound += OnAdFound;

		handlerImpl = new WHandlerImpl();
		handlerImpl._onAdNotFound += OnAdNotFound;
		handlerImpl._onAdFound += OnAdFound;
		handlerImpl._onAdClosed += OnAdClosed;
		handlerImpl._onAdDisplayed += OnAdDisplayed;

		WPresage.Initialize();
		RequestAds();
	}

	public override void OnApplicationPaused(bool isPaused)
	{

	}

	public override void OnEnabled()
	{
	}

	public override bool IsInterstitialReady()
	{
		return active;
	}

	public override bool IsRewardVideoReady()
	{
		return false; // No reward video for Ogury, sorry
	}

	public override string Name()
	{
		return NAME;
	}

	public override void ShowInterstitial()
	{
		ShowAds();
	}

	public override void ShowRewardVideo()
	{
		// No reward video for Ogury, sorry
	}

	private void OnError()
	{
		OnInterstitialFailed();
	}

	private void ShowAds()
	{
		Development.Log("Ads - OguryManager.ShowAds");
		if (adsLoaded && WPresage.CanShow())
		{
			adsLoaded = false;
			WPresage.Show(handlerImpl);
		}
		else
		{
			Development.Log("Ads - OguryManager.Can't Show");
			RequestAds();
			OnError();
		}
	}

	public void RequestAds()
	{
		Development.Log("Ads - OguryRequestAds");
		adsLoaded = false;
		WPresage.Load(handlerImplLoad);
	}

	private void OnAdNotFound()
	{
		adsLoaded = false;
		Development.Log("Ads - OguryManager.onAdNotFound");
	}

	private void OnAdFound()
	{
		adsLoaded = true;
		Development.Log("Ads - OguryManager.OguryonALoaded");
	}

	private void OnAdClosed()
	{
		Development.Log("Ads - OguryManager.OguryonAdClosed");
		OnInterstitialClosed();
		RequestAds();
	}

	private void OnAdError(int code)
	{
		RequestAds();
		Development.Log("Ads - OguryManager.OguryonAdError");
		OnError();
	}

	private void OnAdDisplayed()
	{
		Development.Log("Ads - OguryManager.OguryonAdDisplayed");
	}

#if DEVELOPMENT_BUILD

	private static AdsOgury instance = null;

	public static AdsOgury Instance { get { return instance; } }

	public AdsOgury()
	{
		instance = this;
	}

	public void TestInit()
	{
		Init();
	}

	public void TestShowAds()
	{
		ShowAds();
	}

	public void TestRequestAds()
	{
		RequestAds();
	}

	public void TestAdServe()
	{
		WPresage.AdToServe(handlerImpl);
	}

#endif
}

#endif
