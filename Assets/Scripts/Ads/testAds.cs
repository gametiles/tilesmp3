﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class testAds : MonoBehaviour {

    private InterstitialAd interstitial;
    private BannerView bannerView;
    private RewardBasedVideoAd rewardBasedVideo;
    private RewardedAd rewardedAd;
    private string deviceTest = "88FB34577B8AD24D94D8DF0C48A8DE24";
    private int x;
    private int y;

    public void Start()
    {
        // Initialize the Google Mobile Ads SDK.
        x = Screen.width / 5;
        y = Screen.height / 10;
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(x * 0, y * 0, x, y), "Init_1"))
        {
            MobileAds.Initialize(initStatus => { });
        }

        if (GUI.Button(new Rect(x * 1, y * 0, x, y), "Init_2"))
        {
            MobileAds.Initialize("ca-app-pub-6343462558308394~3068177629");
        }

        if (GUI.Button(new Rect(x * 2, y * 0, x, y), "RQ My BN"))
        {
            string adUnitId = "ca-app-pub-6343462558308394/8619887209";
            bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
            AdRequest request = new AdRequest.Builder().AddTestDevice(deviceTest).Build();
            bannerView.LoadAd(request);
        }

        if (GUI.Button(new Rect(x * 3, y * 0, x, y), "RQ Test BN"))
        {
            string adUnitId = "ca-app-pub-3940256099942544/6300978111";
            bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
            AdRequest request = new AdRequest.Builder().Build();
            bannerView.LoadAd(request);
        }

        if (GUI.Button(new Rect(x * 4, y * 0, x, y), "Show BN"))
        {            
            bannerView.Show();
        }

        if (GUI.Button(new Rect(x * 0, y * 1, x, y), "Init Test"))
        {
            MobileAds.Initialize("ca-app-pub-3940256099942544~3347511713");
        }
        if (GUI.Button(new Rect(x * 1, y * 1, x, y), "RQ My Inter"))
        {
            this.interstitial = new InterstitialAd("ca-app-pub-6343462558308394/9316578121");
            AdRequest request = new AdRequest.Builder().AddTestDevice(deviceTest).Build();
            this.interstitial.LoadAd(request);
        }
        if (GUI.Button(new Rect(x * 2, y * 1, x, y), "RQ Test Inter"))
        {
            this.interstitial = new InterstitialAd("ca-app-pub-3940256099942544/1033173712");
            AdRequest request = new AdRequest.Builder().Build();
            this.interstitial.LoadAd(request);
        }

        if (GUI.Button(new Rect(x * 3, y * 1, x, y), "Show Inter"))
        {
            this.interstitial.Show();
        }
        if (GUI.Button(new Rect(x * 4, y * 1, x, y), "New test RW"))
        {
            rewardedAd = new RewardedAd("ca-app-pub-3940256099942544/5224354917");

            AdRequest request = new AdRequest.Builder().Build();
            this.rewardedAd.LoadAd(request);
            rewardedAd.Show();
        }
        if (GUI.Button(new Rect(x * 0, y * 2, x, y), "New My RW"))
        {
            rewardedAd = new RewardedAd("ca-app-pub-6343462558308394/1567393692");

            AdRequest request = new AdRequest.Builder().Build();
            this.rewardedAd.LoadAd(request);

        }
        if (GUI.Button(new Rect(x * 1, y * 2, x, y), "ShowNEW"))
        {
            rewardedAd.Show();
        }

        if (GUI.Button(new Rect(x * 2, y * 2, x, y), "Old My RW"))
        {
            string adUnitId = "ca-app-pub-6343462558308394/1567393692";
            this.rewardBasedVideo = RewardBasedVideoAd.Instance;
            AdRequest request = new AdRequest.Builder().AddTestDevice(deviceTest).Build();
            this.rewardBasedVideo.LoadAd(request, adUnitId);
        }

        if (GUI.Button(new Rect(x * 3, y * 2, x, y), "ShowOLD"))
        {
            rewardBasedVideo.Show();
        }
    }

    private void RequestInterstitial()
    {
        string adUnitId = "ca-app-pub-6343462558308394/9316578121";
        this.interstitial = new InterstitialAd(adUnitId);
        AdRequest request = new AdRequest.Builder().Build();
        this.interstitial.LoadAd(request);
    }

    private void GameOver()
    {
        if (this.interstitial.IsLoaded())
        {
            this.interstitial.Show();
        }
    }
}
