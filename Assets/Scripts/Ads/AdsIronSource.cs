
using Amanotes.Utils;
using System.Collections;

public class AdsIronSource : AdsProvider
{
	public const string NAME = "IronSource";

	const string USER_ID = "haingo@amanotes.com";
	const string APP_KEY = "674becad";

	bool initialized = false;

	public AdsIronSource()
	{
		try
		{
			AppLovin.InitializeSdk();
		}
		catch (System.Exception)
		{
		}
	}

	public override IEnumerator Initialize()
	{
		Development.Log("Ads - Initializing IronSource");
		initialized = false;

		try
		{

		}
		catch (System.Exception)
		{
		}

		OnInitialized();
		yield break;
	}

	public override void OnApplicationPaused(bool isPaused)
	{
		try
		{
		
		}
		catch(System.Exception) {}
	}

	public override void OnEnabled()
	{
		try
		{

		}
		catch(System.Exception) {}
	}

	public override bool IsInterstitialReady()
	{
		return initialized;
	}

	public override bool IsRewardVideoReady()
	{
		return initialized;
	}

	public override string Name()
	{
		return NAME;
	}

	static string[] placements = new string[] { "A1", "B1", "C1", "D1" };

	public override void ShowInterstitial()
	{
		
	}

	public override void ShowRewardVideo()
	{
		
	}

	void InterstitialAdReadyEvent()
	{
		Development.Log("Ads - IronSource interstitial ready");
	}

	
	void InterstitialAdShowSucceededEvent()
	{
		Development.Log("Ads - IronSource interstitial shown");
	}

	

	void InterstitialAdClickedEvent()
	{
		Development.Log("Ads - IronSource interstitial clicked");
	}

	void InterstitialAdOpenedEvent()
	{
		Development.Log("Ads - IronSource interstitial opened");
	}

	void InterstitialAdClosedEvent()
	{
		Development.Log("Ads - IronSource interstitial closed");
		OnInterstitialClosed();

		
	}

	void InterstitialAdRewardedEvent()
	{
		Development.Log("Ads - IronSource interstitial rewarded");
	}

	void RewardedVideoAvailabilityChangedEvent(bool canShowAd)
	{
		Development.Log("Ads - IronSource rewarded video ready = " + canShowAd);
	}

	void RewardedVideoAdOpenedEvent()
	{
		Development.Log("Ads - IronSource rewarded video opened");
	}

	

	void RewardedVideoAdClosedEvent()
	{
		Development.Log("Ads - IronSource rewarded video closed");
		OnRewardVideoClosed();
	}

	void RewardedVideoAdStartedEvent()
	{
		Development.Log("Ads - IronSource rewarded video started");
	}

	void RewardedVideoAdEndedEvent()
	{
		Development.Log("Ads - IronSource rewarded video ended");
	}

	
}
