
using System;
using System.Collections;

public abstract class AdsProvider
{
	public abstract string Name();
	public abstract IEnumerator Initialize();
	public abstract void OnApplicationPaused(bool isPaused);
	public abstract void OnEnabled();
	public abstract void ShowInterstitial();
	public abstract void ShowRewardVideo();
	public abstract bool IsInterstitialReady();
	public abstract bool IsRewardVideoReady();

	public Action OnInitialized;
	public Action OnInterstitialFailed;
	public Action OnRewardVideoFailed;
	public Action OnInterstitialClosed;
	public Action OnRewardVideoWatched;
	public Action OnRewardVideoClosed;
}
