
using System.Runtime.InteropServices;
using UnityEngine;
using System;

namespace fModStudio
{
	public class FMod
    {
		[DllImport("fMod")] private static extern void pool_loadMp3s(byte[] data, int size, bool parallel);
		[DllImport("fMod")] private static extern void pool_unload();
		[DllImport("fMod")] private static extern bool pool_isLoading();
		[DllImport("fMod")] private static extern void pool_pause();
		[DllImport("fMod")] private static extern void pool_resume();
		[DllImport("fMod")] private static extern void pool_play(int id, float volume);
		[DllImport("fMod")] private static extern int android_nativeSampleRate();

		public static void Pause()
		{
			pool_pause();
		}

		public static void Resume()
		{
			try
			{
				pool_resume();
			}
			catch (Exception)
			{
			}
		}

		static byte[] data = null;

		public static void Load(bool parallel = false)
		{
			string name = android_nativeSampleRate() == 48000 ?
				"Audio/PianoKeysMp3_48000Hz" : "Audio/PianoKeysMp3_44100Hz";
			byte[] data = (Resources.Load(name) as TextAsset).bytes;
			if (data != null)
			{
				pool_loadMp3s(data, data.Length, parallel);
				if (parallel)
				{
                    FMod.data = data;
				}
			}
			else
			{
				Development.Log("Cannot find sound pool data");
			}
		}

		public static void FinishLoading()
		{
			data = null;
		}

		public static void Unload()
		{
			pool_unload();
		}

		public static bool IsLoading()
		{
			return pool_isLoading();
		}

		public static void Play(int id, float volume)
		{
			pool_play(id, volume);
		}
	}
}
