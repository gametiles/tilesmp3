﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioBackground : SingletonMono<AudioBackground>
{
    [SerializeField] private AudioSource audioSource;

    public AudioClip BackgroundMusic = null;
    [HideInInspector] public bool canMusic = true;

    private void Start()
    {
        canMusic = PlayerPrefs.GetInt(ProjectConstants.Keys.canBackgroundMusic, 1) == 1 ? true : false;
    }

    public void SetCanMusic()
    {
        canMusic = !canMusic;

        if (canMusic == true)
        {
            PlaySoundBackground();
            PlayerPrefs.SetInt(ProjectConstants.Keys.canBackgroundMusic, 1);
        }
        else
        {
            StopBackgroundMusic();
            PlayerPrefs.SetInt(ProjectConstants.Keys.canBackgroundMusic, 0);
        }
    }

    public void PlaySoundBackground(float delay = 0)
    {
        if(canMusic)
            StartCoroutine(PlayBackground(delay));
    }

    IEnumerator PlayBackground(float delay = 0)
    {
        yield return new WaitForSeconds(delay);
        if (audioSource.isPlaying == true && audioSource.volume > 0)
            yield break;

        if (BackgroundMusic == null)
            yield break;

        audioSource.clip = BackgroundMusic;
        audioSource.volume = 0.3f;
        audioSource.loop = true;
        audioSource.Play();

        while (audioSource.volume < 0.6)
        {
            audioSource.volume += 0.05f;
            yield return new WaitForSeconds(0.2f);
        }

        yield return null;
    }

    public void StopBackgroundMusic()
    {
        StopCoroutine(PlayBackground());
        StopAllCoroutines();
        StartCoroutine(StopBackground());
    }

    IEnumerator StopBackground()
    {
        while (audioSource.volume > 0)
        {
            audioSource.volume -= 0.05f;
            yield return new WaitForSeconds(0.1f);
        }
        audioSource.Stop();
        yield return null;
    }
}
