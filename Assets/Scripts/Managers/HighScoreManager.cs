using UnityEngine;
using System.Collections.Generic;
using Amanotes.Utils.MessageBus;

namespace Amanotes.PianoChallenge
{
	public enum ScoreType
	{
		Score,
		Star,
		Crown
	}

	public class HighScoreManager : SingletonMono<HighScoreManager>
	{
		private Dictionary<string, ScoreItemModel> records = null;

		private bool isInit = false;

		private int totalStar = 0;

		public int TotalStars
		{
			get { return totalStar; }
		}

		private int totalCrown = 0;

		public int TotalCrowns
		{
			get { return totalCrown; }
		}

		private static Message MSG_USER_PLAY_RECORD_CHANGED = new Message(MessageBusType.SongRecordChanged);

		public void Initialize()
		{
			if (!isInit)
			{
				isInit = true;
				MessageBus.Instance.Subscribe(MessageBusType.SongRecordChanged, OnUserPlayRecordChanged);
			}

			RefreshMetric();
		}

		private void OnUserPlayRecordChanged(Message obj)
		{
			RefreshMetric();
		}

		private int oldNumCrown, oldNumStar;

		public void RefreshMetric()
		{
			if (!isInit)
			{
				Initialize();
			}
			if (!isInit)
				return;

			var recordList = ProfileHelper.Instance.ListHighScore;
			if (recordList == null)
			{
				records = null;
				Debug.LogWarning("Could not initialize high-score manager if player profile has not been initialized first");
				return;
			}

			oldNumCrown = totalCrown;
			oldNumStar = totalStar;
			totalCrown = totalStar = 0;

			records = new Dictionary<string, ScoreItemModel>(recordList.Count);
			foreach (var item in recordList)
			{
				records.Add(item.itemName, item);

				totalStar += Mathf.Clamp(item.highestStar, 0, 3);
				totalCrown += item.highestCrown;
			}

			if ((oldNumStar != totalStar) || (oldNumCrown != totalCrown))
			{
				MessageBus.Annouce(MSG_USER_PLAY_RECORD_CHANGED);
			}
		}

		public int GetHighScore(string levelName, ScoreType type = ScoreType.Score)
		{
			ScoreItemModel record;
			if (records == null || !records.TryGetValue(levelName, out record))
			{
				return 0;
			}

			return GetScore(record, type);
		}

		public bool UpdateHighScore(string levelName, int scoreValue, ScoreType type = ScoreType.Score)
		{
			if (records == null)
			{
				return false;
			}

			ScoreItemModel record;
			if (records.TryGetValue(levelName, out record))
			{
				int currentScore = GetScore(record, type);
				if (currentScore >= scoreValue)
				{
					return false;
				}

				SetScore(record, type, scoreValue);
				return true;
			}

			//if we reached this point, it's mean the item is not existed. We shall create one
			record = new ScoreItemModel();
			record.itemName = levelName;
			SetScore(record, type, scoreValue);
			records.Add(levelName, record);
			ProfileHelper.Instance.ListHighScore.Add(record);
			return true;
		}

		private static int GetScore(ScoreItemModel model, ScoreType type)
		{
			switch (type)
			{
				case ScoreType.Crown:
					return model.highestCrown;
				case ScoreType.Score:
					return model.highestScore;
				case ScoreType.Star:
					return model.highestStar;
				default:
					return 0;
			}
		}

		private void SetScore(ScoreItemModel model, ScoreType type, int score)
		{
			switch (type)
			{
				case ScoreType.Crown:
					model.highestCrown = score;
					RefreshMetric();
					break;
				case ScoreType.Score:
					model.highestScore = score;
					break;
				case ScoreType.Star:
					model.highestStar = score;
					RefreshMetric();
					break;
			}
		}
	}
}
