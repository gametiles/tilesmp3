using UnityEngine;
using MovementEffects;
using System.Collections.Generic;
using Amanotes.PianoChallenge;
using System;
using Amanotes.Utils.MessageBus;

namespace Amanotes.Utils
{
	public class LivesManager : SingletonMono<LivesManager>
	{
		private Timing timingInstance;
		private bool isCountingDown = false;
		private int secondsRemaining = 0;
		private Message timeCountDownMessage = new Message(MessageBusType.TimeLifeCountDown);

		private bool isNextPlayFree = false;
		public bool IsNextPlayFree { get { return isNextPlayFree; } }
		public void SetNextPlayFree() { isNextPlayFree = true; }

		public int CurrentLife
		{
			get { return ProfileHelper.Instance.CurrentLife; }
		}

		public int MaxLife
		{
			get { return ProfileHelper.Instance.MaxLife; }
		}

		public void Initialize()
		{
			//initialize time counter
			timingInstance = gameObject.AddComponent<Timing>();
			timingInstance.TimeBetweenSlowUpdateCalls = 1f;

			SetupCountdown();
			//register with message bus
			MessageBus.MessageBus.Instance.Subscribe(MessageBusType.LifeChanged, OnUserLifeChanged);
		}


		private void SetupCountdown()
		{
			isCountingDown = false;
			secondsRemaining = 0;

			if (GameManager.Instance.GameConfigs == null)
			{
				return;
			}
			//default value to support user who has just updated to this version from older one
			if (GameManager.Instance.GameConfigs.secondsPerLife <= 0)
			{
				GameManager.Instance.GameConfigs.secondsPerLife = 300;
			}

			//get last count down timestamp from user data
			double lastCountdownUnix = ProfileHelper.Instance.LastLifeCountdownUnixTimeStamp;
			DateTime dtLastCountdown = TimeHelper.UnixTimeStampToDateTime(lastCountdownUnix);
			TimeSpan diff = DateTime.UtcNow - dtLastCountdown;
			double secondsElapsed = diff.TotalSeconds;

			//check how many time have passed?
			if (secondsElapsed <= 0)
			{
				//maybe our users has cheat here. Do nothing about it ;)
				secondsElapsed = 0;
			}

			//calculate and add generated life
			if (secondsElapsed > 0)
			{
				int lifeAdded = Mathf.FloorToInt((float)secondsElapsed / GameManager.Instance.GameConfigs.secondsPerLife);
				if (lifeAdded > 0)
				{
					if (lifeAdded + ProfileHelper.Instance.CurrentLife <= ProfileHelper.Instance.MaxLife)
					{
						ProfileHelper.Instance.CurrentLife += (lifeAdded);
					}
					else
					{
						if (ProfileHelper.Instance.CurrentLife < ProfileHelper.Instance.MaxLife)
						{
							ProfileHelper.Instance.CurrentLife = ProfileHelper.Instance.MaxLife;
						}
					}
					secondsElapsed -= (lifeAdded * GameManager.Instance.GameConfigs.secondsPerLife);
				}
			}

			//check if we need to generate more life or not
			if (CurrentLife < MaxLife)
			{
				StartCountDown(GameManager.Instance.GameConfigs.secondsPerLife - (int)secondsElapsed);
			}
			else
			{
				timeCountDownMessage.data = 0;
				MessageBus.MessageBus.Annouce(timeCountDownMessage);
			}
		}

		private void OnUserLifeChanged(Message msg)
		{
			if (CurrentLife < MaxLife)
			{
				if (!isCountingDown)
				{
					StartCountDown(GameManager.Instance.GameConfigs.secondsPerLife);
				}
			}
		}

		private void StartCountDown(int totalSeconds)
		{
			//timingInstance.
			isCountingDown = true;
			secondsRemaining = totalSeconds;

			// only update user data if we start counting down full duration.
			// Why? Because that way, we can reduce read/write to storage, and always make sure that we only need to
			// calculate remaining times with full countdown duration
			if (totalSeconds == GameManager.Instance.GameConfigs.secondsPerLife)
			{
				ProfileHelper.Instance.LastLifeCountdownUnixTimeStamp =
					TimeHelper.DateTimeToUnixTimeStamp(
						DateTime.UtcNow.AddSeconds(
							GameManager.Instance.GameConfigs.secondsPerLife - totalSeconds));
			}

			versionLiveCowndown++;
			if (timingInstance != null)
			{
				timingInstance.RunCoroutineOnInstance(CountingDown(versionLiveCowndown), Segment.SlowUpdate);
			}
		}

		int versionLiveCowndown = 0;
		private IEnumerator<float> CountingDown(int currentVersion)
		{
			while (isCountingDown && versionLiveCowndown == currentVersion)
			{
				--secondsRemaining;
				timeCountDownMessage.data = secondsRemaining;

				if (secondsRemaining <= 0)
				{
					isCountingDown = false;
					secondsRemaining = 0;
					IncreaseLife(1);
				}
				MessageBus.MessageBus.Annouce(timeCountDownMessage);
				yield return 0f;
			}
		}


		private void OnUserRefuseToRechargeLife()
		{
			AnalyticsHelper.Instance.LogClickItem("Refuse Recharge Life");
		}

		private void OnUserChoseToRechargeLife()
		{
			LivesManager.Instance.IncreaseLife(20);
			AnalyticsHelper.Instance.LogClickItem("Recharge Life");
		}

		/// <summary>
		/// Short hand for ChangeLife(-1)
		/// </summary>
		public void ReduceLife()
		{
			if (isNextPlayFree)
			{
				isNextPlayFree = false;
				return;
			}
			//IncreaseLife(-1);
		}

		/// <summary>
		/// Short hand for ChangeLife(1)
		/// </summary>
		public void AddLife()
		{
			IncreaseLife(1);
		}

		/// <summary>
		/// Add more life to current value
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public void IncreaseLife(int value, bool limitbreak = false)
		{
			ProfileHelper.Instance.CurrentLife += value;
		}

		public bool CanPlaySong()
		{
			return (isNextPlayFree || ProfileHelper.Instance.CurrentLife >= 1);
		}

		public void SetLife(int value)
		{
			ProfileHelper.Instance.CurrentLife = value;
		}

		internal void SuggestRechargeLives()
		{
			SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.LivesPopup);
		}

		void OnApplicationPause(bool state)
		{
			RemoveAllPush();
			if (state)
			{
			}
			else
			{
				SetupCountdown();
			}
		}

		public void AddLocalPush(string text, string caption, int second, int id)
		{
#if UNITY_IPHONE
		System.DateTime date = System.DateTime.Now;
		date.AddSeconds(second);
		UnityEngine.iOS.LocalNotification notification = new UnityEngine.iOS.LocalNotification ();
		notification.fireDate = date;
		notification.alertAction = "Alert";
		notification.alertBody = text;
		notification.hasAction = false;
		notification.applicationIconBadgeNumber = 1;
		notification.soundName = UnityEngine.iOS.LocalNotification.defaultSoundName;
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification (notification);
#elif UNITY_ANDROID
#endif
		}

		public void RemoveAllPush()
		{
#if UNITY_IPHONE
		int numNotif = UnityEngine.iOS.NotificationServices.localNotificationCount;
		for (int i = 0; i < numNotif; i++)
		{
			UnityEngine.iOS.LocalNotification notif = UnityEngine.iOS.NotificationServices.GetLocalNotification(i);
			Debug.Log("Cancel Local Notification found " + notif.alertBody);
			UnityEngine.iOS.NotificationServices.CancelLocalNotification(notif);
		}

		UnityEngine.iOS.LocalNotification[] oldSchedule = UnityEngine.iOS.NotificationServices.scheduledLocalNotifications;
		if(oldSchedule!=null)
		{
			Debug.LogWarning ("Remove All Push schedule: "+oldSchedule.Length);
			for (int i = 0; i < oldSchedule.Length; i++)
			{
				UnityEngine.iOS.LocalNotification notif = oldSchedule[i];
				Debug.Log("Cancel schedule Notification found " + notif.alertBody);
				UnityEngine.iOS.NotificationServices.CancelLocalNotification(notif);
			}
		}
		UnityEngine.iOS.NotificationServices.ClearRemoteNotifications();

		UnityEngine.iOS.LocalNotification l = new UnityEngine.iOS.LocalNotification ();
		l.applicationIconBadgeNumber = -1;
		UnityEngine.iOS.NotificationServices.PresentLocalNotificationNow (l);
		Debug.Log ("Inital Push Finish");
#elif UNITY_ANDROID
#endif
		}

		public int SecondsUntilFull()
		{
			return (MaxLife - CurrentLife - 1) * 300 + secondsRemaining;
		}
	}
}
