﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BannerLeftManager : SingletonMono<BannerLeftManager>
{
    public Sprite defaultBannerLeft;
    public List<ImageBannerItemView> listBanner = new List<ImageBannerItemView>();

    [System.Serializable]
    public class ImageBannerItemView
    {
        public int id;
        public Sprite spriteImage;
    }    
}
