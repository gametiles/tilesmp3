﻿using UnityEngine;
using System.Collections;

public class AudioEffect : SingletonMono<AudioEffect>
{
    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioSource audioSource_2;

    public AudioClip clickBottom = null;

    public void PlaySound(AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.loop = false;
        audioSource.Play();
    }

    public void PlaySound_2(AudioClip clip)
    {
        audioSource_2.clip = clip;
        audioSource_2.loop = false;
        audioSource_2.Play();
    }

    public void StopPlaySound_2()
    {
        audioSource_2.Stop();
    }

    public void PlayButtonSoundEffect()
    {
        if(clickBottom != null)
            PlaySound(clickBottom);
    }
	
}
