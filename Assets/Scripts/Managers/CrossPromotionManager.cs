using UnityEngine;
using System.Collections.Generic;
using Amanotes.Utils;

public class CrossPromotionManager : Singleton<CrossPromotionManager>
{
	List<CrossPromotion> items = null;

	public List<CrossPromotion> Items { get { return items; } }

	// Use this for initialization
	public void Init(List<string> dataList)
	{
		if (dataList == null || dataList.Count <= 0)
		{
			return;
		}

		if (items == null)
		{
			items = new List<CrossPromotion>(8);
		}
		else
		{
			items.Clear();
		}

		int k = Application.platform == RuntimePlatform.Android ? 3 : 2;

		foreach (var data in dataList)
		{
			string[] strings = data.Split('#');

			if (strings.Length < 5)
			{ // Data not valid
				continue;
			}

			string imageUrl = strings[0];
			string iconUrl = strings[1];
			string storeUrl = strings[k];
			string trackingCode = strings[4];
			string name = strings[5];
			if (
				string.IsNullOrEmpty(imageUrl) ||
				string.IsNullOrEmpty(iconUrl) ||
				string.IsNullOrEmpty(storeUrl) ||
				string.IsNullOrEmpty(trackingCode) ||
				string.IsNullOrEmpty(name))
			{ // Data not valid
				continue;
			}

			CacheImage(imageUrl);
			CacheImage(iconUrl);

			items.Add(new CrossPromotion(imageUrl, iconUrl, storeUrl, trackingCode, name));
		}
	}

	public CrossPromotion GetRandomItem()
	{
		return items != null && items.Count > 0 ? items[Random.Range(0, items.Count)] : null;
	}

	static void CacheImage(string url)
	{
		AssetDownloader.Instance.DownloadAndCacheAsset(url, 0, null, null, null);
	}
}
