
using System.Collections.Generic;
using Amanotes.PianoChallenge;
using UnityEngine;
using System;
using Amanotes.Utils;

class NotificationManager : Singleton<NotificationManager>
{
	const int SECONDS_IN_A_DAY = 24 * 60 * 60;
	const int FULL_LIFE_PN_ID = 1;
	const int FREE_SONG_PN_ID = 2;
	const int DISCOUNT_SONG_PN_ID = 3;
	const int OFFLINE_PN_ID = 4;
	const int LUCKY_GIFT_PN_ID = 5;
	const string PNDB_FILE = "pn.db";
	const string FREE_SONG_PN_CLICKED = "Free Song Notification Clicked";

	const int MIN_WAIT_SECONDS = 15; // or we will spam
	const int MIN_PN_INTERTAL = 3600;

	bool appPaused = true; // In case OnAppPause and OnAppResume is called multiple times
	bool freePnClicked = false;

	enum ScheduleType { NONE, FREE_PREMIUM_SONG, DISCOUNT_PREMIUM_SONG };

	[Serializable]
	class PN // push notification
	{
		public PN(int id) { this.id = id; }
		public int id;
		public int targetTimeSSE = 0; // SSE = seconds since Epoch
		public int scheduleTimeSSE = 0;
		public int count = 0; // Number of time shown
		public bool scheduled = false;
	}

	[Serializable]
	class PNDB // Push notification database
	{
		public ScheduleType scheduleType = ScheduleType.NONE;
		public int dailyScheduleHour = 0;
		public List<PN> pns = new List<PN>(2);
		public int timeCreatedSSE = 0;
		public bool freePnClicked = false;
		public bool offlineOnScheduled = false;
	}

	public NotificationManager()
	{
		freePnClicked = PlayerPrefs.GetInt(FREE_SONG_PN_CLICKED) > 0;
	}

	private PNDB CreatePNDB(int secondsSinceEpoch)
	{
		var pndb = new PNDB();

		// The first notification is for full life event
		// other notifications are for remiders in next 7 days
		for (int i = 0; i < 4; i++)
		{
			pndb.pns.Add(new PN(i + 1));
		}
		pndb.timeCreatedSSE = secondsSinceEpoch;
		return pndb;
	}

	public void Reschedule()
	{
	}

	public void OnAppPause()
	{
	}

	public bool OnAppResume()
	{
        return false;
	}

	public int CheckPnClicked()
	{
#if UNITY_EDITOR
		int clickedPnId = 0;
#elif UNITY_ANDROID
		AndroidJavaObject activity =
			new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");

		int clickedPnId = activity.Call<int>("getClickedPnId");
		activity.Call("clearClickedPnId");

		if (clickedPnId <= 0)
		{
			return clickedPnId;
		}

		if (clickedPnId == FREE_SONG_PN_ID)
		{
			freePnClicked = true;
			PlayerPrefs.SetInt(FREE_SONG_PN_CLICKED, 1);
		}

#if SEND_PN_CLICK_EVENT
		AnalyticsHelper.Instance.LogLocalPNClicked(clickedPnId);
#endif
		// We shouldn't show dialogs in gameplay
		if (SceneManager.Instance.CurrentScene == ProjectConstants.Scenes.MainGame ||
			SceneManager.Instance.CurrentScene == ProjectConstants.Scenes.OnlineMainGame)
		{
			return clickedPnId;
		}
#elif UNITY_IPHONE
		int clickedPnId = 0;
#endif

		if (clickedPnId == FREE_SONG_PN_ID)
		{
			DailyRewardManager.Instance.ResetFreeSongLimitedTime ();
			if (DailyRewardManager.Instance.FreeSongLimitedTime != null)
			{
				SceneManager.Instance.OpenPopup (ProjectConstants.Scenes.DailyFreeSongPopup);
			}
		}
		return clickedPnId;
	}


	public void Schedule (int id, string title, string message, int secondsFromNow)
	{
#if UNITY_EDITOR
#elif UNITY_ANDROID
		LocalNotificationAndroid.SendNotification(
			id,
			secondsFromNow,
			title,
			message,
			new Color32(0xff, 0x44, 0x44, 255),
			true,
			true,
			true,
			@"drawable/notify_icon_small",
			LocalNotificationAndroid.ExecuteMode.ExactAndAllowWhileIdle);
#endif
	}


	public void ScheduleRepeating (int id, string title, string message, int secondsFromNow, int intervalInSeconds)
	{
#if UNITY_EDITOR
#elif UNITY_ANDROID
		LocalNotificationAndroid.SendRepeatingNotification(
			id,
			secondsFromNow,
			intervalInSeconds,
			title,
			message,
			new Color32(0xff, 0x44, 0x44, 255),
			true,
			true,
			true,
			@"drawable/notify_icon_small");
#endif
	}
}
