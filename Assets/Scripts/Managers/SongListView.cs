using UnityEngine;
using System.Collections.Generic;
using Amanotes.Utils;

namespace Amanotes.PianoChallenge
{
	public class SongListView : MonoBehaviour
	{
		public SongListType type;
		[SerializeField] GameObject lblMessage = null;
		[SerializeField] UIWrapContent itemScrollList = null;
		[SerializeField] NativeAdView nativeAdPrefab = null;
		[SerializeField] CrossPromotionItemView xPromoPrefab = null;
		[SerializeField] GameObject padding = null;

		[SerializeField] Camera cameraForImpesstionNativeAds = null;

		private List<SongDataModel> songs;

		NativeAdView nativeAdView = null;
		CrossPromotionItemView xPromoView = null;

		private bool isInitialized = false;
		void Start()
		{
			Initialize();
		}

		void OnEnable()
		{
			switch(type)
			{
				case SongListType.Normal:
					AnalyticsHelper.Instance.LogOpenScene(AnalyticsHelper.SONG_LIST_NORMAL);
					break;

				case SongListType.Premium:
					AnalyticsHelper.Instance.LogOpenScene(AnalyticsHelper.SONG_LIST_PREMIUM);
					break;

				case SongListType.Favorite:
					break;
			}
			SongManager.Instance.LoadingSong = false;
		}

		public void Initialize()
		{
			if (isInitialized)
			{
				return;
			}

			itemScrollList.onInitializeItem += OnSongItemNeedInitialized;

			for (int i = 0; i < itemScrollList.transform.childCount; i++)
			{
				var item = itemScrollList.transform.GetChild(i).GetComponent<SongItemView>();
				item.OnSongSelected += OnSongSelected;
				item.OnTryToBuySong += OnTryToBuySong;
				item.OnFavoriteSong += OnFavoriteSong;
				item.OnShowRanking += OnShowSongRanking;
			}

			isInitialized = true;
		}

		private void OnTryToBuySong(SongItemView songView)
		{
			SongManager.Instance.TryToBuy(songView.Song, AnalyticsHelper.SONG_LIST_PREMIUM, () => {
				songView.Refresh();
				if (songView.Song.pricePrimary < 0)
				{
					GameManager.Instance.SessionData.endless = false;
					SongManager.Instance.TryToPlay(songView.Song, AnalyticsHelper.SONG_LIST_PREMIUM);
				}
			});
		}

		private void OnFavoriteSong(SongItemView songView, bool value)
		{
			SongManager.Instance.SetFavorite(songView.Song, value);
		}

		private void OnSongItemNeedInitialized(GameObject gameObj, int wrapIndex, int realIndex)
		{
			// Spawn the "More songs coming soon" text
			if (realIndex == itemScrollList.minIndex)
			{
				padding.gameObject.SetActive(true);
				padding.transform.SetParent(gameObj.transform);
				padding.transform.localPosition = Vector3.zero;
				gameObj.transform.GetChild(0).gameObject.SetActive(false);
				gameObj.SetActive(true);
				return;
			}

			int index = Mathf.Abs(realIndex);
			//if (index == GameManager.Instance.GameConfigs.indexNativeAds[0] && type == SongListType.Normal)
			//{
			//	if (nativeAdView == null)
			//	{
			//		nativeAdView = nativeAdPrefab.Spawn();
			//		nativeAdView.LoadNativeAd(cameraForImpesstionNativeAds);
			//	}
			//	nativeAdView.gameObject.SetActive(true);
			//	nativeAdView.transform.SetParent(gameObj.transform);
			//	nativeAdView.transform.localPosition = Vector3.zero;
			//	nativeAdView.transform.localScale = Vector3.one;
			//	gameObj.transform.GetChild(0).gameObject.SetActive(false);
			//	gameObj.SetActive(true);
			//	return;
			//}

			//else if (index == GameManager.Instance.GameConfigs.indexNativeAds[1] && type == SongListType.Normal)
			//{
			//	if (xPromoView == null)
			//	{
			//		xPromoView = xPromoPrefab.Spawn();
			//	}
			//	xPromoView.transform.SetParent(gameObj.transform);
			//	xPromoView.transform.localPosition = Vector3.zero;
			//	xPromoView.transform.localScale = Vector3.one;
			//	xPromoView.Randomize();
			//	xPromoView.gameObject.SetActive(true);
			//	gameObj.transform.GetChild(0).gameObject.SetActive(false);
			//	gameObj.SetActive(true);
			//	return;
			//}

			var item = gameObj.GetComponent<SongItemView>();
			//if (index > GameManager.Instance.GameConfigs.indexNativeAds[0] && type == SongListType.Normal)
			//{
			//	index--;
			//}
			//if (index > GameManager.Instance.GameConfigs.indexNativeAds[1] && type == SongListType.Normal)
			//{
			//	index--;
			//}
			if (index >= songs.Count)
			{
				gameObj.SetActive(false);
				return;
			}
			else
			{
				gameObj.SetActive(true);
			}
			item.Set(index + 1, songs[index]);

			gameObj.transform.GetChild(0).gameObject.SetActive(true);
			for (int i = 1; i < gameObj.transform.childCount; i++)
			{
				gameObj.transform.GetChild(i).gameObject.SetActive(false);
			}
		}

		public void Refresh()
		{
			Initialize();

			var storeData = GameManager.Instance.StoreData;

			int nItems = 0;
			switch (type)
			{
				case SongListType.Normal:
					songs = storeData.listNormalSongs;
					int nNativeAds = 0;
					int nCrossPromos = 0;
					int crossCount = nCrossPromos > 0 ? nNativeAds : nNativeAds - 1;
					nItems = songs.Count + crossCount + 1;
					break;

				case SongListType.Premium:
					songs = storeData.listPremiumSongs;
					nItems = songs.Count + 1;
					break;

				case SongListType.Favorite:
					songs = storeData.GetSongDataModelByListStoreId(ProfileHelper.Instance.ListFavoriteSongs);
					nItems = songs.Count + 1;

					if (lblMessage != null)
					{
						lblMessage.SetActive(songs == null || songs.Count <= 0);
					}
					break;
			}

			itemScrollList.maxIndex = 0;
			itemScrollList.minIndex = -nItems + 1;
			RefreshItems();
		}

		public void RefreshItems()
		{
			if (type == SongListType.Favorite)
			{
				itemScrollList.ResetChildrenPositions();
				gameObject.GetComponent<UIScrollView>().ResetPosition();
			}
			else
			{
				for (int i = 0; i < itemScrollList.transform.childCount; i++)
				{
					var child = itemScrollList.transform.GetChild(i);
					var item = child.GetComponent<SongItemView>();
					if (i < songs.Count && child.GetChild(0).gameObject.activeSelf)
					{
						if (item.Song != null)
						{
							item.Refresh();
						}
					}
				}
			}
		}

		private void OnSongSelected(SongItemView songView)
		{
			SongManager.Instance.TryToPlay(songView.Song, GroupHomeSceneType.SongList.ToString());
		}

		private void OnShowSongRanking(SongDataModel song) {
			SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.SongLeaderboardPopup, song);
		}
	}

	public enum SongListType
	{
		Normal,
		Premium,
		Favorite
	}
}
