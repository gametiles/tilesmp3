using Amanotes.Utils;
using ProjectConstants;
using System;
using UnityEngine;
using System.Collections.Generic;
using Amanotes.Utils.MessageBus;

namespace Amanotes.PianoChallenge
{
	public class DailyRewardManager
	{
		const string DAILY_REWARD_TAG = "daily";
		const string KEY_DAILY_REWARD = "DailyReward";
		const string FREE_SONG_LIMITED_TIME = "Free song limited time";
		const string FIRST_LAUNCH_TIME = "First Launch Time";

		static private DailyRewardManager instance = null;

		static public DailyRewardManager Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new DailyRewardManager();
				}
				return instance;
			}
		}

		private int diamondsToday = 0;
		private int livesToday = 0;
		private int diamondsTomorrow = 0;
		private int livesTomorrow = 0;
		private int dayIndex = 0;
		private bool claimable = false;
		private bool firstTime = false;
		private bool initialized = false;

		public int DiamondsToday { get { return diamondsToday; } }
		public int LivesToday { get { return livesToday; } }
		public int DiamondsTomorrow { get { return diamondsTomorrow; } }
		public int LivesTomorrow { get { return livesTomorrow; } }
		public int DayIndex { get { return dayIndex; } }
		public bool Claimable { get { return claimable; } }
		public bool IsFirstTime
		{
			set
			{
				firstTime = value;
			}
		}
		public bool Initialized { get { return initialized; } }

		DateTime firstLaunchTime;
		public DateTime FirstLaunchTime { get { return firstLaunchTime; } }

		DailyRewardManager()
		{
			string s = PlayerPrefs.GetString(FIRST_LAUNCH_TIME);
			if (string.IsNullOrEmpty(s))
			{
				firstLaunchTime = DateTime.Now;
				PlayerPrefs.SetString(FIRST_LAUNCH_TIME, Convert.ToString(firstLaunchTime));
			}
			else
			{
				firstLaunchTime = Convert.ToDateTime(s);
				if (firstLaunchTime.Year < 2017)
				{
					firstLaunchTime = DateTime.Now;
					PlayerPrefs.SetString(FIRST_LAUNCH_TIME, Convert.ToString(firstLaunchTime));
				}
			}

			LoadDailyReward();
			LoadFreeSongLimitedTime();
		}

		public void Init()
		{
			initialized = true;
		}

		public void OnAppResume()
		{
			LoadDailyReward();
		}

		public void OnAppPause()
		{
			
		}

		private string SaveDailyReward()
		{
			string s = Convert.ToString(DateTime.Now) + "_" + dayIndex;
			PlayerPrefs.SetString(KEY_DAILY_REWARD, s);
			PlayerPrefs.Save();
			return s;
		}

		private void UpdateRewards()
		{
			if (GameManager.Instance.GameConfigs == null)
			{
				return;
			}

			var diamondRewards = GameManager.Instance.GameConfigs.dailyrewardDiamonds;
			diamondsToday = 0;
			diamondsTomorrow = 0;
			if (diamondRewards != null)
			{
				diamondsToday = diamondRewards[dayIndex];
				if (dayIndex + 1 < diamondRewards.Count)
				{
					diamondsTomorrow = diamondRewards [dayIndex + 1];
				}
			}

			var lifeRewards = GameManager.Instance.GameConfigs.dailyrewardLives;
			livesToday = 0;
			livesTomorrow = 0;
			if (lifeRewards != null)
			{
				livesToday = lifeRewards[dayIndex];
				if (dayIndex + 1 < lifeRewards.Count)
				{
					livesTomorrow = lifeRewards[dayIndex + 1];
				}
			}
		}

		private void Reset()
		{
			claimable = true;
			dayIndex = 0;

			UpdateRewards();
		}

		private void LoadDailyReward()
		{
			string save = PlayerPrefs.GetString(KEY_DAILY_REWARD);
			string[] ss = string.IsNullOrEmpty(save) ? null : save.Split('_');

			if (ss == null || ss.Length < 2)
			{
				Reset();
				return;
			}
			else
			{
				DateTime oldDate = Convert.ToDateTime(ss[0]);

				int difference = DateTime.Now.DayOfYear - oldDate.DayOfYear;
				if (difference < 0)
				{
					Development.Log("[Daily discount] Hack detected. Congratulation ;). Enjoy your gift.");
					Reset();
					claimable = true;
					return;
				}
				else
				{
					claimable = difference > 0;
					dayIndex = difference == 1 ? int.Parse(ss[1]) + 1 : 0;

					if (GameManager.Instance.GameConfigs == null ||
						GameManager.Instance.GameConfigs.dailyrewardDiamonds == null)
					{
						claimable = false;
						dayIndex = 0;
					}
					else if (dayIndex >= GameManager.Instance.GameConfigs.dailyrewardDiamonds.Count)
					{ // When all daily rewards have been claimed
						// Reset
						dayIndex = 0;
					}
				}
			}

			UpdateRewards();
		}

		public void CheckFirstReward()
		{
			if (!firstTime)
			{
				return;
			}

			firstTime = false;
			CheckToday();
		}

		public void CheckToday()
		{
			if (!claimable)
			{
				return;
			}

			if (diamondsToday == 0)
			{
				UpdateRewards();
			}
			//SceneManager.Instance.OpenPopup(Scenes.DailyRewardPopUp);

			// reset daily achievements
			AchievementHelper.Instance.ResetAchievementsWithTag(DAILY_REWARD_TAG);
		}

		public void ClaimToday()
		{
			if (!claimable)
			{
				return;
			}

			claimable = false;

			ProfileHelper.Instance.CurrentDiamond += diamondsToday;
			ProfileHelper.Instance.CurrentLife += livesToday;
			string s = SaveDailyReward();
			AnalyticsHelper.Instance.LogClaimDailyReward(s);
		}

		public void CheckOffersToday()
		{
			CheckToday();
		}

		SongDataModel freeSongLimitedTime;
		DateTime freeSongStartTime;
		int freeSongSeconds;

		public SongDataModel FreeSongLimitedTime { get { return freeSongLimitedTime; } }
		public int FreeSongMaxSeconds { get { return freeSongSeconds; } }

		public int FreeSongSecondsRemaining
		{
			get
			{
				return freeSongSeconds - (int)(DateTime.Now - freeSongStartTime).TotalSeconds;
			}
		}

		private void LoadFreeSongLimitedTime()
		{
			if (GameManager.Instance.StoreData == null ||
				GameManager.Instance.StoreData.listPremiumSongs == null)
			{
				freeSongLimitedTime = null;
				Development.Log ("[Free song] Something went wrong");
				return;
			}

			string save = PlayerPrefs.GetString(FREE_SONG_LIMITED_TIME);
			string[] ss = string.IsNullOrEmpty(save) ? null : save.Split('_');

			if (ss == null || ss.Length < 2)
			{
				Development.Log ("[Free song] First free song");
				freeSongLimitedTime = null;
				return;
			}

			freeSongStartTime = Convert.ToDateTime(ss[0]);
			if (DateTime.Now.DayOfYear != freeSongStartTime.DayOfYear || DateTime.Now.Year != freeSongStartTime.Year)
			{
				Development.Log ("[Free song] A new day has come with another free song");
				freeSongLimitedTime = null;
				return;
			}

			int freeScondsConfig = GameManager.Instance.GameConfigs.freeSongHours * 60 * 60;
			freeSongSeconds = ss.Length > 2 ? int.Parse(ss[2]) : freeScondsConfig;
			if (freeSongSeconds <= 0)
			{
				freeSongSeconds = freeScondsConfig;
			}

			freeSongLimitedTime = SongManager.FindSong (ss [1], GameManager.Instance.StoreData.listPremiumSongs);

			if (SongManager.IsBought (freeSongLimitedTime))
			{ // Free song today is bought
				Development.Log ("[Free song] Free song today already bought");
				freeSongLimitedTime = null;
			}
		}

		public void ResetFreeSongLimitedTime()
		{
			if (freeSongLimitedTime != null)
			{
				int t = FreeSongSecondsRemaining;
				if (t > 0 && t < freeSongSeconds)
				{
					return;
				}
			}

			if (GameManager.Instance.StoreData == null ||
				GameManager.Instance.StoreData.listPremiumSongs == null ||
				GameManager.Instance.GameConfigs == null ||
				GameManager.Instance.GameConfigs.freeSongId == null)
			{
				Development.Log ("Something went wrong");
				freeSongLimitedTime = null;
				return;
			}

			freeSongLimitedTime = SongManager.FindSong (
				GameManager.Instance.GameConfigs.freeSongId,
				GameManager.Instance.StoreData.listPremiumSongs);
			if (freeSongLimitedTime == null)
			{
				return;
			}

			freeSongSeconds = GameManager.Instance.GameConfigs.freeSongHours * 60 * 60;
			freeSongStartTime = DateTime.Now;

			string save =
				Convert.ToString(freeSongStartTime) + "_" +
				freeSongLimitedTime.storeID + "_" +
				freeSongSeconds;

			PlayerPrefs.SetString(FREE_SONG_LIMITED_TIME, save);
			PlayerPrefs.Save();
		}
	}
}
