using Parse;
using System.Collections.Generic;
using Facebook.Unity;
using Amanotes.Utils;

namespace Amanotes.PianoChallenge
{
	public class SongLeaderboardManager : SingletonMono<SongLeaderboardManager>
	{
		public static SongRecordComparator recordComparator = new SongRecordComparator();
		SongLeaderboardListFriend friendLeaderboards = null;
		SongLeaderboardListGlobal globalLeaderboards = null;

		private bool scorePushed = false;

		public bool ScorePushed { get { return scorePushed; } }

		public SongLeaderboardListFriend FriendLeaderboards { get { return friendLeaderboards; } }
		public SongLeaderboardListGlobal GlobalLeaderboards { get { return globalLeaderboards; } }

		private void Awake()
		{
			Initialize();
		}

		private void Start()
		{
			Initialize();
		}

		public void Initialize()
		{
			if (friendLeaderboards == null)
			{
				friendLeaderboards = new SongLeaderboardListFriend();
			}

			if (globalLeaderboards == null)
			{
				globalLeaderboards = new SongLeaderboardListGlobal();
			}
		}

		/// <summary>
		/// Update local current ranking for this user after playing any level/song
		/// </summary>
		/// <param name="pModel"></param>
		public void UpdateMySongRecord(string songId, int crowns, int stars, int score)
		{
			friendLeaderboards.UpdateMySongRecord(songId, crowns, stars, score);
			globalLeaderboards.UpdateMySongRecord(songId, crowns, stars, score);

			if (ParseUser.CurrentUser == null || AccessToken.CurrentAccessToken == null)
				return;

			scorePushed = false;

			var parameters = new Dictionary<string, object> {
				{ "userId", AccessToken.CurrentAccessToken.UserId },
				{ "userName", FacebookManager.Instance.UserDisplayName },
				{ "songId", songId },
				{ "crowns", crowns },
				{ "stars", stars },
				{ "score", score },
			};

            fModStudio.Utils.CallParseFunction("UpdateSongRecord", parameters, task => {
				scorePushed = true;
			});
		}

		public void OnUserDataChanged()
		{
			friendLeaderboards.OnUserDataChanged();
			globalLeaderboards.OnUserDataChanged();
		}

		public void RequestFriendRecords()
		{
			friendLeaderboards.RequestFriendRecords();
		}
	}
}
