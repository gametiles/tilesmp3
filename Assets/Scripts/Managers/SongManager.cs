
using UnityEngine;
using Amanotes.Utils;
using Amanotes.PianoChallenge;
using System.Collections.Generic;
using ProjectConstants;

class SongManager
{
	static private SongManager instance = null;

	public bool LoadingSong = false;

	// Premium songs unlock for level 2, 3 and 4
	const string UNLOCKED_PREMIUM_SONG_STATUS = "Unlocked Premium Song Status";
	const string PREMIUM_SONG_PLAYED = "Premium song played";
	const string GAME_RATING = "Rating of this app";
	const string FIRST_SONG = "First Song ID";

	Dictionary<string, int> unlockedPremiumSongs;
	int totalPremiumTries = 0;

	bool premiumSongPlayed = false;
	int gameRating = 0;

	static public SongManager Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new SongManager();
			}
			return instance;
		}
	}

	public int TotalPremiumTries { get { return totalPremiumTries; } }

	public bool PremiumSongPlayed { get { return premiumSongPlayed; } }
	public int GameRating
	{
		get { return gameRating; }
		set
		{
			gameRating = value;
			PlayerPrefs.SetInt(GAME_RATING, value);
		}
	}

	// Check the number of times remaining to play a premium song, return 0 if it's not unlocked yet
	public int PlayTimesRemaining(SongDataModel song)
	{
		int premiumTries;
		if (unlockedPremiumSongs.TryGetValue(song.storeID, out premiumTries))
		{
			if (premiumTries >= 0)
			{
				return premiumTries;
			}
		}
		return 0;
	}

	public bool TryToBuy(SongDataModel song, string place, System.Action onUnlocked = null)
	{
		AnalyticsHelper.Instance.LogBuyPremiumSongClicked(place);

		// Unlock this song by watching ad
		if (song.pricePrimary < 0) 
		{
			AnalyticsHelper.Instance.LogRewardVideoButtonClicked(place, song.storeID);
			AdHelper.Instance.ShowRewardVideo(RewardType.Custom, ()=>
			{
				UnlockPremiumSong(song, -song.pricePrimary);
				LivesManager.Instance.SetNextPlayFree();

				Helpers.Callback(onUnlocked);
			});
			return false;
		}

		// Unlock this song by paying with diamonds
		int price = song.pricePrimary;

		if (ProfileHelper.Instance.CurrentDiamond >= price)
		{
			ProfileHelper.Instance.ListBoughtSongs.Add(song.storeID);
			ProfileHelper.Instance.CurrentDiamond -= price;
			AnalyticsHelper.Instance.LogBuyInGameItem("Song" + song.storeID, 1, song.pricePrimary);
			AchievementHelper.Instance.LogAchievement("numSongPlayed");
			LevelUpProgress.Instance.OnSongPurchased(price);
			AnalyticsHelper.Instance.LogPremiumSongPurchased(song.name, place);

			LivesManager.Instance.SetNextPlayFree();
			Helpers.Callback(onUnlocked);
			return true;
		}

		// Player doesn't have enough diamonds, redirect them to the IAP shop
		AnalyticsHelper.Instance.LogIapOpen(AnalyticsHelper.IAP_TO_BUY_SONG);
		SceneManager.Instance.OpenPopup(Scenes.IAP);
		return false;
	}

	public void SetFavorite(SongDataModel song, bool value)
	{
		if (value)
		{
			if (!ProfileHelper.Instance.ListFavoriteSongs.Contains(song.storeID))
			{
				ProfileHelper.Instance.ListFavoriteSongs.Add(song.storeID);
				ProfileHelper.Instance.SaveLocalUserData();

				int nFavoriteNow = ProfileHelper.Instance.ListFavoriteSongs.Count;
				int nFavoriteRate = GameManager.Instance.GameConfigs.favoriteCountToShowRatePopup;
				if (nFavoriteNow >= nFavoriteRate && gameRating == 0)
				{
					SceneManager.Instance.OpenPopup(Scenes.RatePopupNew);
				}
			}
		}
		else
		{
			if (ProfileHelper.Instance.ListFavoriteSongs.Contains(song.storeID))
			{
				ProfileHelper.Instance.ListFavoriteSongs.Remove(song.storeID);
				ProfileHelper.Instance.SaveLocalUserData();
			}
		}
	}


	public void TryToPlay(SongDataModel song, string place, string param = null)
	{
		if (!LivesManager.Instance.CanPlaySong())
		{
			//LivesManager.Instance.SuggestRechargeLives();
			//return;
		}

		if (LoadingSong)
		{
			return;
		}

		LoadingSong = true;

		GameManager.Instance.SessionData.song = song;
		AnalyticsHelper.Instance.LogSinglePlayClicked(place);
		SceneManager.Instance.OpenScene(Scenes.PSGameplay,song);
	}

	public static bool IsBought(string songId)
	{
		if (ProfileHelper.Instance.ListBoughtSongs == null)
		{
			return false;
		}

		for (int i = 0; i < ProfileHelper.Instance.ListBoughtSongs.Count; i++)
		{
			if (ProfileHelper.Instance.ListBoughtSongs[i].CompareTo(songId) == 0)
			{
				return true;
			}
		}
		return false;
	}

	public static bool IsBought(SongDataModel song)
	{
		return song != null && IsBought(song.storeID);
	}

	public bool IsFavorite(SongDataModel song)
	{
		if (ProfileHelper.Instance.ListFavoriteSongs == null)
		{
			return false;
		}

		for (int i = 0; i < ProfileHelper.Instance.ListFavoriteSongs.Count; i++)
		{
			if (ProfileHelper.Instance.ListFavoriteSongs[i].CompareTo(song.storeID) == 0)
			{
				return true;
			}
		}

		return false;
	}

	// Handle level up event and return the premium song unlocked for this level
	public SongDataModel OnLevelUp(int level)
	{
		int index = level - 2;
		int max = GameManager.Instance.GameConfigs.nLevelUpPremiumSongs - 1;
		var premiumSongs = GameManager.Instance.StoreData.listPremiumSongs;

		if (index < 0 || index > max || premiumSongs == null || !(index < premiumSongs.Count))
		{
			return null;
		}

		SongDataModel song = premiumSongs[index];
		UnlockPremiumSong(song, GameManager.Instance.GameConfigs.nLevelUpPremiumTries);
		return song;
	}

	public void MarkPremiumSongPlayed()
	{
		if (!premiumSongPlayed)
		{
			premiumSongPlayed = true;
			PlayerPrefs.SetInt(PREMIUM_SONG_PLAYED, 1);
		}
	}

	public void OnPremiumSongPlayed(SongDataModel song, int crowns)
	{
		int premiumTries;
		if (unlockedPremiumSongs.TryGetValue(song.storeID, out premiumTries))
		{
			totalPremiumTries--;
			if (crowns >= GameManager.Instance.GameConfigs.crownsToUnlockPremium)
			{
				ProfileHelper.Instance.ListBoughtSongs.Add(song.storeID);
				unlockedPremiumSongs.Remove(song.storeID);
			}
			else
			{
				unlockedPremiumSongs[song.storeID] = premiumTries - 1;
			}
		}

		MarkPremiumSongPlayed();

		Save();
	}

	public static SongDataModel FindSong(string songId, List<SongDataModel> list)
	{
		foreach (var song in list)
		{
			if (song.storeID == songId)
			{
				return song;
			}
		}
		return null;
	}

	private SongManager()
	{
		Load();
	}

	private void UnlockPremiumSong(SongDataModel song, int nTries)
	{
		if (unlockedPremiumSongs == null)
		{
			unlockedPremiumSongs = new Dictionary<string, int>();
		}

		int premiumTries;
		if (unlockedPremiumSongs.TryGetValue(song.storeID, out premiumTries))
		{ // Add tries to the already unlocked song
			unlockedPremiumSongs[song.storeID] = premiumTries + nTries;
		}
		else
		{ // New song unlocked
			unlockedPremiumSongs.Add(song.storeID, nTries);
		}

		totalPremiumTries += nTries;
		AdHelper.Instance.SkipNextInterstitialTrigger();
		Save();
	}

	private void Save()
	{
		if (unlockedPremiumSongs != null)
		{
			string s = string.Empty;
			foreach (var key in unlockedPremiumSongs.Keys)
			{
				s += key + " " + unlockedPremiumSongs[key] + " ";
			}

			PlayerPrefs.SetString(UNLOCKED_PREMIUM_SONG_STATUS, s);
		}
	}

	private void Load()
	{
		totalPremiumTries = 0;
		string s = PlayerPrefs.GetString(UNLOCKED_PREMIUM_SONG_STATUS);
		string[] ss = s.Split(' ');

		unlockedPremiumSongs = new Dictionary<string, int>();

		for (int i = 0; ss.Length >= i * 2 + 2; i++)
		{
			string songId = ss[i * 2];
			int nTries = int.Parse(ss[i * 2 + 1]);
			totalPremiumTries += nTries;
			unlockedPremiumSongs.Add(songId, nTries);
		}

		premiumSongPlayed = PlayerPrefs.GetInt(PREMIUM_SONG_PLAYED) > 0;
		gameRating = PlayerPrefs.GetInt(GAME_RATING);
	}

	public void LoadFirstSong()
	{
		var songId = PlayerPrefs.GetString(FIRST_SONG);
		var song = FindSong(songId, GameManager.Instance.StoreData.listAllSongs);
		if (song != null)
		{
			MakeFirstSongFree(song);
		}
	}

	public void MakeFirstSongFree(SongDataModel song)
	{
		song.pricePrimary = 0;
		song.LvToUnlock = 0;
		if (GameManager.Instance.StoreData.listPremiumSongs.Contains(song))
		{
			GameManager.Instance.StoreData.listPremiumSongs.Remove(song);
		}

		if (GameManager.Instance.StoreData.listNormalSongs.Contains(song))
		{
			GameManager.Instance.StoreData.listNormalSongs.Remove(song);
		}

		GameManager.Instance.StoreData.listNormalSongs.Insert(0, song);
	}

	public void PlayFirstSong(SongDataModel song)
	{
		PlayerPrefs.SetString(FIRST_SONG, song.storeID);
		MakeFirstSongFree(song);
		TryToPlay(song, Scenes.OnBoarding.ToString());
	}
}
