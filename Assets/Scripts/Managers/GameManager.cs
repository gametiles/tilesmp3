using UnityEngine;
using System;
using System.Collections.Generic;
using FullSerializer;

namespace Amanotes.PianoChallenge
{
	/// <summary>
	/// Responsible for managing the game's state
	/// </summary>
	public class GameManager : SingletonMono<GameManager>
	{
		public static int BUILTIN_CONFIG = 461;
		public static int BUILTIN_STORE = 415;
		public static int BUILTIN_ACHIEVEMENT = 391;
		public static int BUILTIN_LOCALIZATION = 413;
		public static int BUILTIN_LEVEL_CONFIG = 16;

		#region Const variables
		private const string GAME_CONFIG_FILE = "34658328956";
		private const string STORE_DATA_FILE = "9867934593324";
		private const string GAME_VERSION_DATA_FILE = "54320958_20170630";
		private const string GAME_STATE_DATA_FILE = "42364237648965435";
		private const string ACHIEVEMENT_DATA_FILE = "395867345";
		private const string ACHIEVEMENT_PROPERTY_DATA_FILE = "1334465436";
		public const string ENCRYPTION_KEY = "sowhatthehellareyouwaitingfor";
		#endregion

		public LocalParserInitialize localParse;

		#region Public properties
		public bool printDebug = true;

		public bool sessionLevel = false;

		private bool isGamePlay = false;

		[Header("Game's configuration URL")]
		[SerializeField] string configURL = null;

		[Header("Game's versions' data URL")]
		[SerializeField] string gameVersionURL = null;

		private GameConfigModel configs;
		public GameConfigModel GameConfigs { get { return configs; } }

		public ConfigUserLevel configUserLevel = null;

		private SessionDataModel sessionData = new SessionDataModel();
		public SessionDataModel SessionData { get { return sessionData; } }

		//Store data
		private StoreDataModel storeData;
		public StoreDataModel StoreData { get { return storeData; } }

		private GameVersionModel versionsData;
		public GameVersionModel VersionsData { get { return versionsData; } }

		private string deviceID;
		private bool deviceIDFetched = false;
		public string DeviceID
		{
			get
			{
				if (deviceIDFetched)
				{
					return deviceID;
				}
				else
				{
					deviceIDFetched = true;
#if UNITY_ANDROID && !UNITY_EDITOR
					AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
					AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject>("currentActivity");
					AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");
					AndroidJavaClass secure = new AndroidJavaClass("android.provider.Settings$Secure");
					deviceID = secure.CallStatic<string>("getString", contentResolver, "android_id");
#else
					deviceID = SystemInfo.deviceUniqueIdentifier;
#endif
					return deviceID;
				}
			}
		}

		private GameDataModel gameData;
		public GameDataModel GameData { get { return gameData; } }

		//screen
		Option _option;
		public Option option
		{
			get
			{
				if (_option == null)
					_option = new Option();

				return _option;
			}
		}

		#endregion

		private string storedataWritePath, gamestateWritePath, gameconfigWritePath, gameversionWritePath;
		private string achievementPropertiesWritePath, achievementDataWritePath;

		bool isFirstTime = false;

		//the csv text of achievement properties and data of achievements
		void Awake()
		{
			isFirstTime = PlayerPrefs.GetInt(GameConsts.OnBoarding, -1) == -1;

			//generate save, load path to reduce string generation
			storedataWritePath = FileUtilities.GetWritablePath(STORE_DATA_FILE);
			gameconfigWritePath = FileUtilities.GetWritablePath(GAME_CONFIG_FILE);
			gamestateWritePath = FileUtilities.GetWritablePath(GAME_STATE_DATA_FILE);
			gameversionWritePath = FileUtilities.GetWritablePath(GAME_VERSION_DATA_FILE);
			achievementDataWritePath = FileUtilities.GetWritablePath(ACHIEVEMENT_DATA_FILE);
			achievementPropertiesWritePath = FileUtilities.GetWritablePath(ACHIEVEMENT_PROPERTY_DATA_FILE);

			Application.targetFrameRate = 60;
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
		}

		void Start()
		{
			if (PlayerPrefs.GetInt("SettingEffect", 0) == 1)
			{
				SettingEffect();
			}
		}

#if UNITY_EDITOR && false
		void OnApplicationFocus(bool hasFocus)
		{
			bool isPaused = !hasFocus;
#else
		void OnApplicationPause(bool isPaused)
		{
#endif
			if (!isPaused)
			{
				DailyRewardManager.Instance.OnAppResume();
				BattleManager.Instance.OnAppResume();
			}
			else
			{
			}
		}

		public void Initialize()
		{
			//well, this code is a hack, to reduce number of drawcall by 1 :v
			//remember, 1 camera = 1 drawcall
			var goSolidCam = GameObject.Find("SolidCamera");
			if (goSolidCam != null)
			{
				var solidCam = goSolidCam.GetComponent<Camera>();
				if (solidCam != null)
				{
					solidCam.clearFlags = CameraClearFlags.Nothing;
				}
			}
		}

		#region Save / Load game's data
		public void SaveStoreData()
		{
			FileUtilities.SerializeObjectToFile(storeData, storedataWritePath, ENCRYPTION_KEY, true);
		}

		public void SaveGameConfigData()
		{
			FileUtilities.SerializeObjectToFile(configs, gameconfigWritePath, ENCRYPTION_KEY, true);
		}

		public void LoadGameConfigDataFromLocal()
		{
			configs = FileUtilities.DeserializeObjectFromFile<GameConfigModel>(gameconfigWritePath, ENCRYPTION_KEY, true);
			if (configs == null)
			{
				configs = new GameConfigModel();
				configs.configVersion = BUILTIN_CONFIG;
			}
		}

		/// <summary>
		/// Save current game's state into local storage. Such as played level, 
		/// </summary>
		public void SaveGameData()
		{
			FileUtilities.SerializeObjectToFile(gameData, gamestateWritePath, ENCRYPTION_KEY, true);
		}

		public void LoadLocalGameData()
		{
			gameData = FileUtilities.DeserializeObjectFromFile<GameDataModel>(gamestateWritePath, ENCRYPTION_KEY, true);
			if (gameData == null)
			{
				this.Print("No local game data found, creating new one...");
				gameData = new GameDataModel();
				gameData.numStart = 0;
			}
			GameData.numStart += 1;
		}

		private void SaveGameVersionsData()
		{
			FileUtilities.SerializeObjectToFile(versionsData, gameversionWritePath, ENCRYPTION_KEY, true);
		}

		internal void LoadLocalGameVersions()
		{
			versionsData = FileUtilities.DeserializeObjectFromFile<GameVersionModel>(gameversionWritePath, ENCRYPTION_KEY, true);

			if (versionsData == null)
			{
				versionsData = new GameVersionModel();
				versionsData.config = BUILTIN_CONFIG;
				versionsData.achievement = BUILTIN_ACHIEVEMENT;
				versionsData.localization = BUILTIN_LOCALIZATION;
				versionsData.store = BUILTIN_STORE;
				versionsData.configLevel = BUILTIN_LEVEL_CONFIG;
			}
			else
			{
				if (versionsData.config < BUILTIN_CONFIG)
				{
					versionsData.config = BUILTIN_CONFIG;
				}
				if (versionsData.achievement < BUILTIN_ACHIEVEMENT)
				{
					versionsData.achievement = BUILTIN_ACHIEVEMENT;
				}
				if (versionsData.localization < BUILTIN_LOCALIZATION)
				{
					versionsData.localization = BUILTIN_LOCALIZATION;
				}
				if (versionsData.store < BUILTIN_STORE)
				{
					versionsData.store = BUILTIN_STORE;
				}
				if (versionsData.configLevel < BUILTIN_LEVEL_CONFIG)
				{
					versionsData.configLevel = BUILTIN_LEVEL_CONFIG;
				}
			}
		}

		/// <summary>
		/// Save current achievement model to hard disk
		/// </summary>
		private void SaveAchievementData(string propertyData, string achievementData)
		{
			if (!string.IsNullOrEmpty(propertyData))
			{
				FileUtilities.SaveFileWithPassword(propertyData, achievementPropertiesWritePath, ENCRYPTION_KEY, true);
			}

			if (!string.IsNullOrEmpty(achievementData))
			{
				FileUtilities.SaveFileWithPassword(achievementData, achievementDataWritePath, ENCRYPTION_KEY, true);
			}
		}
		#endregion

		#region Download data from internet

		byte[] LoadLocalizationResource()
		{
			TextAsset asset = Resources.Load<TextAsset>("Localization");
			if (asset != null)
			{
				return asset.bytes;
			}
			return null;
		}

		bool LoadBuiltinLocalization(Action<byte[]> onComplete)
		{
			var bytes = LoadLocalizationResource();
			if (bytes != null)
			{
				onComplete(bytes);
				return true;
			}
			return false;
		}

		internal void RefreshLocalizationData(Action<byte[]> onComplete = null, Action<string> onError = null)
		{
			if (BUILTIN_LOCALIZATION == VersionsData.localization && LoadBuiltinLocalization(onComplete))
			{
				return;
			}

			AssetDownloader.Instance.DownloadAndCacheAsset(GameConfigs.localizationURL, VersionsData.localization,
				(progress) =>
				{
					//TODO: Report download process here
				},
				(errorMessage) =>
				{
					if (LoadBuiltinLocalization(onComplete))
					{
						Development.Log("Failed to download game localization data from the Internet, using local one");
					}
					else
					{
						onError("Error trying to download game localization data: " + errorMessage);
					}
				},
				(downloadedData) =>
				{
					//try to parse latest config data
					if (downloadedData != null)
					{
						onComplete(downloadedData.bytes);
					}
					else if (LoadBuiltinLocalization(onComplete))
					{
						Development.Log("Failed to download game localization data from the Internet, using local one");
					}
					else
					{
						onError("Error trying to download game localization data");
					}
				});
		}

		bool LoadBuiltintAchievementData()
		{
			TextAsset achievementAsset = Resources.Load<TextAsset>("AchievementData");
			if (achievementAsset == null)
			{
				return false;
			}

			TextAsset propertyAsset = Resources.Load<TextAsset>("AchievementPropertyData");
			if (propertyAsset == null)
			{
				return false;
			}

			string propertyData = propertyAsset.text;
			string achievementData = achievementAsset.text;
			AchievementHelper.Instance.InitializeWithCSV(propertyData, achievementData);
			SaveAchievementData(propertyData, achievementData);

			return true;
		}

		void OnUnableToDownloadAchievementData(
			string propertyData,
			string achievementData,
			Action onComplete,
			Action<string> onError,
			string errorMessage1,
			string errorMessage2)
		{
			//but the game require new data to be downloaded
			if (string.IsNullOrEmpty(achievementData) || string.IsNullOrEmpty(propertyData))
			{
				if (LoadBuiltintAchievementData())
				{
					onComplete();
				}
				else
				{
					onError(errorMessage1);
				}
			}
			else
			{
				AchievementHelper.Instance.InitializeWithCSV(propertyData, achievementData);
				//there is no connection can be made, should report as error
				onComplete();
				this.Print(errorMessage2);
			}
		}

		internal void RefreshAchievementData(Action onComplete = null, Action<string> onError = null)
		{
			if (BUILTIN_ACHIEVEMENT == VersionsData.achievement)
			{
				if (LoadBuiltintAchievementData())
				{
					onComplete();
					return;
				}
			}

			string propertyData = FileUtilities.LoadFileWithPassword(achievementPropertiesWritePath, ENCRYPTION_KEY, true);
			string achievementData = FileUtilities.LoadFileWithPassword(achievementDataWritePath, ENCRYPTION_KEY, true);

			if (true)
			{
				OnUnableToDownloadAchievementData(
					achievementData, propertyData, onComplete, onError,
					"Fail to initialize game achievement properties",
					"Can not connect to the Internet, using local achievement properties");
				return;
			}

			AssetDownloader.Instance.DownloadAndCacheAsset(configs.achievementPropertiesURL, VersionsData.achievement,
				(progress) =>
				{
				},
				(errorMessage) =>
				{
					OnUnableToDownloadAchievementData(
						achievementData, propertyData, onComplete, onError,
						"Failed to download Achievement properties with error: " + errorMessage,
						"Failed to download new achievement properties from the Internet, using local one");
				},
				(www) =>
				{
					string properties = www.text;
					if (!string.IsNullOrEmpty(properties))
					{
						//Download Achievement data
						AssetDownloader.Instance.DownloadAndCacheAsset(configs.achievementURL, VersionsData.achievement,
							(progress) =>
							{
							},
							(errorMessage) =>
							{
								OnUnableToDownloadAchievementData(
									achievementData, propertyData, onComplete, onError,
									"Failed to download Achievement data with error: " + errorMessage,
									"Failed to download new achievement data from the Internet, using local one");
							},
							(www2) =>
							{
								string data = www2.text;
								if (!string.IsNullOrEmpty(data))
								{
									propertyData = properties;
									achievementData = data;

									AchievementHelper.Instance.InitializeWithCSV(propertyData, achievementData);
									//TODO: populate unlocked achievements here
									SaveAchievementData(propertyData, achievementData);
									Helpers.Callback(onComplete);
								}
							});
					}
				} );
		}

		internal void RefreshGameVersionsData(Action onComplete = null, Action<string> onError = null)
		{
            if (true)
            {
                Helpers.Callback(onComplete);
                return;
            }

			if (Application.internetReachability == NetworkReachability.NotReachable)
			{
				this.Print("Can not connect to the Internet, using local game version data");
				//there is no changes to the game config, inform listener about that
				Helpers.Callback(onComplete);
				return;
			}

			if (isFirstTime)
			{
				Helpers.Callback(onComplete);
			}

			AssetDownloader.Instance.DownloadAsset(gameVersionURL,
				(progress) =>
				{
					//TODO: Report download process here
				},
				(errorMessage) =>
				{
					this.Print("Failed to download game versions data from the Internet, using local one");
					if (!isFirstTime)
					{
						Helpers.Callback(onComplete);
					}
				},
				(downloadedData) =>
				{
					this.Print("Successfully download game versions from the internet");

					//try to parse latest config data
					string json = downloadedData.text;
					fsData jsonData = fsJsonParser.Parse(json);

					//then deserialize it for easier acccess
					GameVersionModel gameVersions = null;
					fsResult result = FileUtilities.JSONSerializer.TryDeserialize(jsonData, ref gameVersions);

					if (result.Failed || gameVersions == null)
					{
						this.Print("Failed to download game version data from the Internet, using local game version");
						if (!isFirstTime)
						{
							Helpers.Callback(onComplete);
						}
					}
					else
					{
						//we made it!!!!
						versionsData = gameVersions;
						SaveGameVersionsData();
						if (!isFirstTime)
						{
							Helpers.Callback(onComplete);
						}
					}
				});
		}

		public void RefreshLevelConfig(Action onComplete = null, Action<string> onError = null)
		{
			if (BUILTIN_LEVEL_CONFIG == VersionsData.configLevel)
			{
				TextAsset asset = Resources.Load<TextAsset>("LevelConfig");
				if (asset != null)
				{
					configUserLevel = new ConfigUserLevel();
					if (configUserLevel.AppendConfigFromData(asset.text))
					{
						onComplete();
						return;
					}
				}
			}

			AssetDownloader.Instance.DownloadAsset(GameConfigs.ConfigUserLevel_Link,
				(progress) =>
				{
					//TODO: Report download process here
				},
				(errorMessage) =>
				{
					onError(errorMessage);
				},
				(downloadedData) =>
				{
					//try to parse latest config data
					configUserLevel = new ConfigUserLevel();
					if (configUserLevel.AppendConfigFromData(downloadedData.text))
					{
						onComplete();
					}
					else
					{
						onError("Level data corrupted!");
					}
				});
		}


		/// <summary>
		/// Refresh latest game config data from server
		/// </summary>
		/// <param name="onComplete">Callback when the data has been loaded. True if there is any changes in game config</param>
		/// <param name="onError">Callback when there is error getting the file. With detail error message</param>
		public void RefreshGameConfigData(Action onComplete = null, Action<string> onError = null)
		{
			if (GameConfigs.configVersion == VersionsData.config)
			{
				Development.Log("No changed in game config version, skipping updates...");
				Helpers.Callback(onComplete);
				return;
			}

			if (configs.configVersion != BUILTIN_CONFIG)
			{
				configs = new GameConfigModel();
				configs.configVersion = BUILTIN_CONFIG;
				if (BUILTIN_CONFIG == VersionsData.config)
				{
					SaveGameConfigData();
					Helpers.Callback(onComplete);
					return;
				}
			}

			if (true)
			{
				//there is no changes to the game config, inform listener about that
				Helpers.Callback(onComplete);
				this.Print("Can not connect to the Internet, using local game config");
				return;
			}

			AssetDownloader.Instance.DownloadAndCacheAsset(configURL, VersionsData.config,
				(progress) =>
				{
					//TODO: Report download process here
				},
				(errorMessage) =>
				{
					this.Print("Failed to download game data from the Internet, using local game config");
					Helpers.Callback(onComplete);
				},
				(downloadedData) =>
				{
					this.Print("Successfully download game config");

					//try to parse latest config data
					string json = downloadedData.text;
					fsData jsonData = fsJsonParser.Parse(json);

					//then deserialize it for easier acccess
					GameConfigModel gameConfig = null;
					fsResult result = FileUtilities.JSONSerializer.TryDeserialize(jsonData, ref gameConfig);

					if (result.Failed || gameConfig == null)
					{
						this.Print("Failed to download game data from the Internet, using local game config");
						Helpers.Callback(onComplete);
					}
					else
					{
						//save new config version
						configs = gameConfig;
						GameConfigs.configVersion = VersionsData.config;
						SaveGameConfigData();
						this.Print("New game config data loaded, version: " + gameConfig.configVersion);
						Helpers.Callback(onComplete);
					}
				});
		}

		bool LoadBuiltinStoreData(string builtinStoreDataName)
		{
			storeData = new StoreDataModel();
			TextAsset asset = Resources.Load<TextAsset>(builtinStoreDataName);
			if (asset != null)
			{
				storeData = ParseStoreData(asset.text);
				return true;
			}
			return false;
		}

		void OnUnableToDownloadStoreData(
			bool needDownload,
			string builtinStoreDataName,
			Action onComplete,
			Action<string> onError,
			string errorMessage1,
			string errorMessage2)
		{
			if (needDownload && !LoadBuiltinStoreData(builtinStoreDataName))
			{
				onError(errorMessage1);
				return;
			}
			Debug.Log(errorMessage2);
			onComplete();
		}

		/// <summary>
		/// Refresh latest store data from server
		/// </summary>
		/// <param name="onComplete">Callback when the data has been loaded. True if there is any changes in store data</param>
		/// <param name="onError">Callback when there is error getting the file. With detail error message</param>
		public void RefreshStoreData(string builtinStoreDataName, Action onComplete = null, Action<string> onError = null)
		{
            //int currentVersion = AssetDownloader.Instance.GetAssetVersion(configs.storeDataURL);
            //if (currentVersion == VersionsData.store)
            //{
            //    storeData = FileUtilities.DeserializeObjectFromFile<StoreDataModel>(storedataWritePath, ENCRYPTION_KEY, true);
            //}
            //else if (VersionsData.store == BUILTIN_STORE)
            //{
            //    LoadBuiltinStoreData(builtinStoreDataName);
            //}
            //storeData = FileUtilities.DeserializeObjectFromFile<StoreDataModel>(storedataWritePath, ENCRYPTION_KEY, true);

            bool needDownload =
				storeData == null ||
				storeData.listAllSongs == null ||
				storeData.listAllSongs.Count <= 0;

			if (!needDownload)
			{
				this.Print("No changes to store data, skipping updates...");
				SaveStoreData();
				onComplete();
				return;
			}

			if (true)
			{
				OnUnableToDownloadStoreData(needDownload, builtinStoreDataName, onComplete, onError,
					"Fail to load store data file",
					"Can not connect to the Internet, using local game store data");
				return;
			}

			AssetDownloader.Instance.DownloadAndCacheAsset(configs.storeDataURL, VersionsData.store,
				(progress) =>
				{
					//TODO: Report download process here
				},
				(errorMessage) =>
				{
					OnUnableToDownloadStoreData(needDownload, builtinStoreDataName, onComplete, onError,
						"Error trying to download store data: " + errorMessage,
						"Failed to download game data from the Internet, using local game store data");
				},
				(downloadedData) =>
				{
					Debug.Log("Successfully download store data csv");

					//try to parse latest config data
					string csv = downloadedData.text;
					//then de-serialize it for easier access
					StoreDataModel downloadedStoreData = ParseStoreData(csv);

					if (downloadedStoreData == null)
					{
						OnUnableToDownloadStoreData(needDownload, builtinStoreDataName, onComplete, onError,
							"Could not de-serialize game data downloaded from server",
							"Failed to download game data from the Internet, using local store data");
					}
					else
					{
						//store data has been parsed successfully, 
						storeData = downloadedStoreData;
						storeData.version = VersionsData.store;
						SaveStoreData();
						Debug.Log("New game store data loaded, version: " + downloadedStoreData.version);
						onComplete();
					}
				});
		}

		private static StoreDataModel ParseStoreData(string csv)
		{
			if (string.IsNullOrEmpty(csv))
			{
				return null;
			}

			StoreDataModel storeData = new StoreDataModel();
			var songs = new List<SongDataModel>(100);
			var listHot = new List<SongDataModel>(10);
			var listNew = new List<SongDataModel>(10);
			var songsNormal = new List<SongDataModel>(100);
			var songsPremium = new List<SongDataModel>(100);
			Dictionary<string, int> columnName = new Dictionary<string, int>(10);
			CSVReader.LoadFromString(
				csv,
				//LineReader
				(lineIndex, content) =>
				{
					if (lineIndex == 0)
					{
						//parse header line, then, when parsing rows, instead of calling content[0], we can use content[columnName["ID"]]
						//for the sake of easy understanding
						for (int i = 0; i < content.Count; i++)
						{
							columnName.Add(content[i], i);
						}

						return;
					}

					if (content.Count != columnName.Count)
					{
						return;
					}

					//Column's name: ID,name,author,songURL,fileVersion,storeID,price,starsToUnlock,maxBPM,tickPerTile,speedModifier,speedPerStar,type
					SongDataModel song = new SongDataModel();
					song.ID = int.Parse(content[columnName["ID"]]);
					song.name = content[columnName["name"]];
					song.author = content[columnName["author"]];
					song.songURL = content[columnName["songURL"]];
					song.version = int.Parse(content[columnName["fileVersion"]]);
					song.storeID = content[columnName["storeID"]];
					song.pricePrimary = int.Parse(content[columnName["price"]]);
					song.starsToUnlock = int.Parse(content[columnName["starsToUnlock"]]);
					song.LvToUnlock = int.Parse(content[columnName["LvToUnlock"]]);
					song.maxBPM = int.Parse(content[columnName["maxBPM"]]);
					song.ticksPerTile = int.Parse(content[columnName["tickPerTile"]]);
					song.speedModifier = float.Parse(content[columnName["speedModifier"]]);
					if (!string.IsNullOrEmpty(content[columnName["speedPerStar"]].ToString()))
					{
						fsData speedJson = fsJsonParser.Parse(content[columnName["speedPerStar"]]);
						var speed = speedJson.AsList;
						List<float> listSpeed = new List<float>();
						for (int i = 0; i < speed.Count; i++)
						{
							listSpeed.Add((float)(speed[i].AsDouble));
						}
						if (listSpeed.Count > 0)
						{
							song.speedPerStar = listSpeed;
						}
						else
						{
							song.speedPerStar = null;
						}
					}
					song.type = (SongItemType)(int.Parse(content[columnName["type"]]));

					if (song.type == SongItemType.Hot)
					{
						listHot.Add(song);
					}
					else if (song.type == SongItemType.New)
					{
						listNew.Add(song);
					}

					if (song.pricePrimary <= 0)
					{
						songsNormal.Add(song);
					}
					else
					{
						songsPremium.Add(song);
					}

					song.isPianoTiles = int.Parse(content[columnName["isPianoTiles"]]);

					// Battle info
					try
					{
						song.battleSpeedFactor = float.Parse(content[columnName["BattleSpeedChange"]]);
					}
					catch (Exception)
					{
						song.battleSpeedFactor = 1.0f;
					}

					songs.Add(song);
				}
			);

			storeData.listAllSongs = songs;
			storeData.listHotSongs = listHot;
			storeData.listNewSongs = listNew;

			storeData.listNormalSongs = songsNormal;
			storeData.listPremiumSongs = songsPremium;
			return storeData;
		}

		#endregion

		#region Setting Effect for old device
		public void SettingEffect()
		{
			Debug.Log("setting effect");
			try
			{
				if (PlayerPrefs.GetInt("SettingEffect", 0) == 0)
				{
					PlayerPrefs.SetInt("SettingEffect", 1);
					PlayerPrefs.Save();
#if UNITY_IOS
					UnityEngine.iOS.DeviceGeneration iphoneGen= UnityEngine.iOS.Device.generation;
					Debug.Log("Iphone device is:"+iphoneGen);
					if(iphoneGen==UnityEngine.iOS.DeviceGeneration.iPad1Gen
						||iphoneGen==UnityEngine.iOS.DeviceGeneration.iPad2Gen
						||iphoneGen==UnityEngine.iOS.DeviceGeneration.iPadMini1Gen
						||iphoneGen==UnityEngine.iOS.DeviceGeneration.iPhone3G
						||iphoneGen==UnityEngine.iOS.DeviceGeneration.iPhone3GS
						||iphoneGen==UnityEngine.iOS.DeviceGeneration.iPhone4
						||iphoneGen==UnityEngine.iOS.DeviceGeneration.iPhone4S
						||iphoneGen==UnityEngine.iOS.DeviceGeneration.iPodTouch1Gen
						||iphoneGen==UnityEngine.iOS.DeviceGeneration.iPodTouch2Gen
						||iphoneGen==UnityEngine.iOS.DeviceGeneration.iPodTouch3Gen
						||iphoneGen==UnityEngine.iOS.DeviceGeneration.iPodTouch4Gen
						||iphoneGen==UnityEngine.iOS.DeviceGeneration.iPodTouch5Gen)
					{
						Debug.LogWarning("Setting Effect Off for old device");
						optons.HDGraphicOn = false;
					}
#endif
#if UNITY_ANDROID
					int TOTAL_PROCESSOR = SystemInfo.processorCount * SystemInfo.processorFrequency;
					if (SystemInfo.maxTextureSize >= CheckScreenAPI.Instance.MAX_TEXTURE_REQUIRE
						&& TOTAL_PROCESSOR >= CheckScreenAPI.Instance.TOTAL_PROCESSOR_REQUIRE)
					{
						option.HDGraphicOn = true;
					}
					else
					{
						option.HDGraphicOn = false;
					}
#endif
				}

				if (option.HDGraphicOn)
				{
					Debug.Log("ResolutionHD");
					CheckScreenAPI.Instance.ResolutionHD();
				}
				else
				{
					Debug.Log("ResolutionSD");
					CheckScreenAPI.Instance.ResolutionSD();
				}
			}
			catch (Exception ex)
			{
				Debug.LogError("Setting Effect Exception:" + ex.Message);
			}
		}
		#endregion
	}

	public class Option
	{
		public bool HDGraphicOn
		{
			get
			{
				return PlayerPrefs.GetInt("HDGraphicOn", 0) == 1;
			}
			set
			{
				PlayerPrefs.SetInt("HDGraphicOn", value ? 1 : 0);
				PlayerPrefs.Save();
			}
		}
	}
}
