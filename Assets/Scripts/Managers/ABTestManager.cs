
using Amanotes.PianoChallenge;
using Amanotes.Utils;
using UnityEngine;

class ABTestManager
{
	static private ABTestManager instance = null;

	static public ABTestManager Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new ABTestManager();
			}
			return instance;
		}
	}

	const string LEVEL_CONFIG_OPTION = "AB Test - Level Config";
	const string SONG_CONFIG_OPTION = "AB Test - Song Config";
	const string IN_GAME_ACHIEVEMENT_OPTION = "AB Test - In Game Achievement";

	const string INTERSTITIAL_POLICY = "AB Test - Interstitial Policy";
	const string DAY_0_PN = "AB Test - Day 0 Notification";

	public int LevelConfigOption = 0;
	public string LevelConfigExtension { get { return new string[] { "", "A", "B" }[LevelConfigOption]; } }

	public int SongConfigOption = 0;
	public string SongConfigExtension { get { return new string[] { "", "A", "B", "C", "D" }[SongConfigOption]; } }

	public int InGameAchievementOption = 0;
	public bool NewInGameAchievementEnabled { get { return InGameAchievementOption > 0; } }

	public int InterstitialPolicy = 0;

	public int Day0PnOption = 0;
	public bool OfflinePnEnabled { get { return Day0PnOption == 1 || Day0PnOption == 2; } }
	public bool FullLifePnEnabled { get { return Day0PnOption == 0 || Day0PnOption == 2; } }

	public void Initialize()
	{
		LogInfo();
	}

	public void LogInfo()
	{
		Debug.Log("Level Config: " + new string[] { "Normal", "A", "B" }[LevelConfigOption]);
		Debug.Log("Song Config: " + new string[] { "Normal", "A", "B", "C", "D" }[SongConfigOption]);
		Debug.Log("Interstitial Policy: " + new string[] { "Normal", "Skip 1st", "Skip 1 in 2", "Skip 2 in 3" }[InterstitialPolicy]);
		Debug.Log("In game achievement: " + new string[] { "A", "B" }[InGameAchievementOption]);
		Debug.Log("Day 0 notification: " + new string[] { "A", "B", "C" }[Day0PnOption]);
	}

	private ABTestManager()
	{
		// Level & song config
		LevelConfigOption = LoadOption(LEVEL_CONFIG_OPTION, new string[] {"Normal", "A", "B"});
		SongConfigOption = LoadOption(SONG_CONFIG_OPTION, new string[] { "Normal", "A", "B", "C", "D"});
		// In-game achievement
		InGameAchievementOption = LoadOption(IN_GAME_ACHIEVEMENT_OPTION, new string[] { "A", "B" });

		InterstitialPolicy = LoadOption(INTERSTITIAL_POLICY, new string[] { "Normal", "Skip 1st", "Skip 1 in 2", "Skip 2 in 3" });
		Day0PnOption = LoadOption(DAY_0_PN, new string[] { "A", "B", "C" });
	}

	private int LoadOption(string name, string[] options)
	{
		int option = PlayerPrefs.GetInt(name, -1);
		if (option < 0 || !(option < options.Length))
		{
			option = Random.Range(0, options.Length);
			PlayerPrefs.SetInt(name, option);
		}
		AnalyticsHelper.Instance.LogABTest(name, options[option]);
		return option;
	}
}
