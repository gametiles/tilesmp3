using UnityEngine;
using System.Collections;
using System;
using FullSerializer;
using Amanotes.PianoChallenge;

namespace Amanotes.Utils
{
	public class GeoLocationManager : SingletonMono<GeoLocationManager>
	{
		public const string GEOLOCATION_KEY = "geolocation";

		private CountryCodeModel countryCode;

		public CountryCodeModel CountryCode
		{
			get { return countryCode; }
		}

		private bool isLoaded = false;

		public bool IsLoaded
		{
			get
			{
				return isLoaded;
			}
		}

		public void Initialize()
		{
			if (!IsInitialize())
			{
				fsData jsonData;
				fsResult result;
				string cacheCode = PlayerPrefs.GetString(GEOLOCATION_KEY, "");
				Development.Log("[Location] Cached geolocation info: " + cacheCode);
				if (cacheCode.Length > 2)
				{
					jsonData = fsJsonParser.Parse(cacheCode);
					result = FileUtilities.JSONSerializer.TryDeserialize(jsonData, ref countryCode);
				}
				if (countryCode == null)
				{
					//default when not cache
					countryCode = new CountryCodeModel();
				}
				
				if (Application.internetReachability != NetworkReachability.NotReachable)
				{
					StartCoroutine(IEGetGeoLocation(OnCompletedRequest));
				}
				else
				{ // khong co mang
					isLoaded = true;
				}
			}
		}

		public bool IsInitialize()
		{
			if (countryCode == null)
				return false;
			else
				return true;
		}

		// get geolocation from http://ip-api.com/json
		IEnumerator IEGetGeoLocation(Action<bool, string> result, float timeOut = 5f)
		{
			float timer = Time.realtimeSinceStartup;
			string link = GameManager.Instance.GameConfigs != null &&
				GameManager.Instance.GameConfigs.CHECK_GEO_IP_URL != null ?
				GameManager.Instance.GameConfigs.CHECK_GEO_IP_URL :
				"http://52.71.9.24:8081/geoip?pass=AmanotesJSC";

			using (WWW www = new WWW(link))
			{
				while (!www.isDone)
				{
					if (Time.realtimeSinceStartup - timer >= timeOut)
					{
						if (result != null)
						{
							result(false, "Request timed out");
						}
						yield break;
					}
					yield return new WaitForSeconds(0.1f);
				}
				yield return www;

				if (!string.IsNullOrEmpty(www.error))
				{
					if (result != null)
						result(false, www.error);
				}
				else
				{
					if (result != null)
						result(true, www.text);
				}
			}
		}

		private void OnCompletedRequest(bool onsucced, string jsonString)
		{
			countryCode = new CountryCodeModel();
			fsData jsonData;
			fsResult result;
			if (onsucced)
			{
				jsonData = fsJsonParser.Parse(jsonString);
				result = FileUtilities.JSONSerializer.TryDeserialize(jsonData, ref countryCode);
				if (!result.Failed)
				{
					PlayerPrefs.SetString(GEOLOCATION_KEY, jsonString);
					PlayerPrefs.Save();

					Development.Log("[Location] Downloaded geolocation info: " + jsonString);
				}
				else
				{
					string jsonLocation = GetLocalLocation();
					jsonData = fsJsonParser.Parse(jsonLocation);
					result = FileUtilities.JSONSerializer.TryDeserialize(jsonData, ref countryCode);

					Development.Log("[Location] Failed to parse downloaded geolocation info. Take the local one instead: " + jsonString);
				}
			}
			else
			{
				string jsonLocation = GetLocalLocation();
				jsonData = fsJsonParser.Parse(jsonLocation);
				result = FileUtilities.JSONSerializer.TryDeserialize(jsonData, ref countryCode);

				Development.Log("[Location] Failed to download geolocation info. Take the local one instead: " + jsonString);
			}
			isLoaded = true;
		}

		private string GetLocalLocation()
		{
			if (string.IsNullOrEmpty(PlayerPrefs.GetString(GEOLOCATION_KEY)))
			{
				TextAsset bindata = Resources.Load("geolocation") as TextAsset;
				return bindata.text;
			}
			else
			{
				return PlayerPrefs.GetString(GEOLOCATION_KEY);
			}
		}
	}
}
