using System.Runtime.InteropServices;
using UnityEngine;
using System;

namespace fModStudio
{
	public class fGeneral
	{
		[DllImport("fMod")] private static extern void synth_load(byte[] data, int size, bool parallel);
		[DllImport("fMod")] private static extern void synth_unload();
		[DllImport("fMod")] private static extern bool synth_isLoading();
		[DllImport("fMod")] private static extern void synth_start();
		[DllImport("fMod")] private static extern void synth_stop();
		[DllImport("fMod")] private static extern void synth_pause();
		[DllImport("fMod")] private static extern void synth_resume();
		[DllImport("fMod")] private static extern void synth_noteOn(short channel, short key, short velocity);
		[DllImport("fMod")] private static extern void synth_noteOff(short channel, short key);
		[DllImport("fMod")] private static extern void synth_controlChange(short channel, short param1, short param2);
		[DllImport("fMod")] private static extern void synth_programChange(short channel, short param);
		[DllImport("fMod")] private static extern void synth_setGain(float gain);
		[DllImport("fMod")] private static extern float synth_getGain();

		static byte[] data = null;

		public static void Pause()
		{
			synth_pause();
		}

		public static void Resume()
		{
			synth_resume();
		}

		public static void Load(string name, bool parallel = false)
		{
			byte[] sfData = (Resources.Load("Audio/" + name) as TextAsset).bytes;
			if (sfData != null)
			{
				synth_load(sfData, sfData.Length, parallel);
				if (parallel)
				{
					data = sfData;
				}
			}
		}

		public static void FinishLoading()
		{
			data = null;
		}

		public static void Unload()
		{
			synth_unload();
		}

		public static bool IsLoading()
		{
			return synth_isLoading();
		}

		public static void Start()
		{
			synth_start();
		}

		public static void Stop()
		{
			try
			{
				synth_stop();
			}
			catch (Exception)
			{
			}

		}

		public static void NoteOn(short channel, short key, short velocity)
		{
			synth_noteOn(channel, key, velocity);
		}

		public static void NoteOff(short channel, short key)
		{
			synth_noteOff(channel, key);
		}

		public static void ProgramChange(short channel, short param)
		{
			synth_programChange(channel, param);
		}

		public static void ControlChange(short channel, short param1, short param2)
		{
			synth_controlChange(channel, param1, param2);
		}

		public static void SetGain(float gain)
		{
			synth_setGain(gain);
		}

		public static float GetGain()
		{
			return synth_getGain();
		}
	}
}
