using System;
using System.Collections.Generic;
using UnityEngine;

namespace Amanotes.Utils
{
	public class AudioManager : SingletonMono<AudioManager>
	{
		public bool Initialized { get { return !IsLoading; } }

		enum Mode { SOUND_POOL, SOUND_FONT }
		Mode mode = Mode.SOUND_FONT;
		string soundName = null;

		public bool cancelSound = false;

		class Note
		{
			public Note(short channel, short key, float time)
			{
				this.channel = channel;
				this.key = key;
				this.time = time;
			}

			public short key;
			public short channel;
			public float time;
		}

		List<Note> notes = new List<Note>(20);
		private short channel = 0;

		public void Initialize(Action<float> onProgress = null, Action onCompleted = null)
		{
			if (mode == Mode.SOUND_POOL)
			{
                fModStudio.FMod.Load(true);
				soundName = "Default";
			}
			else
			{
				soundName = "GrandPiano";
				fModStudio.fGeneral.SetGain(1.6f);
                fModStudio.fGeneral.Load("GrandPiano", true);
                fModStudio.fGeneral.Start();
			}
		}

		public void LoadSoundFont(string name, bool parallel = false)
		{
			if (soundName == name)
			{
				return;
			}
			if (name == "Default")
			{
				if (mode == Mode.SOUND_POOL)
				{
					return;
				}
				mode = Mode.SOUND_POOL;

				notes.Clear();
                fModStudio.fGeneral.Stop();
                fModStudio.fGeneral.Unload();
                fModStudio.FMod.Load(parallel);
			}
			else
			{
				if (mode == Mode.SOUND_POOL)
				{
                    fModStudio.FMod.Unload();
				}
				mode = Mode.SOUND_FONT;
                fModStudio.fGeneral.Stop();
                fModStudio.fGeneral.Load(name);
                fModStudio.fGeneral.Start();
			}
			soundName = name;
		}

		public bool IsLoading
		{
			get
			{
				return fModStudio.fGeneral.IsLoading() || fModStudio.FMod.IsLoading();
			}
		}

		public void FinishLoading()
		{
            fModStudio.fGeneral.FinishLoading();
            fModStudio.FMod.FinishLoading();
		}

		public bool MuteSFX { get; set; }

		public void PlaySound(int id, float volume = 1)
		{
			if (cancelSound)
			{
				return;
			}

			if (mode == Mode.SOUND_FONT)
			{
				fModStudio.fGeneral.NoteOn(channel, (short)id, (short)(volume * 127));
				notes.Add(new Note(channel, (short)id, Time.realtimeSinceStartup + 1.5f));
				channel++;
				if (channel >= 8)
				{
					channel = 0;
				}
			}
			else
			{
                fModStudio.FMod.Play(id, volume);
			}
		}

		public void StopSession()
		{
			if (mode == Mode.SOUND_POOL)
			{
                fModStudio.FMod.Pause();
			}
			else
			{
                fModStudio.fGeneral.Stop();
			}
		}

		public void StartSession()
		{
			if (mode == Mode.SOUND_POOL)
			{
                fModStudio.FMod.Resume();
			}
			else
			{
				fModStudio.fGeneral.Start();
			}
		}

		private void Update()
		{
			if (mode == Mode.SOUND_FONT)
			{
				var now = Time.realtimeSinceStartup;
				for (int i = 0; i < notes.Count; i++)
				{
					var note = notes[i];
					if (now >= note.time)
					{
                        fModStudio.fGeneral.NoteOff(note.channel, note.key);
						i--;
						notes.Remove(note);
					}
				}
			}
		}

		private void OnApplicationQuit()
		{
            fModStudio.fGeneral.Stop();
		}

		private void OnApplicationPause(bool pause)
		{
			if (pause)
			{
				if (mode == Mode.SOUND_POOL)
				{
                    fModStudio.FMod.Pause();
				}
				else
				{
                    fModStudio.fGeneral.Pause();
				}
			}
			else
			{
				if (mode == Mode.SOUND_POOL)
				{
                    fModStudio.FMod.Resume();
				}
				else
				{
                    fModStudio.fGeneral.Resume();
				}
			}
		}
	}
}
