﻿using UnityEngine;
using Amanotes.PianoChallenge;

namespace Amanotes
{
	public class EventManager
	{
		const string SOCIAL_EVENT_VERSION = "MPHightlight";
		const string IAP_EVENT_VERSION = "IAP Event Version";

		static EventManager instance = null;
		bool socialNotifiable = false;
		bool iapNotifiable = false;

		static public EventManager Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new EventManager();
				}
				return instance;
			}
		}

		EventManager ()
		{
			var configs = GameManager.Instance.GameConfigs;
			socialNotifiable = PlayerPrefs.GetInt (SOCIAL_EVENT_VERSION) < configs.socialEventVersion;
			iapNotifiable = PlayerPrefs.GetInt (IAP_EVENT_VERSION) < configs.iapEventVersion;
		}

		public bool SocialNotifiable
		{
			get { return socialNotifiable; }
			set {
				socialNotifiable = value;
				if (!value)
				{
					PlayerPrefs.SetInt (SOCIAL_EVENT_VERSION, GameManager.Instance.GameConfigs.socialEventVersion);
					PlayerPrefs.Save ();
				}
			}
		}

		public bool IapNotifiable
		{
			get { return iapNotifiable; }
			set {
				iapNotifiable = value;
				if (!value)
				{
					PlayerPrefs.SetInt (IAP_EVENT_VERSION, GameManager.Instance.GameConfigs.iapEventVersion);
					PlayerPrefs.Save ();
				}
			}
		}
	}
}

