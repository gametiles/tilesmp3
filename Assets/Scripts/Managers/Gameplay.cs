using UnityEngine;
using System;
using System.Collections.Generic;
using DG.Tweening;
using MovementEffects;

namespace Amanotes.PianoChallenge
{
	public class LongNotePlayedData
	{
		//system time when started to play the note data list
		public float timeStampBeginTouch = 0;
		//system time when played the first note
		public float timeStartPlayFirstNote = 0;
		//the time stamp of the first note played
		public float timeStampOfFirstNote = 0;

		//the order index of last played note
		public int noteDataPlayedIndex = -1;
		public bool touchRelease = false;
		public bool isScored = false;
	}

	public class Gameplay : MonoBehaviour
	{
		public const string TRIGGER_NORMAL = "Reset";
		public const string TRIGGER_FIRST_STAR = "FirstStar";
		public const string TRIGGER_FIRST_CROWN = "FirstCrown";
		public const string TRIGGER_SECOND_CROWN = "SecondCrown";
		public const string TRIGGER_THIRD_CROWN = "ThirdCrown";

		//a really small number (entropy)
		public static readonly float ENTROPY = 0.005f;
		public static readonly int TILE_COLUMN = 4;
		public enum GameState
		{
			Invalid,
			Initialized,
			Playing,
			Paused,
			GameOver,
			Continue,
			AutoPlay
		}

		bool autoPlayForExpCalc = false;

		#region Editor's variable
		[SerializeField] private Camera tileRenderCamera = null;
		public NoteStart noteStart = null;

		[Tooltip("Normal (black) tile's height in screen with ratio of 16:9")]
		[SerializeField] private int normalTileHeightScreenRatio169 = 484;
		private int tileRowHeight = 480;

		[Tooltip("The endless label to be shown")]
		[SerializeField] UILabel endlessLabel = null;
		[SerializeField] GameObject endlessUnlockedText = null;

		[Header("Stars and crowns")]
		[Space(5)]
		[Tooltip("Star sprites to be shown when needed")]
		[SerializeField] RecordView recordviewStars = null;
		[Tooltip("Crown sprites to be shown when needed")]
		[SerializeField] RecordView recordviewCrowns = null;
		[SerializeField] UILabel lbScoreDisplay = null;
		[SerializeField] Animation anmScoreDisplay = null;

		[Header("Live background variables")]
		[SerializeField] Animator bgAnimator = null;
		[SerializeField] ParticleSystem particleBubble = null;
		[SerializeField] ParticleSystem particleSnow = null;

		[Header("Tile's size")]
		[Tooltip("Index 0 is head, index 1 is tail")]
		//this one is used for calculating tile's length in unit
		[SerializeField] List<Transform> tileMarkArray = null;
		[SerializeField] float tileLengthInUnit = 0;

		[Header("Stars/Crown progress")]
		[SerializeField] UIProgressBar[] milestones = null;
		#endregion

		#region Tile management
		//hold references for all spawned tiles
		private List<TilesRow> tiles;
		//hold a record of how many tiles have been spawned
		private int numTileGenerated = 0;
		//at which index a dual tile has been created?
		private int lastDualTileIndex = -1;
		//used to make sure 2 consequence tiles will not have the same column
		private int lastGeneratedTileColumn = 0;
		//number of tiles have been touched
		private int numTileConsumed = 0;
		#endregion

		#region Game Speed
		//control the start, current and max speed of the game
		private float baseRunSpeed = 0.75f;
		private float currentRunSpeed = 0.75f;
		private float maxRunSpeed = 0.75f * 4;

		//game speed to start with after user chose to Continue game
		private float speedAfterContinue = 2f;

		//base run speed of the game, in local NGUI position format
		private float baseLocalRunSpeed;

		//use to notify the game that it need to increase its speed
		private bool isIncreasingSpeed = false;
		private float targetSpeed;

		//read from level config, how fast the game should run?
		private float speedModifier = 1.0f;
		//this variable is used to control how fast audio notes in consequence will be play. 1 for normal speed
		private float speedRatio;

		private List<float> speedTableNotes = new List<float>(6);
		#endregion

		#region Miscellaneous 
		private LevelDataModel currentLevelData;
		/// <summary>
		/// Get the level data which has been loaded into this game logic
		/// </summary>
		public LevelDataModel CachedLevelData { get { return currentLevelData; } }

		public GameState currentState = GameState.Invalid;

		//store data of each tiles will appear in the game
		private List<TileData> listTilesData;

		//an array of index, at which the game will reward user with 1 star
		private int[] starRecords = new int[7];
		private int currentNumStar = 0;

		//only used in calculating speed, nothing serious here, should be removed
		private int dataCountUsedInSpeedCalculating = 0;

		//list of long notes that has been registered as touched, this list will help us prevent users from touching the long note many time, as well as decide when, where and what sound to play
		private Dictionary<int, LongNotePlayedData> listLongNotePlayedData;

		public const int MAX_RETRY = 3;
		private int numContinue = 0;

		private int diamondDropped = 0;

		[SerializeField] GameObject btnStopAutoPlay = null;
		#endregion

		//if 2 notes have time delta below this threshold, they will be move closer together
		private readonly float MINIMUM_TIME_DIFFERENT = 0.05f;

		private bool speedUp = false;
		public bool SpeedUp { get { return speedUp; } }

		private bool speedDown = false;
		public bool SpeedDown { get { return speedDown; } }

		public event Action OnGameOver;
		public event Action OnGameReady = null;
		public event Action OnGameStarted = null;

		public static Gameplay instance = null;
		private bool isInit = false;
		private bool endlessUnlocked = false;

		private bool newInGameAchievementEnabled = false;

		public float GetSpeedRatio()
		{
			return speedRatio;
		}

		public float GetCurrentSpeed()
		{
			return currentRunSpeed;
		}

		private void OnEnable()
		{
			instance = this;
		}

		private void OnDisable()
		{
			instance = null;
		}

		public void Initialize()
		{
			if (isInit)
				return;

			listLongNotePlayedData = new Dictionary<int, LongNotePlayedData>(5000);
			InGameUIController.instance.Setup();
			isInit = true;
		}

		public float GetSpeedNotes()
		{
			return currentRunSpeed;
		}

		private IEnumerator<float> coroutineIncreasingSpeed;

		public void ResetGame()
		{
			int stars = HighScoreManager.Instance.GetHighScore(
				currentLevelData.songData.storeID,
				ScoreType.Star);

			endlessUnlocked = stars >= 3;

			// Reset endless mode to be safe
			if (!endlessUnlocked)
			{
				GameManager.Instance.SessionData.endless = false;
			}

			Counter.Clear();

			listLongNotePlayedData.Clear();

			//reset game speed
			if (isIncreasingSpeed)
			{
				Timing.KillCoroutine(coroutineIncreasingSpeed);
				isIncreasingSpeed = false;
			}

			GameManager.Instance.SessionData.listen = false;
			btnStopAutoPlay.SetActive(false);
			PrepareSpeedTableNotes();

			//hide all record view
			if (recordviewCrowns.gameObject.activeInHierarchy)
			{
				recordviewCrowns.ShowNumRecord(0);
				recordviewCrowns.SetVisible(false);
			}
			if (recordviewStars.gameObject.activeInHierarchy)
			{
				recordviewStars.ShowNumRecord(0);
				recordviewStars.SetVisible(false);
			}
			currentNumStar = 0;

			tileRowHeight = normalTileHeightScreenRatio169;

			//pre-calculate at which index we will increase number of star
			starRecords[0] = Mathf.CeilToInt(listTilesData.Count / 3);
			starRecords[1] = Mathf.CeilToInt(listTilesData.Count / 3 * 2);
			starRecords[2] = Mathf.CeilToInt(listTilesData.Count);
			starRecords[3] = Mathf.CeilToInt(listTilesData.Count / 3 * 4);
			starRecords[4] = Mathf.CeilToInt(listTilesData.Count * 2);
			starRecords[5] = Mathf.CeilToInt(listTilesData.Count * 3);
			starRecords[6] = Mathf.CeilToInt(listTilesData.Count * 100000);

			IncreaseAndShowScore(0);

			numContinue = 0;

			diamondDropped = 0;

			bgAnimator.SetTrigger(TRIGGER_NORMAL);
			//particleBubble.gameObject.SetActive(false);
			//particleSnow.gameObject.SetActive(false);

			numTileConsumed = numTileGenerated = 0;
			lastDualTileIndex = -1;

			//calculate how long (in unit) a tile should be, with proper scale from reference screen ratio;
			tileLengthInUnit = tileMarkArray[0].position.y - tileMarkArray[1].position.y;

			//calculate how many tiles must be spawned each second to keep up with the speed of the song
			float tilesPerSecond = (currentLevelData.BPM / 60f * currentLevelData.tickPerQuarterNote * 4 / Mathf.Pow(2, currentLevelData.denominator) / currentLevelData.songData.ticksPerTile);
			baseRunSpeed = ConvertTilePerSec2Speed(tilesPerSecond);
			//speed of the game is in Unit/Second, hence, equals to ((number of tiles) * tile length) per second. Then multiply by unique modifier from level's description
			if (currentLevelData.dicNotePlay != null)
			{
				tilesPerSecond = 4;
				float coff = currentLevelData.songData.maxBPM / GameManager.Instance.GameConfigs.PT2_GAME_DEFAULT_SPEED;
				baseRunSpeed = 2 * coff;// 960/1920*coff
				GameConsts.COFF_SPEED_PT = 2 * coff;//2/coff;//960/1920*coff
			}
			else
			{
				GameConsts.COFF_SPEED_PT = 1;
			}

			// Modify base speed for battle mode
			speedUp = speedDown = false;
            /*
			if (GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.ONLINE &&
				GameManager.Instance.GameConfigs.adjustBattleSpeed &&
				!BattleManager.Instance.PlayingWithFriends &&
				AvNetworkManager.Instance.Room != null &&
				currentLevelData.songData.battleSpeedFactor > 0)
			{
				List<float> speeds;
				List<int> allows;

				speeds = GameManager.Instance.GameConfigs.onlineSongBaseSpeeds;
				allows = GameManager.Instance.GameConfigs.adjustBattleSpeeds;

				int i = AvNetworkManager.Instance.Room.room_type;
				int maxLevel = GameManager.Instance.GameConfigs.maxBattleLevel;
				if (maxLevel > speeds.Count - 2) maxLevel = speeds.Count - 2;
				i = i < 0 ? 0 : (i >= maxLevel ? maxLevel : i); // Safe index

				int allow = allows[i];
				if (allow > 0)
				{// Only adjust speed for this level if it's allowed
					float minSpeed = speeds[i];
					float maxSpeed = speeds[i + 1];

					float tilesPerSec = tilesPerSecond * speedModifier;

					speedUp = tilesPerSec < minSpeed && (allow == 1 || allow == 3);
					speedDown = tilesPerSec > maxSpeed && (allow == 2 || allow == 3);

					if (speedUp || speedDown)
					{
						float desiredSpeed = tileLengthInUnit * (minSpeed + maxSpeed) / 2;
						float configuredSpeed = baseRunSpeed + currentLevelData.songData.battleSpeedFactor * (desiredSpeed - baseRunSpeed);
						baseRunSpeed = configuredSpeed;

						speedModifier *= configuredSpeed / baseRunSpeed;
					}
				}
			}
            */
			currentRunSpeed = baseRunSpeed * speedTableNotes[0];
			maxRunSpeed = baseRunSpeed * (currentLevelData.songData.maxBPM / currentLevelData.BPM);
			speedAfterContinue = GameManager.Instance.GameConfigs.firstNoteSpeed_dcrease;
			//mark base speed for tile placement
			baseLocalRunSpeed = tilesPerSecond * tileRowHeight * currentLevelData.songData.speedModifier;

			//slow to fast, prevent user from missing at first tile
			targetSpeed = currentRunSpeed;
			if (targetSpeed < speedAfterContinue)
			{
				isIncreasingSpeed = false;
			}
			else
			{
				IncreaseGameSpeed(speedAfterContinue, targetSpeed);
			}

			speedRatio = currentLevelData.songData.speedModifier;
			currentState = GameState.Initialized;

			//calculate which value to use for data count. Its value must be dividable by 3
			dataCountUsedInSpeedCalculating = listTilesData.Count;
			if (dataCountUsedInSpeedCalculating % 3 != 0)
			{
				int min, max;
				min = max = dataCountUsedInSpeedCalculating;
				while (true)
				{
					--min;
					++max;
					if (min % 3 == 0)
					{
						dataCountUsedInSpeedCalculating = min;
						break;
					}

					if (max % 3 == 0)
					{
						dataCountUsedInSpeedCalculating = max;
						break;
					}
				}
			}

			InGameUIController.instance.ResetForNewGame();

			if (GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.ONLINE)
			{
				noteStart.SetLoading(true);
				newInGameAchievementEnabled = false;
			}
			else // normal mode
			{
				newInGameAchievementEnabled = ABTestManager.Instance.NewInGameAchievementEnabled;
				for (int i = 0; i < 6; i++)
				{
					milestones[i].value = 0;
				}

				if (GameManager.Instance.SessionData.endless && !GameManager.Instance.SessionData.listen)
				{

					currentNumStar = Counter.Count(Counter.KeyStar, 3);
					targetSpeed = baseRunSpeed * speedTableNotes[(currentNumStar)];
					IncreaseGameSpeed(currentRunSpeed, targetSpeed);
					bgAnimator.SetTrigger(TRIGGER_FIRST_STAR);
					ShowEndlessLabel(-1);

					starRecords[0] = starRecords[1] = starRecords[2] = 0;

					int songTotalScore = 0;
					foreach (var tile in listTilesData)
					{
						songTotalScore += tile.score;
					}
					int initialScore = (int)(songTotalScore * GameManager.Instance.GameConfigs.endlessInitScoreRatio);
					IncreaseAndShowScore(initialScore);
				}
				else
				{
					milestones[0].transform.parent.gameObject.SetActive(newInGameAchievementEnabled);
					milestones[3].transform.parent.gameObject.SetActive(false);
				}
			}

			//Collect GC now, when the game is preparing
			GC.Collect();
		}

		private float ConvertTilePerSec2Speed(float tilesPerSecond)
		{
			return (((tilesPerSecond) * tileLengthInUnit)) * currentLevelData.songData.speedModifier;
		}

		public int GetNumTileTillNextStar(int currentStar)
		{
			if (currentStar >= 0 && currentStar < starRecords.Length)
			{
				return starRecords[currentStar] - numTileConsumed;
			}

			return -1;
		}

		private int zz_rest_distance = 0;

		//this thing is not intent to be used anywhere else!!! Leave it here and forget it please
		//private int zz_generatenexttile_count_before_mark_parent_as_changed = 0;
		//private static int zz_max_generatenexttile_count_before_mark_parent_as_changed = 3;
		/// <summary>
		/// 
		/// </summary>
		public void GenerateNextTile()
		{
			if (listTilesData.Count <= 0)
			{
				return;
			}

			if (numTileGenerated < 0)
			{
				numTileGenerated = 0;
			}
			//get suitable tile data
			int tileIndex = numTileGenerated % listTilesData.Count;
			if (tileIndex == 0 && numTileGenerated != 0)
			{
				zz_rest_distance = 1500;
			}
			else
			{
				zz_rest_distance = 0;
			}

			TileData tileData = listTilesData[tileIndex];

			//calculate tile's position
			Vector3 spawnPos = new Vector3(0, 480 - 1140, 0);
			int tileColumIndex = 0;

			//if this is not the first tile to be spawn
			if (numTileGenerated > 0)
			{
				NoteSimple note = InGameUIController.instance.GetLastNoteGenerate();
				TileData lastTile = note.data;
				if (((lastTile.subType != TileType.Dual) || (tileData.subType != TileType.Dual)) || (numTileGenerated - lastDualTileIndex <= 1))
				{
					//if the tile is a dual note or a note right after dual note, set it as next to the last generated tile
					if (tileData.subType == TileType.Dual || lastTile.subType == TileType.Dual)
					{
						tileColumIndex = (lastGeneratedTileColumn + 1) % TILE_COLUMN;
					}
					else
					{
						//if the tile is a normal one, random for a column index
						tileColumIndex = UnityEngine.Random.Range(0, TILE_COLUMN);
						//but the random column can not be the same as the last one
						if (tileColumIndex == lastGeneratedTileColumn)
						{
							//so we improvise here
							tileColumIndex = lastGeneratedTileColumn + UnityEngine.Random.Range(1, TILE_COLUMN - 1);
							tileColumIndex = tileColumIndex % TILE_COLUMN;
						}
					}

					//calculate time appear of this to-be-generated-tile
					float supposeAppearTime = lastTile.startTime + lastTile.duration;
					float appearTime = tileData.startTime;

					//if it's not too far from the last tile
					if (appearTime <= supposeAppearTime + MINIMUM_TIME_DIFFERENT)
					{
						//spawn it at normal position (without any empty area in between them)
						spawnPos.y = note.transform.localPosition.y + note.height - 1140 + zz_rest_distance;
					}
					else
					{
						//spawnPos.y = note.transform.localPosition.y + note.height - 1140 + zz_rest_distance;
						if (currentLevelData.dicNotePlay == null)
						{// giu nguyen nhu cu
							spawnPos.y = note.transform.localPosition.y + note.height - 1140 + zz_rest_distance + (appearTime - supposeAppearTime) * baseLocalRunSpeed;
						}
						else
						{// khoang cach cua piano tile
							spawnPos.y = note.transform.localPosition.y + note.height - 1140 + zz_rest_distance + (appearTime - supposeAppearTime) * 960;
						}
					}
				}
				else
				{
					//record current index as last dual tile created
					lastDualTileIndex = numTileGenerated;
					tileColumIndex = (lastGeneratedTileColumn + 2) % TILE_COLUMN;
					spawnPos.y = note.transform.localPosition.y - 1140 + zz_rest_distance;
				}
			}

			lastGeneratedTileColumn = tileColumIndex;

			#region new code

			//create a new tile
			int tileLength = 0;
			if (tileData.type == TileType.LongNote)
			{
				tileLength = currentLevelData.dicNotePlay != null ?
					Mathf.RoundToInt(tileData.durationInTicks * 960 / 1000.0f) :
					Mathf.RoundToInt(tileData.durationInTicks * 1.0f / currentLevelData.songData.ticksPerTile * tileRowHeight);
			}
			else
			{
				tileLength = tileData.score * tileRowHeight;
			}


			bool withBonusTile = false;

			if (CurrentGameStage == GameState.Playing &&
				numTileGenerated > listTilesData.Count &&
				tileData.subType != TileType.Dual &&
				diamondDropped < GameManager.Instance.GameConfigs.maxDiamondPerGame &&
				currentRunSpeed >= GameManager.Instance.GameConfigs.speedToDropDiamond &&
				Counter.GetQuantity(Counter.KeyScore) >= GameManager.Instance.GameConfigs.scoreToDropDiamond)
			{
				float chance = UnityEngine.Random.Range(0, 1.001f);
				if (chance <= GameManager.Instance.GameConfigs.diamondChance)
				{
					withBonusTile = true;
					++diamondDropped;
				}
			}

			InGameUIController.instance.CreateNewNote(tileData, spawnPos, numTileGenerated, tileColumIndex, tileLength, withBonusTile);

			#endregion

			numTileGenerated++;
		}

		/// <summary>
		/// Setup UI when autoplay is clicked
		/// </summary>
		private void SetAutoPlayUI()
		{
			particleBubble.maxParticles = 50;
			particleBubble.gameObject.SetActive(true);
			bgAnimator.SetTrigger(TRIGGER_SECOND_CROWN);
			particleSnow.gameObject.SetActive(true);
			particleSnow.Play();
			if (GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.NORMAL)
			{
				btnStopAutoPlay.SetActive(true);
			}
		}

		private void HideEndlessLabel()
		{
			var tween = endlessLabel.GetComponent<TweenAlpha>();
			tween.PlayReverse();
			tween.SetOnFinished(() => {
				endlessLabel.gameObject.SetActive(false);
			});
		}

		private void ShowEndlessLabel(float duration = 1)
		{
			endlessLabel.gameObject.SetActive(true);
			endlessUnlockedText.SetActive(!endlessUnlocked);

			var tween = endlessLabel.GetComponent<TweenAlpha>();
			tween.enabled = true;
			tween.PlayForward();

			if (duration > 0)
			{
				tween.SetOnFinished(() => {
					DOTween.Sequence()
					.AppendInterval(1)
					.OnComplete(() =>
					{
						HideEndlessLabel();
					})
					.Play();
				});
			}
		}

		/// <summary>
		/// Show animation when a new star has been reached
		/// </summary>
		/// <param name="stars"></param>
		private void ShowRecordProgress(int stars)
		{
			//play background animation
			if (stars == 1)
			{
				bgAnimator.SetTrigger(TRIGGER_FIRST_STAR);
                particleBubble.maxParticles = 15;
                particleSnow.maxParticles = 15;
            }
			else if (stars == 2)
			{
                bgAnimator.SetTrigger(TRIGGER_FIRST_CROWN);
                particleBubble.maxParticles = 20;
                particleSnow.maxParticles = 20;
            }
			else if (stars == 3)
			{
                bgAnimator.SetTrigger(TRIGGER_SECOND_CROWN);
                particleBubble.maxParticles = 25;
                particleSnow.maxParticles = 25;
            }
			else if (stars == 4)
			{
                bgAnimator.SetTrigger(TRIGGER_THIRD_CROWN);
                particleBubble.maxParticles = 30;
                particleSnow.maxParticles = 30;
            }
			else if (stars == 5)
			{
                particleBubble.maxParticles = 35;
                particleSnow.maxParticles = 30;
            }
			else if (stars == 6)
			{
                particleBubble.maxParticles = 50;
                particleSnow.maxParticles = 30;
            }

			if (GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.ONLINE)
				return;

			if (newInGameAchievementEnabled)
			{
				if (stars == 3)
				{
					milestones[0].transform.parent.gameObject.SetActive(false);

					var crownGroup = milestones[3].transform.parent.gameObject;
					crownGroup.SetActive(true);

					var tweenScale = crownGroup.GetComponent<TweenScale>();
					tweenScale.SetOnFinished(() => {
						tweenScale.PlayReverse();
					});
					tweenScale.PlayForward();
				}
				else
				{
					var tweenScale = milestones[stars - 1].gameObject.GetComponent<TweenScale>();
					var tweenColor = milestones[stars - 1].gameObject.GetComponent<TweenColor>();
					milestones[stars - 1].gameObject.transform.GetChild(0).gameObject.SetActive(false);

					tweenColor.duration = 0.1f;
					tweenScale.SetOnFinished(() =>
					{
						tweenScale.PlayReverse();
						tweenScale.SetOnFinished(() =>
						{
							tweenColor.duration = 3;
							tweenColor.PlayReverse();
						});
					});
					tweenScale.PlayForward();
					tweenColor.PlayForward();
				}
			}
			else
			{
				if (stars <= 3)
				{
					recordviewStars.SetVisible(true, 0.25f);
					recordviewStars.ShowNumRecord(stars);
				}
				else
				{
					recordviewCrowns.SetVisible(true, 0.25f);
					recordviewCrowns.ShowNumRecord(stars - 3);
				}

				DOTween.Sequence()
					.AppendInterval(1.25f)
					.OnComplete(() =>
						{
							recordviewStars.SetVisible(false, 0.25f);
							recordviewCrowns.SetVisible(false, 0.25f);
						})
					.Play();
			}

			if (stars == 3)
			{
				ShowEndlessLabel();
			}
		}

		/// <summary>
		/// Increase speed of the game smoothly
		/// </summary>
		/// <param name="startSpeed">Speed to smoothing from</param>
		/// <param name="targetSpeed">Speed to achieve after smoothing end</param>
		/// <param name="duration">The duration of the smoothing process</param>
		/// <param name="timeStep">How often the speed should be increase</param>
		private void IncreaseGameSpeed(float startSpeed, float targetSpeed, float duration = 3f, float timeStep = 0.1f)
		{
			if (duration < 0 || timeStep <= 0 || timeStep > duration)
			{
				Debug.LogWarning(string.Format("Invalid arguments when trying to increase game speed. Duration: {0}, TimeStep: {1}", duration, timeStep));
				return;
			}

			targetSpeed = Mathf.Min(targetSpeed, maxRunSpeed);

			//do nothing if the speed is too much already
			if (startSpeed >= Mathf.Min(targetSpeed, maxRunSpeed))
			{
				isIncreasingSpeed = false;
				return;
			}

			if (!isIncreasingSpeed)
			{
				coroutineIncreasingSpeed = Timing.RunCoroutine(C_SmoothIncreasingSpeed(startSpeed, targetSpeed, duration, timeStep));
			}
			else
			{
				Timing.KillCoroutine(coroutineIncreasingSpeed);
				coroutineIncreasingSpeed = Timing.RunCoroutine(C_SmoothIncreasingSpeed(startSpeed, targetSpeed, duration, timeStep));
			}
		}

		/// <summary>
		/// As it said, increasing speed of the game smooth-fucking-ly
		/// </summary>
		/// <param name="startSpeed">From this speed</param>
		/// <param name="targetSpeed">To this fucking one</param>
		/// <param name="duration">In this much seconds</param>
		/// <param name="timeStep">With the frequency equals to this value</param>
		/// <returns></returns>
		private IEnumerator<float> C_SmoothIncreasingSpeed(float startSpeed, float targetSpeed, float duration, float timeStep)
		{
			isIncreasingSpeed = true;

			float speedStep = (targetSpeed - startSpeed) / duration * timeStep;

			currentRunSpeed = startSpeed;
			while (currentRunSpeed + 0.00001 < targetSpeed)
			{
				yield return Timing.WaitForSeconds(timeStep);

				//only increase speed if the game is playing
				if (currentState == GameState.Playing)
				{
					currentRunSpeed += speedStep;
					speedRatio = (currentRunSpeed / baseRunSpeed) * speedModifier;
				}
			}

			isIncreasingSpeed = false;
		}


		void Update()
		{
			InGameUIController.instance.OnProcessInputControl();
			if (currentState == GameState.Playing || currentState == GameState.AutoPlay)
			{
				InGameUIController.instance.ProcessUpdate(currentRunSpeed);
			}
		}

		public void TileFinished()
		{
			++numTileConsumed;
		}

		/// <summary>
		/// This right here, my friend, is an example of bad code, DO NOT follow
		/// </summary>
		/// <param name="score"></param>
		public void IncreaseAndShowScore(int score = 1)
		{
			if (GameManager.Instance.SessionData.listen)
			{
				return;
			}

			if (score >= 0)
			{
				anmScoreDisplay.Play();
				if (!lbScoreDisplay.gameObject.activeSelf)
					lbScoreDisplay.gameObject.SetActive(true);
				lbScoreDisplay.text = Counter.Count(Counter.KeyScore, score).ToString();
				TryIncreaseSpeed();
			}
		}


		/// <summary>
		/// Fetch the speed sheet from thin air, ready to move
		/// </summary>
		private void PrepareSpeedTableNotes()
		{
			speedTableNotes = null;
			if (currentLevelData.songData.speedPerStar != null)
			{
				//priority speed table of each level
				speedTableNotes = currentLevelData.songData.speedPerStar;
			}
			else if (GameManager.Instance.GameConfigs != null)
			{
				speedTableNotes = GameManager.Instance.SessionData.endless && !GameManager.Instance.SessionData.listen ?
					GameManager.Instance.GameConfigs.endlessSpeedTable :
					GameManager.Instance.GameConfigs.defaultSpeedTable;
			}

			if (speedTableNotes == null)
			{
				//in case game data can't be read, use default speed table
				speedTableNotes = new List<float>() { 1f, 1.05f, 1.1f, 1.15f, 1.25f, 1.5f, 1.8f, 0.05f };
			}
		}

		/// <summary>
		/// Oh, we got a bonus tiles, better handle that shit
		/// </summary>
		/// <param name="tile"></param>
		public void ProcessBonusTile(NoteBonus tile)
		{
			switch (tile.bonusType)
			{
				case BonusType.Diamond:
					ProfileHelper.Instance.CurrentDiamond += GameManager.Instance.GameConfigs.diamondDrop;
					break;
				case BonusType.Life:
					ProfileHelper.Instance.CurrentLife += 1;
					break;
			}
		}

		/// <summary>
		/// Try hard, but not too hard, should be safe to call every frame
		/// </summary>
		private void TryIncreaseSpeed()
		{
			if (newInGameAchievementEnabled && currentNumStar < 6)
			{
				int tilesSinceLastStar = numTileConsumed;
				int tilesToNextStar = starRecords[currentNumStar];
				if (currentNumStar > 0)
				{
					int n = starRecords[currentNumStar - 1];
					tilesSinceLastStar -= n;
					tilesToNextStar -= n;
				}
				milestones[currentNumStar].value = (float)tilesSinceLastStar / tilesToNextStar;
			}

			//check to see if we need to increase moving speed or not
			if (!isIncreasingSpeed && currentRunSpeed < maxRunSpeed)
			{
				if (currentNumStar < starRecords.Length)
				{
					if (numTileConsumed >= starRecords[currentNumStar])
					{
						if (numTileConsumed < starRecords[currentNumStar + 1])
						{
							currentNumStar = Counter.Count(Counter.KeyStar);
							ShowRecordProgress(currentNumStar);
							if (currentNumStar >= 1)
							{
								targetSpeed = baseRunSpeed * speedTableNotes[(currentNumStar)];
								IncreaseGameSpeed(currentRunSpeed, targetSpeed);
								return;
							}
						}
					}
				}

				if (currentNumStar >= 6)
				{
					// start to increase speed only if the player has finished original song
					// increase speed each 1/3 of the song
					if ((((numTileConsumed - dataCountUsedInSpeedCalculating) * 3) % dataCountUsedInSpeedCalculating) == 0)
					{
						targetSpeed = currentRunSpeed * (1 + speedTableNotes[speedTableNotes.Count - 1]);
						IncreaseGameSpeed(currentRunSpeed, targetSpeed);
					}
				}
			}
		}


		//LOL, the player fucked up, we should humiliate him
		public void ProcessGameOverEvent()
		{
			CalcTilesToNextStarOrCrown(); // goi truoc khi currentState = GameState.GameOver;
			Helpers.Callback(OnGameOver);
			currentState = GameState.GameOver;
		}

		void CalcTilesToNextStarOrCrown()
		{
			int newStar = Mathf.Clamp(Counter.GetQuantity(Counter.KeyStar), 0, 3);
			int newCrown = Counter.GetQuantity(Counter.KeyStar) - 3;
			newStar = newStar < 0 ? 0 : newStar;
			newCrown = newCrown < 0 ? 0 : newCrown;

			int indexRecord = newStar + newCrown;
			if (indexRecord <= starRecords.Length - 2 && indexRecord >= 0 && !autoPlayForExpCalc) // = 5 la index cuoi cua starRecords
			{
				int tilesCurrenStarOrCrown = indexRecord >= 1 ? starRecords[indexRecord - 1] : 0;
				int delta = indexRecord >= 1 ? numTileConsumed - tilesCurrenStarOrCrown : numTileConsumed; // so tiles bam duoc tinh tu star or crown da dat dc
				int nextTiles = starRecords[indexRecord] - tilesCurrenStarOrCrown;
				float percenTiles = nextTiles > 0 ? (delta / (float)nextTiles) * 100 : 0;

				Counter.Count(Counter.KeyPercenToNextStar, (int)percenTiles);
			}
			else
			{
				Counter.Count(Counter.KeyPercenToNextStar, 0);
			}
		}

		//Huh, when did I write this thing? Look professional
		void OnApplicationPause(bool pauseStatus)
		{
			if (pauseStatus)
			{
				PauseGame();
			}
			else
			{
				ContinueGame();
			}
		}

		public void StopAutoPlay()
		{
			ChangeStatusToGameOver();
			ProcessGameOverEvent();
		}

		public void PrepareNewGame(LevelDataModel levelData, List<TileData> tileList)
		{
			currentLevelData = levelData;
			listTilesData = tileList;

			PrepareNewGame();
		}

		public void PrepareNewGame()
		{
			ResetGame();

			//spawn 12 rows of tiles
			for (int i = 0; i < 15; i++)
			{
				GenerateNextTile();
			}

			if (GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.NORMAL)
			{
				noteStart.gameObject.SetActive(true);
				Timing.RunCoroutine(WaitForLoadingAds());
			}
			else if (GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.ONLINE)
			{
				noteStart.SetLoading(false);
			}
		}

		/// <summary>
		/// Slow down crazy child, let the ads load, then you can play
		/// </summary>
		/// <returns></returns>
		IEnumerator<float> WaitForLoadingAds()
		{
			noteStart.SetLoading(true);
			yield return Timing.WaitForSeconds(0.5f);
			noteStart.SetLoading(false);
		}

		public void PauseGame()
		{
			currentState = GameState.Paused;
		}

		public bool CanContinue()
		{
			if (CurrentGameStage != GameState.AutoPlay)
			{
				return numContinue < GameManager.Instance.GameConfigs.continuePrices.Count;
			}
			else
			{
				return false;
			}
		}

		public int GetPriceForContinuePlaying()
		{
			if (numContinue < GameManager.Instance.GameConfigs.continuePrices.Count)
			{
				return GameManager.Instance.GameConfigs.continuePrices[numContinue];
			}
			return 15; // This should never happen
		}

		/// <summary>
		/// Continue playing the game
		/// </summary>
		/// <param name="afterPausing">Is this continue comes after pausing the game? If not, the game will increase continue counter</param>
		public void ContinueGame(bool afterPausing = true)
		{
			if (!afterPausing)
			{
				++numContinue;
				// Increasing speed after continue game
				if (currentRunSpeed > speedAfterContinue)
				{
					IncreaseGameSpeed(speedAfterContinue, targetSpeed, 5);
				}
			}

			//Continue game after some delay to make sure any UI animation has been completed
			Timing.RunCoroutine(C_RoutineContinueGame());
		}
		private IEnumerator<float> C_RoutineContinueGame()
		{
			yield return Timing.WaitForSeconds(0.5f);
			currentState = GameState.Continue;
		}

		public void GameplayReady()
		{
			// Annouce gameplay ready
			Helpers.Callback(OnGameReady);
		}

		#region Function for new UI gameplay
		public void StartGame()
		{
			if (GameManager.Instance.SessionData.listen)
			{
				SetAutoPlayUI();
				currentRunSpeed = targetSpeed;
				currentState = GameState.AutoPlay;
				autoPlayForExpCalc = true;
			}
			else
			{
				currentState = GameState.Playing;
				autoPlayForExpCalc = false;

				if (GameManager.Instance.SessionData.endless &&
					GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.NORMAL)
				{
					HideEndlessLabel();
				}
			}

			Helpers.Callback(OnGameStarted);
		}
		public GameState CurrentGameStage
		{
			get
			{
				return currentState;
			}
		}

		public void ChangeStatusToGameOver()
		{
			currentState = GameState.GameOver;
		}

		#endregion

		public int GetTotalTitles()
		{
			return listTilesData != null ? listTilesData.Count : 0;
		}

		#region GM Command
		public static void GMSetGameSpeed(float newSpeed)
		{
			if (instance != null)
			{
				instance.currentRunSpeed = newSpeed;
			}
			Debug.Log("Current game speed changed into: " + newSpeed);
		}

		public static void GMSetAutoPlay(bool shouldAutoPlay)
		{
			GameManager.Instance.SessionData.autoplay = shouldAutoPlay;
			Debug.Log("Game auto play changed into " + shouldAutoPlay);
		}

		public static void GMPauseGame()
		{
			instance.currentState = GameState.Paused;
		}

		public static void GMContinueGame()
		{
			instance.currentState = GameState.Continue;
		}
		#endregion
	}
}
