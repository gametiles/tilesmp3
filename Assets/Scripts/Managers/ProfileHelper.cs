using UnityEngine;
using Amanotes.Utils.MessageBus;
using System;
using Parse;
using Facebook.Unity;
using System.Threading.Tasks;
using FullSerializer;
using Amanotes.Utils;
using System.Collections.Generic;
using MovementEffects;

namespace Amanotes.PianoChallenge
{
	public class ProfileHelper : SingletonMono<ProfileHelper>
	{
		private const string USER_DATA_FILE = "userdata";
		private const string PARSE_VERSION = "Parse version";

		private static Message MSG_DIAMOND_CHANGED = new Message(MessageBusType.DiamondChanged);
		private static Message MSG_LIFE_CHANGED = new Message(MessageBusType.LifeChanged);
		private static Message MSG_STARS_CHANGED = new Message(MessageBusType.SongRecordChanged);
		private static Message MSG_DATA_CHANGED = new Message(MessageBusType.UserDataChanged);
		private static string deviceId;

		private UserDataModel rawUserData;

		private UserDataModel RawUserData
		{
			get { return rawUserData; }
			set
			{
				rawUserData = value;
				if (value != null)
				{
					isUserDataLoaded = true;
				}
			}
		}

		private bool isInitialized = false;
		private bool isUserDataLoaded = false;

		//used when fetching user data from facebook to add into Parse
		private bool isParseUserLoggedIn = false;

		//a variable to store temporary user data before and after logged into facebook
		private string userdataBeforeFB, userdataAfterFB;

		//used to delay push operation, avoid overload on server
		private bool isUserDataChanged = false;
		private Timing timer;
		private float timeToPushData;
		private float lastTimePushData;
		private string userdataWritePath;

		void Awake()
		{
			userdataWritePath = FileUtilities.GetWritablePath(USER_DATA_FILE);
		}

		void OnApplicationPause(bool isPaused)
		{
			if (IsUserDataLoaded)
			{
				SaveUserData();
				PushUserData();
			}
		}

		public void Initialize(Action<bool> OnInitializeCompleted = null)
		{
			deviceId = GameManager.Instance.DeviceID;
			MessageBus.Instance.Subscribe(MessageBusType.FacebookLoggedIn, OnFacebookLoggedIn);

			Helpers.CallbackWithValue(OnInitializeCompleted, true);
			lastTimePushData = Time.time;
			timeToPushData = GameManager.Instance.GameConfigs.timeSyncData;
			if (timer == null)
			{
				timer = gameObject.AddComponent<Timing>();
				timer.TimeBetweenSlowUpdateCalls = timeToPushData;
				timer.RunCoroutineOnInstance(CheckPushUserData(), Segment.SlowUpdate);
			}
			else
			{
				timer.KillCoroutineOnInstance(CheckPushUserData());
				timer.RunCoroutineOnInstance(CheckPushUserData(), Segment.SlowUpdate);
			}
		}

		IEnumerator<float> CheckPushUserData()
		{
			while (true)
			{
				yield return 0;
				if (isUserDataChanged)
				{
					//only proceed if user is not playing game or initializing game to prevent hiccup
					if (SceneManager.Instance.CurrentScene != ProjectConstants.Scenes.MainGame &&
						SceneManager.Instance.CurrentScene != ProjectConstants.Scenes.OnlineMainGame &&
						SceneManager.Instance.CurrentScene != ProjectConstants.Scenes.SplashScreen)
					{
						PushUserData();
						isUserDataChanged = false;
					}
				}
			}
		}

		public void SaveLocalUserData()
		{
		    RegisterOrLoginParse(deviceId, deviceId);
		    RawUserData.lastsyncDevice = deviceId;

			SaveUserData();
		}

		private void RegisterOrLoginParse(string username, string password)
		{
		}

		public void IncreaseDataVersion()
		{
			RawUserData.lastsyncDevice = deviceId;
			RawUserData.dataVersion += 1;
		}

		/// <summary>
		/// Push user data from local device onto cloud
		/// </summary>
		public void PushUserData()
		{
			if (rawUserData == null)
			{
				Debug.LogWarning("Trying to push null data. No-go.");
				return;
			}

			isUserDataChanged = false;
			RawUserData.lastsyncDevice = deviceId;

			SaveLocalUserData();
		}

		public bool IsUserDataLoaded
		{
			get { return isUserDataLoaded; }
		}

		internal bool HasConnectedFacebook()
		{
			return rawUserData.isLoggedInFacebook;
		}

		/// <summary>
		/// Load user data from local storage
		/// </summary>
		/// <param name="OnInitializeCompleted"></param>
		public void InitializeUserData(Action<bool> OnInitializeCompleted)
		{
			LoadLocalUserData();

			
			isInitialized = true;
			Helpers.CallbackWithValue(OnInitializeCompleted, true);
		}

		public void SaveUserData(bool alsoSaveToParse = false)
		{
			IncreaseDataVersion();
			string json = SerializedUserData();
			FileUtilities.SaveFileWithPassword(json, userdataWritePath, GameManager.ENCRYPTION_KEY, true);
		}

		internal void LoadLocalUserData()
		{
			string json = FileUtilities.LoadFileWithPassword(userdataWritePath, GameManager.ENCRYPTION_KEY, true);
			DeserializeUserData(json);
		}

		public void LoginFacebook()
		{
			if (!FacebookManager.Instance.IsLogin())
			{
				AnalyticsHelper.Instance.LogFBLogin();
				FacebookManager.Instance.FBLogin(OnFBLoggedIn);
			}
		}

		public void LogoutFacebook()
		{
			if (FacebookManager.Instance.IsLogin())
			{
				FacebookManager.Instance.FBLogout();
				if (ParseUser.CurrentUser != null)
				{
					ParseUser.LogOutAsync();
				}
				UnlinkUserData();
				isUserDataChanged = true;
				MessageBus.Annouce(new Message(MessageBusType.FacebookLoggedOut));
			}
		}


		private void OnFBLoggedIn(CallbackFromFacebook cb)
		{
			MessageBus.Annouce(new Message(MessageBusType.FacebookLoggedIn));
			if (cb.result_code == 1)
			{
				AnalyticsHelper.Instance.LogFacebookLoggedIn();
			}
			else
			{
				Debug.LogWarning("Log in facebook failed");
			}
		}

		private void AnnouceDataChanged()
		{
			MessageBus.Annouce(MSG_DIAMOND_CHANGED);
			MessageBus.Annouce(MSG_LIFE_CHANGED);
			MessageBus.Annouce(MSG_STARS_CHANGED);
			MessageBus.Annouce(MSG_DATA_CHANGED);
		}

		private void SolveUserDataConflict(bool localChosen)
		{
			Debug.Log("Solve data conflicted by using data from...");
			if (localChosen)
			{
				Debug.Log("...local device");
			}
			else
			{
				Debug.Log("...remote server");

				if (UpdateUserDataFromString(userdataAfterFB))
				{
					Debug.Log("New user data parsed");
					isInitialized = true;
				}
				else
				{
					Debug.Log("User data failed to parse. Will use old data");
				}
			}

			LinkUserData();
		}

		public void MarkUserDataChanged()
		{
			if (rawUserData != null)
			{
				isUserDataChanged = true;
			}
		}

		/// <summary>
		/// Parse userdata model from provided string and try to set it to current user data if success, then return the result.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private bool UpdateUserDataFromString(string data)
		{
			ParseUser.CurrentUser[GameConsts.PK_DATA] = data;
			UserDataModel userData = ParseUserDataModel(data);
			if (userData == null)
			{
				return false;
			}
			else
			{
				RawUserData = userData;

				string property = AchievementPropertiesCSV;
				string achievement = AchievementUnlockedAndClaimedCSV;
				if (!string.IsNullOrEmpty(property) && !string.IsNullOrEmpty(achievement))
				{
					AchievementHelper.Instance.RestoreAchievementDataFromDump(property, achievement);
				}
				else
				{
					Debug.LogWarning("Achievement data is null or empty when trying to solve user data conflict. Skipping...");
				}
				AnnouceDataChanged();
				return true;
			}
		}

		public void OnFacebookUserDataReceived()
		{
			if (ParseUser.CurrentUser != null)
			{
				if (isParseUserLoggedIn)
				{
					Debug.Log("Setting user data");
					ParseUser.CurrentUser[GameConsts.PK_USERNAME] = FacebookManager.Instance.UserDisplayName;
					ParseUser.CurrentUser[GameConsts.PK_USER_EMAIL] = FacebookManager.Instance.UserEmail;
					ParseUser.CurrentUser[GameConsts.PK_FB_AVATAR_URL] = FacebookManager.Instance.AvatarURL;
					ParseUser.CurrentUser[GameConsts.PK_FB_COVER_URL] = FacebookManager.Instance.CoverURL;
					ParseUser.CurrentUser[GameConsts.PK_USER_ID] = FacebookManager.Instance.UserID;
					isUserDataChanged = true;
				}
			}
		}

		private void OnFacebookLoggedIn(Message message)
		{
			if (AccessToken.CurrentAccessToken == null)
			{
				return;
			}

			if (ParseUser.CurrentUser != null && ParseUser.CurrentUser.ContainsKey(GameConsts.PK_DATA))
			{
				userdataBeforeFB = ParseUser.CurrentUser[GameConsts.PK_DATA] as string;
			}
			else
			{
				userdataBeforeFB = GetJSONString(rawUserData);
			}
			LoginWithParse();
		}

		public Task LoginWithParse()
		{
			var taskloginfb = ParseFacebookUtils.LogInAsync(
				AccessToken.CurrentAccessToken.UserId,
				AccessToken.CurrentAccessToken.TokenString,
				AccessToken.CurrentAccessToken.ExpirationTime);
			return taskloginfb.ContinueWith(OnParseUserInitialized);
		}

		private void OnCloudDataReceived(CloudDataModel cloudData)
		{

		}

		private void PushCloudData()
		{
			if (AccessToken.CurrentAccessToken != null)
			{
				var parameters = new Dictionary<string, object> {
						{ "userId", AccessToken.CurrentAccessToken.UserId },
						{ "userName", FacebookManager.Instance.UserDisplayName },
						{ "crowns",  HighScoreManager.Instance.TotalCrowns },
						{ "stars",  HighScoreManager.Instance.TotalStars },
						{ "highScores", rawUserData.listHighscore },
						{ "exp", rawUserData.totalExp },
						{ "level", rawUserData.level },
						{ "life", rawUserData.life },
						{ "diamonds", rawUserData.diamond },
						{ "superSongs", null },
						{ "boughtSongs",  rawUserData.listBoughtSongs },
						{ "favoriteSongs",  rawUserData.listFavoriteSongs },
						{ "lifeCountDownStartTime", rawUserData.timeStartCountdown },
						{ "achievementPropertyData", rawUserData.achievementPropertiesValueCSV },
						{ "achievementUnlockData",  rawUserData.achievementUnlockedAndClaimedCSV },
						{ "wins", rawUserData.wins },
						{ "loses", rawUserData.loses },
						{ "battleRating", rawUserData.battleRating },
						{ "supersongCompensated", 1 },
						{ "lastsyncDevice", rawUserData.lastsyncDevice },
						{ "dataVersion", rawUserData.dataVersion },
					};

                fModStudio.Utils.CallParseFunction("UpdatePlayerData", parameters);
			}
		}

		private void PullCloudData()
		{
			var parameters = new Dictionary<string, object> {
				{ "userId", AccessToken.CurrentAccessToken.UserId },
			};

            fModStudio.Utils.CallParseFunction("GetPlayerData", parameters, task => {
				if (task.IsFaulted)
				{
					return;
				}

				fsData responseData = null;
				FileUtilities.JSONSerializer.TrySerialize(task.Result as IDictionary<string, object>, out responseData);
				CloudDataModel cloudData = null;
				fsResult fsresult = FileUtilities.JSONSerializer.TryDeserialize(responseData, ref cloudData);

				if (fsresult.Failed || cloudData == null)
				{

					return;
				}

				OnCloudDataReceived(cloudData);
			});
		}

		private void OnParseUserInitialized(Task<ParseUser> parseUserInitializeTask)
		{
			if (!parseUserInitializeTask.IsCompleted)
			{
				return;
			}

			if (parseUserInitializeTask.IsFaulted || parseUserInitializeTask.IsCanceled)
			{
				Debug.Log("Could NOT get user data from parse server");
				return;
			}

			DoOnMainThread.Instance.QueueOnMainThread(() =>
			{
				var parseUser = parseUserInitializeTask.Result;
				if (FacebookManager.Instance.IsUserInfoAvailable)
				{
					parseUser[GameConsts.PK_USERNAME] = FacebookManager.Instance.UserDisplayName;
					parseUser[GameConsts.PK_USER_EMAIL] = FacebookManager.Instance.UserEmail;
					parseUser[GameConsts.PK_FB_AVATAR_URL] = FacebookManager.Instance.AvatarURL;
					parseUser[GameConsts.PK_FB_COVER_URL] = FacebookManager.Instance.CoverURL;

					if (!ParseUser.CurrentUser.ContainsKey(GameConsts.PK_USER_ID))
					{
						isParseUserLoggedIn = true;
					}
				}
				else
				{
					isParseUserLoggedIn = true;
				}

				//check if the user has user data on it, if yes, may be we need to solve conflict
				if (!parseUser.ContainsKey(GameConsts.PK_DATA))
				{
					LinkUserData();
					CurrentDiamond += GameManager.Instance.GameConfigs.firstLoginDiamonds;
					return;
				}

				// Check if cloud save is valid or same as the local one
				userdataAfterFB = (string)(parseUser[GameConsts.PK_DATA]);
				if (string.IsNullOrEmpty(userdataAfterFB) || userdataAfterFB.Contains(userdataBeforeFB))
				{
					print("Either parse data is null or current data is the same with remote one");
					LinkUserData();
					isInitialized = true;
					return;
				}

				UserDataModel remoteData = ParseUserDataModel(userdataAfterFB);
				if (RawUserData == null)
				{
					UpdateUserDataFromString(userdataAfterFB);
					RawUserData.lastsyncDevice = deviceId;
					LinkUserData();
					isInitialized = true;
					return;
				}

				Debug.Log("Conflict happened at user data");
				var info = new ConflictInfo();
				info.localSave = RawUserData;
				info.cloudSave = remoteData;
				info.onLocalChosen = delegate
				{
					SolveUserDataConflict(true);
				};

				info.onCloudChosen = delegate
				{
					SolveUserDataConflict(false);
				};
				SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.ProfileConflict, info);
			});
		}

		private void LinkUserData()
		{
			rawUserData.isLoggedInFacebook = true;
			isUserDataChanged = true;
			MessageBus.Annouce(new Message(MessageBusType.UserDataLinked));

			FacebookManager.Instance.LoadFriends();
		}

		private void UnlinkUserData()
		{
			rawUserData.isLoggedInFacebook = false;
			isUserDataChanged = true;
		}

		/// <summary>
		/// Convert current user data into a json object for saving purpose
		/// </summary>
		public string SerializedUserData()
		{
			return GetJSONString(rawUserData);
		}

		/// <summary>
		/// Parse user data from a previous backup json object
		/// </summary>
		public void DeserializeUserData(string json)
		{
			//initialize user data if input string is empty
			if (string.IsNullOrEmpty(json))
			{
				RawUserData = PrepareNewUserData();
			}
			else
			{
				UserDataModel data = ParseUserDataModel(json);
				if (data == null)
				{
					Debug.LogError("Could not de-serialize user data...");
				}
				else
				{
					RawUserData = data;
				}
			}
		}

		private UserDataModel PrepareNewUserData()
		{
			UserDataModel ud = new UserDataModel();
			ud.life = MaxLife;
			ud.diamond = 20;
			ud.listHighscore = new List<ScoreItemModel>();
			ud.lastsyncDevice = deviceId;
			ud.listBoughtSongs = new List<string>();
			ud.listFavoriteSongs = new List<string>();

			return ud;
		}

		private string GetJSONString(UserDataModel data)
		{
			fsData jsonData;
			FileUtilities.JSONSerializer.TrySerialize(data, out jsonData);
			string json = jsonData.ToString();
			return json;
		}

		private UserDataModel ParseUserDataModel(string json)
		{
			UserDataModel data = new UserDataModel();
			if (!string.IsNullOrEmpty(json))
			{
				fsData jsonData = fsJsonParser.Parse(json);
				fsResult result = FileUtilities.JSONSerializer.TryDeserialize(jsonData, ref data);
				if (result.Failed)
				{
					return null;
				}
				else
				{
					return data;
				}
			}
			else
			{
				data.listHighscore = new List<ScoreItemModel>();
				return data;
			}
		}

		#region Get User information

		public List<string> ListBoughtSongs
		{
			get { return rawUserData == null ? null : rawUserData.listBoughtSongs; }
		}

		public List<string> ListFavoriteSongs
		{
			get
			{
				if (rawUserData.listFavoriteSongs == null)
				{
					rawUserData.listFavoriteSongs = new List<string>();
				}
				return rawUserData.listFavoriteSongs;
			}
		}


		public double LastLifeCountdownUnixTimeStamp
		{
			get { return isInitialized ? rawUserData.timeStartCountdown : 0; }
			set
			{
				if (isInitialized)
				{
					if (rawUserData.timeStartCountdown != value)
					{
						rawUserData.timeStartCountdown = value;
						isUserDataChanged = true;
					}
				}
			}
		}

		private int oldLife = 0;

		public int CurrentLife
		{
			get
			{
				return isInitialized ? rawUserData.life : 0;
			}

			set
			{
				if (isInitialized)
				{
					oldLife = rawUserData.life;
					rawUserData.life = value;
					if (oldLife != value)
					{
						isUserDataChanged = true;
						MessageBus.Annouce(MSG_LIFE_CHANGED);
					}
				}
			}
		}

		public int MaxLife
		{
			get
			{
				if (isInitialized)
				{
					return rawUserData.isLoggedInFacebook ?
						GameManager.Instance.GameConfigs.maxLifeLinkedUser :
						GameManager.Instance.GameConfigs.maxLifeGuestUser;
				}
				else
				{
					return 20;
				}
			}
		}

		// Battle info
		public int Wins
		{
			get { return isInitialized ? rawUserData.wins : 0; }
			set { if (isInitialized) rawUserData.wins = value; }
		}

		public int Loses
		{
			get { return isInitialized ? rawUserData.loses : 0; }
			set { if (isInitialized) rawUserData.loses = value; }
		}

		public int BattleRating
		{
			get { return isInitialized ? rawUserData.battleRating : 0; }
			set { if (isInitialized) rawUserData.battleRating = value; }
		}

		public int CurrentDiamond
		{
			get
			{
				return isInitialized ? rawUserData.diamond : 0;
			}

			set
			{
				if (isInitialized)
				{
					if (rawUserData.diamond == value)
					{
						return;
					}

					rawUserData.diamond = value;
					// Saving user data after changing diamond
					if (SceneManager.Instance.CurrentScene != ProjectConstants.Scenes.MainGame &&
						SceneManager.Instance.CurrentScene != ProjectConstants.Scenes.OnlineMainGame)
					{
						SaveLocalUserData();
					}
					isUserDataChanged = true;
					MessageBus.Annouce(MSG_DIAMOND_CHANGED);
				}
			}
		}

		public string AchievementPropertiesCSV
		{
			get
			{
				return isInitialized ? rawUserData.achievementPropertiesValueCSV : string.Empty;
			}

			set
			{
				if (isInitialized)
				{
					rawUserData.achievementPropertiesValueCSV = value;
				}
			}
		}

		public string AchievementUnlockedAndClaimedCSV
		{
			get
			{
				return isInitialized ? rawUserData.achievementUnlockedAndClaimedCSV : "";
			}

			set
			{
				if (isInitialized)
				{
					rawUserData.achievementUnlockedAndClaimedCSV = value;
				}
			}
		}

		public List<ScoreItemModel> ListHighScore
		{
			get
			{
				return isInitialized ? rawUserData.listHighscore : null;
			}

			set
			{
				if (isInitialized)
				{
					rawUserData.listHighscore = value;
				}
			}
		}

		public int TotalStars
		{
			get
			{
				return HighScoreManager.Instance.TotalStars;
			}
		}

		public int TotalCrowns
		{
			get
			{
				return HighScoreManager.Instance.TotalCrowns;
			}
		}

		public bool IsLoggedInFacebook
		{
			get
			{
				return (isUserDataLoaded && (RawUserData.isLoggedInFacebook || FacebookManager.Instance.IsLogin()));
			}
		}

		public string Email
		{
			get
			{
				if (IsLoggedInFacebook)
				{
					return FacebookManager.Instance.UserEmail;
				}

				return string.Empty;
			}
		}

		#endregion

		#region Level
		public int Level
		{
            //get { return rawUserData != null ? rawUserData.level : -1; }
            get { return 100; }
		}

		public int TotalExp
		{
			get { return rawUserData != null ? rawUserData.totalExp : -1; }
		}

		public List<int> AddExpForUser(int expAdd, ConfigUserLevel cofigLv)
		{
			List<int> levelUp = new List<int>();
			ConfigUserLevelItem configLvItemNextLevel = cofigLv.GetConfigUserByLevel(RawUserData.level + 1);
			if (configLvItemNextLevel == null) return levelUp;

			if (expAdd > 0)
			{
				if (RawUserData.totalExp <= configLvItemNextLevel.EXPInTotal || configLvItemNextLevel.ExpNeedtoNextLevel > 0)
					RawUserData.totalExp += expAdd;

				if (RawUserData.totalExp >= configLvItemNextLevel.EXPInTotal && configLvItemNextLevel.ExpNeedtoNextLevel > 0)
				{
					RawUserData.level += 1;
					levelUp.Add(RawUserData.level);
					configLvItemNextLevel = cofigLv.GetConfigUserByLevel(RawUserData.level + 1);

					if (configLvItemNextLevel != null)
					{
						if (RawUserData.totalExp >= configLvItemNextLevel.EXPInTotal && configLvItemNextLevel.ExpNeedtoNextLevel > 0)
						{
							RawUserData.totalExp = configLvItemNextLevel.EXPInTotal - 1;
						}

					}
				}
			}

			return levelUp;
		}

		private int SetForceLevel(int level)
		{
			Debug.Log("MaxLevel " + GameManager.Instance.configUserLevel.GetLevelMax());
			if (level >= GameManager.Instance.configUserLevel.GetLevelMax())
			{
				level = GameManager.Instance.configUserLevel.GetLevelMax() - 1;
			}

			level = level <= 0 ? 1 : level;


			ConfigUserLevelItem configLvItem = GameManager.Instance.configUserLevel.GetConfigUserByLevel(level);
			RawUserData.level = level;
			RawUserData.totalExp = configLvItem.EXPInTotal;
			SaveLocalUserData();

			return level;
		}

		public int CalculateLevel()
		{
			//add level
			if (Level != 1 || TotalExp != 0)
			{
				return 0;
			}

			List<ScoreItemModel> listHighscore = RawUserData.listHighscore;
			int _totalStar = 0;
			for (int i = 0; i < listHighscore.Count; i++)
			{
				_totalStar += Mathf.Clamp(listHighscore[i].highestStar, 0, 3);
			}

			StoreDataModel storeData = GameManager.Instance.StoreData;

			SongDataModel SongMaxStar = storeData.listAllSongs[0];
			for (int i = 0; i < storeData.listAllSongs.Count; i++)
			{
				if (storeData.listAllSongs[i].starsToUnlock <= _totalStar &&
					storeData.listAllSongs[i].starsToUnlock >= 0)
				{
					SongMaxStar = storeData.listAllSongs[i];
					break;
				}
			}

			for (int i = 0; i < storeData.listAllSongs.Count; i++)
			{
				if (storeData.listAllSongs[i].starsToUnlock > SongMaxStar.starsToUnlock &&
					storeData.listAllSongs[i].starsToUnlock <= _totalStar &&
					storeData.listAllSongs[i].starsToUnlock >= 0)
				{
					SongMaxStar = storeData.listAllSongs[i];
				}
			}

			if (SongMaxStar.LvToUnlock < 1)
			{
				return 0;
			}

			int lv = SetForceLevel(SongMaxStar.LvToUnlock);
			SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.MessageForceLevelPopup, lv);
			return lv;
		}
		#endregion

		/// <summary>
		/// Save current value of achievement system into hard disk
		/// </summary>
		public void SaveAchievementValue()
		{
			string propertyData = AchievementHelper.Instance.DumpCurrentProperties();
			if (!string.IsNullOrEmpty(propertyData))
			{
				AchievementPropertiesCSV = propertyData;
			}

			string achievementData = AchievementHelper.Instance.DumpUnlockedAndClaimedAchievements();
			if (!string.IsNullOrEmpty(achievementData))
			{
				AchievementUnlockedAndClaimedCSV = achievementData;
			}
		}

		#region variable online

#pragma warning disable 618
		[Obsolete("ruby la bien local khong dung de goi o bat ky dau, phai su dung Ruby")]
		public long last_time_disconnect = 0;

		[fsIgnore]
		public long LastTimeDisconnect
		{
			get
			{
				if (last_time_disconnect < 0)
				{
					last_time_disconnect = 0;
				}
				return last_time_disconnect;
			}
			set
			{
				if (last_time_disconnect != value)
				{
					last_time_disconnect = value;
				}
			}
		}

		#endregion
	}
}
