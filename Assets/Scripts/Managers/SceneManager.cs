using UnityEngine;
using ProjectConstants;
using System;
using System.Collections.Generic;
using MovementEffects;
using Amanotes.Utils.MessageBus;

namespace Amanotes.PianoChallenge
{
	public class SceneManager : SingletonMono<SceneManager>
	{
		public Scenes CurrentScene { get; private set; }

		public Scenes BeforeScene { get; private set; }

		public GroupHomeSceneType currentGroupHomeUI;

		[SerializeField] GameObject gmCommand = null;

		public MainMenuController MainMenu { get { return mainMenu; } }
		private MainMenuController mainMenu = null;
		private bool isMenuReady = false;

		//check condition show rating popup
		private int countingEndGame = 0;
		private int ratingCondition = 0;
		private int defaultRatingCondition = 5;
		private const string PLAYERPREF_SHOWRATING = "showRatingInGame";
		private bool isShow;
		private bool canChangeScene = true;

		public event Action<Scenes> onSceneChange;

		public string LastOpenSceneName { get; private set; }
		private static Dictionary<string, MainMenuState> menuStateByScene;

		public void Initialize()
		{
#if DEVELOPMENT_BUILD || UNITY_EDITOR // Always cheat in development build
			ShowGmCommand();
#endif
			menuStateByScene = new Dictionary<string, MainMenuState>(15);
			//full menu
			menuStateByScene.Add(Scenes.HomeUI.GetName(), MainMenuState.ShowAll);

			//only top menu
			menuStateByScene.Add(Scenes.ResultUI.GetName(), MainMenuState.ShowBGAndTop);
            menuStateByScene.Add(Scenes.PSResultUI.GetName(), MainMenuState.ShowBGAndTop);

			//no menu at all
			menuStateByScene.Add(Scenes.MainGame.GetName(), MainMenuState.HideAll);
            menuStateByScene.Add(Scenes.PSGameplay.GetName(), MainMenuState.HideAll);
            menuStateByScene.Add(Scenes.OnBoarding.GetName(), MainMenuState.ShowBGOnly);

			SSSceneManager.Instance.onSceneFocus += OnSceneOpened;

			//check rating
			int onRating = PlayerPrefs.GetInt(PLAYERPREF_SHOWRATING);
			isShow = (onRating == 1) ? true : false;
		}

		public void RegisterMenu(MainMenuController menu)
		{
			mainMenu = menu;
			isMenuReady = true;
		}

		public void ShowGmCommand()
		{
			gmCommand.SetActive(true);
		}

		public void HideGmCommand()
		{
			gmCommand.SetActive(false);
		}

		private void OnSceneOpened(string sceneName)
		{
			LastOpenSceneName = sceneName;
			SetupMainMenu(sceneName);
		}


		private IEnumerator<float> IEOpenScene(Scenes scene, object data)
		{
			canChangeScene = false;
			if (onSceneChange != null)
				onSceneChange(scene);
			yield return Timing.WaitForSeconds(0.12f);
			OnProgressChangeScene(scene, data);
			canChangeScene = true;
		}


		/// <summary>
		/// wait for run effect life
		/// </summary>
		/// <param name="scene"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		private IEnumerator<float> IEOpenMainGame(Scenes scene, object data)
		{
			MessageBus.Annouce(new Message(MessageBusType.PlaySongAction));
			yield return Timing.WaitForSeconds(0.3f);
			OnProgressChangeScene(scene, data);
		}

		/// <summary>
		/// Open specified scene
		/// </summary>
		/// <param name="scene"></param>
		public void OpenScene(Scenes scene, object data = null, bool playAnm = false)
		{
			if (CurrentScene != scene)
			{
				if (scene == Scenes.MainGame)
				{
					Timing.RunCoroutine(IEOpenMainGame(scene, data));
				}
				else
				{
					if (playAnm)
					{
						if (!canChangeScene)
							return;
						Timing.RunCoroutine(IEOpenScene(scene, data));
					}
					else
					{
						OnProgressChangeScene(scene, data);
					}
				}
			}
		}

		private void OnProgressChangeScene(Scenes scene, object data)
		{
			BeforeScene = CurrentScene;
			CurrentScene = scene;
			SSSceneManager.Instance.Screen(scene.GetName(), data);
			Utils.AnalyticsHelper.Instance.LogOpenScene(scene.GetName());

			if (scene == Scenes.ResultUI || scene == Scenes.PSResultUI)
			{
				Utils.AdHelper.Instance.ShowInterstitial();
			}
		}

		private IEnumerator<float> IECheckShowRatingPopUp()
		{
			yield return Timing.WaitForSeconds(1.5f);
			CheckShowRatingInGame();
		}

		public void CheckShowRatingInGame()
		{
			//check rating
			if (!isShow)
			{
				if (ratingCondition == 0)
				{
					if (GameManager.Instance.GameConfigs.ratingCondition > 0)
						ratingCondition = GameManager.Instance.GameConfigs.ratingCondition;
					else
						ratingCondition = defaultRatingCondition;
				}

				countingEndGame++;
				if (countingEndGame >= ratingCondition)
				{
					OpenPopup(Scenes.RatePopUp);
					isShow = true;
					PlayerPrefs.SetInt(PLAYERPREF_SHOWRATING, 1);
					PlayerPrefs.Save();
				}

			}
		}

		public void SetupMainMenu(string sceneName)
		{
			StartCoroutine(AdjustMainMenu(sceneName));
		}

		private IEnumerator<float> AdjustMainMenu(string sceneName)
		{

			while (!isMenuReady)
			{
				yield return Timing.WaitForSeconds(0.05f);
			}

			if (menuStateByScene.ContainsKey(sceneName))
			{
				MainMenuState state;
				if (GroupHomeSceneManager.Instance.currentPanel == GroupHomeSceneType.SettingUI &&
					sceneName == Scenes.HomeUI.GetName())
				{
					state = MainMenuState.ShowAll;
				}
				else
				{
					state = menuStateByScene[sceneName];
				}
				mainMenu.SetMenuState(state);
				mainMenu.ResetToggleMainMenu(CurrentScene);
			}
		}

		public void OpenPopup(Scenes popup, object data = null)
		{
			Utils.AnalyticsHelper.Instance.LogOpenScene(popup.GetName());
			SSSceneManager.Instance.PopUp(popup.GetName(), data);
		}

		public void CloseScene()
		{
			SSSceneManager.Instance.Close();
		}

		public void LoadMenu()
		{
			SSSceneManager.Instance.LoadMenu(Scenes.MainMenu.GetName());
		}

		/// <summary>
		/// Show confirmation message before exiting the game
		/// </summary>
		public void OpenExitConfirmation()
		{
			OpenPopup(Scenes.ExitPopup);
		}

		public void SetLoadingVisible(bool isActive = true)
		{
			if (isActive)
			{
				SSSceneManager.Instance.ShowLoading();
			}
			else
			{
				SSSceneManager.Instance.HideLoading();
			}
		}
	}
}
