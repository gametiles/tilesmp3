using FullSerializer;
using Parse;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Amanotes.PianoChallenge
{
	public class LeaderboardManager : Singleton<LeaderboardManager>
	{
		private static readonly string LOCAL_RANKING = "global_rank";
		private static readonly double MAX_REQUEST_INTERVAL_IN_SECONDS = 60 * 5; // 5 minutes
		private enum Status { NOT_LOADED, LOADING, LOADED };

		private GlobalRankingUser globalRanking = null;
		private DateTime lastRequestTime = new DateTime(1970, 1, 1);

		public bool RankingLoaded { get { return status == Status.LOADED && globalRanking != null; } }
		public List<GlobalRankingModel> Ranks { get { return RankingLoaded ? globalRanking.data : null; } }
		public int MyRank { get { return RankingLoaded ? globalRanking.index_me + 1 : 0; } }

		private Status status = Status.NOT_LOADED;

		public void RequestRankingData(Action<bool> OnRankingDataReceived)
		{
			if (status == Status.LOADING)
			{
				return;
			}

			// If we already have rank data, use it for short time to avoid spamming request
			if (status == Status.LOADED)
			{
				double secondsSinceLastRequest = (DateTime.Now - lastRequestTime).TotalSeconds;
				if (secondsSinceLastRequest < MAX_REQUEST_INTERVAL_IN_SECONDS)
				{
					OnRankingDataReceived(true);
					return;
				}
			}

			status = Status.LOADING;

			lastRequestTime = DateTime.Now; // Save timestamp

			// Request ranking data from server
			IDictionary<string, object> dictionary = new Dictionary<string, object> {
				{ "score", ProfileHelper.Instance.TotalCrowns }
			};
			ParseCloud.CallFunctionAsync<object>("GetGlobalRanking", dictionary).
			ContinueWith(t =>
			{
				DoOnMainThread.Instance.QueueOnMainThread(() =>
				{
					Debug.Log("Go reqest");
					if (!t.IsFaulted)
					{

						var result = t.Result;

						fsData data = null;
						FileUtilities.JSONSerializer.TrySerialize(result as IDictionary<string, object>, out data);
						string json = data.ToString();
						GlobalRankingUser globalRanking = new GlobalRankingUser();
						fsData jsonData = fsJsonParser.Parse(json);
						fsResult fsresult = FileUtilities.JSONSerializer.TryDeserialize(jsonData, ref globalRanking);
						if (fsresult.Failed || globalRanking == null)
						{
							LoadLocalRanking(OnRankingDataReceived);
						}
						else
						{
							FileUtilities.SerializeObjectToFile(globalRanking, LOCAL_RANKING);
							status = Status.LOADED;
							OnRankingDataReceived(true);
						}
					}
					else
					{
						LoadLocalRanking(OnRankingDataReceived);
						Debug.LogError("fail");
					}
				});
			});
		}

		private void LoadLocalRanking(Action<bool> OnFinishedRequestParseCloud)
		{
			Debug.LogError("Load Local ranking");
			globalRanking = FileUtilities.DeserializeObjectFromFile<GlobalRankingUser>(LOCAL_RANKING);

			if (globalRanking.data != null && globalRanking.data.Count > 0)
			{
				globalRanking.data.RemoveAt(0); // My ranking is not needed;
				status = Status.LOADED;
				OnFinishedRequestParseCloud(true);
			}
			else
			{
				status = Status.NOT_LOADED;
				OnFinishedRequestParseCloud(false);
			}
		}
	}
}
