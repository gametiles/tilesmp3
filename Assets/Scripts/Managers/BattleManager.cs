using Amanotes.Utils;
using Parse;
using System;
using UnityEngine;

namespace Amanotes.PianoChallenge
{
	public class BattleManager
	{
		const string DAILY_BATTLE_EVENT_KEY = "Daily Battle Event";
		const string DAILY_BATTLE_REWARD = "DailyBattleReward";

		public string sceneName = "";
		public string stepName = "";
		public string friendRoomCode = null;
		public bool goingToBattle = false; // flag that we need to go to battle or not

		private DateTime lastBattleDate = new DateTime(1970, 1, 1);
		private int lastDailyBattles = 0;
		private int lastDailyFriendBattles = 0;
		private bool dailyBattleRewardClaimed = false;

		private int dailyEventSent = 1;

		static private BattleManager instance = null;

		static public BattleManager Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new BattleManager();
				}
				return instance;
			}
		}

		public void OnAppResume()
		{
			Reload();
		}

		public void Reload()
		{
			LoadDailyReward();
			dailyEventSent = PlayerPrefs.GetInt(DAILY_BATTLE_EVENT_KEY);
		}

		BattleManager()
		{
			Reload();
		}

		private void SaveDailyReward()
		{
			string s =
				lastBattleDate.ToString("yyyy-MM-dd") + "_" +
				lastDailyBattles + "_" +
				dailyBattleRewardClaimed + "_" +
				lastDailyFriendBattles;

			PlayerPrefs.SetString(DAILY_BATTLE_REWARD, s);
		}

		private void LoadDailyReward()
		{
			string s = PlayerPrefs.GetString(DAILY_BATTLE_REWARD);
			if (string.IsNullOrEmpty(s))
			{
				lastBattleDate = DateTime.Today;
				lastDailyBattles = 0;
				dailyBattleRewardClaimed = false;
			}
			else
			{
				string[] ss = s.Split('_');
				lastBattleDate = DateTime.Parse(ss[0]);
				lastDailyBattles = int.Parse(ss[1]);
				dailyBattleRewardClaimed = bool.Parse(ss[2]);
				lastDailyFriendBattles = ss.Length >= 4 ? int.Parse(ss[3]) : 0;

				if (DateTime.Today.Year != lastBattleDate.Year ||
					DateTime.Today.DayOfYear != lastBattleDate.DayOfYear)
				{
					lastDailyFriendBattles = 0;
					lastDailyBattles = 0;
					dailyBattleRewardClaimed = false;
				}
			}
		}

		private bool PlayedBattleToday
		{
			get
			{
				return
					DateTime.Today.Year == lastBattleDate.Year &&
					DateTime.Today.DayOfYear == lastBattleDate.DayOfYear;
			}
		}

		public int TodayBattles
		{
			get { return lastDailyBattles + lastDailyFriendBattles; }
		}

		public bool DailyRewardClaimed { get { return dailyBattleRewardClaimed; } }

		public bool DailyRewardClaimable
		{
			get
			{
				return
					PlayedBattleToday &&
					TodayBattles >= GameManager.Instance.GameConfigs.dailyBattlesForReward;
			}
		}

		public void ClaimDailyReward()
		{
			if (DailyRewardClaimable && !DailyRewardClaimed)
			{
				ProfileHelper.Instance.CurrentDiamond += GameManager.Instance.GameConfigs.dailyBattleRewardAmount;
				dailyBattleRewardClaimed = true;
				SaveDailyReward();
			}
		}

		public void OnBattlePlayed()
		{
			if (PlayedBattleToday)
			{
				if (playingWithFriends)
				{
					lastDailyBattles++;
				}
				else
				{
					lastDailyFriendBattles++;
				}

				if (TodayBattles == 5)
				{
					if (dailyEventSent == 0)
					{
						dailyEventSent = 1;
						AnalyticsHelper.Instance.LogDailyBattleCount(TodayBattles);
						PlayerPrefs.SetInt(DAILY_BATTLE_EVENT_KEY, 1);
					}
				}
			}
			else
			{
				dailyEventSent = 0;
				lastBattleDate = DateTime.Today;
				lastDailyBattles = playingWithFriends ? 0 : 1;
				lastDailyFriendBattles = playingWithFriends ? 1 : 0;

				DateTime firstDate = DailyRewardManager.Instance.FirstLaunchTime;
				if (lastBattleDate.Year == firstDate.Year &&
					lastBattleDate.DayOfYear == firstDate.DayOfYear)
				{
					AnalyticsHelper.Instance.LogOnline(AnalyticsHelper.EVENT_BATTLE_ON_FIRST_DATE);
				}
			}

			SaveDailyReward();
		}

		private static bool CheckSufficientDiamonds()
		{
			if (ProfileHelper.Instance.CurrentDiamond < GameManager.Instance.GameConfigs.rubyForOnline)
			{
				AnalyticsHelper.Instance.LogRewardVideoButtonShown(AnalyticsHelper.LACK_DIAMONDS_FOR_BATTLE, "183");
				SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.MessageInviteFriends,
					new MessageBoxDataModel(
						Localization.Get("183").Replace("{0}", GameManager.Instance.GameConfigs.rubyForOnline.ToString()),
						Localization.Get("exit_online"),
						() =>
						{
							SceneManager.Instance.CloseScene();
							GroupHomeSceneManager.Instance.OpenPanel(GroupHomeSceneType.BattleHome);
						}, Localization.Get("227"), () =>
						{
							SceneManager.Instance.CloseScene();
							AnalyticsHelper.Instance.LogRewardVideoButtonClicked(AnalyticsHelper.LACK_DIAMONDS_FOR_BATTLE, "183");
							AdHelper.Instance.ShowRewardVideo();
						}));
				return false;
			}
			else
			{
				return true;
			}
		}

		public void JoinBattle(object data = null)
		{
			playingWithFriends = false;
			friendRoomCode = null;
			goingToBattle = false;
			if (CheckSufficientDiamonds())
			{
				SceneManager.Instance.OpenScene(ProjectConstants.Scenes.JoinRoom, data, true);
			}
		}

		public void Replay()
		{
			goingToBattle = false;
			if (!CheckSufficientDiamonds())
			{
				return;
			}

			AnalyticsHelper.Instance.LogBattleLobbyOpen("Replay");
			if (PlayingWithFriends)
			{
				SceneManager.Instance.OpenScene(ProjectConstants.Scenes.BattleLobbyFriends, friendRoomCode, true);
			}
			else
			{
				SceneManager.Instance.OpenScene(ProjectConstants.Scenes.JoinRoom, null, true);
			}
		}

		public void BattleWithFriends()
		{
			if (!CheckSufficientDiamonds())
			{
				return;
			}

			SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.MessageInviteFriends,
					new MessageBoxDataModel(
						Localization.Get("316"),
						Localization.Get("317"),
						() =>
						{
							SceneManager.Instance.CloseScene();
							CreateFriendsLobby();
						}, Localization.Get("318"), () =>
						{
							SceneManager.Instance.CloseScene();
							JoinFriendLobby();
						}));
		}

		public void CreateFriendsLobby()
		{
			goingToBattle = false;
			playingWithFriends = true;
			AnalyticsHelper.Instance.LogBattleLobbyOpen("create_room");
			SceneManager.Instance.OpenScene(ProjectConstants.Scenes.BattleLobbyFriends, null, true);
		}

		public void JoinFriendLobby()
		{
			SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.DialogInput,
					new DialogInputDataModel(
						Localization.Get("316"),
						Localization.Get("pu_rewardloadfailed_btn_ok"),
						(roomCode) =>
						{
							if (roomCode.Length <= 0)
							{
								return;
							}

							SceneManager.Instance.CloseScene();

							goingToBattle = false;
							playingWithFriends = true;

							AnalyticsHelper.Instance.LogBattleLobbyOpen("join_room");
							SceneManager.Instance.OpenScene(ProjectConstants.Scenes.BattleLobbyFriends, roomCode, true);
						}, Localization.Get("320"), () =>
						{
							SceneManager.Instance.CloseScene();
						}));
		}

		public string GenerateUserName()
		{
			string _name = PlayerPrefs.GetString("name_online", "");

			if (_name == "")
			{
				_name = UnityEngine.Random.Range(100000, 999999).ToString();
				PlayerPrefs.SetString("name_online", _name);
				PlayerPrefs.Save();
			}
			return _name;
		}

		public void GetUserInfo(ref string user_id, ref string user_name)
		{
			string auto_id = "UserId_" + SystemHelper.deviceUniqueID;
			string auto_name = "Player_" + GenerateUserName();

			user_id = auto_id;
			user_name = auto_name;
			if (Facebook.Unity.AccessToken.CurrentAccessToken != null)
			{
				if (Facebook.Unity.AccessToken.CurrentAccessToken.UserId.Length > 0)
					user_id = Facebook.Unity.AccessToken.CurrentAccessToken.UserId;

				try
				{
					if (ParseUser.CurrentUser != null)
					{
						if (ParseUser.CurrentUser.ContainsKey(GameConsts.PK_USERNAME))
							user_name = (string)ParseUser.CurrentUser[GameConsts.PK_USERNAME];
					}
				}
				catch (System.Exception e)
				{
					user_name = auto_name;
					Debug.LogError(e);
					//throw;
				}
			}
		}

		public void HandleError(int result_code = -102)
		{

		}

		public void OnDisconnected()
		{
			AnalyticsHelper.Instance.LogBattleDisconnected();
			SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.MessageInviteFriends,
				new MessageBoxDataModel(Localization.Get("188"),
					() =>
					{
						SceneManager.Instance.OpenScene(ProjectConstants.Scenes.HomeUI, GroupHomeSceneType.BattleHome);
					}));
		}

		public void Leave()
		{

		}

		public int CalculateVictoryReward()
		{
            return 0;
		}

		bool playingWithFriends = false;
		public bool PlayingWithFriends { get { return playingWithFriends; } }
	}
}
