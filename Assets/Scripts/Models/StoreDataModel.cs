using System.Collections.Generic;

namespace Amanotes.PianoChallenge
{
	public class StoreDataModel
	{
		public int version;
		public List<SongDataModel> listAllSongs;
		public List<SongDataModel> listHotSongs;
		public List<SongDataModel> listNewSongs;
		public List<SongDataModel> listNormalSongs;
		public List<SongDataModel> listPremiumSongs;

		public SongDataModel GetSongDataModelById(int id)
		{
			for (int i = 0; i < listAllSongs.Count; i++)
			{
				if (listAllSongs[i].ID == id)
					return listAllSongs[i];
			}

			return null;
		}

		public List<SongDataModel> GetSongDataModelByListStoreId(List<string> Lid)
		{

			if (Lid == null)
			{
				return null;
			}

			List<SongDataModel> LSongByStoreId = new List<SongDataModel>();
			for (int j = Lid.Count - 1; j >= 0; j--)
			{
				for (int i = 0; i < listAllSongs.Count; i++)
				{
					if (Lid[j].Equals(listAllSongs[i].storeID))
						LSongByStoreId.Add(listAllSongs[i]);
				}
			}

			return LSongByStoreId;
		}

		public List<SongDataModel> GetSongDataModelByLevel(int level)
		{
			List<SongDataModel> LSongBylevel = new List<SongDataModel>();

			for (int i = 0; i < listAllSongs.Count; i++)
			{
				if (listAllSongs[i].LvToUnlock == level)
					LSongBylevel.Add(listAllSongs[i]);
			}

			return LSongBylevel;
		}
	}
}
