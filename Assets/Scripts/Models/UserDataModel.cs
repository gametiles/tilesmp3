using System.Collections.Generic;
using Amanotes.PianoChallenge;

public class UserDataModel
{
	public int totalExp = 999999;
	////level is calculated from xp
	public int level = 100;
	public int life;
	public int diamond;
	public List<ScoreItemModel> listHighscore;
	public List<string> listBoughtSongs;
	public List<string> listFavoriteSongs = new List<string> ();
	public bool isLoggedInFacebook = false;
	public double timeStartCountdown;

	//achievement
	public string achievementPropertiesValueCSV;
	public string achievementUnlockedAndClaimedCSV;

	// Battle info
	public int wins = 0;
	public int loses = 0;
	public int battleRating = 0;

	//used to sync data between devices
	public string lastsyncDevice;
	public int dataVersion;
	public int modelVersion = 0;
}

public class CloudDataModel
{
	public string userId;
	public string userName;
	public int crowns;
	public int stars;
	public List<ScoreItemModel> highScores;
	public int exp;
	public int level;
	public int life;
	public int diamonds;
	public List<string> superSongs;
	public List<string> boughtSongs;
	public List<string> favoriteSongs;
	public double lifeCountDownStartTime;
	public string achievementPropertyCsv;
	public string achievementUnlockCsv;
	public int wins;
	public int loses;
	public int battleRating;
	public int supersongCompensated;
	public string lastsyncDevice;
	public int dataVersion;

	public bool Equals(UserDataModel other)
	{
		return (level == other.level &&
			diamonds == other.diamond &&
			achievementPropertyCsv.Equals(other.achievementPropertiesValueCSV) &&
			achievementUnlockCsv.Equals(other.achievementUnlockedAndClaimedCSV) &&
            fModStudio.Utils.ListsEqual(boughtSongs, other.listBoughtSongs) &&
			wins == other.wins &&
			loses == other.loses);
	}
}
