﻿using System.Collections.Generic;
using System;
namespace Amanotes.PianoChallenge {
    [Serializable]
    public class LevelDataModel   {
        public SongDataModel songData;
        public List<NoteData> playbackData;
        public List<NoteData> noteData;
		public Dictionary<NoteData,List<NoteData>> dicNotePlay;
		public Dictionary<int,PTDuetNote> dicDuetNote;
        //the bpm of the level
        public float BPM;
        //number of ticks for a whole note
        public int tickPerQuarterNote;
        public int denominator;
    }
}