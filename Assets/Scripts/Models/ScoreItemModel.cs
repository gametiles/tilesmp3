
namespace Amanotes.PianoChallenge
{
	public class ScoreItemModel
	{
		public int highestScore = 0;
		public int highestStar = 0;
		public int highestCrown = 0;
		public string itemName = null;
	}
}
