using Facebook.Unity;
using System;
using UnityEngine;

namespace Amanotes.PianoChallenge
{
	[Serializable]
	public class SongRecord
	{
		public int index;
		public string userId;
		public string userName;
		public int stars;
		public int crowns;
		public int score;
		public int value; // used to sort
		public Sprite avatar;
		public SongRecord(string userId, string userName, int crowns, int stars, int score)
		{
			this.userId = userId;
			this.userName = userName;
			this.crowns = crowns;
			this.stars = stars;
			this.score = score;
			value = CalculateValue(score, crowns, stars);
		}

		public bool IsMe {
			get
			{
				return userId == null ||
					(AccessToken.CurrentAccessToken != null &&
					AccessToken.CurrentAccessToken.UserId == userId);
			}
		}

		public static int CalculateValue(int score, int crowns, int stars)
		{
			return score * 100 + crowns * 10 + stars;
		}
	}

	public class SongRecordComparator : System.Collections.Generic.IComparer<SongRecord>
	{
		public int Compare(SongRecord x, SongRecord y)
		{
			return y.value - x.value;
		}
	}
}
