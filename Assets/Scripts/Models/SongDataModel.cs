using System;
using System.Collections.Generic;

namespace Amanotes.PianoChallenge
{
	public enum SongItemType
	{
		Normal = 0,
		Hot = 1,
		New = 2
	}

	public enum SongGlobalLeaderboardStatus
	{
		Eligible,
		LackStars,
		LackScore
	}

	[Serializable]
	public class SongDataModel
	{
		//unique id for each song
		public int ID;
		//name of the song to be display to user
		public string name;
		//author or origin of the song
		public string author;
		//url to download level data
		public string songURL;
		//the unique id of this song item
		public string storeID;
		//version of this song data
		public int version;
		// Price in primary currency of the game (currency bought from cash).
		// Negative value indicates number of times player can play this song for free after watching a video ad.
		public int pricePrimary;
		//how many star needed to unlock this song
		public int starsToUnlock;
		//how many level needed to unlock this song
		public int LvToUnlock;
		//maximum speed this level will reach
		public float maxBPM;
		//number of ticks for a black tiles
		public int ticksPerTile;
		//faster or slower speed
		public float speedModifier;
		public float battleSpeedFactor = 1; // 0: Same as single mode, 1: speed equivalent to player's level
		public List<float> speedPerStar;
		//type of the song (new, hot or normal one)
		public SongItemType type;
		public int isPianoTiles;

		public bool CanViewGlobalLeaderboard {
			get {
				return GlobalLeaderboardEligibility == SongGlobalLeaderboardStatus.Eligible;
			}
		}

		public SongGlobalLeaderboardStatus GlobalLeaderboardEligibility
		{
			get
			{
				int starsRequired = GameManager.Instance.GameConfigs.songLeaderboardStarsRequired;
				int scoreRequired = GameManager.Instance.GameConfigs.songLeaderboardScoreRequired;

				if (starsRequired > 0)
				{
					int stars = HighScoreManager.Instance.GetHighScore(storeID, ScoreType.Star);
					if (stars < starsRequired)
					{
						return SongGlobalLeaderboardStatus.LackStars;
					}
				}
				else if (scoreRequired > 0)
				{
					int stars = HighScoreManager.Instance.GetHighScore(storeID, ScoreType.Score);
					if (stars < starsRequired)
					{
						return SongGlobalLeaderboardStatus.LackScore;
					}
				}
				return SongGlobalLeaderboardStatus.Eligible;
			}
		}
	}
}
