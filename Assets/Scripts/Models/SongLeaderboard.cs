
using Amanotes.PianoChallenge;
using System;
using System.Collections.Generic;

public class SongLeaderboard
{
	public DateTime lastSyncTime = DateTime.Now;
	public List<SongRecord> records = new List<SongRecord>();
	public int rank = 1;
	public int myIndex = 0;
	public int myRank = 1;
	public int myOldRank = 1;

	public bool CanSyncNow(int score, int crowns, int stars)
	{
		if (records.Count > 0)
		{
			int value = SongRecord.CalculateValue(score, crowns, stars);
			if (!records[0].IsMe && value > records[0].value)
			{
				return true;
			}
		}

		int intervalMinutes = GameManager.Instance.GameConfigs.songLeaderboardSyncIntervalMinutes;
		return (DateTime.Now - lastSyncTime).TotalMinutes > intervalMinutes;
	}

	public void Sort()
	{
		myOldRank = myRank;
		records.Sort(SongLeaderboardManager.recordComparator);

		for (int i = 0; i < records.Count; i++)
		{
			if (records[i].IsMe)
			{
				myIndex = i;
				myRank = rank + i;
				break;
			}
		}
	}

	public int MyRankUp { get { return myOldRank - myRank; } }

	public void ResetRankUp()
	{
		myOldRank = myRank;
	}

	public SongRecord RecordBelowMe
	{
		get
		{
			return records != null && (myIndex + 1) < records.Count ? records[myIndex + 1] : null;
		}
	}

	public bool Populated
	{
		get
		{
			return records != null && records.Count > 1;
		}
	}
}
