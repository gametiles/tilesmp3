
using Amanotes.Utils;
using UnityEngine;

public class CrossPromotion
{
	public string imageUrl = null;
	public string iconUrl = null;
	public string storeUrl = null;
	public string trackingCode = null;
	public string name = null;

	public string ImageUrl { get { return imageUrl; } }
	public string IconUrl { get { return iconUrl; } }
	public string StoreUrl { get { return storeUrl; } }
	public string Code { get { return trackingCode; } }
	public string Name { get { return name; } }

	public CrossPromotion(
		string imageUrl,
		string iconUrl,
		string storeUrl,
		string trackingCode,
		string name)
	{
		this.imageUrl = imageUrl;
		this.iconUrl = iconUrl;
		this.storeUrl = storeUrl;
		this.trackingCode = trackingCode;
		this.name = name;
	}

	public void OnClick()
	{
		Application.OpenURL(storeUrl);
		AnalyticsHelper.Instance.LogCrossPromotionClicked(trackingCode);
	}
}
