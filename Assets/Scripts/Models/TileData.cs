
using System.Collections.Generic;
using System;
namespace Amanotes.PianoChallenge
{
	[Serializable]
	public class TileData
	{
		public List<NoteData> notes;
		public TileType type = TileType.Empty;
		public TileType subType = TileType.Normal;
		public float soundDelay;
		public float startTime;
		public float startTimeInTicks;
		public int durationInTicks;
		public float duration;
		public int score;
		public int duetNode = 0;
	}
	[Serializable]
	public class SongTileData
	{
		public SongDataModel songDataModel;
		public List<TileData> tileList;
		//the bpm of the level
		public float BPM;
		//number of ticks for a whole note
		public int tickPerQuarterNote;
		public int denominator;

		// Piano tiles content
		public Dictionary<NoteData, List<NoteData>> dicNotePlay;
		public Dictionary<int, PTDuetNote> dicDuetNote;
	}
}
