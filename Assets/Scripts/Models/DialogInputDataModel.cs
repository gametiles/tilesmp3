using System;

namespace Amanotes.PianoChallenge
{
	public class DialogInputDataModel
	{
		public string message;
		public string messageYes;
		public Action<string> OnYesButtonClicked;
		public string messageNo;
		public Action OnNoButtonClicked;
		private string p1;
		private string p2;
		private Action action;

		public DialogInputDataModel(string msg, string msY, Action<string> onY, string meN, Action onN)
		{
			this.message = msg;
			this.messageYes = msY;
			this.OnYesButtonClicked = onY;
			this.messageNo = meN;
			this.OnNoButtonClicked = onN;
		}
	}
}
