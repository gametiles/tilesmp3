
using Amanotes.PianoChallenge;
using Amanotes.Utils;
using Amanotes.Utils.MessageBus;
using Facebook.Unity;
using FullSerializer;
using System.Collections.Generic;

public class SongLeaderboardListFriend : SongLeaderboardList
{
	const string CACHE_NAME = "FriendSongLeaderboards";

	class FriendLeaderboardResponse
	{
		public string songId = null;
		public List<RecordData> records = null;
	};

	class FriendLeaderboardsResponse
	{
		public List<FriendLeaderboardResponse> leaderboards = null;
	}

	public SongLeaderboardListFriend()
		: base(MessageBusType.FriendSongLeaderboardLoaded, CACHE_NAME)
	{
	}

	public void OnUserDataChanged()
	{
		leaderboards = new Dictionary<string, SongLeaderboard>();
		StoreCache();
		RequestFriendRecords();
	}

	public override bool RequestData(string songId)
	{
		if (loading)
		{
			return false;
		}

		SongLeaderboard leaderboard = null;
		if (leaderboards == null)
		{
			if (!RequestFriendRecords())
			{
				return false;
			}
		}

		leaderboards.TryGetValue(songId, out leaderboard);

		var hm = HighScoreManager.Instance;
		int crowns = hm.GetHighScore(songId, ScoreType.Crown);
		int stars = hm.GetHighScore(songId, ScoreType.Star);
		int score = hm.GetHighScore(songId, ScoreType.Score);

		if (leaderboard != null && !leaderboard.CanSyncNow(score, crowns, stars))
		{
			return true;
		}

		if (!FacebookManager.Instance.HasFriends)
		{
			if (leaderboard == null)
			{
				leaderboard = new SongLeaderboard();
				leaderboards[songId] = leaderboard;
				AddMyRecord(leaderboard, crowns, stars, score);
			}
			return true;
		}

		loading = true;

		var parameters = new Dictionary<string, object> {
				{ "songId", songId },
				{ "friends", FacebookManager.Instance.Friends },
			};

		fModStudio.Utils.CallParseFunction("GetFriendSongRecords", parameters, task => {
			if (task.IsFaulted)
			{
				AnalyticsHelper.Instance.LogSongLeaderboardLoadFailed("friend", "cannot connect");
				OnLeaderboardLoaded();
				return;
			}

			fsData responseData = null;
			FileUtilities.JSONSerializer.TrySerialize(
				task.Result as IDictionary<string, object>,
				out responseData);

			FriendLeaderboardResponse response = null;

			fsResult fsresult = FileUtilities.JSONSerializer.TryDeserialize(responseData, ref response);
			if (fsresult.Failed || response == null || response.records == null || response.records.Count <= 0)
			{
				AnalyticsHelper.Instance.LogSongLeaderboardLoadFailed("friend", "no data");
				leaderboard = new SongLeaderboard();
				leaderboards[songId] = leaderboard;
				AddMyRecord(leaderboard, crowns, stars, score);
				OnLeaderboardLoaded();
				return;
			}

			PopulateFriendLeaderboard(songId, response.records);
			StoreCache();

			OnLeaderboardLoaded();
		});

		return false;
	}

	public bool RequestFriendRecords()
	{
		if (leaderboards != null)
		{
			return !loading;
		}
		leaderboards = new Dictionary<string, SongLeaderboard>();

		if (!FacebookManager.Instance.HasFriends)
		{
			return !loading;
		}

		if (loading)
		{
			return false;
		}

		loading = true;

		var parameters = new Dictionary<string, object> {
				{ "friends", FacebookManager.Instance.Friends },
			};

		fModStudio.Utils.CallParseFunction("GetFriendSongLeaderboards", parameters, task => {
			if (task.IsFaulted)
			{
				AnalyticsHelper.Instance.LogSongLeaderboardLoadFailed("friend", "cannot connect");
				OnLeaderboardLoaded();
				return;
			}

			fsData responseData = null;
			FileUtilities.JSONSerializer.TrySerialize(
				task.Result as IDictionary<string, object>,
				out responseData);

			FriendLeaderboardsResponse response = null;

			fsResult fsresult = FileUtilities.JSONSerializer.TryDeserialize(responseData, ref response);
			if (fsresult.Failed || response == null || response.leaderboards == null || response.leaderboards.Count <= 0)
			{
				AnalyticsHelper.Instance.LogSongLeaderboardLoadFailed("friend", "no data");
				OnLeaderboardLoaded();
				return;
			}

			for (int i = 0; i < response.leaderboards.Count; i++)
			{
				string songId = response.leaderboards[i].songId;
				PopulateFriendLeaderboard(
					response.leaderboards[i].songId,
					response.leaderboards[i].records);
			}

			StoreCache();
			OnLeaderboardLoaded();
		});

		return false;
	}

	void OnLeaderboardLoaded()
	{
		loading = false;
		MessageBus.Annouce(message);
	}

	void PopulateFriendLeaderboard(string songId, List<RecordData> records)
	{
		if (records == null)
		{
			return;
		}

		SongLeaderboard leaderboard = new SongLeaderboard();

		Populate(leaderboard, records);
		leaderboard.records.Add(new SongRecord(
				AccessToken.CurrentAccessToken.UserId,
				FacebookManager.Instance.UserDisplayName,
				HighScoreManager.Instance.GetHighScore(songId, ScoreType.Crown),
				HighScoreManager.Instance.GetHighScore(songId, ScoreType.Star),
				HighScoreManager.Instance.GetHighScore(songId, ScoreType.Score)
				));

		leaderboard.rank = 1;
		leaderboard.Sort();
		SetLeaderboard(songId, leaderboard);
	}
}
