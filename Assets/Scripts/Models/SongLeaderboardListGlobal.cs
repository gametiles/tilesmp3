using Amanotes.PianoChallenge;
using Amanotes.Utils;
using Amanotes.Utils.MessageBus;
using Facebook.Unity;
using FullSerializer;
using System.Collections.Generic;

public class SongLeaderboardListGlobal : SongLeaderboardList
{
	const string CACHE_NAME = "GlobalSongLeaderboards";

	class GlobalLeaderboardResponse
	{
		public int rank = 0;
		public List<RecordData> records = null;
	}

	public SongLeaderboardListGlobal() : base(
		MessageBusType.GlobalSongLeaderboardLoaded, CACHE_NAME)
	{
	}

	public void OnUserDataChanged()
	{
		leaderboards = new Dictionary<string, SongLeaderboard>(); ;
		StoreCache();
	}

	public override bool RequestData(string songId)
	{
		if (loading)
		{
			return false;
		}

		SongLeaderboard leaderboard = null;
		if (leaderboards == null)
		{
			leaderboards = new Dictionary<string, SongLeaderboard>();
		}
		else
		{
			leaderboards.TryGetValue(songId, out leaderboard);
		}

		var hm = HighScoreManager.Instance;
		int crowns = hm.GetHighScore(songId, ScoreType.Crown);
		int stars = hm.GetHighScore(songId, ScoreType.Star);
		int score = hm.GetHighScore(songId, ScoreType.Score);

		if (leaderboard != null && !leaderboard.CanSyncNow(score, crowns, stars))
		{
			return true;
		}

		loading = true;

		var parameters = new Dictionary<string, object> {
				{ "songId", songId },
				{ "crowns", crowns },
				{ "stars",  stars  },
				{ "score",  score  },
			};

        fModStudio.Utils.CallParseFunction("GetGlobalSongLeaderboard", parameters, task => {
			if (task.IsFaulted)
			{
				AnalyticsHelper.Instance.LogSongLeaderboardLoadFailed("global", "cannot connect");
				OnLeaderboardLoaded();
				return;
			}

			var result = task.Result as IDictionary<string, object>;
			if (!result.ContainsKey("data"))
			{
				AnalyticsHelper.Instance.LogSongLeaderboardLoadFailed("global", "no data");
				OnLeaderboardLoaded();
				return;
			}

			var data = result["data"] as IDictionary<string, object>;
			fsData responseData = null;
			FileUtilities.JSONSerializer.TrySerialize(data, out responseData);

			GlobalLeaderboardResponse response = null;

			fsResult fsresult = FileUtilities.JSONSerializer.TryDeserialize(
				responseData, ref response);
			if (fsresult.Failed || response == null || response.records == null)
			{
				AnalyticsHelper.Instance.LogSongLeaderboardLoadFailed("global", "no data");
				OnLeaderboardLoaded();
				return;
			}

			if (response.records.Count <= 0)
			{
				leaderboard = new SongLeaderboard();
				leaderboards[songId] = leaderboard;
				AddMyRecord(leaderboard, crowns, stars, score);
				OnLeaderboardLoaded();
				return;
			}

			leaderboard = new SongLeaderboard();

			leaderboard.rank = response.rank;
			Populate(leaderboard, response.records);

			bool meInTheList = false;
			if (response.records != null)
			{
				foreach (var record in response.records)
				{
					if (AccessToken.CurrentAccessToken != null && record.userId == AccessToken.CurrentAccessToken.UserId)
					{
						meInTheList = true;
						break;
					}
				}
			}

			if (!meInTheList)
			{
				AddMyRecord(leaderboard, crowns, stars, score);
			}

			SetLeaderboard(songId, leaderboard);
			StoreCache();
			OnLeaderboardLoaded();
		});

		return false;
	}

	void OnLeaderboardLoaded()
	{
		loading = false;
		MessageBus.Annouce(message);
	}
}
