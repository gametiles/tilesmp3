using System.Collections.Generic;

namespace Amanotes.PianoChallenge
{
	public class GameConfigModel
	{
		//localize
		public List<string> localizeConfig = new List<string> {
			"English#English",
			"Vietnamese#Vietnamese",
			"Japanese#Japanese",
			"Spanish#Spanish",
			"Korean#Korean",
			"Russian#Russian",
			"Chinese (Sim)#Chinese",
			"Chinese (Traditional)#Chinese",
			"Portuguese#Portugeese",
			"Arabic#Arabic",
			"German#Germany",
			"French#French",
			"Italian#Italian",
			"Turkish#Turkish",
			"Indonesian#Indonesian"
		};

		//cross 1
		public List<string> crossPromotionStore = new List<string> {
			"https://s3.amazonaws.com/pianochallenge2/pc201/Other/%5BTYa%5D__Cross-promotion_resize.PNG#https://s3.amazonaws.com/marketingcross/Material/icon/TYi.png#https://taps.io/BxzCw#https://itunes.apple.com/us/app/tap-tap-reborn-2-popular-songs/id1206306782?mt=8#Cross_PCa_TYa#Tap and feel with most popular music of all",
			"https://s3.amazonaws.com/pianochallenge2/Image/PI2.png#https://s3.amazonaws.com/pianochallenge2/Image/icon/PI.png#https://taps.io/BxzEA# https://itunes.apple.com/us/app/magic-tiles-3-music-instrument/id1145692161?mt=8#Cross_PCa_PIa#Best melody game ever!!!",
			"https://s3.amazonaws.com/pianochallenge2/pc201/Other/%5BTTM%5D_Porfolio_-_More_game-_banner_o2.png#https://s3.amazonaws.com/marketingcross/Material/icon/TTMi.png#https://taps.io/BxwZQ#https://itunes.apple.com/us/app/dancing-ballz-color-line/id1265019448?mt=8#Cross_PCa_TTMa#ONE TAP ONE TUNE",
			"https://s3.amazonaws.com/pianochallenge2/pc201/Other/MJ+Portfolio+banner+-2.png#https://s3.amazonaws.com/marketingcross/Material/icon/MJ+.png#https://taps.io/BxwYA#https://itunes.apple.com/us/app/dancing-ballz-color-line/id1265019448?mt=8#Cross_PCa_MJa#The world's hardest game ever!",
			"https://s3.amazonaws.com/marketingcross/Material/portfoliopopup/PCC.png#https://s3.amazonaws.com/marketingcross/Material/icon/PCCi.png#https://play.google.com/store/apps/details?id=com.amanotes.classicalpiano#https://itunes.apple.com/us/app/piano-challenges-2-white-tiles/id1077947558?mt=8#Cross_PCa_PCCa#The ultimate classical piano experience"
		};

		//level
		public string ConfigUserLevel_Link = "https://s3.amazonaws.com/pianochallenge2/pc201/ConfigGeneral/PC2_ConfigLevel_Aug2.txt";
		public float FirstExpForStar = 0.10f;
		public float receivedExpForStar = 0.02f;
		public float FirstExpForCrown = 0.06f;
		public float receivedExpForCrown = 0.06f;
		public float expDeductPerLevel = 0.05f;
		public float MaxexpDeductPerLevel = 0.9f;

		//CheckScreenAPI
		public List<string> adsProviders = new List<string> { "Ogury", "IronSource" };
		public List<string> listRegionOgury = new List<string>() { "US", "GB", "IT", "FR", "ES", "DE" };

		////server
		public bool ENABLE_PROTOBUG_MODE = true;

		// ProcessBonusTile
		public int diamondDrop = 1;

		// setting graphics
		public int touchCount = 4;
		// config file version
		public int configVersion = 461;

		public int idsongfirst = 1;

		// Config for maximum user's life
		public long TIME_DISCONNECT_ALLOW = 3600;//1h
		//server realtime URL
		public string ONLINE_URL_SERVER = "http://52.44.115.185:7004";

		public string CHECK_GEO_IP_URL = "http://52.71.9.24:8081/geoip?pass=AmanotesJSC";

		public int maxLifeLinkedUser = 30;
		public int maxLifeGuestUser = 20;
		// how many seconds needed to elapse to increase life?
		public int secondsPerLife = 300;

		//time per session
		public int sessionTime = 30;
		public int rubyForOnline = 2;

		public float pecentRewardOnline = 1;
		// Push Notification IDs
		public string google_project_id = "168277909596";

		//setting parse
		public string PARSE_SERVER_URL = "http://52.44.115.185:1346/parse/";
		public string PARSE_APPLICATION_ID = "PC";
		public string PARSE_DOT_NET_KEY = "zucpADI5Zo053oHbyCIyFM9YaGh3taVIlNY2ImoA";
		public int parseVersion = 1;

		public List<int> indexNativeAds = new List<int> { 20, 7 };
		public List<string> nativead_ids = new List<string> {
			"979269728858634_1074602419325364",	// Android
			"979269728858634_1074602592658680"	// iOS
		};

		//unity ads
		public int videoAdsReward = 20;
		public int firstLoginDiamonds = 100;

		//in app purchase value
		public List<int> iap_values = new List<int> { 200, 450, 900 };
		public bool iapBonusEnabled = true;
		public int iapEventVersion = 1;

		//life exchange value
		public List<int> lives_exchange_values = new List<int> { 5, 10, 25, 60 };
		public List<int> lives_exchange_prices = new List<int> { 100, 200, 450, 800 };
		//first notes speed
		public float firstNoteSpeed_dcrease = 2.6f;
		//ad interval
		public List<float> interstitialIntervals = new List<float> { 60, 75, 90 };

		// store data file URL
		public string storeDataURL = "https://s3.amazonaws.com/pianochallenge2/pc201/ConfigGeneral/Songdata_Sep3.csv";
		// URL of the csv file contains properties of achievement
		public string achievementPropertiesURL = "https://s3.amazonaws.com/pianochallenge2/pc201/Apr2017/PropertyData.csv";
		// URL of the csv file contains definition of achievements in game
		public string achievementURL = "https://s3.amazonaws.com/pianochallenge2/pc201/Apr2017/AchievementData.csv";
		public string localizationURL = "https://s3.amazonaws.com/pianochallenge2/pc201/ConfigGeneral/Localization_Dec2.csv";

		public string fanpageLink = "https://www.facebook.com/Game-Piano-Tiles-116049953123376";
		/// IN#GAME CONFIG
		///
		//Price for continue game
		public int maxDiamondPerGame = 2;
		public float diamondChance = 0.025f;
		public List<int> continuePrices = new List<int> {5, 10, 15};
		public int continueCountDown = 8;
		//at which point the game start spawning diamonds
		public int scoreToDropDiamond = 400;
		public float speedToDropDiamond = 2.2f;
		public float TIME_SYNC_LIMIT = 0.1f;

		//daily reward
		public List<int> dailyrewardLives = new List<int> {
			0,
			0,
			0,
			3,
			3,
			4,
			5
		};

		public List<int> dailyrewardDiamonds = new List<int> {
			10,
			20,
			30,
			40,
			50,
			60,
			100
		};

		//speed
		public List<float> defaultSpeedTable = new List<float> {
			1,
			1.05f,
			1.1f,
			1.15f,
			1.25f,
			1.5f,
			1.7f,
			0.015f
		};

		public List<float> endlessSpeedTable = new List<float> {
			1,
			1.05f,
			1.1f,
			1.15f,
			1.5f,
			1.7f,
			1.8f,
			0.015f
		};

		public int ratingCondition = 6;

		public float maxRectTouch = 0.7f;
		public float maxSpeedOverlapRect = 4.8f;
		public float startSpeedToOverlap = 2f;

		public float distanOverByMissing = 300f;

		public float timeSyncData = 60f;

		public float PT2_GAME_DEFAULT_SPEED = 240;

		// Configurations for battle
		public int dailyBattlesForReward = 5;
		public int dailyBattleRewardAmount = 20;

		// Battle speed
		public bool adjustBattleSpeed = true;
		public int maxBattleLevel = 4; // Battle level ranges from 0 to 5

		// Speed adjustment allowence for each group
		// 0: Not allowed
		// 1: Allow to adjust lower speed only
		// 2: Allow to adjust higher speed only
		// >2: Allow to adjust both higher and lower speed
		public List<int> adjustBattleSpeeds = new List<int> { 3, 3, 3, 3, 3, 3 };

		public List<float> onlineSongBaseSpeeds = new List<float> {
				//    0          1          2          3          4          5
				3.0f, 3.555556f, 4.173913f, 4.537815f, 5.111111f, 6.333333f, 6.666667f
			};

		// Exp for battle
		public List<float> battleExpFactors = new List<float> { 0.66667f, 0.4f, 0.3f, 0.3f };

		// Cheat
		public bool cheatEnabled = true;
		public bool allowEveryoneToInvite = true;
		public float battleWaitTime = 10f; // max waiting time since song loading was done

		// Premium songs unlocked for specific levels
		public int nLevelUpPremiumTries = 1;
		public int nLevelUpPremiumSongs = 3;

		// Battle premium songs
		public List<int> battlesPremiumSongs = new List<int> {
			1010,
			17,
			1029,
			1030,
			7,
			1028,
			1011,
			1043,
			1021,
			1047,
			1054,
			1058,
			1059,
			1060,
			1061,
			1063,
			1073,
			1072,
			1086,
			1082 };

		public int socialEventVersion = 1;

		public bool useAppsflyer = true;

		public int songConfigOverride = -1;
		public int levelConfigOverride = -1;
		public int inGameAchievementOverride = 0;
		public int interstitialPolicy = -1;
		public int day0NotificationOverride = 0;

		public int crownsToUnlockPremium = 3;
		public string updatePrompt = "none"; // "none"; "optional"; "mandatory"
		public int freeSongHours = 2;
		public int notificationTime = 1800; // 18:00
		public string freeSongId = "christmas";
		public float endlessInitScoreRatio = 0.9f;
		public int offlineModeNotificationTime = 2200; // 22:00

		public int songLeaderboardSyncIntervalMinutes = 60;
		public int songLeaderboardStarsRequired = 1;
		public int songLeaderboardScoreRequired = 0;

		public int favoriteCountToShowRatePopup = 2;
		public List<string> firstSongs = new List<string> {
			"twinkle", "JingleBells", "cannonind2", "beauti", "alway", "auldlangsyne", "riverflowsinyou"
		};
	}
}
