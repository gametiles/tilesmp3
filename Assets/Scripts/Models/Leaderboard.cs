
using System;
using System.Collections.Generic;
using UnityEngine;

class PlayerRecord
{
	public string userId;
	public string userName;
	public int crowns;
	public int stars;
	public Sprite avatar;
}

class Leaderboard
{
	public DateTime lastSyncTime = DateTime.Now;
	public List<PlayerRecord> records = new List<PlayerRecord>();
	public int rank;
	public int myRank;

	private void LoadCache(string name)
	{

	}

	private void StoreCache(string name)
	{

	}
}
