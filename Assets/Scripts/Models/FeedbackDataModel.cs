﻿using UnityEngine;
using System.Collections;

namespace Amanotes.PianoChallenge {
    public class FeedbackDataModel {
        public string title;
        public string subtitle;
        public string buttonLabel;
        public string type;
    }
}