
namespace Amanotes.PianoChallenge
{
	public class SessionDataModel
	{
        public SongDataModel song = null;
		public bool needAttentionAchievement = false;

		public GAME_PLAY_MODE playMode = GAME_PLAY_MODE.NORMAL;

		// Song mode
		public bool listen = false;
		public bool endless = false;
		public bool autoplay = false;
	}
}

public enum GAME_PLAY_MODE
{
    NORMAL,
    ONLINE
}
