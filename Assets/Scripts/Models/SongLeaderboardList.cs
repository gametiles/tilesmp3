
using Amanotes.PianoChallenge;
using Amanotes.Utils;
using Amanotes.Utils.MessageBus;
using Facebook.Unity;
using System;
using System.Collections.Generic;

class SongLeaderboardCache
{
	public Dictionary<string, SongLeaderboard> leaderboards = new Dictionary<string, SongLeaderboard>();
}

public abstract class SongLeaderboardList
{
	protected class RecordData
	{
		public string userId;
		public string userName;
		public int crowns;
		public int stars;
		public int score;
	}

	public bool IsGlobal
	{
		get
		{ // Dirty trick
			return message.messageType == MessageBusType.GlobalSongLeaderboardLoaded;
		}
	}

	protected Message message = null;
	protected Dictionary<string, SongLeaderboard> leaderboards = new Dictionary<string, SongLeaderboard>();
	protected string cacheName = null;
	protected bool loading = false;

	public bool Loading { get { return loading; } }

	protected SongLeaderboardList(MessageBusType type, string cacheName)
	{
		message = new Message(type);
		this.cacheName = cacheName;
		LoadCache();
	}

	public MessageBusType OnLeaderboardLoadedMessage { get { return message.messageType; } }

	public abstract bool RequestData(string songId);

	public SongLeaderboard GetLeaderboard(string songId)
	{
		SongLeaderboard leaderboard = null;
		if (leaderboards != null)
		{
			leaderboards.TryGetValue(songId, out leaderboard);
		}

		return leaderboard;
	}

	public void UpdateMySongRecord(string songId, int crowns, int stars, int score)
	{
		SongLeaderboard leaderboard = null;
		leaderboards.TryGetValue(songId, out leaderboard);
		if (leaderboard == null)
		{
			return;
		}

		for (int i = 0; i < leaderboard.records.Count; i++)
		{
			SongRecord record = leaderboard.records[i];
			if (record.IsMe)
			{
				if (AccessToken.CurrentAccessToken != null &&
					AccessToken.CurrentAccessToken.UserId != record.userId)
				{
					record.userId = AccessToken.CurrentAccessToken.UserId;
				}
				record.score = score;
				record.stars = stars;
				record.crowns = crowns;
				leaderboard.Sort();
				break;
			}
		}

		StoreCache();
	}

	protected static void AddMyRecord(SongLeaderboard leaderboard, int crowns, int stars, int score)
	{
		leaderboard.records.Add(new SongRecord(
				AccessToken.CurrentAccessToken != null ? AccessToken.CurrentAccessToken.UserId : null,
				AccessToken.CurrentAccessToken != null ? FacebookManager.Instance.UserDisplayName : Localization.Get("225"),
				crowns, stars, score));

		leaderboard.Sort();
	}

	protected static void Populate(SongLeaderboard leaderboard, List<RecordData> records)
	{
		if (records == null)
		{
			return;
		}

		leaderboard.lastSyncTime = DateTime.Now;
		foreach (var record in records)
		{
			leaderboard.records.Add(new SongRecord(
				record.userId,
				record.userName,
				record.crowns,
				record.stars,
				record.score
				));
		}
	}

	protected void LoadCache()
	{
		SongLeaderboardCache cache = FileUtilities.DeserializeObjectFromFile<SongLeaderboardCache>(cacheName);
		if (cache != null && cache.leaderboards != null)
		{
			leaderboards = cache.leaderboards;
		}

		foreach (var pair in leaderboards)
		{
			pair.Value.ResetRankUp();
		}
	}

	protected void StoreCache()
	{
		SongLeaderboardCache cache = new SongLeaderboardCache();
		cache.leaderboards = leaderboards;

		FileUtilities.SerializeObjectToFile(cache, cacheName);
	}

	protected void SetLeaderboard(string songId, SongLeaderboard leaderboard)
	{
		SongLeaderboard oldLeaderboard = null;
		if (leaderboards.TryGetValue(songId, out oldLeaderboard))
		{
			leaderboard.myOldRank = oldLeaderboard.myRank;
		}
		leaderboards[songId] = leaderboard;
	}
}
