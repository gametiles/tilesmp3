
using Amanotes.Utils.MessageBus;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	[Serializable]
	public class PSSongInfo
	{
		public PSSongInfo(string title, string mp3File, string noteFile)
		{
			this.title = title;
			this.mp3File = mp3File;
			this.noteFile = noteFile;
			score = 0;
			stars = 0;
		}

		public string MusicFile
		{
			get
			{
#if UNITY_EDITOR
				return "C:" + mp3File;
#else
                return mp3File;
#endif
			}
		}

		public string NoteFile
		{
			get
			{
				return PSSongManager.NoteFile(title);
			}
		}

		public string title;
		public string mp3File;
		public string noteFile;
		public int score;
		public int stars;
		public bool imported = true;
	}

	class PSSongManager
	{
		const string IMPORT_SONG_LIST_FILE = "ImportedSongs.dat";
		static private PSSongManager instance = null;
		static public PSSongManager Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new PSSongManager();
				}
				return instance;
			}
		}
		class SongList { public List<PSSongInfo> songs; }
		List<PSSongInfo> songs;
		Dictionary<string, int> songDict;

		public static string NoteFile(string title)
		{
			return FileUtilities.GetWritablePath(title + ".imported");
		}

		private PSSongManager()
		{
			songDict = new Dictionary<string, int>();
			try
			{
				string path = FileUtilities.GetWritablePath(IMPORT_SONG_LIST_FILE);
				string json = System.IO.File.ReadAllText(path, System.Text.Encoding.UTF8);
				var list = JsonUtility.FromJson<SongList>(json);
				if (list != null && list.songs != null)
				{
					songs = list.songs;
				}
			}
			catch (Exception){}

			if (songs == null)
			{
				songs = new List<PSSongInfo>(16);
			}

			if (songs.Count <= 0)
			{
                //Noti Update "songSuggestCount"  at PSSongListView when AddSong
                AddSong("Imagine Anything", "CustomSongs/Imagine_Anything_mp3", "CustomSongs/Imagine_Anything_notes");
                AddSong("Interesting Ideas", "CustomSongs/Interesting_Ideas_mp3", "CustomSongs/Interesting_Ideas_notes");
                AddSong("Old Saloon", "CustomSongs/Old_Saloon_mp3", "CustomSongs/Old_Saloon_notes");
                AddSong("When The Apple Fell", "CustomSongs/When_The_Apple_Fell_mp3", "CustomSongs/When_The_Apple_Fell_notes");
                AddSong("A New Dawn", "CustomSongs/A_New_Dawn_mp3", "CustomSongs/A_New_Dawn_notes");
				return;
			}

#if false
			songs = new List<PSSongInfo>() {
				new PSSongInfo("Twinkle Twinkle Little Star", "" ),
				new PSSongInfo("Castle in the Sky", "" ),
				new PSSongInfo("Always with me", "" ),
				new PSSongInfo("Fuck You Bitch", "" ),
				new PSSongInfo("BITCH!", "" ),
			};
#endif
			foreach (var song in songs)
			{
				songDict.Add(song.mp3File, 1);
			}
		}

		void SaveSongs()
		{
			try
			{
				string path = FileUtilities.GetWritablePath(IMPORT_SONG_LIST_FILE);
				SongList list = new SongList();
				list.songs = songs;
				string json = JsonUtility.ToJson(list);
				System.IO.File.WriteAllText(path, json, System.Text.Encoding.UTF8);
			}
			catch (Exception) { }
		}

		public bool HasSong(string path)
		{
			return songDict.ContainsKey(path);
		}

		private void AddSong(PSSongInfo song)
		{
			songs.Add(song);
			songDict.Add(song.mp3File, 1);
			SaveSongs();

		}

		public void AddSong(string title, string mp3File, string noteFile)
		{
			var song = new PSSongInfo(title, mp3File, noteFile);
			song.imported = false;
			AddSong(song);
		}


		public void RemoveSong(PSSongInfo song)
		{
			if (songs.Remove(song))
			{
				System.IO.File.Delete(song.noteFile);
				songDict.Remove(song.mp3File);
				SaveSongs();
			}
		}

		public int SongCount { get { return songs.Count; } }

		public void UpdateSong(PSSongInfo song, int score, int stars)
		{
			song.score = score;
			song.stars = stars;
			SaveSongs();
		}

		public PSSongInfo GetSong(int index) { return songs[index]; }

		public PSSong LoadSong(PSSongInfo song)
		{
			return song.imported ?
				PSSong.Load(song.noteFile) :
				PSSong.FromResource(song.noteFile);
		}
	}
}
