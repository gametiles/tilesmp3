using UnityEngine;

namespace Nama
{
	public class PSSimpleNote : PSNote
	{
		[SerializeField] SpriteRenderer noteBg = null;
        [SerializeField] SpriteRenderer noteBg2 = null;

        static Color PRESSED_COLOR = new Color(0, 0, 0, 0.15f);
		public const float HEIGHT = 486.0f;

		protected override void OnTouchDown(Vector2 position)
		{
            noteBg2.color = PRESSED_COLOR;
		}

		protected override void OnTouchUp()
		{

		}

		protected override void OnTouchUpdate()
		{
		}

		protected override void OnSetup(float unitsPerMs)
		{
            noteBg2.color = Color.white;
			height = HEIGHT;

			var pos = noteBg.transform.localPosition;
			pos.y = height / 2;
			noteBg.transform.localPosition = pos;
		}

		public override int score()
		{
			return 1;
		}
	}
}
