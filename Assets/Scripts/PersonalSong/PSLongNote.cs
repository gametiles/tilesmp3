using UnityEngine;

namespace Nama
{
	public class PSLongNote : PSNote
	{
		[SerializeField] SpriteRenderer noteBg = null;
		[SerializeField] SpriteRenderer line = null;
		[SerializeField] GameObject noteHead = null;
		[SerializeField] SpriteRenderer noteTail = null;
		[SerializeField] GameObject blinkAnimation = null;

		const float DOT_MARGIN = 120.0f;

		Vector3 scale;
		Vector3 pos;
		float sy;

		protected override void OnTouchDown(Vector2 position)
		{
			// detect progress based on touch position
			float r = (position.y - noteBg.bounds.min.y - PSSimpleNote.HEIGHT) / height;
			if (r < 0)
			{
				r = 0;
			}
			timeMs = duration * r;
			noteTail.gameObject.SetActive(true);

			noteHead.SetActive(true);
			blinkAnimation.SetActive(true);

			OnTouchUpdate();
		}

		protected override void OnTouchUp()
		{
			blinkAnimation.SetActive(false);
		}

		protected override void OnTouchUpdate()
		{
			float r = timeMs / duration + (PSSimpleNote.HEIGHT - DOT_MARGIN) / height;
			float rmax = (height - DOT_MARGIN) / height;
			if (r > rmax)
			{
				r = 1;

				noteHead.SetActive(false);
				blinkAnimation.SetActive(false);
			}
			else
			{
				float rh = timeMs / duration + PSSimpleNote.HEIGHT / height;
				if (rh > 1) rh = 1;
				pos.y = height * rh;
				noteHead.transform.localPosition = pos;
			}

			scale.y = sy * r;
			noteTail.transform.localScale = scale;

			pos.y = height * r / 2;
			noteTail.transform.localPosition = pos;
		}

		protected override void OnSetup(float unitsPerMs)
		{
			noteTail.gameObject.SetActive(false);
			noteHead.SetActive(false);
			blinkAnimation.SetActive(false);

			// Calculate height of long note based on its duration
			height = unitsPerMs * duration;

			// Scale
			float derivedScale = noteBg.bounds.size.x / 270.0f;
			float currentHeight = (noteBg.bounds.size.y) / derivedScale;
			float ratio = height / currentHeight;
			scale = noteBg.transform.localScale;
			scale.y *= ratio;
			noteBg.transform.localScale = scale;

			// Move
			pos = noteBg.transform.localPosition;
			pos.y = height / 2;
			noteBg.transform.localPosition = pos;

			// Save some values for later use
			pos = noteTail.transform.localPosition;
			sy = scale.y;

			// Reset scale
			scale.y = 0;
			scale.x = noteTail.transform.localScale.x;
			noteTail.transform.localScale = scale;

			// Middle Line
			derivedScale = line.bounds.size.x / (270.0f / 84.0f);
			currentHeight = (line.bounds.size.y) / derivedScale;
			ratio = (height - DOT_MARGIN) / currentHeight;
			var lineScale = line.transform.localScale;
			lineScale.y *= ratio;
			line.transform.localScale = lineScale;

			var linePos = line.transform.localPosition;
			linePos.y = (height - DOT_MARGIN) / 2 + DOT_MARGIN;
			line.transform.localPosition = linePos;
		}

		public override int score()
		{
			float r = timeMs / duration + PSSimpleNote.HEIGHT / height;
			return (int)Mathf.Ceil(r * height / PSSimpleNote.HEIGHT);
		}
	}

}
