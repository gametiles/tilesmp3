
using UnityEngine;

namespace Nama
{
	public class PSGameplayController : SSController
	{
		[SerializeField] PSGameplay gameplay;

		public override void OnSet(object data)
		{
            if (data != null)
                gameplay.StartSong((data as Amanotes.PianoChallenge.SongDataModel));
        }
	}

}
