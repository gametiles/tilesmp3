using UnityEngine;

namespace Nama
{
	public abstract class PSNote : MonoBehaviour
	{
		public bool NeedUpdate { get { return touchId >= 0; } }

		protected float duration;

		int touchId = -1;
		public bool Hold { get { return touchId >= 0; } }
		public int TouchId { get { return touchId; } }

		protected float height;
		public float Height { get { return height; } }

		public float Bottom { get { return gameObject.transform.localPosition.y; } }
		public float Top { get { return Bottom + height; } }
		protected float timeMs; // Elapsed time since touch in milliseconds

		public bool Touched { get { return touched; } }
		bool touched = false;

		float time;
		public float Time { get { return time; } }

		// Use this for initialization
		public void Setup(float time_, float duration, float unitPerSec)
		{
			time = time_;
			touchId = -1;
			timeMs = 0;
			touched = false;
			this.duration = duration;
			OnSetup(unitPerSec);
		}

		public void TouchDown(int touchId, Vector2 position)
		{
			if (Hold)
			{
				return;
			}

			this.touchId = touchId;
			touched = true;
			OnTouchDown(position);
		}

		public void TouchUp(int touchId)
		{
			if (this.touchId == touchId)
			{
				this.touchId = -1;
				OnTouchUp();
			}
		}

		public void UpdateActions(float deltaMs)
		{
			if (!Hold)
			{
				return;
			}

			timeMs += deltaMs;

			if (Hold)
			{
				OnTouchUpdate();
			}
		}

		protected abstract void OnTouchUpdate();
		protected abstract void OnTouchDown(Vector2 position);
		protected abstract void OnTouchUp();
		protected abstract void OnSetup(float unitsPerMs);
		public abstract int score();
	}
}
