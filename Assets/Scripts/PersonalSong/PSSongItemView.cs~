using Amanotes.PianoChallenge;
using ProjectConstants;
using UnityEngine;
using Amanotes.Utils;
using Firebase.Storage;
using System.Threading.Tasks;
using System.Collections;
using System.Threading;
using System;

namespace Nama
{
	public class PSSongItemView : MonoBehaviour
	{
		[SerializeField] GameObject songInfoBar;
		[SerializeField] UILabel title;
		[SerializeField] GameObject removeButton;
        [SerializeField] UISprite bgIcon;
        [SerializeField] GameObject btnPlay;
        [SerializeField] GameObject btnPlayAds;

		PSSongListView list;
		PSSongInfo song;

		public void ShowSongInfo(bool active)
		{
			songInfoBar.SetActive(active);
		}

		public void Set(PSSongListView list, PSSongInfo song)
		{
			this.list = list;
			this.song = song;
			removeButton.SetActive(song.imported);
            btnPlay.SetActive(song.imported);
            btnPlayAds.SetActive(!song.imported);

            if (song.imported)
                bgIcon.spriteName = "ui-blue";
            else
                bgIcon.spriteName = "ui-yellow";
			title.text = song.title;
		}

		public void TryToPlay()
		{
			bool valid = !song.imported ||
				(FileUtilities.IsFileExist(song.MusicFile, true) &&
				FileUtilities.IsFileExist(song.NoteFile, true));

			if (valid)
			{
				SceneManager.Instance.OpenScene(Scenes.PSGameplay, song);
			}
			else
			{
				RemoveSong();
			}
		}

        public void TryToPlayAds()
        {
            AnalyticsHelper.Instance.LogPSVideoRewardClick("top_upload");
            bool valid = !song.imported ||
                (FileUtilities.IsFileExist(song.MusicFile, true) &&
                    FileUtilities.IsFileExist(song.NoteFile, true));

            if (valid)
            {
                AdHelper.Instance.ShowRewardVideo(RewardType.Custom, ()=>
                    {
                        AnalyticsHelper.Instance.LogPSVideoRewardWatched("top_upload");
                        SceneManager.Instance.OpenScene(Scenes.PSGameplay, song);
                    });
            }
            else
            {
                RemoveSong();
            }
        }

		public void RemoveSong()
		{
			PSSongManager.Instance.RemoveSong(song);
			list.Refresh();
		}

        public void ShareMp3Song()
        {
            AnalyticsHelper.Instance.LogShareMp3SongClick();
            ShareSongDataModel shareSong = new ShareSongDataModel(song.title, song.mp3File,"", song.noteFile);
            if (!FacebookManager.instance.IsLogin())
            {
                SceneManager.instance.OpenPopup(Scenes.FacebookLoginUp, shareSong);
            }
            else
            {                
                SceneManager.instance.OpenPopup(Scenes.ShareMp3Song, shareSong);
            }
        }
	}

}
