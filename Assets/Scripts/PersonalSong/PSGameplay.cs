using Amanotes.PianoChallenge;
using Amanotes.Utils;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
using System;
using ProjectConstants;

namespace Nama
{
    public class PSGameplay : MonoBehaviour
    {
        [DllImport("fMod")] private static extern void music_load_file(string file);
        [DllImport("fMod")] private static extern void music_load(byte[] data, int size);
        [DllImport("fMod")] private static extern void music_start();
        [DllImport("fMod")] private static extern void music_stop();
        [DllImport("fMod")] private static extern void music_pause();
        [DllImport("fMod")] private static extern void music_resume();
        [DllImport("fMod")] private static extern bool music_isPlaying();
        [DllImport("fMod")] private static extern void music_setSpeed(float speed);
        [DllImport("fMod")] private static extern float music_time();
        [DllImport("fMod")] private static extern void music_seek(float time);

        const int LONG_NOTE_POOL_SIZE = 5;
        const int SIMPLE_NOTE_POOL_SIZE = 20;
        const float FINISH_TIME = 5.0f;

        [SerializeField] Camera noteCamera = null;
        [SerializeField] Camera bgCamera = null;
        [SerializeField] GameObject longNote = null;
        [SerializeField] GameObject simpleNote = null;
        [SerializeField] UIRoot uiRoot = null;
        [SerializeField] GameObject touchLine = null;
        [SerializeField] UILabel scoreLabel = null;
        [SerializeField] SpriteRenderer deadNote = null;
        [SerializeField] GameObject startNote = null;
        [SerializeField] NoteTouchEffect touchEffect = null;
        [SerializeField] Animation anmScoreDisplay = null;
        [SerializeField] GameObject bannerStart = null;

        [Header("Info Game")]
        [SerializeField] UILabel lbSongName = null;
        [SerializeField] UILabel lbAuthor = null;
        [SerializeField] UILabel lbHighScore = null;
        [SerializeField] List<GameObject> starHighScore = null;

        [Header("Live background variables")]
        [SerializeField] ParticleSystem particleBubble;
        [SerializeField] ParticleSystem particleSnow;
        [SerializeField] SpriteRenderer sprMain;
        [SerializeField] SpriteRenderer sprStar1;
        [SerializeField] SpriteRenderer sprStar2;
        [SerializeField] TweenAlpha tweenStar1;
        [SerializeField] TweenAlpha tweenStar2;
        [SerializeField] AudioClip clip_Click_Replay;
        [SerializeField] List<GameObject> star;
        [SerializeField] Camera camUI2D;

        [Header("Loading data game")]
        [SerializeField] GameObject loading;
        [SerializeField] UILabel txtProgress;
        [SerializeField] UILabel txtMessLoading;
        [SerializeField] GameObject btnWhenError;
        [SerializeField] GameObject btnHomeOnly;
        [SerializeField] GameObject iconLoading;

        [Header("Progress Bar")]
        [SerializeField] UIProgressBar progressBar;
        [SerializeField] List<GameObject> listStarActive;
        [SerializeField] TweenAlpha tweenProgressBar;

        static PSGameplay instance = null;
        public static PSGameplay Instance { get { return instance; } }

        // Song
        [HideInInspector] public PSSong song;
        byte[] mp3Data;

        // Score
        [HideInInspector] public int nTiles;
        [HideInInspector] public int score;

        // for continue
        [HideInInspector] public int nStars;
        [HideInInspector] public int scoreToNextStar;

        //for Result
        [HideInInspector] public PSSongInfo currentSongInfo;
        [HideInInspector] public SongDataModel songData;

        // Timing
        float speedModifier = 1.0f;
        float unitsPerMs = 1.79105747f;
        float lastTime;

        // Falling down notes
        List<PSLongNote> longNotePool;
        List<PSSimpleNote> simpleNotePool;
        LinkedList<PSNote> notes;
        Dictionary<int, PSNote> holdNotes;
        PSNote noteToTouch;

        int iLastVisibleNote;
        float generateMs; // Last time a note is generated
        float waitTime;
        float waitDuration;

        float activeHeight;
        float touchPos;
        bool finished;
        bool paused = false;
        bool dead = false;
        bool started = false;
        bool loadedData = false;

        // Use this for initialization
        void Start()
        {
            instance = this;
            if(clip_Click_Replay != null)
                AudioEffect.Instance.PlaySound(clip_Click_Replay);
        }

        private void OnEnable()
        {
            AudioManager.Instance.StopSession();
            AudioBackground.Instance.StopBackgroundMusic();
        }

        private void OnDisable()
        {
            music_stop();
            AudioManager.Instance.StartSession();
        }

        // Touch
        bool canTouch = true;

        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                music_pause();
                paused = true;
            }
        }

        private void OnDestroy()
        {
            instance = null;
            music_stop();
        }

        void UpdateCamera(float dtms)
        {
            var position = noteCamera.transform.localPosition;
            position.y += unitsPerMs * dtms;
            noteCamera.transform.localPosition = position;
        }

        void OnStartGame()
        {
            bannerStart.SetActive(false);
        }

        void Finish()
        {
            // TODO: Show result
            music_stop();
            if (mp3Data != null)
            {
                mp3Data = null;
            }

            Static.countPSContinuePopup = 0;
            SceneManager.Instance.OpenScene(ProjectConstants.Scenes.PSResultUI, this);
        }

        void UpdateTouchNotStarted(Vector3 pos)
        {
            Vector2 rayOrigin = bgCamera.ScreenToWorldPoint(pos);

            Vector2 diagonPoint = new Vector2(rayOrigin.x + 0.001f, rayOrigin.y - 0.1f);
            Collider2D hit = Physics2D.OverlapArea(
                rayOrigin,
                diagonPoint,
                ProjectConstants.Layers.BackgroundMask);

            if (hit != null && hit.gameObject.GetComponent<PSNote>() != null)
            { // Acceptable miss
                started = true;
                startNote.SetActive(false);
                OnStartGame();
            }
        }

        void UpdateNoStarted()
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                UpdateTouchNotStarted(Input.mousePosition);
            }
#else
			if (Input.touchCount > 0)
			{
				UpdateTouchNotStarted(Input.touches[0].position);
			}
#endif
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                //Finish();
            }
            if (song == null || mp3Data == null)
            {
                return;
            }

            GenerateNextNotes();
            if (!started)
            {
                if (!loadedData)
                    return;

                UpdateNoStarted();
                return;
            }
            if (waitTime >= 0)
            {
                waitTime -= Time.smoothDeltaTime;
                if (waitTime <= 0)
                {
                    if (finished)
                    {
                        Finish();
                    }
                    else
                    {
                        music_start();
                    }
                }
            }

            if (dead)
            {
                UpdateDead();
            }
            else
            {
                float deltaMs = Time.smoothDeltaTime * 1000;
                deltaMs *= speedModifier;

                // Update camera
                UpdateCamera(deltaMs);

                // Update notes' visibility
                UpdateNotes(deltaMs);

                // Process notes
                UpdateTouch();
            }
        }

        void InitNote(PSNote note)
        {
            note.gameObject.transform.SetParent(transform);
            note.gameObject.transform.localScale = Vector3.one;
            note.gameObject.SetActive(false);
        }

        PSLongNote CreatePSLongNote()
        {
            PSLongNote note = Instantiate(longNote).GetComponent<PSLongNote>();
            InitNote(note);
            return note;
        }

        PSSimpleNote CreatePSSimpleNote()
        {
            PSSimpleNote note = Instantiate(simpleNote).GetComponent<PSSimpleNote>();
            InitNote(note);
            return note;
        }

        public void StartSong(PSSongInfo songInfo)
        {
            if (songInfo == null)
                return;
            currentSongInfo = songInfo;

            song = PSSongManager.Instance.LoadSong(songInfo);
            if (songInfo.imported)
            {
                music_load_file(songInfo.MusicFile);
            }
            else
            {
                mp3Data = (Resources.Load(songInfo.mp3File) as TextAsset).bytes;
                music_load(mp3Data, mp3Data.Length);
            }
            Init();
        }

        void Init()
        {
            finished = false;
            unitsPerMs = song.bpm / 60.0f;
            activeHeight = uiRoot.activeHeight;
            Vector3 cameraPos = noteCamera.transform.localPosition;
            cameraPos.y = 0;
            noteCamera.transform.localPosition = cameraPos;

            var pos = touchLine.transform.localPosition;
            touchPos = -activeHeight / 2 + PSSimpleNote.HEIGHT;
            pos.y = touchPos;
            touchLine.transform.localPosition = pos;

            pos = startNote.transform.localPosition;
            pos.y = touchPos;
            startNote.transform.localPosition = pos;
            startNote.SetActive(true);
            deadNote.gameObject.SetActive(false);

            lastTime = 0;
            iLastVisibleNote = 0;
            generateMs = 0;
            waitDuration = PSSimpleNote.HEIGHT / unitsPerMs - song.notes[0].timeMs;
            if (waitDuration < 0)
            {
                waitDuration = 0;
            }
            waitDuration /= 1000.0f;
            waitTime = waitDuration;

            if (longNotePool == null)
            {
                longNotePool = new List<PSLongNote>(LONG_NOTE_POOL_SIZE);
                for (int i = 0; i < LONG_NOTE_POOL_SIZE; i++)
                {
                    longNotePool.Add(CreatePSLongNote());
                }
            }
            else
            {
                foreach (var note in longNotePool)
                {
                    note.gameObject.SetActive(false);
                }
            }
            if (simpleNotePool == null)
            {
                simpleNotePool = new List<PSSimpleNote>(SIMPLE_NOTE_POOL_SIZE);
                for (int i = 0; i < SIMPLE_NOTE_POOL_SIZE; i++)
                {
                    simpleNotePool.Add(CreatePSSimpleNote());
                }
            }
            else
            {
                foreach (var note in simpleNotePool)
                {
                    note.gameObject.SetActive(false);
                }
            }

            if (notes != null)
            {
                foreach (var note in notes)
                {
                    if (note.GetType() == typeof(PSLongNote))
                    {
                        longNotePool.Add((PSLongNote)note);
                    }
                    else
                    {
                        simpleNotePool.Add((PSSimpleNote)note);
                    }
                }
                notes.Clear();
            }
            else
            {
                notes = new LinkedList<PSNote>();
            }

            holdNotes = new Dictionary<int, PSNote>();
            noteToTouch = null;
            paused = false;
            dead = false;
            started = false;

            nTiles = 0;
            nStars = 0;
            score = 0;
            progressBar.value = 0;
            scoreToNextStar = 0;
            scoreLabel.text = "0";
            bannerStart.SetActive(true);
            ShowInfo();
            StartCoroutine(HiddenLoading());
        }

        private void ShowInfo()
        {
            lbSongName.text = songData.name;
            lbAuthor.text = songData.author;
            string score = HighScoreManager.Instance.GetHighScore(songData.storeID).ToString();
            lbHighScore.text = Localization.Get("high_score") + ": " + score;

            int star = HighScoreManager.Instance.GetHighScore(songData.storeID, ScoreType.Star);

            for (int i = 0; i < star; i++)
            {
                starHighScore[i].transform.GetChild(0).gameObject.SetActive(true);
            }

        }

        PSLongNote allocateLongNote()
        {
            PSLongNote note;
            if (longNotePool.Count > 0)
            {
                int i = longNotePool.Count - 1;
                note = longNotePool[i];
                longNotePool.RemoveAt(i);
            }
            else
            {
                note = CreatePSLongNote();
            }
            return note;
        }

        PSSimpleNote allocateSimpleNote()
        {
            PSSimpleNote note;
            if (simpleNotePool.Count > 0)
            {
                int i = simpleNotePool.Count - 1;
                note = simpleNotePool[i];
                simpleNotePool.RemoveAt(i);
            }
            else
            {
                note = CreatePSSimpleNote();
            }
            return note;
        }

        PSNote[] occupiers = new PSNote[4] { null, null, null, null };
        int[] slots = new int[4] { 0, 1, 2, 3 };
        int iLastSlot = -1;

        void GenerateNextNotes()
        {
            float top =
                noteCamera.transform.localPosition.y +
                activeHeight +
                PSSimpleNote.HEIGHT;

            if (notes != null && notes.Last != null && notes.Last.Value.Top > top)
            {
                return;
            }

            for (int j = iLastVisibleNote; j < song.notes.Length; j++)
            {
                var note_ = song.notes[j];
                generateMs = note_.timeMs;
                float y = (generateMs + waitDuration * 1000.0f) * unitsPerMs + touchPos;

                float minMs = 60000.0f / song.bpm;

                PSNote note;
                if (note_.durationMs < minMs)
                {
                    note = allocateSimpleNote();
                }
                else
                {
                    note = allocateLongNote();
                }
                note.Setup(note_.timeMs, note_.durationMs, unitsPerMs);

                // Make sure there are no two notes overlap
                int nSlots = 0;
                for (int i = 0; i < 4; i++)
                {
                    if (occupiers[i] != null && occupiers[i].Top < y)
                    {
                        occupiers[i] = null;
                    }
                    if (occupiers[i] == null && i != iLastSlot)
                    {
                        slots[nSlots++] = i;
                    }
                }
                int iSlot = UnityEngine.Random.Range(0, nSlots == 0 ? 4 : nSlots);
                iSlot = slots[iSlot];
                iLastSlot = iSlot;
                occupiers[iSlot] = note;

                // Finialize note
                note.gameObject.transform.localPosition = new Vector3(-405 + 270 * iSlot, y, 0);
                note.gameObject.SetActive(true);
                notes.AddLast(note);

                iLastVisibleNote = j + 1;
                if (iLastVisibleNote >= song.notes.Length)
                {
                    waitTime = FINISH_TIME;
                    finished = true;
                }
                if (note.Bottom > top)
                {
                    if (noteToTouch == null)
                    {
                        var node = notes.First;
                        while (node.Value.Touched) node = node.Next;
                        noteToTouch = node.Value;
                    }
                    return;
                }
            }
        }

        enum DeadStatus { MOVING, BLINKING, IDLE, REVIVED }
        DeadStatus deadStatus = DeadStatus.MOVING;

        bool missedTap;
        int lineMissed;
        float targetCamPos;
        float camMovingVel;
        float deadTime;
        float blinkTime;
        bool blink = true;
        float blinkHeight = PSSimpleNote.HEIGHT;
        GameObject blinkNote;

        void GoDead()
        {
            try
            {
                deadStatus = DeadStatus.MOVING;
                dead = true;
                paused = true;                
                music_seek(noteToTouch.Time / 1000);

                deadTime = 0.5f;
                targetCamPos = noteToTouch.Bottom - PSSimpleNote.HEIGHT + activeHeight / 2;
                camMovingVel = (targetCamPos - noteCamera.transform.localPosition.y) / deadTime;
                blinkTime = 0;
                blink = true;
                music_pause();
            }
            catch (Exception e)
            {
                Debug.LogError("AAA GO DEAD 1: " + e);
                Debug.LogException(e);
            }

            if (missedTap)
            {
                try
                {
                    var s = deadNote.transform.localScale;
                    s.y *= noteToTouch.Height / blinkHeight;
                    blinkHeight = noteToTouch.Height;
                    deadNote.transform.localScale = s;

                    var p = deadNote.transform.localPosition;
                    p.y = noteToTouch.transform.localPosition.y + blinkHeight / 2;
                    p.x = lineMissed * 270.0f + 135.0f;
                    deadNote.transform.localPosition = p;

                    blinkNote = deadNote.gameObject;
                    blinkNote.SetActive(true);
                }
                catch (Exception e)
                {
                    Debug.LogError("AAA GO DEAD 2: " + e);
                    Debug.LogException(e);
                }
            }
            else
            {
                try
                {
                    blinkNote = noteToTouch.gameObject;
                }
                catch (Exception e)
                {
                    Debug.LogError("AAA GO DEAD 3: " + e);
                    Debug.LogException(e);
                }
            }
        }

        void UpdateBlink()
        {
            blinkTime -= Time.smoothDeltaTime;
            if (blinkTime < 0)
            {
                blinkTime = 0.2f;
                blink = !blink;
                if(blinkNote != null)
                    blinkNote.SetActive(blink);
            }
        }

        public void Continue()
        {
            if (deadStatus == DeadStatus.IDLE)
            {
                deadStatus = DeadStatus.REVIVED;
            }
        }

        void UpdateDead()
        {
            switch (deadStatus)
            {
                case DeadStatus.MOVING:
                    try
                    {
                        Vector2 v = noteCamera.transform.localPosition;
                        v.y += camMovingVel * Time.smoothDeltaTime;
                        noteCamera.transform.localPosition = v;
                        deadTime -= Time.smoothDeltaTime;
                        UpdateBlink();
                        if (deadTime < 0)
                        {
                            v.y = noteToTouch.Bottom - PSSimpleNote.HEIGHT + activeHeight / 2;
                            noteCamera.transform.localPosition = v;
                            deadStatus = DeadStatus.BLINKING;
                            deadTime = 2.0f;
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("AAA DeadStatus.MOVING: " + e);
                        Debug.LogException(e);
                    }
                    break;
                case DeadStatus.BLINKING:
                    try
                    {
                        deadTime -= Time.smoothDeltaTime;
                        UpdateBlink();
                        if (deadTime < 0)
                        {
                            deadStatus = DeadStatus.IDLE;
                            blinkNote.SetActive(!missedTap);
                            // TODO: Show Continue popup
                            TryToShowContinue();
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("AAA DeadStatus.BLINKING: " + e);
                        Debug.LogException(e);
                    }
                    break;
                case DeadStatus.IDLE:
                    break;
                case DeadStatus.REVIVED:
                    UpdateTouch();
                    break;
            }
        }

        private IEnumerator UpdateBackgroundEffect(int star)
        {
            StartCoroutine(ShowStar(star));
            if (star == 1)
            {
                particleBubble.gameObject.SetActive(true);
                particleBubble.maxParticles = 30;
                particleBubble.Play();
                listStarActive[0].SetActive(true);
                sprStar1.enabled = true;
                tweenStar1.enabled = true;
                tweenStar1.PlayForward();

                yield return new WaitForSeconds(4f);
                sprMain.enabled = false;
            }
            else if (star == 2)
            {
                particleSnow.gameObject.SetActive(true);
                particleSnow.maxParticles = 20;
                particleSnow.Play();
                particleBubble.maxParticles = 40;
                listStarActive[1].SetActive(true);
                sprStar2.enabled = true;
                tweenStar2.enabled = true;
                tweenStar2.PlayForward();
                yield return new WaitForSeconds(4f);
                sprStar1.enabled = false;
            }
            else if (star == 3)
            {
                listStarActive[2].SetActive(true);
            }
            yield return null;
        }

        private IEnumerator ShowStar(int currentStar)
        {
            int newStar = currentStar - 1;
            for (int i = 0; i < star.Count; i++)
            {
                if (newStar == i)
                {
                    star[i].SetActive(true);
                    star[i].GetComponent<TweenAlpha>().PlayForward();
                }
                else
                {
                    star[i].SetActive(false);
                }
            }
            yield return new WaitForSeconds(4f);
            star[newStar].GetComponent<TweenAlpha>().PlayReverse();
            yield return new WaitForSeconds(1f);
            star[newStar].SetActive(false);
        }

        private void TryToShowContinue()
        {
            if (Static.countPSContinuePopup < 3)
            {
                camUI2D.depth = 14f;
                scoreToNextStar = (int)((song.notes.Length / 3) * (nStars + 1) - nTiles);
                SceneManager.Instance.OpenPopup(ProjectConstants.Scenes.PSContinuePopup, this);
                Static.countPSContinuePopup++;
            }
            else
            {
                Finish();
            }

        }

        void UpdateNotes(float deltaMs)
        {
            var next = notes.First;
            while (next != null)
            {
                var node = next;
                next = node.Next;

                var note = node.Value;
                if (note.NeedUpdate)
                {
                    note.UpdateActions(deltaMs);
                }

#if false
				if (note.Bottom <= noteCamera.transform.localPosition.y - activeHeight / 2 + PSSimpleNote.HEIGHT &&
					!note.Touched)
				{
					note.TouchDown(0, note.transform.position);
					return;
				}
#endif
                float limit =
                        noteCamera.transform.localPosition.y -
                        activeHeight / 2 -
                        note.Height;

                if (note.transform.localPosition.y >= limit)
                {
                    continue;
                }

                if (note.Hold)
                {
                    holdNotes.Remove(note.TouchId);
                }
                else if (!note.Touched)
                { // Died
                    missedTap = false;
                    GoDead();
                    return;
                }

                notes.Remove(node);

                note.gameObject.SetActive(false);
                if (note.GetType() == typeof(PSSimpleNote))
                {
                    simpleNotePool.Add((PSSimpleNote)note);
                }
                else
                {
                    longNotePool.Add((PSLongNote)note);
                }
            }
        }

        void UpdateTouch()
        {
            if (!canTouch)
            {
                return;
            }

#if UNITY_EDITOR

            if (Input.GetMouseButtonDown(0))
            {
                UpdateTouch(0, Input.mousePosition, TouchPhase.Began);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                UpdateTouch(0, Input.mousePosition, TouchPhase.Ended);
            }
            else if (Input.GetMouseButton(0))
            {
                UpdateTouch(0, Input.mousePosition, TouchPhase.Stationary);
            }
#else
			// for multi-touch on device
			for (int i = 0; i < Input.touchCount; i++)
			{
				UpdateTouch(
					Input.touches[i].fingerId,
					Input.touches[i].position,
					Input.touches[i].phase);
			}
#endif
        }

        static Vector2 DIAGON_VECTOR = new Vector2(-2000, 0);
        void CheckMiss(Vector2 origin)
        {
            Collider2D hit = Physics2D.OverlapArea(
                origin - DIAGON_VECTOR,
                origin + DIAGON_VECTOR,
                ProjectConstants.Layers.MainGameMask);
            if (hit == null || noteToTouch != hit.gameObject.GetComponent<PSNote>())
            { // Acceptable miss
                return;
            }

            // Unacceptable miss
            float x = noteCamera.transform.worldToLocalMatrix.MultiplyPoint(origin).x;
            lineMissed = (int)(x / 270);
            if (x < 0) lineMissed--;
            missedTap = true;
            GoDead();
        }

        void UpdateTouch(int id, Vector2 pos, TouchPhase phase)
        {
            // Hit
            switch (phase)
            {
                case TouchPhase.Began:
                    {
                        Vector2 rayOrigin = noteCamera.ScreenToWorldPoint(pos);

                        Vector2 diagonPoint = new Vector2(rayOrigin.x, rayOrigin.y - 0.001f);
                        Collider2D hit = Physics2D.OverlapArea(
                            rayOrigin,
                            diagonPoint,
                            ProjectConstants.Layers.MainGameMask);

                        if (hit == null)
                        { // Missed
                            CheckMiss(rayOrigin);
                            return;
                        }

                        PSNote note = hit.gameObject.GetComponent<PSNote>();
                        if (note == null)
                        { // Missed
                            CheckMiss(rayOrigin);
                            return;
                        }

                        if (note != noteToTouch)
                        { // Wrong note
                            return;
                        }

                        if (note.Touched)
                        { // Touch already touched
                            break;
                        }

                        holdNotes[id] = note;
                        note.TouchDown(id, rayOrigin);
                        touchEffect.OnTouchDown(bgCamera.ScreenToWorldPoint(pos));
                        var next = notes.Find(noteToTouch).Next;
                        noteToTouch = next != null ? next.Value : null;

                        if (paused)
                        {
                            paused = false;
                            dead = false;
                            music_resume();
                        }

                        nTiles++;
                        UpdateProgressBar();
                        if (nTiles >= (song.notes.Length / 3) * (nStars + 1))
                        {
                            nStars++;
                            StartCoroutine(UpdateBackgroundEffect(nStars));
                        }
                    }
                    break;

                case TouchPhase.Stationary:
                case TouchPhase.Moved:
                    break;

                default:
                    {
                        PSNote capturedNote;
                        if (holdNotes.TryGetValue(id, out capturedNote))
                        {
                            capturedNote.TouchUp(id);
                            touchEffect.OnTouchUp();
                            holdNotes.Remove(id);

                            score += capturedNote.score();
                            anmScoreDisplay.Play();
                            scoreLabel.text = score.ToString();
                        }
                    }
                    break;
            }
        }

        private void UpdateProgressBar()
        {
            progressBar.value = (float)nTiles / song.notes.Length;
        }

        IEnumerator HiddenLoading()
        {
            yield return new WaitForSeconds(0.7f);
            TweenAlpha tween = loading.GetComponent<TweenAlpha>();
            tween.enabled = true;
            tween.PlayForward();

            tweenProgressBar.enabled = true;
            tween.PlayForward();
            yield return new WaitForSeconds(0.5f);
            loading.SetActive(false);
            loadedData = true;
        }

        private void ShowbtnHomeOny()
        {
            btnHomeOnly.SetActive(true);
        }

        private void ShowbtnWhenError()
        {
            btnWhenError.SetActive(true);
        }

        public void StartSong(SongDataModel _songData)
        {
            if (_songData == null)
            {
                Debug.LogError("PSGameplay.cs -> StartSong(NULL) Line 857");
                txtMessLoading.gameObject.SetActive(true);
                txtMessLoading.text = Localization.Get("er_parse_song_failed");
                Invoke("ShowbtnHomeOny",1.2f);
                return;
            }

            songData = _songData;
            music_stop();
            PSSongInfo songInfo = new PSSongInfo(songData.name, "", "");
            currentSongInfo = songInfo;

            //Load song json imported
            string pathSong = "Mp3Source/" + songData.storeID + "_imported";
            song = PSSong.FromResource(pathSong);

            //Try to load Mp3 from reource
            string pathMp3 = "Mp3Source/" + songData.storeID + "_mp3";
            try
            {
                mp3Data = (Resources.Load(pathMp3) as TextAsset).bytes;
            }
            catch (Exception e)
            {
            }


            if (mp3Data != null)
            {
                music_load(mp3Data, mp3Data.Length);
                Init();
            }
            else
            {
                //Try to load mp3 from fileUlti
                string store = songData.storeID + "_mp3.bytes";
                string filePath = FileUtilities.GetWritablePath(store);
                try
                {
                    mp3Data = System.IO.File.ReadAllBytes(filePath);
                }
                catch (Exception e)
                {
                }
                

                if (mp3Data != null)
                {
                    music_load(mp3Data, mp3Data.Length);
                    Init();
                }
                else
                {
                    //Try to download 
                    if (Application.internetReachability == NetworkReachability.NotReachable)
                    {
                        //show error internet and try download
                        iconLoading.SetActive(true);
                        txtMessLoading.gameObject.SetActive(true);
                        txtMessLoading.text = Localization.Get("er_download_song_failed");
                        txtProgress.gameObject.SetActive(true);
                        txtProgress.text = "0%";
                        Invoke("ShowbtnWhenError", 1.1f);
                    }
                    else
                    {
                        //download song data
                        iconLoading.SetActive(true);
                        txtMessLoading.gameObject.SetActive(true);
                        txtMessLoading.text = Localization.Get("maingame_downloading");
                        txtProgress.gameObject.SetActive(true);
                        txtProgress.text = "0%";

                        DownloadMp3();
                    }
                }
            }
            

        }

        #region DownloadMp3
        void DownloadMp3()
        {
            string url = songData.songURL;
            AssetDownloader.Instance.DownloadAsset(url,
                ProgressMp3Download,
                DownloadFail,
                DownloadFinish
            );
        }

        void ProgressMp3Download(float progress)
        {
            int percent = (int)(progress * 100);
            txtProgress.text = percent + "%";
        }

        void DownloadFinish(WWW file)
        {
            byte[] data = file.bytes;
            string store = songData.storeID + "_mp3.bytes";
            string filePath = FileUtilities.GetWritablePath(store);

            System.IO.File.WriteAllBytes(filePath, data);
            txtProgress.text = "100%";
            mp3Data = data;
            music_load(mp3Data, mp3Data.Length);
            Init();
        }

        void DownloadFail(string fail)
        {
            iconLoading.SetActive(true);
            txtMessLoading.gameObject.SetActive(true);
            txtMessLoading.text = Localization.Get("er_download_storedata_failed");
            Invoke("ShowbtnWhenError", 1.1f); 
        }
        #endregion

        public void BackHome()
        {
            SceneManager.Instance.OpenScene(Scenes.HomeUI, GroupHomeSceneType.BattleHome);
        }

        public void RetryDownload()
        {
            iconLoading.SetActive(true);
            txtMessLoading.gameObject.SetActive(true);
            txtMessLoading.text = Localization.Get("maingame_downloading");
            txtProgress.gameObject.SetActive(true);
            txtProgress.text = "0%";
            Invoke("ShowbtnWhenError", 1.1f);
            DownloadMp3();
        }

        private void OnGUI()
        {
            #if UNITY_EDITOR
            GUIStyle guiStyles = new GUIStyle(GUI.skin.button);
            guiStyles.fontSize = 15;

            GUIStyle guiLabel = new GUIStyle(GUI.skin.label);
            guiLabel.fontSize = 35;

            if (GUI.Button(new Rect(Screen.width / 5 * 0, Screen.width / 10 * 0, Screen.width / 5, Screen.width / 10), "ZhaoFangJing", guiStyles))
            {
                music_stop();
                PSSongInfo songInfo = new PSSongInfo("ZhaoFangJing", "", "");
                currentSongInfo = songInfo;

                mp3Data = (Resources.Load("Mp3Source/ZhaoFangJing_mp3") as TextAsset).bytes;
                music_load(mp3Data, mp3Data.Length);
                song = PSSong.FromResource("Mp3Source/ZhaoFangJing_imported");
                Init(); 
            }
            #endif
        }
    }
}

