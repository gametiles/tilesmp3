using System;
using UnityEngine;

namespace Nama
{
	[Serializable]
	public class PSSong
	{
		[Serializable]
		public class Note
		{
			public float timeMs;
			public float durationMs;
		}

		public float bpm;
		public float duration;
		public Note[] notes;

		public static PSSong Load(string path)
		{
			string json = System.IO.File.ReadAllText(path);
			PSSong song = JsonUtility.FromJson<PSSong>(json);
			return song;
		}

		public static PSSong FromResource(string name)
		{
			string json = (Resources.Load(name) as TextAsset).text;
			PSSong song = JsonUtility.FromJson<PSSong>(json);
			return song;
		}
	}
}
