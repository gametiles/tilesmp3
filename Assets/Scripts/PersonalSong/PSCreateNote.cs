﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PSCreateNote : MonoBehaviour
{
    public string SongCreate = "";
    public int bpmSongCreate = 120;

    #region Tao Note Nhac
    [Serializable]
    public class Note
    {
        public string name;
        public int midi;
        public float time;
        public float velocity;
        public float duration;
    }

    [Serializable]
    public class ListNote
    {
        public List<Note> notes = new List<Note>();
    }

    [Serializable]
    public class note
    {
        public float timeMs;
        public float durationMs;
    }

    [Serializable]
    public class Songdata
    {
        public float bpm;
        public List<note> notes = new List<note>();
    }


    private void OnGUI()
    {
#if UNITY_EDITOR
        GUIStyle guiStyles = new GUIStyle(GUI.skin.button);
        guiStyles.fontSize = 15;

        GUIStyle guiLabel = new GUIStyle(GUI.skin.label);
        guiLabel.fontSize = 35;

        if (GUI.Button(new Rect(Screen.width / 5 * 0, Screen.width / 10 * 1, Screen.width / 5, Screen.width / 10), "Tao Note", guiStyles))
        {
            TaoNote();
        }
#endif
    }

    void TaoNote()
    {
        Songdata data = new Songdata();
        TextAsset asset = Resources.Load<TextAsset>("Mp3Json/" + SongCreate);
        ListNote listnotes = null;
        if (asset != null)
        {
            listnotes = JsonUtility.FromJson<ListNote>(asset.text);
        }
        List<Note> notes = listnotes.notes;

        data.bpm = bpmSongCreate;
        data.notes = new List<note>();
        for (int i = 0; i < notes.Count; i++)
        {
            note note = new note();
            note.timeMs = notes[i].time * 1000;
            note.durationMs = notes[i].duration * 1000;
            data.notes.Add(note);
        }
        string json = JsonUtility.ToJson(data);

        //string filePath = FileUtilities.GetWritablePath("Doremon.imported");
        string filePath2 = "E:/My/TilesMp3/Assets/Resources/Mp3Source/" + SongCreate + "_imported.json";
        System.IO.File.WriteAllText(filePath2, json);
        Debug.LogError("Tao Note Sussesfull");
    }
    #endregion
}
