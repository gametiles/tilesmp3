using UnityEngine;
using System.Collections;
using Amanotes.PianoChallenge;
using ProjectConstants;
using DG.Tweening;
using Amanotes.Utils;

public class OnBoarding : SSController
{
	[SerializeField] TweenScale helloScale = null;
	[SerializeField] TweenPosition helloPos = null;
	[SerializeField] TweenAlpha helloAlpha = null;
	[SerializeField] Animation wellcomeAnm = null;
	[SerializeField] TweenAlpha wcAlpha = null;
	[SerializeField] TweenAlpha pcAlpha = null;

	[SerializeField] TweenAlpha decorAlpha = null;
	[SerializeField] TweenScale decorScale = null;
	[SerializeField] UILabel lbNameSong = null;
	[SerializeField] UI2DSprite diskCover = null;
	[SerializeField] FirstSongItemView[] songItems = null;
	[SerializeField] GameObject firstSongList = null;

	private bool canPlay = true;

	public AudioSource au;
	public override void OnEnable()
	{
		AnalyticsHelper.Instance.LogOnBoarding("Wellcome Scene");

		StartCoroutine(TweenIcon());
		diskCover.alpha = 0;

		var songIds = GameManager.Instance.GameConfigs.firstSongs;
		var allSongs = GameManager.Instance.StoreData.listAllSongs;
		for (int i = 0; i < songIds.Count; i++)
		{
			var song = SongManager.FindSong(songIds[i], allSongs);
			songItems[i].OnSongSelected += PlaySong;
			songItems[i].SetSong(song);
		}
	}

	private void OnSongItemClicked(SongItemView songView)
	{
		SongManager.Instance.PlayFirstSong(songView.Song);
	}

	public IEnumerator TweenIcon()
	{
		yield return new WaitForSeconds(1f);
		au.Play();
		helloPos.PlayForward();
		helloScale.PlayForward();
		yield return new WaitForSeconds(1.5f);
		wellcomeAnm.Play();
		helloAlpha.PlayForward();
		yield return new WaitForSeconds(2f);
		pcAlpha.PlayForward();
		yield return new WaitForSeconds(3f);
		pcAlpha.PlayReverse();
		wcAlpha.PlayForward();
		yield return new WaitForSeconds(2f);
		decorAlpha.PlayForward();
		decorScale.PlayForward();
		firstSongList.SetActive(true);
		AnalyticsHelper.Instance.LogOnBoarding("Song Scene");
	}

	public void PlaySong(SongDataModel song)
	{
		if (canPlay)
		{
			canPlay = false;
			AnalyticsHelper.Instance.LogOnBoarding("Play Song");
			au.DOFade(0, .5f).OnComplete(() =>
			{
				SongManager.Instance.PlayFirstSong(song);
			}).Play();
		}
	}
}
