﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

class LocalNotificationAndroid
{
	/// <summary>
	/// Inexact uses `set` method
	/// Exact uses `setExact` method
	/// ExactAndAllowWhileIdle uses `setAndAllowWhileIdle` method
	/// Documentation: https://developer.android.com/intl/ru/reference/android/app/AlarmManager.html
	/// </summary>
	public enum ExecuteMode
	{
		Inexact = 0,
		Exact = 1,
		ExactAndAllowWhileIdle = 2
	}

#if UNITY_ANDROID && !UNITY_EDITOR
	private const string NOTIFICATION_MANAGER_CLASS = "com.musicheroesrevenge.pianochallenge2.UnityNotificationManager";
	private const string MAIN_ACTIVITY_CLASS = "com.musicheroesrevenge.pianochallenge2.UnityPlayerActivity";
#endif

	public static void SendNotification (int id, TimeSpan delay, string title, string message)
	{
		SendNotification (id, (int)delay.TotalSeconds, title, message, Color.white);
	}

	public static void SendNotification (
		int id,
		long delay,
		string title,
		string message,
		Color32 bgColor,
		bool sound = true,
		bool vibrate = true,
		bool lights = true,
		string bigIcon = "",
		ExecuteMode executeMode = ExecuteMode.Inexact)
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass pluginClass = new AndroidJavaClass(NOTIFICATION_MANAGER_CLASS);
		if (pluginClass == null)
		{
			return;
		}

		pluginClass.CallStatic(
				"SetNotification",
				id,
				delay * 1000L,
				title,
				message,
				message,
				sound ? 1 : 0,
				vibrate ? 1 : 0,
				lights ? 1 : 0,
				bigIcon,
				"notify_icon_small",
				bgColor.r * 65536 + bgColor.g * 256 + bgColor.b,
				(int)executeMode,
				MAIN_ACTIVITY_CLASS);
#endif
	}

	public static void SendRepeatingNotification (
		int id,
		long delay,
		long timeout,
		string title,
		string message,
		Color32 bgColor,
		bool sound = true,
		bool vibrate = true,
		bool lights = true,
		string bigIcon = "")
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass pluginClass = new AndroidJavaClass(NOTIFICATION_MANAGER_CLASS);
		if (pluginClass == null)
		{
			return;
		}

		pluginClass.CallStatic(
				"SetRepeatingNotification",
				id,
				delay * 1000L,
				title,
				message,
				message,
				timeout * 1000,
				sound ? 1 : 0,
				vibrate ? 1 : 0,
				lights ? 1 : 0,
				bigIcon,
				"notify_icon_small",
				bgColor.r * 65536 + bgColor.g * 256 + bgColor.b,
				MAIN_ACTIVITY_CLASS);
#endif
	}

	public static void CancelNotification (int id)
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass pluginClass = new AndroidJavaClass(NOTIFICATION_MANAGER_CLASS);
		if (pluginClass != null)
		{
			pluginClass.CallStatic("CancelNotification", id);
		}
#endif
	}
}
