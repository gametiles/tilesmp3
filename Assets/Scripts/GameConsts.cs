
namespace Amanotes.PianoChallenge
{
	public class GameConsts
	{
		public const int VERSIONCODE_NEW = 300;
		// neu update thay doi cau truc cac file config thi tang so nay theo versin file config
		public const string CACHE_LANGUAGE = "cache_language";
		//PK ~ Parse key
		public const string PK_DATA = "data";
		public const string PK_TOTAL_STAR = "totalStar";
		public const string PK_TOTAL_CROWN = "totalCrown";
		public const string PK_BATTLES_WON = "battlesWon";
		public const string PK_BATTLES_LOST = "battlesLost";
		public const string PK_USERNAME = "fbUsername";
		public const string PK_FB_AVATAR_URL = "fbAvatar";
		public const string PK_FB_COVER_URL = "fbCover";
		public const string PK_USER_EMAIL = "fbEmail";
		public const string PK_USER_ID = "user_id";
		public const string PK_RANKING = "ranking";
		public const string PK_LEVELRANKING_CONTENT = "content";
		public const string USER_AVATAR_SIZE = "normal";

		//PT ~ Parse Table
		public const string PT_USER = "User";
		public const string PT_LEVEL_RANKING = "LevelRanking";

		internal static string LINK_FACEBOOK_APP = "https://fb.me/992898520829088";
		internal static string APP_ICON_URL = "https://fbcdn-photos-a-a.akamaihd.net/hphotos-ak-xft1/t39.2081-0/p128x128/13311289_979292892189651_384926545_n.png";
		internal static bool BUILD_LIVE = false;

		public const string RESTORE = "restore";

		public const string DeviceType = "DeviceType";
		public const string DeviceModel = "DeviceModel";

		//touch addY
		public const int ADDY_SIMPLE = 320;

		public const string OnBoarding = "ONBOARDING";

		//level
		public const string PREF_CONFIG_LEVEL_VERSION = "Pref_Level_Version";
		public const string FILE_LOCAL_CONFIG_LEVEL = "configlevel";
		// encypt
		private const bool isEncypt = true;

		public static bool IsEncypt {
			get {
				return isEncypt;
			}
		}

		// secret key
		private const string secretKey = "ama_tyzs";

		public static string SecretKey {
			get {
				return secretKey;
			}
		}

		public static float COFF_SPEED_PT = 1;
	}
}
