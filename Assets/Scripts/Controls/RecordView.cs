using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Amanotes.PianoChallenge
{
	[RequireComponent(typeof(UIWidget))]
	public class RecordView : MonoBehaviour
	{
		[SerializeField] UIGrid gridLayout = null;
		[SerializeField] GameObject[] recordItem = null;
		[SerializeField] UIWidget widget = null;

		void Start()
		{
			if (widget == null) { widget = gameObject.GetComponent<UIWidget>(); }
		}

		/// <summary>
		/// Show or hide this control, with or without animation. NOTE: this method also set the alpha of this widget to either 0 or 1
		/// </summary>
		/// <param name="visible">Show or hide?</param>
		/// <param name="fadeDuration">Default is 0, meaning no animation</param>
		public void SetVisible(bool visible, float fadeDuration = 0)
		{
			float toAlpha = visible ? 1 : 0;

			if (fadeDuration == 0)
			{
				widget.cachedGameObject.SetActive(visible);
				widget.alpha = toAlpha;
			}
			else
			{
				bool shouldVisible = visible;
				if (shouldVisible) { widget.cachedGameObject.SetActive(true); }
				DOTween.To((alpha) => widget.alpha = alpha, widget.alpha, toAlpha, fadeDuration)
					.OnComplete(() => widget.cachedGameObject.SetActive(shouldVisible))
					.Play();
			}
		}

		/// <summary>
		/// Show a specified number of record items
		/// </summary>
		/// <param name="num">Number of record items to show</param>
		/// <param name="hideInactive">Should this control show the inactive ones</param>
		public void ShowNumRecord(int num, bool hideInactive = true)
		{
			//validate before going
			if (num <= recordItem.Length)
			{
				for (int i = 0; i < recordItem.Length; i++)
				{
					recordItem[i].SetActive(i < num);
				}
				gridLayout.repositionNow = true;
			}
		}
	}
}
