using UnityEngine;
using System;
using DG.Tweening;

public class CountdownView : MonoBehaviour
{
	[SerializeField] UILabel lbCountdown;
	[SerializeField] UI2DSprite spriteCountdown;

	private Tween tween;
	
	/// <summary>
	/// Show countdown effect and call an action when finished
	/// </summary>
	/// <param name="duration">How long will the countdown last</param>
	/// <param name="timePerStep">How long each countdown step last in seconds</param>
	/// <param name="OnComplete">The action to call after countdown completed</param>
	public void Restart(float duration, float timePerStep, Action OnComplete = null)
	{
		Stop();

		float remaining = duration;

		gameObject.SetActive(true);

		lbCountdown.text = duration.ToString();
		spriteCountdown.fillAmount = 1;
		int loop = Mathf.CeilToInt(duration / timePerStep);

		tween = DOTween.Sequence()
			.Join(DOTween.To(
				() => spriteCountdown.fillAmount,
				(fill) => spriteCountdown.fillAmount = fill,
				0,
				duration)
				.SetEase(Ease.Linear)
				.Play())
			.Join(DOTween.Sequence()
				.AppendInterval(timePerStep)
				.AppendCallback(() =>
				{
					remaining -= timePerStep;
					lbCountdown.text = remaining.ToString();
				})
				.SetLoops(loop)
				.Play()
				)
			.OnComplete(() =>
			{
				Helpers.Callback(OnComplete);
			})
			.Play();
	}

	internal void Stop()
	{
		if (tween != null && tween.IsPlaying()) { tween.Kill(false); }
	}

	internal void Pause()
	{
		if (tween != null && tween.IsPlaying()) { tween.Pause(); }
	}

	internal void Resume()
	{
		if (tween != null && !tween.IsPlaying()) { tween.Play(); }
	}
}
