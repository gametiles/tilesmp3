using UnityEngine;

namespace Amanotes.PianoChallenge
{
	public class RewardDetailView : MonoBehaviour
	{
		[SerializeField] UISprite rewardIcon = null;
		[SerializeField] UILabel lbRewardValue = null;

		public Color lifeColor;
		public Color rubyColor;

		public void SetReward(string iconSpriteName, string rewardValue)
		{
			if (rewardIcon != null)
			{
				rewardIcon.spriteName = iconSpriteName;
			}

			if (lbRewardValue != null)
			{
				lbRewardValue.text = rewardValue;

				if (iconSpriteName == "icon_heart")
				{
					rewardIcon.color = lifeColor;
				}
				else
				{
					rewardIcon.color = rubyColor;
				}
			}
		}
	}
}
