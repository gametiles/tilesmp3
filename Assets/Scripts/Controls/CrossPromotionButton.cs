using UnityEngine;
using Amanotes.Utils;
public class CrossPromotionButton : MonoBehaviour
{
	[SerializeField] protected UI2DSprite image = null;
	[SerializeField] protected UIDragScrollView dragScrollView = null;

	protected CrossPromotion promotedItem = null;

	public void Init(CrossPromotion item, UIScrollView scrollview = null)
	{
		dragScrollView.scrollView = scrollview;
		promotedItem = item;
        fModStudio.Utils.DownloadSprite(item.imageUrl, sprite => image.sprite2D = sprite);
	}

	public virtual void OnClickPlay()
	{
		promotedItem.OnClick();
	}
}
