using UnityEngine;
using Amanotes.Utils.MessageBus;

public class CrossPromotionItemView : MonoBehaviour
{
	[SerializeField] UI2DSprite icon = null;
	[SerializeField] UILabel lblTitle = null;

	CrossPromotion item = null;

	private void OnEnable()
	{
		MessageBus.Instance.Subscribe(MessageBusType.LanguageChanged, OnLanguageChanged);
		Randomize();
	}

	private void OnDisable()
	{
		MessageBus.Instance.Unsubscribe(MessageBusType.LanguageChanged, OnLanguageChanged);
	}

	void OnLanguageChanged(Message msg)
	{
		lblTitle.text = Localization.Get(item.name);
	}

	public bool Randomize()
	{
		item = CrossPromotionManager.Instance.GetRandomItem();
		if (item == null)
		{
			return false;
		}
        fModStudio.Utils.DownloadSprite(item.iconUrl, sprite => icon.sprite2D = sprite);
		OnLanguageChanged(null);
		return true;
	}

	public void OnClickPlay()
	{
		if (item != null)
		{
			item.OnClick();
		}
		Randomize();
	}
}
