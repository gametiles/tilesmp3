using Amanotes.PianoChallenge;
using Amanotes.Utils;
using AudienceNetwork;
using System.Collections;
using UnityEngine;

public class NativeAdView : MonoBehaviour
{
	[SerializeField] UI2DSprite iconImage = null;
	[SerializeField] UIButton btnCall2Action = null;
	[SerializeField] UILabel txtCall2Action = null;
	[SerializeField] UILabel txtTitle = null;
	[SerializeField] UILabel txtSubTitle = null;
	[SerializeField] UILabel txtSocialContext = null;
	[SerializeField] GameObject nativeAdObj;
	[SerializeField] CrossPromotionItemView crossPromoItem;

	static WaitForSeconds waitDownloadImage = new WaitForSeconds(0.25f);

	private void Start()
	{
		nativeAdObj.SetActive(false);
		crossPromoItem.gameObject.SetActive(true);
	}

	private void OnDestroy()
	{
		DestroyNativeAd();
	}

	public void SponsorClick()
	{
		Application.OpenURL("https://m.facebook.com/ads/ad_choices");
	}

	private IEnumerator LoadIcon()
	{
		StartCoroutine(nativeAd.LoadIconImage(nativeAd.IconImageURL));
		while (nativeAd.IconImage == null)
		{
			yield return waitDownloadImage;
		}

		iconImage.sprite2D = nativeAd.IconImage;
	}

	NativeAd nativeAd = null;
	public void LoadNativeAd(Camera camera)
	{
#if UNITY_EDITOR
		OnNativeAdLoadFailed(string.Empty);
#else
		if (nativeAd != null)
		{
			OnNativeAdLoadFailed(string.Empty);
			return;
		}


#if UNITY_IPHONE
		string placement = GameManager.Instance.GameConfigs.nativead_ids[1];
#else
		string placement = GameManager.Instance.GameConfigs.nativead_ids[0];
#endif

		nativeAd = new NativeAd(placement);
		nativeAd.RegisterGameObjectForImpression(gameObject, new UIButton[] { btnCall2Action }, camera);
		nativeAd.NativeAdDidLoad = OnNativeAdLoaded;
		nativeAd.NativeAdDidFailWithError = OnNativeAdLoadFailed;
		nativeAd.NativeAdWillLogImpression = OnNativeAdWillLogImpression;
		nativeAd.NativeAdDidClick = OnNativeAdClicked;
		nativeAd.LoadAd();
#endif
	}

	public void DestroyNativeAd()
	{
		if (nativeAd != null)
		{
			nativeAd.NativeAdDidLoad -= OnNativeAdLoaded;
			nativeAd.NativeAdDidFailWithError -= OnNativeAdLoadFailed;
			nativeAd.NativeAdWillLogImpression -= OnNativeAdWillLogImpression;
			nativeAd.NativeAdDidClick -= OnNativeAdClicked;
			nativeAd.Dispose();
			nativeAd = null;
		}
	}

	private void OnNativeAdClicked()
	{
		AnalyticsHelper.Instance.LogClickItem("Native Ad");
	}


	private void OnNativeAdWillLogImpression()
	{
	}

	private void OnNativeAdLoaded()
	{
		crossPromoItem.gameObject.SetActive(false);
		nativeAdObj.SetActive(true);
		StartCoroutine(LoadIcon());

		txtCall2Action.text = nativeAd.CallToAction;
		txtTitle.text = nativeAd.Title;
		txtSubTitle.text = nativeAd.Subtitle;

		bool socialContextAvailable = !string.IsNullOrEmpty(nativeAd.SocialContext);
		txtSocialContext.gameObject.SetActive(socialContextAvailable);
		if (socialContextAvailable)
		{
			txtSocialContext.text = nativeAd.SocialContext;
		}
	}

	private void OnNativeAdLoadFailed(string error)
	{
		Development.Log("NATIVE ADS: Load native ads failed with error: " + error);
		nativeAdObj.SetActive(false);
		crossPromoItem.gameObject.SetActive(true);
	}
}
