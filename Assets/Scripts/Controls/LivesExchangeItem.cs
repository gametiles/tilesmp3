using UnityEngine;
using System;

namespace Amanotes.PianoChallenge
{
	public class LivesExchangeItem : MonoBehaviour
	{
		[SerializeField] UILabel lblEnergySupply = null;
		[SerializeField] UILabel lblRubyPrice = null;
		[HideInInspector] public int livesValue;
		[HideInInspector] public int price;

		public event Action<LivesExchangeItem> OnLifeItemClicked;

		/// <summary>
		/// init ui for x5,x10,x20 panel prefab
		/// </summary>
		public void InitUI()
		{
			lblEnergySupply.text = "x" + livesValue;
			lblRubyPrice.text = "x" + price;
		}

		/// <summary>
		/// "BuyEnergySupply" btn buy
		/// </summary>
		public void BuyEnergySupply()
		{
			Helpers.CallbackWithValue(OnLifeItemClicked, this);
		}
	}
}
