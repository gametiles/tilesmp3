using UnityEngine;
using Amanotes.Utils;
using MovementEffects;
using System.Collections.Generic;
using Amanotes.Utils.MessageBus;

namespace Amanotes.PianoChallenge
{

	//Hear it lay, the start of every thing
	//      .--,       .--,
	// ( (  \.---./  ) )
	//  '.__/o   o\__.'
	//     {=  ^  =}
	//      >  -  <
	//     /       \
	//    //       \\
	//   //|   .   |\\
	//   "'\       /'"_.-~^`'-.
	//      \  _  /--'         `
	//    ___)( )(___
	//   (((__) (__)))
	public class GameInitializer : MonoBehaviour
	{
		private enum InitializeState
		{
			GameStarted,
			GameVersions,
			GameConfigs,
			GameLocalization,
			UserData,
			AchievementData,
			GameStates,
			GameStoreData,
			END
		}

		private const int NUM_INITIALIZE_STEP = (int)InitializeState.END;

		public SplashScreenController splashScreen;

		private bool[] gamestepInitialized;

		public void StartInitData()
		{
			DG.Tweening.DOTween.Init();
			MessageBus.Instance.Initialize();
			splashScreen.Initialize();

			GameManager.Instance.LoadLocalGameData();
			GameManager.Instance.SaveGameData();
			GameManager.Instance.Initialize();
			SceneManager.Instance.Initialize();
			GeoLocationManager.Instance.Initialize();
			MidiPlayer.Instance.Initialize();
			Debug.Log("Game is initializing...");

			FacebookManager.Instance.Init();
			AudioManager.Instance.Initialize();

			ShowLoadingScreen();

//			// Tapstream
//#if !UNITY_EDITOR
//			Tapstream.Config conf = new Tapstream.Config();
//#if UNITY_IPHONE
//			conf.Set("idfa", "<IDFA goes here>");
//#endif
//			Tapstream.Create("amanotesjsc", "I5XQCO8nQi2NGn1Lz42Y3g", conf);
//#endif
		}

		private void LoadLanguageConfig()
		{
			string systemLanguage = (Application.systemLanguage.ToString());

			if (PlayerPrefs.GetString(GameConsts.CACHE_LANGUAGE, "") != "")
			{
				systemLanguage = PlayerPrefs.GetString(GameConsts.CACHE_LANGUAGE, "");
			}

			List<string> lLanguage = GameManager.Instance.GameConfigs.localizeConfig;
			for (int i = 0; i < lLanguage.Count; i++)
			{
				if (lLanguage[i].Contains(systemLanguage) && systemLanguage != "")
				{
					Localization.LoadAndSelect(systemLanguage);
					PlayerPrefs.SetString(GameConsts.CACHE_LANGUAGE, systemLanguage);
					PlayerPrefs.Save();
					return;
				}
			}

			Localization.LoadAndSelect("English");
		}

		private void ShowLoadingScreen()
		{
			splashScreen.ShowLoading();
			InitializeGame();
		}

		/// <summary>
		/// Start the initialization of the game
		/// </summary>
		private void InitializeGame()
		{
			gamestepInitialized = new bool[NUM_INITIALIZE_STEP];
			gamestepInitialized[(int)InitializeState.GameStarted] = true;
			Debug.Log("Initializing game...");

			Timing.RunCoroutine(C_InitializeGameSteps());
			//start coroutine checking the game and enter if all steps have been initialized
			Timing.RunCoroutine(C_EnterGameWhenReady());

			//make that bitch calculates its id
			string nothing = GameManager.Instance.DeviceID;
		}

		/// <summary>
		/// Initialize each step of the game correctly
		/// </summary>
		private IEnumerator<float> C_InitializeGameSteps()
		{
			// initialize game version first
			int stepGameVersion = (int)InitializeState.GameVersions;
			if (gamestepInitialized[stepGameVersion] != true)
			{
				Timing.RunCoroutine(C_InitializeGameVersions());
			}
			while (gamestepInitialized[stepGameVersion] != true)
			{
				yield return Timing.WaitForSeconds(0.03f);
			}

			//initialize game configs
			int stepGameConfig = (int)InitializeState.GameConfigs;
			if (gamestepInitialized[stepGameConfig] != true)
			{
				Timing.RunCoroutine(C_InitializeGameConfig());
			}
			while (gamestepInitialized[stepGameConfig] != true)
			{
				yield return Timing.WaitForSeconds(0.03f);
			}

			int stepUserData = (int)InitializeState.UserData;
			if (gamestepInitialized[stepUserData] != true)
			{
				Timing.RunCoroutine(C_InitializeUserData());
			}
			while (gamestepInitialized[stepUserData] != true)
			{
				yield return Timing.WaitForSeconds(0.03f);
			}

			//initialize steps that can be run parallel
			int stepStoreData = (int)InitializeState.GameStoreData;
			if (gamestepInitialized[stepStoreData] != true)
			{
				Timing.RunCoroutine(C_InitializeStoreData());
			}

			int stepAchievement = (int)InitializeState.AchievementData;
			if (gamestepInitialized[stepAchievement] != true)
			{
				Timing.RunCoroutine(C_InitializeAchievementData());
			}

			int stepLocalization = (int)InitializeState.GameLocalization;
			if (gamestepInitialized[stepLocalization] != true)
			{
				Timing.RunCoroutine(C_InitializeGameLocalization());
			}

			int stepGameState = (int)InitializeState.GameStates;
			if (gamestepInitialized[stepGameState] != true)
			{
				PrepareGameState();
			}
		}

		/// <summary>
		/// The sequence to wait and start game as necessary
		/// </summary>
		private IEnumerator<float> C_EnterGameWhenReady()
		{
			int i, count;
			bool shouldWait = true;
			while (shouldWait)
			{
				count = 0;
				//check and count all initialized step of the game 
				for (i = 0; i < NUM_INITIALIZE_STEP; i++)
				{
					if (gamestepInitialized[i] == true)
					{
						++count;
					}
				}

				//if number of initialized step is equal to total step, meaning the game has been initialized, no need to wait any more
				if (count == NUM_INITIALIZE_STEP)
				{
					shouldWait = false;
				}

				yield return Timing.WaitForSeconds(0.05f);
			}

			//restore achievement data here after the player profile has been initialized
			if (!string.IsNullOrEmpty(ProfileHelper.Instance.AchievementPropertiesCSV) &&
				!string.IsNullOrEmpty(ProfileHelper.Instance.AchievementUnlockedAndClaimedCSV))
			{
				AchievementHelper.Instance.RestoreAchievementDataFromDump(
					ProfileHelper.Instance.AchievementPropertiesCSV,
					ProfileHelper.Instance.AchievementUnlockedAndClaimedCSV);
			}

			while (!AudioManager.Instance.Initialized)
			{
				yield return Timing.WaitForSeconds(0.05f);
			}

			splashScreen.EnterGame();
		}

		/// <summary>
		/// Download and check local version of all download-able config files to see if there are anything need updated
		/// </summary>
		IEnumerator<float> C_InitializeGameVersions()
		{
			yield return Timing.WaitForSeconds(0.01f);
			GameManager.Instance.LoadLocalGameVersions();
			GameManager.Instance.RefreshGameVersionsData(OnGameVersionsInitialized, OnGameVersionRefreshFailed);
		}

		private void OnGameVersionsInitialized()
		{
			gamestepInitialized[(int)InitializeState.GameVersions] = true;
		}

		private void OnGameVersionRefreshFailed(string errorMessage)
		{
			Debug.LogError("Error downloading game version data: " + errorMessage);
			splashScreen.ShowMessage(Localization.Get("er_download_version_failed"));
			splashScreen.ShowRetryButton();
		}

		/// <summary>
		/// Prepare latest available game config for this game
		/// </summary>
		IEnumerator<float> C_InitializeGameConfig()
		{
			//load old game config first 
			GameManager.Instance.LoadGameConfigDataFromLocal();
			yield return Timing.WaitForSeconds(0.01f);
			Debug.Log("Initializing game config...");
			GameManager.Instance.RefreshGameConfigData(OnGameConfigInitialized, OnGameConfigRefreshFailed);
		}

		private void OnGameConfigInitialized()
		{
			// Overwrite AB test options
			if (GameManager.Instance.GameConfigs.songConfigOverride >= 0)
			{
				ABTestManager.Instance.LevelConfigOption = GameManager.Instance.GameConfigs.songConfigOverride;
			}
			if (GameManager.Instance.GameConfigs.levelConfigOverride >= 0)
			{
				ABTestManager.Instance.LevelConfigOption = GameManager.Instance.GameConfigs.levelConfigOverride;
			}
			if (GameManager.Instance.GameConfigs.inGameAchievementOverride >= 0)
			{
				ABTestManager.Instance.InGameAchievementOption = GameManager.Instance.GameConfigs.inGameAchievementOverride;
			}
			if (GameManager.Instance.GameConfigs.interstitialPolicy >= 0)
			{
				ABTestManager.Instance.InterstitialPolicy = GameManager.Instance.GameConfigs.interstitialPolicy;
			}
			if (GameManager.Instance.GameConfigs.day0NotificationOverride >= 0)
			{
				ABTestManager.Instance.Day0PnOption = GameManager.Instance.GameConfigs.day0NotificationOverride;
			}

			#region CROSSPROMOTION
			//CrossPromotionManager.Instance.Init(GameManager.Instance.GameConfigs.crossPromotionStore);
#endregion
			//AB Testing for ad interval
#region INITPARSE
			//GameManager.Instance.localParse.Init();
#endregion
			//initialize game services after receiving config from server
			Debug.Log(GameManager.Instance.GameConfigs.storeDataURL);
			gamestepInitialized[(int)InitializeState.GameConfigs] = true;
			if (PlayerPrefs.GetInt("SettingEffect", 0) == 0)
			{
				GameManager.Instance.SettingEffect();
			}
            			
		}

		private void OnGameConfigRefreshFailed(string errorMessage)
		{
			Debug.LogError("Error downloading game config data " + errorMessage);
			splashScreen.ShowMessage(Localization.Get("er_download_config_failed"));
			splashScreen.ShowRetryButton();
		}

		void LoadGameInit()
		{
			InitializeAdsService();
			InitializeAnalyticService();
			InitializePushNotificationService();

		}

		void ShowErrorDownload(string mssage)
		{
			splashScreen.ShowMessage(mssage);
			splashScreen.ShowRetryButton();
		}

		public void Step5_1_DownloadConfigUserLevel()
		{
			GameManager.Instance.RefreshLevelConfig(
				()=> { },
				(error)=> {
					ShowErrorDownload("Error downloading config level");
				});
		}

		IEnumerator<float> C_InitializeGameLocalization()
		{
			Debug.Log("Initializing game localization...");
			yield return Timing.WaitForSeconds(0.05f);
			GameManager.Instance.RefreshLocalizationData(
				OnGameLocalizationDataInitialized,
				OnGameLocalizationDataRefreshFailed);
		}

		private void OnGameLocalizationDataInitialized(byte[] localizationData)
		{
			if (localizationData != null)
			{
				Localization.LoadCSV(localizationData, false);
				LoadLanguageConfig();
			}
			else
			{
				Debug.LogWarning("Can't load localization CSV");
			}
			gamestepInitialized[(int)InitializeState.GameLocalization] = true;
		}

		private void OnGameLocalizationDataRefreshFailed(string errorMessage)
		{
			Debug.LogError("Error downloading game localization data: " + errorMessage);
			splashScreen.ShowMessage(Localization.Get("er_download_localization_failed"));
			splashScreen.ShowRetryButton();
		}

		IEnumerator<float> C_InitializeAchievementData()
		{
			yield return Timing.WaitForSeconds(0.05f);
			Debug.Log("Initializing Achievement data...");
			GameManager.Instance.RefreshAchievementData(OnAchivementInitialized, OnAchievementRefreshFailed);
		}

		private void OnAchievementRefreshFailed(string obj)
		{
			splashScreen.ShowMessage(Localization.Get("er_download_ach_failed"));
			splashScreen.ShowRetryButton();
		}

		private void OnAchivementInitialized()
		{
			gamestepInitialized[(int)InitializeState.AchievementData] = true;
		}

		IEnumerator<float> C_InitializeUserData()
		{
			ProfileHelper.Instance.Initialize();
			yield return Timing.WaitForSeconds(0.05f);
			Debug.Log("Initializing user data...");
			ProfileHelper.Instance.InitializeUserData(OnUserDataInitialized);
		}

		private void OnUserDataInitialized(bool success)
		{
			if (success)
			{
				LivesManager.Instance.Initialize();
				HighScoreManager.Instance.Initialize();
				gamestepInitialized[(int)InitializeState.UserData] = true;

				Step5_1_DownloadConfigUserLevel();
			}
			else
			{
				Debug.LogError("Error loading user data");
				splashScreen.ShowMessage(Localization.Get("er_download_userdata_failed"));
				splashScreen.ShowRetryButton();
			}
		}

		/// <summary>
		/// Actually initialize game state and all game related aspect
		/// </summary>
		public void PrepareGameState()
		{
			//MidiPlayer.Instance.LoadSong(MidiPlayer.Instance.backgroundMidiFile.bytes);
			gamestepInitialized[(int)InitializeState.GameStates] = true;
		}


		/// <summary>
		/// Prepare latest available store data for this game
		/// </summary>
		IEnumerator<float> C_InitializeStoreData()
		{
			float timer = Time.realtimeSinceStartup;
			float timeOut = 2f;
			while (true)
			{
				if (GeoLocationManager.Instance.IsLoaded == true)
				{
					break;
				}
				if (Time.realtimeSinceStartup - timer >= timeOut)
				{
					break;
				}
				yield return Timing.WaitForSeconds(0.1f);
			}

			string extension = string.Empty;
			if (GeoLocationManager.Instance.CountryCode.GetCountryCode().Contains("us"))
			{
				ABTestManager.Instance.InterstitialPolicy = 0; // Don't AB test interstitial in US
				extension = "-us";
			}
			else
			{
				ABTestManager.Instance.InGameAchievementOption = 0; // AB test in game achievement in US only
			}

			string builtinStoreDataName = "SongData" + extension;

			extension += ABTestManager.Instance.SongConfigExtension;
			string url = GameManager.Instance.GameConfigs.storeDataURL;
			GameManager.Instance.GameConfigs.storeDataURL = url.Insert(url.LastIndexOf('.'), extension);

			Debug.Log("Initializing store data...");
			GameManager.Instance.RefreshStoreData(builtinStoreDataName, OnStoreDataInitialized, OnStoreDataRefreshFailed);
		}

		private void OnStoreDataInitialized()
		{
			Debug.Log("Store data initialized...");
			gamestepInitialized[(int)InitializeState.GameStoreData] = true;
			DailyRewardManager.Instance.Init();
		}

		private void OnStoreDataRefreshFailed(string errorMessage)
		{
			Debug.LogError("Error downloading store data " + errorMessage);
			splashScreen.ShowMessage(Localization.Get("er_download_storedata_failed"));
			splashScreen.ShowRetryButton();
		}

		private void InitializeAdsService()
		{
			AdHelper.Instance.Initialize();
		}

		private void InitializeAnalyticService()
		{
			AnalyticsHelper.Instance.Initialize();
			AnalyticsHelper.Instance.LogGameLaunch(GameManager.Instance.GameData.numStart);
			ABTestManager.Instance.Initialize();
		}

		private void InitializePushNotificationService()
		{
		}

		/// <summary>
		/// Button retry clicked
		/// </summary>
		public void RetryLastInitialization()
		{
			splashScreen.HideMessage();
			splashScreen.HideRetryButton();

			Timing.RunCoroutine(C_InitializeGameSteps());
		}
	}
}
