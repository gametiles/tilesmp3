using UnityEngine;
using Amanotes.PianoChallenge;
using Amanotes.Utils.MessageBus;
using DG.Tweening;

public class LevelBarInMenu : MonoBehaviour
{
	[SerializeField] UISlider lvProgress = null;
	[SerializeField] UILabel lblLevel = null;
	[SerializeField] Animator animPlusExp = null;

	// Use this for initialization
	void OnEnable()
	{
		MessageBus.Instance.Subscribe(MessageBusType.ExpIncreased, OnExpIncreased);
		MessageBus.Instance.Subscribe(MessageBusType.UserDataChanged, RefeshInfo);
		RefeshInfo();
	}

	void RefeshInfo(Message mgs)
	{
		RefeshInfo();
	}

	void RefeshInfo()
	{
		int lv = ProfileHelper.Instance.Level;
		int totalExp = ProfileHelper.Instance.TotalExp;

		try
		{
			ConfigUserLevelItem configLv = GameManager.Instance.configUserLevel.GetConfigUserByLevel(lv);
			int ExpNextLv = configLv.ExpNeedtoNextLevel;
			if (ExpNextLv > 0)
			{
				float expProgressBar = (totalExp - configLv.EXPInTotal) / (float)ExpNextLv;

				expProgressBar = expProgressBar < 0 ? 0 : expProgressBar;

				lvProgress.value = expProgressBar;
				lblLevel.text = lv.ToString();
			}
			else
			{
				lvProgress.value = 0;
				lblLevel.text = lv.ToString();
			}
		}
		catch (System.Exception)
		{

		}
	}

	void OnDisable()
	{
		MessageBus.Instance.Unsubscribe(MessageBusType.ExpIncreased, OnExpIncreased);
		MessageBus.Instance.Unsubscribe(MessageBusType.UserDataChanged, RefeshInfo);
	}

	void OnExpIncreased(Message msg)
	{
		int exp = (int)msg.data;

		animPlusExp.SetTrigger("default");
		int animatingScore = 0;
		//update score with a little animation
		float delay = 0f;
		if (exp == 0)
		{
			delay = 0.5f;
		}
		DOTween.To(() => animatingScore, x => animatingScore = x, exp, .5f)
			.OnUpdate(() =>
			{
			})
				.SetDelay(delay)
			.OnComplete(() =>
			{
				animPlusExp.SetTrigger("PlayAnim");
				RefeshInfo();
			})
			.Play();
	}

	// Update is called once per frame
	void Update()
	{

	}
}
