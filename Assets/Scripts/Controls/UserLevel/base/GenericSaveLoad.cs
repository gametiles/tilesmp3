using UnityEngine;
using FullSerializer;
using System;

public static class GenericSaveLoad
{
	public static void SaveObjectToFile<T>(T data, string fileName)
	{
		if (data != null)
		{
			fsData jsonData;
			FileUtilities.JSONSerializer.TrySerialize<T>(data, out jsonData);
			string json = jsonData.ToString();
			string encode = FileUtilities.EncodeData(json);
			FileUtilities.SaveFile(System.Text.Encoding.UTF8.GetBytes(encode), fileName);
		}
	}

	public static void SaveTextToFile(string data, string fileName)
	{
		if (data != null)
		{
			string encode = FileUtilities.EncodeData(data);
			FileUtilities.SaveFile(System.Text.Encoding.UTF8.GetBytes(encode), fileName);
		}
	}

	public static string LoadTextFromFile(string fileName)
	{
		string data = "";
		byte[] localSaved = FileUtilities.LoadFile(fileName);
		if (localSaved == null)
		{
			data = "";
		}
		else
		{
			string encode = System.Text.Encoding.UTF8.GetString(localSaved);
			string json = FileUtilities.DecodeData(encode);
			fsData jsonData = fsJsonParser.Parse(json);
			fsResult result = FileUtilities.JSONSerializer.TryDeserialize(jsonData, ref data);
			if (result.Failed)
			{
				Debug.Log("Failed to de-serialize object for file " + fileName);
			}
			else
			{
			}
		}
		return data;
	}

	public static T LoadObjectFromFile<T>(string fileName)
	{
		T data = default(T);
		byte[] localSaved = FileUtilities.LoadFile(fileName);
		if (localSaved == null)
		{
			data = Activator.CreateInstance<T>();
			Debug.Log(data.ToString() + " not exist, create default");
		}
		else
		{
			string encode = System.Text.Encoding.UTF8.GetString(localSaved);
			string json = FileUtilities.DecodeData(encode);
			fsData jsonData = fsJsonParser.Parse(json);
			fsResult result = FileUtilities.JSONSerializer.TryDeserialize(jsonData, ref data);
			if (result.Failed)
			{
				Debug.Log("Failed to de-serialize object for file " + fileName);
			}
			else
			{
			}
		}
		return data;
	}
}
