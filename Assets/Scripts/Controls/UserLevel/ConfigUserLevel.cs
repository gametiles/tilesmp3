using System;
using FileHelpers;

[DelimitedRecord("\t")]
[IgnoreFirst(1)]
[IgnoreEmptyLines()]
[IgnoreCommentedLines("//")]
public class ConfigUserLevelItem
{
	public int level;
	public int EXPInTotal;
	public int ExpNeedtoNextLevel;
	public int RewardRuby;
	public float MultiExp;
	[FieldIgnored]
	public int maxTurn = 15;
}

public class ConfigUserLevel : GConfigDataTable<ConfigUserLevelItem>
{
	public ConfigUserLevel()
		: base("ConfigUserLevel")
	{
	}

	protected override void OnDataLoaded()
	{
		RebuildIndexField<int>("level");
	}

	public ConfigUserLevelItem GetConfigUserByLevel(int level)
	{
		try
		{
			return FindRecordByIndex("level", level);
		}
		catch (Exception)
		{
			return null;
		}

	}

	public int GetLevelMax()
	{
		int level = 1;
		ConfigUserLevelItem configLv = GetConfigUserByLevel(level);

		while (configLv.ExpNeedtoNextLevel > 0)
		{
			level++;
			configLv = GetConfigUserByLevel(level);

			if (configLv == null)
				break;
		}

		return level;
	}
}
