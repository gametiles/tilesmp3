using UnityEngine;
using Amanotes.PianoChallenge;
using Amanotes.Utils;
using Amanotes.Utils.MessageBus;
using ProjectConstants;

public class UserInfoPopup : SSController
{
	[SerializeField] UILabel lblUserName = null;
	[SerializeField] UILabel lblLevel = null;
	[SerializeField] UILabel lblExp = null;
	[SerializeField] UILabel lblCrown = null;
	[SerializeField] UILabel lblStar = null;
	[SerializeField] UISlider progressBar = null;
	[SerializeField] UITexture avatar = null;
	[SerializeField] Texture2D defaultAvatar = null;
	[SerializeField] GameObject winMedal = null;
	[SerializeField] GameObject loseMedal = null;
	[SerializeField] UILabel winsText = null;
	[SerializeField] UILabel facebookInstruction = null;
	[SerializeField] UILabel battleInstruction = null;

	private int facebookTextChoice = 1;
	private string facebookText = null;

	new private void Start()
	{
		facebookTextChoice = Random.Range(0, 2); // Random 0 or 1
	}

	// Update is called once per frame
	public override void OnEnable()
	{
		lblCrown.text = ProfileHelper.Instance.TotalCrowns.ToString();
		lblStar.text = ProfileHelper.Instance.TotalStars.ToString();

		winMedal.SetActive(ProfileHelper.Instance.BattleRating > 0);
		loseMedal.SetActive(ProfileHelper.Instance.BattleRating <= 0);

		winsText.text = "[FFA300]" + ProfileHelper.Instance.Wins + "[525252]|[13B300]" + (ProfileHelper.Instance.Loses);

		int lv = ProfileHelper.Instance.Level;
		lblLevel.text = lv.ToString();

		int totalExp = ProfileHelper.Instance.TotalExp;
		ConfigUserLevelItem configLv = GameManager.Instance.configUserLevel.GetConfigUserByLevel(lv);
		int ExpNextLv = configLv.ExpNeedtoNextLevel;
		if (ExpNextLv > 0)
		{
			float expProgressBar = (totalExp - configLv.EXPInTotal) / (float)ExpNextLv;

			expProgressBar = expProgressBar < 0 ? 0 : expProgressBar;

			if (expProgressBar > 1)
			{
				lblExp.text = Localization.Get("275");// max level
				expProgressBar = 1;
			}
			else
			{
				lblExp.text = (totalExp - configLv.EXPInTotal).ToString() + "/" + ExpNextLv;
			}

			progressBar.value = expProgressBar;
		}

		DownloadFacebookAvatar();

		MessageBus.Instance.Subscribe(MessageBusType.UserDataChanged, OnUserDataChanged);

		RefreshInstructions();
	}

	public override void OnDisable()
	{
		MessageBus.Instance.Unsubscribe(MessageBusType.UserDataChanged, OnUserDataChanged);
	}

	public void GotoBattle()
	{
		SceneManager.Instance.CloseScene(); // Close this popup first
		AnalyticsHelper.Instance.LogBattleLobbyOpen(Scenes.UserInfoPopup.ToString());
		BattleManager.Instance.JoinBattle();
	}

	private void RefreshInstructions()
	{
		bool fbLoggedIn = Facebook.Unity.AccessToken.CurrentAccessToken != null;
		if (!fbLoggedIn)
		{
			facebookTextChoice = (facebookTextChoice + 1) % 2;
			if (facebookTextChoice == 0)
			{
				facebookInstruction.text = Localization.Get("resultscene_loginmessage").Replace("{0}", 20.ToString());
				facebookText = "resultscene_loginmessage";
			}
			else
			{
				facebookInstruction.text = Localization.Get("293");
				facebookText = "293";
			}
			facebookInstruction.gameObject.SetActive(true);
			AnalyticsHelper.Instance.LogFacebookTextShown(Scenes.UserInfoPopup.ToString());
		}
		else
		{
			facebookInstruction.gameObject.SetActive(false);
		}

		int battles = ProfileHelper.Instance.Wins + ProfileHelper.Instance.Loses;
		if (fbLoggedIn && battles == 0)
		{
			battleInstruction.gameObject.SetActive(true);
		}
		else
		{
			battleInstruction.gameObject.SetActive(false);
		}
	}

	private void OnUserDataChanged(Message obj)
	{
		RefreshInstructions();
		DownloadFacebookAvatar();
	}

	private void DownloadFacebookAvatar()
	{
		if (Facebook.Unity.AccessToken.CurrentAccessToken != null)
		{
			try
			{
				if (Parse.ParseUser.CurrentUser != null)
				{
					if (Parse.ParseUser.CurrentUser.ContainsKey(GameConsts.PK_USERNAME))
						lblUserName.text = (string)Parse.ParseUser.CurrentUser[GameConsts.PK_USERNAME];
				}
			}
			catch (System.Exception e)
			{
				Debug.LogError(e);
				throw;
			}

			string url = "https://graph.facebook.com/" + Facebook.Unity.AccessToken.CurrentAccessToken.UserId + "/picture?type=normal";
			ResourceLoaderManager.Instance.DownLoadTexture(url, texture =>
			{
				//Debug.Log("DownloadFacebookAvatar finish");
				if (texture != null)
				{
					avatar.mainTexture = texture;
				}
			});
		}
		else
		{
			avatar.mainTexture = defaultAvatar;
			lblUserName.text = "";
		}
	}

	public void LoginFacebook()
	{
		FacebookManager.Instance.loginPlace = Scenes.UserInfoPopup.ToString();
		FacebookManager.Instance.incentiveTextChoice = facebookText;
		ProfileHelper.Instance.LoginFacebook();
	}

	public void OnClose()
	{
		SceneManager.Instance.CloseScene();
	}
}
