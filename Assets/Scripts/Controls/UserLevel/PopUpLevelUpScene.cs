using UnityEngine;
using Amanotes.PianoChallenge;
using Amanotes.Utils;
using System.Collections.Generic;
using ProjectConstants;

public class PopUpLevelUpScene : SSController
{
	[SerializeField] UILabel lblLevel = null;
	[SerializeField] UILabel lblRuby = null;
	[SerializeField] UILabel lblLevelUnlock = null;
	[SerializeField] SongItemViewLite songItemView = null;
	[SerializeField] Transform grid = null;

	bool onsetFinish = false;

	List<int> levelUps;

	private int bonusDiamonds = 1;

	public override void OnSet(object data)
	{
		levelUps = data as List<int>;
		if (levelUps == null || levelUps.Count <= 0)
		{ // Something wrong happend
			SceneManager.Instance.CloseScene();
			return;
		}

		int rewardedDiamonds = GameManager.Instance.configUserLevel.GetConfigUserByLevel(levelUps[0]).RewardRuby;
		lblRuby.text = "+" + rewardedDiamonds;

		LevelUpProgress.Instance.OnLevelUp(levelUps[0]);
		SongDataModel premiumSong = SongManager.Instance.OnLevelUp(levelUps[0]);
		SetSongListUnlock(levelUps[0], premiumSong);

		levelUps.RemoveAt(0);

		lblLevel.text = ProfileHelper.Instance.Level.ToString();

		onsetFinish = true;
	}

	void SetSongListUnlock(int level, SongDataModel premiumSong)
	{
		StoreDataModel mode = GameManager.Instance.StoreData;
		List<SongDataModel> listSongdata = GameManager.Instance.StoreData.GetSongDataModelByLevel(level);

		if ((listSongdata == null || listSongdata.Count == 0) && premiumSong == null)
		{
			lblLevelUnlock.gameObject.SetActive(false);
			return;
		}

		lblLevelUnlock.gameObject.SetActive(true);
		for (int i = 0; i < listSongdata.Count; i++)
		{
			SongItemViewLite songItem = Instantiate(songItemView);
			songItem.transform.SetParent(grid);
			songItem.transform.localScale = new Vector3(1, 1, 1);
			songItem.OnSongSelected += OnSongItemClicked;
			songItem.SetSong(listSongdata[i]);
		}

		if (premiumSong != null)
		{
			SongItemViewLite songItem = Instantiate(songItemView);
			songItem.transform.SetParent(grid);
			songItem.transform.localScale = new Vector3(1, 1, 1);
			songItem.OnSongSelected += OnSongItemClicked;
			int nTries = SongManager.Instance.PlayTimesRemaining(premiumSong);
			songItem.SetSong(premiumSong, nTries);
		}
	}

	public void OnClickCollect()
	{
		if (!onsetFinish) return;

		SceneManager.Instance.CloseScene();

		if (levelUps != null && levelUps.Count > 0)
		{ // There are still new levels unprocessed
			SceneManager.Instance.OpenPopup(Scenes.LevelUpPopUp, levelUps);
			return;
		}

		if (BattleManager.Instance.goingToBattle)
		{ // Redirect to battle
			AnalyticsHelper.Instance.LogBattleLobbyOpen("Replay");
			BattleManager.Instance.Replay();
			return;
		}

		if (GameManager.Instance.SessionData.playMode == GAME_PLAY_MODE.NORMAL)
		{ // First reward
			DailyRewardManager.Instance.CheckFirstReward();
		}
	}

	private void OnSongItemClicked(SongDataModel song)
	{
		BattleManager.Instance.goingToBattle = false;

		//check if user has enough lives to continue?
		SongManager.Instance.TryToPlay(song, Scenes.LevelUpPopUp.ToString());
	}

	private void OnRewardVideoWatched()
	{
		ProfileHelper.Instance.CurrentDiamond += bonusDiamonds;
		OnClickCollect();
	}

	public override void OnKeyBack()
	{
		OnClickCollect ();
	}
}
