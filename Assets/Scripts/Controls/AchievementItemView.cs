using UnityEngine;
using Achievement;
using System;

namespace Amanotes.PianoChallenge
{
	public class AchievementItemView : MonoBehaviour
	{
		public event Action<AchievementItemView> OnClaimAchievement;

		public const string MESSAGE_HIDDEN_ACHIEVEMENT = "Hidden. Continue playing, and maybe, with some luck, you can unlock it";
		public static readonly Color colorbgClaimed = new Color(215 / 255f, 215 / 255f, 215 / 255f);
		public static readonly Color colorbgNormal = new Color(1, 1, 1);

		[Header("Achievement's information")]
		[SerializeField]
		private UILabel lbTitle;
		[SerializeField]
		private UILabel lbDescription;
		[SerializeField]
		private UILabel lblProgress;
		[SerializeField]
		private GameObject imgCompletedStamp;
		[SerializeField]
		private UISprite imgOverlay;

		private AchievementModel model;
		public AchievementModel Model { get { return model; } set { model = value; RefreshItemView(); } }
		public int index;

		[Header("Rewards")]
		[SerializeField] RewardDetailView rewardDetail;

		[Header("Interaction")]
		[SerializeField]
		private UIButton btnClaim;

		public void RefreshItemView()
		{
			if (model != null)
			{
				btnClaim.gameObject.SetActive(false);
				imgCompletedStamp.SetActive(false);

				//set title
				lbTitle.text = Localization.Get(model.title);
				lbDescription.text = Localization.Get(model.description);

				//if achievement is unlocked
				if (model.isUnlocked)
				{
					imgOverlay.cachedGameObject.SetActive(false);
					//and claimed
					if (model.isClaimed)
					{
						imgCompletedStamp.SetActive(true);
					}
					//if not claimed, show the claim button
					else
					{
						btnClaim.gameObject.SetActive(true);
					}
				}
				//if the achievement is not unlocked
				else
				{
					imgOverlay.cachedGameObject.SetActive(true);
					if (model.listReward.Count > 0)
					{
						//show the reward details
						lblProgress.text = Math.Round(AchievementHelper.Instance.GetAchievementProgress(model.ID) * 100, 2) + "%";
					}
				}
			}
			string spriteName = null;
			if (model.listReward[0].type == "life")
			{
				spriteName = "icon_heart";
			}
			else
			{
				spriteName = "icon_ruby";
			}
			rewardDetail.SetReward(spriteName, model.listReward[0].value.ToString());
		}

		public void OnClaimButtonClicked()
		{
			Helpers.CallbackWithValue(OnClaimAchievement, this);
		}
	}
}
