using UnityEngine;
using System;

namespace Amanotes.PianoChallenge
{
	public class SongItemViewLite : MonoBehaviour
	{
		[SerializeField] UILabel lbSongTitle;
		[SerializeField] UILabel lbAuthor;

		[SerializeField] GameObject bannerHot;
		[SerializeField] GameObject bannerNew;
		[SerializeField] GameObject bannerPremium;
		[SerializeField] UIButton favoritebtn;
		[SerializeField] UISprite favoritebtnActive;

		[SerializeField] UILabel lbTimes;

		public event Action<SongDataModel> OnSongSelected;
		public event Action<SongItemViewLite, bool> OnFavoriteSong;

		private SongDataModel model;

		public void RefreshView(int triesRemaining)
		{
			lbAuthor.text = model.author;
			lbSongTitle.text = model.name;

			bannerHot.SetActive(model.type == SongItemType.Hot && triesRemaining <= 0);
			bannerNew.SetActive(model.type == SongItemType.New && triesRemaining <= 0);
			bannerPremium.SetActive(triesRemaining > 0);

			if (triesRemaining > 0)
			{
				lbTimes.gameObject.SetActive(true);
				lbTimes.text = triesRemaining.ToString();
			}
			else
			{
				lbTimes.gameObject.SetActive(false);
			}
		}

		void SetFavoriteButton(bool value)
		{
			if (value)
				favoritebtnActive.gameObject.SetActive(true);
			else
				favoritebtnActive.gameObject.SetActive(false);
		}

		private bool IsFavorite(string storeID)
		{
			if (ProfileHelper.Instance.ListFavoriteSongs == null)
			{
				return false;
			}

			for (int i = 0; i < ProfileHelper.Instance.ListFavoriteSongs.Count; i++)
			{
				if (ProfileHelper.Instance.ListFavoriteSongs[i].CompareTo(storeID) == 0)
				{
					return true;
				}
			}

			return false;
		}

		public void SetSong(SongDataModel song, int triesRemaining = 0)
		{
			model = song;
			RefreshView(triesRemaining);
		}

		public void OnSongItemClicked()
		{
			if (OnSongSelected != null)
			{
				GameManager.Instance.SessionData.endless = false;
				OnSongSelected(model);
			}
		}

		public void OnSongFavoriteItemClicked()
		{
			if (OnFavoriteSong != null)
			{
				OnFavoriteSong(this, !IsFavorite(model.storeID));

				Utils.AnalyticsHelper.Instance.LogFavorite(model.name, !IsFavorite(model.storeID));
			}

			SetFavoriteButton(IsFavorite(model.storeID));
		}

	}
}
