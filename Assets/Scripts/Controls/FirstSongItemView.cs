using UnityEngine;
using System;

namespace Amanotes.PianoChallenge
{
	public class FirstSongItemView : MonoBehaviour
	{
		[SerializeField] UILabel lbSongTitle;
		[SerializeField] UILabel lbAuthor;

		public event Action<SongDataModel> OnSongSelected;
		private SongDataModel song;

		public void SetSong(SongDataModel song)
		{
			this.song = song;
			lbAuthor.text = song.author;
			lbSongTitle.text = song.name;
		}

		public void Play()
		{
			if (OnSongSelected != null)
			{
				OnSongSelected(song);
			}
		}

	}
}
