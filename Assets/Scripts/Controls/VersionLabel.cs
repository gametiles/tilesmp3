using UnityEngine;

namespace Amanotes
{
	public class VersionLabel : MonoBehaviour
	{
		public UILabel lbVersion;

		void Start()
		{
			lbVersion.text = "v" + Application.version;
		}
	}
}
