using UnityEngine;
using System.Collections.Generic;
using Amanotes.Utils;
using System.Collections;
using System;

namespace Amanotes.PianoChallenge
{
	public class LevelLoader : MonoBehaviour
	{
		[SerializeField] private Gameplay gameplay;

		[Header("Elements to communicate with player")]

		[SerializeField] private UISlider progressBar;
		[SerializeField] private UILabel progressText;
		[SerializeField] private UILabel messageText;

		[Header("Elements to show after download failed")]

		[SerializeField] private GameObject backButton;
		[SerializeField] private GameObject retryButton;

		private SongDataModel song = null;
		public event Action OnLevelLoaded = null;

		public void OnEnable()
		{
			if (backButton != null) backButton.SetActive(false);
			if (retryButton != null) retryButton.SetActive(false);
		}

		/// <summary>
		/// Prepare data and fire up the main game's UI
		/// </summary>
		public void LoadLevel(SongDataModel song)
		{
			this.song = song;
			LoadLevel();
		}

		private LevelDataModel levelData = null;
		public void OnReady()
		{
			OnNewGame(gameplay.CachedLevelData);
		}

		public bool MovingToGameplay { get { return state == State.TransitioningToGamePlay; } }

		void LoadLevel()
		{
			progressBar.gameObject.SetActive(false);

			//only try to load game if the level to load is different from cached, or there is no level being loaded
			if (gameplay.CachedLevelData == null || !gameplay.CachedLevelData.songData.name.Equals(song.name))
			{
				SongLoader.Instance.Load(song,
					OnDownloadError,
					OnDownloadProgress,
					OnDownloadComplete,
					OnParseError,
					OnParseProgress,
					OnParseComplete);
			}
			else
			{
				StartCoroutine(PrepareNewGame());
			}
		}

		private void OnDownloadError(string error)
		{
			messageText.text = Localization.Get("er_parse_song_failed");
			ChangeState(State.DownloadError);
		}

		private void OnParseError(string error)
		{
			messageText.text = Localization.Get("er_parse_song_failed");
			ChangeState(State.ParseError);
		}

		private void OnDownloadProgress(float progress)
		{
			progressBar.gameObject.SetActive(true);
			progressBar.value = progress;
			progressText.text = (int)(progress * 100) + "%";
		}

		private void OnParseProgress(float progress)
		{
			progressBar.gameObject.SetActive(true);
			progressBar.value = progress;
			progressText.text = (int)(progress * 100) + "%";
		}

		private void OnDownloadComplete()
		{
			ChangeState(State.ParsingLevelData);
		}

		private void OnParseComplete(LevelDataModel levelData, List<TileData> tileList)
		{
			progressBar.value = 1;
			progressText.text = 100 + "%";
			StartCoroutine(PrepareNewGame(levelData, tileList));
		}

		private void OnNewGame(LevelDataModel levelData)
		{
			if (state == State.TransitioningToGamePlay)
			{
				return;
			}

			//1 life per game
			LivesManager.Instance.ReduceLife();

			//disable loom so that no GC will be alloc
			AnalyticsHelper.Instance.LogLevelStarted(levelData.songData.name);

			//log achievement
			AchievementHelper.Instance.LogAchievement("numTurnPlayed");

			GameManager.Instance.sessionLevel = true;

			ChangeState(State.TransitioningToGamePlay);

			// Annouce gameplay ready
			gameplay.GameplayReady();
		}

		// Prepare a new game with the newly loaded level
		private IEnumerator PrepareNewGame(LevelDataModel levelData, List<TileData> tileList)
		{
			yield return new WaitUntil(() => InGameUIController.instance != null && Gameplay.instance != null);
			this.levelData = levelData;

			gameplay.Initialize();
			gameplay.PrepareNewGame(levelData, tileList);
			Helpers.Callback(OnLevelLoaded);
		}

		// Prepare a new game with a cached level
		private IEnumerator PrepareNewGame()
		{
			yield return new WaitUntil(() => InGameUIController.instance != null && Gameplay.instance != null);
			levelData = gameplay.CachedLevelData;

			gameplay.PrepareNewGame();
			Helpers.Callback(OnLevelLoaded);
		}

		private enum State
		{
			Initialized,
			Downloading,
			DownloadError,
			ParsingLevelData,
			ParseError,
			TransitioningToGamePlay
		};

		private State state = State.Initialized;

		private void ChangeState(State state)
		{
			this.state = state;
			RefreshUI();
		}

		private void RefreshUI()
		{
			switch (state)
			{
				case State.Initialized:
				case State.Downloading:
					{
						//hide unnecessary elements
						if (backButton != null) backButton.SetActive(false);
						if (retryButton != null) retryButton.SetActive(false);
						//show progress elements
						progressBar.gameObject.SetActive(false);
						progressBar.value = 0;
						messageText.gameObject.SetActive(true);
						//show message
						messageText.text = Localization.Get("maingame_downloading");
					}
					break;

				case State.ParsingLevelData:
					{
						//hide unnecessary elements
						if (backButton != null) backButton.SetActive(false);
						if (retryButton != null) retryButton.SetActive(false);
						//show progress elements
						messageText.gameObject.SetActive(true);
						//show message
						messageText.text = Localization.Get("maingame_preparing");
					}
					break;

				//this state need user interaction, display any interactable elements for them
				case State.ParseError:
				case State.DownloadError:
					{
						if (backButton != null) backButton.SetActive(true);
						if (retryButton != null) retryButton.SetActive(true);
						messageText.gameObject.SetActive(true);
						
						//hide unnecessary elements
						progressBar.gameObject.SetActive(false);
					}
					break;
			}
		}
	}
}
