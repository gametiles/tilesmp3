using UnityEngine;

namespace Amanotes.PianoChallenge
{
	public class InfoTile : MonoBehaviour
	{
		[SerializeField] UILabel lbSongTitle = null;
		[SerializeField] UILabel lbHighScore = null;
		[SerializeField] GameObject listenButton = null;
		[SerializeField] GameObject normalIcon = null;
		[SerializeField] GameObject premiumIcon = null;

		private void OnEnable()
		{
			// Hide "listen" button in the first song
			if (listenButton != null)
			{
				listenButton.SetActive(PlayerPrefs.GetInt(GameConsts.OnBoarding, -1) > 0);
			}
		}

		public void SetSongInfo(string title, int score, bool premium = false)
		{
			lbHighScore.text = Localization.Get("maingame_highscore")  + score.ToString();

			lbSongTitle.text = title;
			if (normalIcon != null)
			{
				normalIcon.SetActive(!premium);
			}
			if (premiumIcon != null)
			{
				premiumIcon.SetActive(premium);
			}
		}
	}
}
