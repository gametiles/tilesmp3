﻿using UnityEngine;
using Amanotes.Utils;
using ProjectConstants;
using Amanotes.Utils.MessageBus;

namespace Amanotes.PianoChallenge
{
    public class PSContinuePopUpController : SSController
    {
        public CountdownView countdown;

        [SerializeField] RecordView starView = null;
        [SerializeField] UILabel lbSupportiveMessage = null;
        [SerializeField] UILabel lbPriceToContinue = null;
        [SerializeField] AudioClip heart = null;

        private int priceToContinue = 0;
        private int continueCount = 0;
        private bool decisionMade = false;
        private Nama.PSGameplay psGamePlay = null;

        private string watchAdText = "continue";

        public override void OnKeyBack()
        {
            FinishGame();
        }

        public override void OnEnable()
        {
            base.OnEnable();
            decisionMade = false;
            StartCountDown();
            MessageBus.Instance.Subscribe(MessageBusType.DiamondChanged, OnDiamondsChanged);
            if(heart != null)
                AudioEffect.Instance.PlaySound_2(heart);
        }

        public override void OnDisable()
        {
            base.OnDisable();
            AudioEffect.Instance.StopPlaySound_2(); 
            psGamePlay.Continue();
            MessageBus.Instance.Unsubscribe(MessageBusType.DiamondChanged, OnDiamondsChanged);
        }

        public void StartCountDown()
        {
            countdown.Restart(5f, 1, FinishGame);
        }

        public override void OnSet(object data)
        {
            base.OnSet(data);
            psGamePlay = data as Nama.PSGameplay;
            OnDiamondsChanged();

            int star = psGamePlay.nStars + 1;
            int tilesTillNextStar = psGamePlay.scoreToNextStar;

            lbSupportiveMessage.gameObject.SetActive(true);
            lbSupportiveMessage.text = Localization.Get("pu_continue_supportive").Replace("{0}", tilesTillNextStar.ToString());
                
            starView.gameObject.SetActive(true);
            starView.SetVisible(true);
            starView.ShowNumRecord(star);

            AnalyticsHelper.Instance.LogRewardVideoButtonShown(Scenes.ContinuePopup.GetName(), watchAdText);
        }

        /// <summary>
        /// Finish current game and go to result scene
        /// </summary>
        public void FinishGame()
        {
            Utils.AnalyticsHelper.Instance.LogClickItem("Finish Game");
            countdown.Stop();
            SceneManager.Instance.CloseScene();
            SceneManager.Instance.OpenScene(ProjectConstants.Scenes.PSResultUI, psGamePlay);
        }

        /// <summary>
        /// Continue to play this game, at current progress
        /// </summary>
        public void ContinueGame()
        {
            if (ProfileHelper.Instance.CurrentDiamond >= priceToContinue)
            {
                decisionMade = true;

                ProfileHelper.Instance.CurrentDiamond -= priceToContinue;
                LevelUpProgress.Instance.OnContinuePurchased(priceToContinue);
                AnalyticsHelper.Instance.LogClickItem("Continue Game");
                countdown.Stop();
                SceneManager.Instance.CloseScene();
            }
            else
            {
                countdown.Pause();
                AnalyticsHelper.Instance.LogIapOpen(AnalyticsHelper.IAP_TO_CONTINUE);
                SceneManager.Instance.OpenPopup(Scenes.IAP);
            }
        }

        public void WatchRewardVideo()
        {
            AnalyticsHelper.Instance.LogRewardVideoButtonClicked(Scenes.ContinuePopup.GetName(), watchAdText);
            AdHelper.Instance.ShowRewardVideo(RewardType.Continue, OnRewardVideoWatched, OnRewardVideoFailed);
            countdown.Pause();
        }

        public void OnRewardVideoWatched()
        {
            decisionMade = true;
            countdown.Stop();
            SceneManager.Instance.CloseScene();
            //Play game
        }

        private void OnRewardVideoFailed()
        {

        }

        private void OnDiamondsChanged(Message msg = null)
        {
            if (decisionMade)
            {
                return;
            }

            int count = Static.countPSContinuePopup - 1;
            priceToContinue = 5 + (count * 5);

            bool enough = priceToContinue <= ProfileHelper.Instance.CurrentDiamond || AdHelper.Instance.RewardVideoProviderCount <= 0;

            //how much does it cost to continue playing
            lbPriceToContinue.text = "x " + priceToContinue.ToString();
        }


    }
}
