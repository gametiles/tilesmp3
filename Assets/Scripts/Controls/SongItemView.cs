using UnityEngine;
using System;
using Amanotes.Utils;
using Facebook.Unity;

namespace Amanotes.PianoChallenge
{
	public class SongItemView : MonoBehaviour
	{
		[Header("Song's information")]
		[SerializeField] UILabel lbSongTitle = null;
		[SerializeField] UILabel lbAuthor = null;
		[SerializeField] UILabel lbIndex = null;
		[Header("Song record")]
		[SerializeField] GameObject starImages = null;
		[SerializeField] GameObject crownImages = null;

		[Header("Color for new or hot song")]
		[SerializeField] GameObject bannerHot = null;
		[SerializeField] GameObject bannerNew = null;
		[Header("Lock Panel Songs")]
		[SerializeField] GameObject lockBG = null;
		[SerializeField] UILabel lblDeficientStar = null;
		[SerializeField] GameObject btnPlaySong = null;
		[SerializeField] GameObject records = null;
		[SerializeField] UILabel priceRuby = null;
		[SerializeField] GameObject discountText = null;

		[Header("Favorite")]
		[SerializeField] UIButton favoritebtn = null;
		[SerializeField] UISprite favoritebtnActive = null;

		[SerializeField] GameObject normalSongIcon = null;
		[SerializeField] GameObject lockedSongIcon = null;
		[SerializeField] GameObject premiumSongIcon = null;

		[SerializeField] GameObject watchAdButton = null;

		[Header("Endless mode")]
		[SerializeField] GameObject endlessGroup = null;

		[Header("Leaderboard")]
		[SerializeField] GameObject leaderboardButton = null;

		public event Action<SongItemView> OnSongSelected;
		public event Action<SongItemView> OnTryToBuySong;
		public event Action<SongItemView, bool> OnFavoriteSong;
		public event Action<SongDataModel> OnShowRanking;

		private SongDataModel song;
		public SongDataModel Song { get { return song; } set { song = value; Refresh(); } }

		// Free song for limited time
		bool inFreePeriod = false;
		float freeSecondsRemaining = 0;
		int freeSecondsDisplay = 0;
		bool freeExpireSoon = false;
        bool _promo = false;

		// Endless mode unlocked
		bool endlessUnlocked = false;

		static Color EXPIRING_COLOR = new Color(0.949f, 0.314f, 0.314f);
		static Color TIME_COLOR = new Color(0.329f, 0.737f, 0.431f);

		private int index;

		public void Set(int index, SongDataModel song)
		{
			this.index = index;
			this.song = song;
			Refresh();
		}

		private void RefreshSongInfo()
		{
			lbAuthor.text = song.author;
			lbSongTitle.text = song.name;
			lbIndex.text = index.ToString();
			bannerHot.SetActive(song.type == SongItemType.Hot);
			bannerNew.SetActive(song.type == SongItemType.New);
		}

		void UpdateFreeSongTime()
		{
			int n = (int)freeSecondsRemaining;
			if (n == freeSecondsDisplay)
			{
				return;
			}
			freeSecondsDisplay = n;
			if (n < 600 && !freeExpireSoon) // 10 minutes
			{
				freeExpireSoon = true;
			}
		}

		private void Update()
		{
			if (!inFreePeriod)
			{
				return;
			}
			freeSecondsRemaining -= Time.deltaTime;
			if (freeSecondsRemaining < 0)
			{
				Refresh ();
				return;
			}
			UpdateFreeSongTime ();
		}

        private void RefreshLockStatus()
        {
            bool locked = ProfileHelper.Instance.Level < song.LvToUnlock;
            bool premium = song.pricePrimary != 0;
            bool promo = song.pricePrimary < 0;
            _promo = promo;

            normalSongIcon.SetActive(!locked && !premium);
            lockedSongIcon.SetActive(locked && !premium);
            premiumSongIcon.SetActive(premium);

            bool shouldLock = false;
            bool triesRemain = false;
            int triesRemaining = 0;
            inFreePeriod = false;
            endlessUnlocked = HighScoreManager.Instance.GetHighScore(song.storeID, ScoreType.Star) >= 3;

            //only display song as locked if there is not enough stars to unlock
            if (locked)
            {
                shouldLock = true;
            }
            else if (premium && !SongManager.IsBought(song))
            {
                shouldLock = true;
                triesRemaining = SongManager.Instance.PlayTimesRemaining(song);
                triesRemain = triesRemaining > 0;

                if (song == DailyRewardManager.Instance.FreeSongLimitedTime)
                {
                    freeSecondsRemaining = DailyRewardManager.Instance.FreeSongSecondsRemaining;
                    inFreePeriod =
                        freeSecondsRemaining > 0 &&
                        freeSecondsRemaining < DailyRewardManager.Instance.FreeSongMaxSeconds;
                }
                else
                {
                    inFreePeriod = false;
                }

                if (inFreePeriod)
                {
                    freeExpireSoon = false;
                    UpdateFreeSongTime();
                }
            }

            bool canPlay = !shouldLock || triesRemain || inFreePeriod;

            leaderboardButton.SetActive(false);
            favoritebtn.gameObject.SetActive(!shouldLock || promo);
            lockBG.SetActive(shouldLock && !inFreePeriod && !triesRemain && !promo);
            records.SetActive(!shouldLock || promo);
			lblDeficientStar.gameObject.SetActive(locked && !inFreePeriod);
			priceRuby.transform.parent.gameObject.SetActive(premium && !inFreePeriod);

            btnPlaySong.SetActive(canPlay);
            //btnPlaySong.SetActive(canPlay && !endlessUnlocked);
            //endlessGroup.SetActive(canPlay && endlessUnlocked);

            bool watchAdButtonActive = shouldLock && (promo && !triesRemain);
			watchAdButton.SetActive(watchAdButtonActive);

			bool discount = false;
			discountText.SetActive(shouldLock && discount && !inFreePeriod);

			if (shouldLock)
			{
				if (inFreePeriod)
				{
				}
				else if (triesRemain)
				{
					string crownsToUnlock = GameManager.Instance.GameConfigs.crownsToUnlockPremium.ToString();
					lblDeficientStar.text = Localization.Get("336").Replace("{0}", crownsToUnlock);
				}
				else if (promo)
				{
					AnalyticsHelper.Instance.LogRewardVideoButtonShown("SongItem", song.storeID);
				}
				else if (premium)
				{
					int price = discount ? song.pricePrimary / 2 : song.pricePrimary;
					priceRuby.text = "x " + price.ToString();
				}
				else
				{
					lblDeficientStar.text = Localization.Get("270").Replace("-", song.LvToUnlock.ToString());
				}
			}
			else
			{
				bool canViewSongLeaderboard = AccessToken.CurrentAccessToken != null || song.CanViewGlobalLeaderboard;
				leaderboardButton.transform.GetChild(0).gameObject.SetActive(canViewSongLeaderboard);
				leaderboardButton.transform.GetChild(1).gameObject.SetActive(!canViewSongLeaderboard);

				SetFavoriteBtn(SongManager.Instance.IsFavorite(song));
			}

            if (premium)
            {
                for (int i = 0; i < BannerLeftManager.Instance.listBanner.Count; i++)
                {
                    if (song.ID == BannerLeftManager.Instance.listBanner[i].id)
                    {
                        premiumSongIcon.GetComponent<UI2DSprite>().sprite2D = BannerLeftManager.Instance.listBanner[i].spriteImage;
                        break;
                    }
                    else
                    {
                        premiumSongIcon.GetComponent<UI2DSprite>().sprite2D = BannerLeftManager.Instance.defaultBannerLeft;
                    }
                }
            }
        }

		void SetFavoriteBtn(bool value)
		{
			if (value)
			{
				favoritebtnActive.gameObject.SetActive(true);
			}
			else
			{
				favoritebtnActive.gameObject.SetActive(false);
			}
		}

		private void RefreshRecords()
		{
			//set high-score
			int stars = HighScoreManager.Instance.GetHighScore(song.storeID, ScoreType.Star);
			int crowns = HighScoreManager.Instance.GetHighScore(song.storeID, ScoreType.Crown);
			if (lockBG && lockBG.activeSelf && !_promo)
			{
				starImages.SetActive(false);
				crownImages.SetActive(false);
			}
			else
			{
				ShowStarsOrCrowns(stars, crowns);
			}
		}

		public void Refresh()
		{
			RefreshSongInfo();
			RefreshLockStatus();
			RefreshRecords();
		}

		void ShowAchievements(int n, GameObject obj)
		{
			for (int i = 0; i < 3; i++)
			{
				obj.transform.GetChild (i * 2 + 0).gameObject.SetActive (i < n);
				obj.transform.GetChild (i * 2 + 1).gameObject.SetActive (i >= n);
			}
		}

		public void ShowStarsOrCrowns(int stars, int crowns)
		{
			starImages.SetActive (crowns <= 0);
			crownImages.SetActive (crowns > 0);

			if (crowns <= 0)
			{
				ShowAchievements (stars, starImages);
			}
			else
			{
				ShowAchievements (crowns, crownImages);
			}
		}

		public void OnSongItemClicked()
		{
			GameManager.Instance.SessionData.endless = false;
			Helpers.CallbackWithValue(OnSongSelected, this);
		}

		public void PlayEndless()
		{
			if (OnSongSelected != null)
			{
				GameManager.Instance.SessionData.endless = true;
				OnSongSelected(this);
			}
		}

		public void OnSongBuyItemClicked()
		{
			Helpers.CallbackWithValue(OnTryToBuySong, this);
		}

        public void WatchAdsToPlay()
        {
            AdHelper.Instance.ShowRewardVideo(RewardType.Custom, () =>
            {
                GameManager.Instance.SessionData.endless = false;
                Helpers.CallbackWithValue(OnSongSelected, this);
            });
        }

		public void OnSongFavoriteItemClicked()
		{
			bool favoriteAfter = !SongManager.Instance.IsFavorite(song);
			if (OnFavoriteSong != null)
			{
				OnFavoriteSong(this, favoriteAfter);
				AnalyticsHelper.Instance.LogFavorite(song.name, favoriteAfter);
			}

			SetFavoriteBtn(favoriteAfter);
		}

		public void ShowRanking()
		{
			Helpers.CallbackWithValue(OnShowRanking, song);
		}


        #region HomeItemView
        public void PlaySong()
        {
            SongManager.Instance.TryToPlay(song, "HOME");
        }

        public void BuySong()
        {
            SongManager.Instance.TryToBuy(song, AnalyticsHelper.SONG_LIST_PREMIUM, () => 
            {
                Refresh();
                if (song.pricePrimary < 0)
                {
                    GameManager.Instance.SessionData.endless = false;
                    SongManager.Instance.TryToPlay(song, AnalyticsHelper.SONG_LIST_PREMIUM);
                }
            });
        }

        public void PlayAds()
        {
            AdHelper.Instance.ShowRewardVideo(RewardType.Custom, () =>
            {
                GameManager.Instance.SessionData.endless = false;
                SongManager.Instance.TryToPlay(song, "HOME");
            });
        }

        public void FavoriteSongClick()
        {
            bool favoriteAfter = !SongManager.Instance.IsFavorite(song);
            SongManager.Instance.SetFavorite(song, favoriteAfter);
            SetFavoriteBtn(favoriteAfter);
        }

        #endregion

    }
}
